using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System;
using System.Text;

public class Player : Photon.PunBehaviour , IPunObservable 
{
	public bool _isRebornProtected = true;

	public Color _color_inner;
	public Color _color_ring;
	public Color _color_poison;

	float m_fProtectTime= 0.0f;

	public List<Ball> m_lstBalls = new List<Ball>();
	int m_nBallIndex = 0;

	public Vector2 m_vecPos = new Vector2 ();

	static Vector3 vecTempPos = new Vector3 ();
    static Vector3 vecTempSize = new Vector3();
    static Vector3 vecTempPos1 = new Vector3();
    static Vector2 vec2Temp = new Vector2 ();

	
    public CSkillManager m_SkillManager;

    float m_fTotalEatArea = 0f; // 这个特指“吃别人玩家球”体积
    float m_fTotalEatFoodArea = 0f; // 这个是广义的吃食体积，包括吃别人的玩家球，以及豆子、刺、场景静态球

    int m_nKillCount = 0;
    int m_nBeKilled = 0;
    int m_IncAssistAttackCount = 0;
    int m_nEatThornNum = 0; // 炸刺数

    public int GetKillCount()
    {
        return m_nKillCount;
    }

    public void SetKillCount( int val )
    {
        m_nKillCount = val;
    }

    public int GetBeKilledCount()
    {
        return m_nBeKilled;
    }

    public void SetBeKilledCount( int val )
    {
        m_nBeKilled = val;
    }


    public int GetAssistAttackCount()
    {
        return m_IncAssistAttackCount;
    }

    public void SetAssistAttackCount( int val )
    {
        m_IncAssistAttackCount = val;
    }


    public int GetEatThornNum()
    {
        return m_nEatThornNum;
    }

    public void SetEatThornNum( int val )
    {
       m_nEatThornNum = val;
    }

    int[] m_aryItemNum = new int[4];
    public void SetItemNum(int nIndex, int nNum )
    {
        if ( nIndex >= m_aryItemNum.Length)
        {
            return;
        }
        m_aryItemNum[nIndex] = nNum;
    }

    public int GetItemNum( int nIndex )
    {
        if (nIndex >= m_aryItemNum.Length)
        {
            return 0;
        }
        
        return m_aryItemNum[nIndex];
    }


    // 玩家的基础数值
    public struct sPlayerBaseData
    {
        public float fBaseSpeed;
        public float fBaseMp;
        public float fBaseSize;
        public float fBaseShellTime;
        public float fBaseSpitDistance;
    };
    sPlayerBaseData m_BaseData;

    float m_fBaseVolumeByLevel = 0f; // 随等级增加的基础体积

    // Use this for initialization
    void Start () 
	{
		Init ();
	}

	void Awake()
	{
       CPlayerManager.s_Instance.AddPlayer( this );

        int nPlayerId = photonView.ownerId;
        
	}

	public void Reset ()
	{
		SetMoving (false);
	}
	
	// Update is called once per frame
	void Update () {
		if ( m_bObserve ) {
			return;
		}
        
        CalculateCurTotalArea();


        ProtectTimeCount ();
		//SyncBallsInfo ();
		//Splitting ();
		//Syncing_FullInfo ();
		//PreDrawLoop ();
		SpittingSpore ();
        Attenuate ();
        //MoveToCenter ();
        //TestExplode ();
        //PreIniting();
        Initing ();
		SyncMoveInfo ();
		AdjustMoveInfo ();
        SyncPaiHangBangInfo();
        DustCollisonLoop();

        AdjustRelatedInfo();

		ClearSpitTargetCounting ();
		//SyncPosAfterEjectCounting ();
		ShowDebugInfo ();


		BuffLoop ();

		UpdateBuffEffect ();
        UpdateMoveSpeed();

        Spitting();
        PreCastSkill_Loop_SpitBallHalf();

        // 技能：变刺
        BecomeThorn_QianYaoLoop();
        BecomeThorn_ActAsThornLoop();
        // end 技能：变刺

        // 技能：潜行
        SneakingLoop();
        // end 技能：潜行

        // 技能：湮灭
        SkillLoop_Annihilate();
        //end  技能：湮灭

        // 技能：魔盾
        SkillLoop_MagicShield();
        // end 技能：魔盾

        // 技能：秒合
        SkillLoop_MiaoHe();
        // end 技能：秒合

        // 技能：狂暴
        HenzyLoop();
        // end 技能：狂暴

        // 技能：金壳
        SkillLoop_Gold();
        // end 技能：金壳
        
        EatThornSyncQueueLoop();

        GenerateSporeId();
    }

    void ShowDebugInfo()
	{
		if (!IsMainPlayer ()) {
			return;
		}
        string szContent = "";
        szContent += "当前总体积： " + m_fTotalArea.ToString("f2") + "\n";
        szContent += "基础体积：" + GetBornBaseTiJi() + "\n";
        szContent += "（原始配置:" + Main.s_Instance.m_fBornMinTiJi + ",道具加成:" + m_BaseData.fBaseSize + "，等级加成:" + m_fBaseVolumeByLevel + ")\n";
        szContent += "累计吃球体积：" + m_fTotalEatArea.ToString( "f2" ) + "\n";
		szContent += "速度系数：" + m_fBaseSpeed +  "\n";
        szContent += "分球距离加成：" + ( (GetBaseSpitDistance() * 100 ) + "%") + "\n";
        szContent += "壳时间减：" + GetDecreaseShellTime() + "秒\n";
        MapEditor.s_Instance.UpdateDebugInfo(szContent);

        CGrowSystem.s_Instance.SetCurTotalVolume(m_fTotalArea);
        CGrowSystem.s_Instance.SetPlayerSpeed(m_fBaseSpeed);

    }

    public int GetOwnerId()
    {
        return photonView.ownerId;
    }

    public bool IsMainPlayer()
    {
        return photonView.isMine;
    }
		

	public int GetCurAvailableBallCount()
	{
		return (int)Main.s_Instance.m_fMaxBallNumPerPlayer - GetCurLiveBallsNum ();
	}

	public void Init()
	{
		_isRebornProtected = true; // 第一次出生时一定是处于“重生保护状态”

		if (Main.s_Instance == null )
        {
            Debug.LogError("Main.s_Instance == null ");
			return;
		}

        if ( Main.s_Instance.m_goBalls == null) {
            Debug.LogError(" Main.s_Instance.m_goBalls == null");
            return;
        }

        this.gameObject.name = "player_" + photonView.ownerId;
		this.gameObject.transform.parent = Main.s_Instance.m_goBalls.transform;


		int color_index = 0;
		_color_ring = ColorPalette.RandomOuterRingColor (ref color_index);
		_color_inner = ColorPalette.RandomInnerFillColor ( photonView.ownerId/*color_index*/ );
		_color_poison = ColorPalette.RandomPoisonFillColor ( photonView.ownerId/*color_index*/);

		if (!photonView.isMine) { // 不是本机，是其它客户端的Player
			BeginInit();
		}

        //m_nCurSkinIndex = Main.s_Instance.GetSpriteIdByPhotonOwnerId ( photonView.ownerId );
        SetSkin(Main.s_Instance.GetSpriteIdByPhotonOwnerId(photonView.ownerId));


        if (photonView.isMine && AccountManager.s_bObserve) {
			SetObserve ( true );
		}
	}

	// 因为球球数众多，必须分帧处理
	bool m_bIniting = false;
	bool m_bInitCompleted = false;
    bool m_bBallInstantiated = false;

	public bool IsInitCompleted()
	{
		return m_bInitCompleted;
	}

    bool m_bPreInitCompleted = false;
    void PreIniting()
    {
        if (m_bPreInitCompleted)
        {
            return;
        }

        if (Main.s_Instance == null || Main.s_Instance.m_goBalls == null)
        {
            Debug.LogError("Main.s_Instance == null|| Main.s_Instance.m_goBalls == null");
            return;
        }

        this.gameObject.name = "player_" + photonView.ownerId;
        this.gameObject.transform.parent = Main.s_Instance.m_goBalls.transform;


        int color_index = 0;
        _color_ring = ColorPalette.RandomOuterRingColor(ref color_index);
        _color_inner = ColorPalette.RandomInnerFillColor(photonView.ownerId/*color_index*/ );
        _color_poison = ColorPalette.RandomPoisonFillColor(photonView.ownerId/*color_index*/);

        if (!photonView.isMine)
        { // 不是本机，是其它客户端的Player
            BeginInit();
        }

        m_nCurSkinIndex = Main.s_Instance.GetSpriteIdByPhotonOwnerId(photonView.ownerId);

        if (photonView.isMine && AccountManager.s_bObserve)
        {
            SetObserve(true);
        }

        m_bPreInitCompleted = true;
    }

    void BeginInit()
	{
		m_bIniting = true;
	}

	void EndInit()
	{
		m_bIniting = false;
        m_bBallInstantiated = true;

        // 本机的客户端资源初始化完毕，同房间的其它玩家可以对我来一次全量同步了，把你们的当前信息统统发过来
        PleaseFuckMyAss();
    }

    public void PleaseFuckMyAss()
    {
        photonView.RPC("RPC_FuckMe", PhotonTargets.Others);
    }

	void Initing()
	{
		if (!m_bIniting) {
			return;
		}

        // 配置文件初始化成功之前，不要初始化Player。因为网络读取文件是异步的。
        if (!MapEditor.s_Instance.IsMapInitCompleted ()) {
			return;
		}

		if (m_lstBalls.Count >= Main.s_Instance.m_fMaxBallNumPerPlayer) {
			EndInit ();
			return;
		}

		Ball ball = GameObject.Instantiate ( Main.s_Instance.m_preBall ).GetComponent<Ball>();
        vecTempPos.x = 10000;
        vecTempPos.y = 10000;
        ball.SetPos(vecTempPos);
        AddOneBall ( ball );
	}

	public void FuckYou()
	{
		
	}

	[PunRPC]
	public void RPC_FuckMe()
	{
		if (!photonView.isMine) {
			return;
		}

		SyncFullInfo ();

        if (PhotonNetwork.isMasterClient)
        {
            SyncPublicInfo();
        }

	}

    public void PleaseSyncThePublicInfo()
    {
        photonView.RPC("RPC_PleaseSyncThePublicInfo", PhotonTargets.Others);
    }

    [PunRPC]
    public void RPC_PleaseSyncThePublicInfo()
    {
        if (PhotonNetwork.isMasterClient)
        {
            SyncPublicInfo(); 
        }
    }

    public void PhotonRemoveObject( GameObject go )
	{
		PhotonNetwork.Destroy( go );
	}

	public void OnPhotonSerializeView (PhotonStream stream, PhotonMessageInfo info)
	{

	}

	public void BeginProtect()
	{
		photonView.RPC ( "RPC_BeginProtect", PhotonTargets.All );
	}

	[PunRPC]
	public void RPC_BeginProtect()
	{
		m_fProtectTime = Main.s_Instance.m_fBornProtectTime;
        _isRebornProtected = true;

        for ( int i = 0; i < m_lstBalls.Count; i++ )
        {
            Ball ball = m_lstBalls[i];
            ball.SetRebornProtectEfffect( true );
        }
	}

	void ProtectTimeCount()
	{
        if (!_isRebornProtected) {
            return;
        }
		m_fProtectTime -= Time.deltaTime;
        if (m_fProtectTime <= 0.0f)
        {
            EndProtect();
        }
    }

	public bool IsProtect()
	{
        //return m_fProtectTime > 0.0f;
        return _isRebornProtected;
	}

    public void EndProtect()
    {
        for (int i = 0; i < m_lstBalls.Count; i++)
        {
            Ball ball = m_lstBalls[i];
            ball.SetRebornProtectEfffect(false);
        }
        _isRebornProtected = false;
    }

	int m_nKingExplodeIndex = 0;
	bool m_bKingExploding = false;
	public void KingExplode ()
	{

	}


	[PunRPC]
	public void RPC_SyncSprayInfo( int nSprayGUID, int nDrawStatus, float fParam1, float fParam2, float fParam3 )
	{
		MapEditor.s_Instance.SyncSprayInfo (nSprayGUID, nDrawStatus, fParam1, fParam2, fParam3 );
	}

	string m_szPlayerName = "";
	public string GetPlayerName()
	{
		return m_szPlayerName;
	}

    public Sprite GetAvatar()
    {
        return m_sprCurAvatar;
    }
    
    public void SetPlayerName( string szPlayerName )
	{
		m_szPlayerName = szPlayerName;
		for (int i = 0; i < m_lstBalls.Count; i++) {
			Ball ball = m_lstBalls [i];
			ball.SetPlayerName ( m_szPlayerName );
		}
	}

	// 凡事要一分为二的来看。编程准则中强调的“尽量不要去遍历”，指的是大数量级的列表，比如几十万个节点的列表。这种几十个节点的小微列表怎么不可以去遍历？
	// 抛开数量级谈优化都是耍流氓。 为了一些根本没必要的优化，倒弄一些Bug出来。
	public int GetCurLiveBallsNum()
	{
		int n = 0;
		for (int i = 0; i < m_lstBalls.Count; i++) {
			Ball ball = m_lstBalls [i];
			if (ball == null || ball.IsDead ()) {
				continue;
			}
			n++;
		}
		return n;
	}

	public Ball GetOneLiveBall( ref int nLiveNum )
	{
		Ball ball = null;
		nLiveNum = 0;
		for (int i = 0; i < m_lstBalls.Count; i++) {
			if (!(m_lstBalls [i].IsDead ())) {
				ball = m_lstBalls [i];
				nLiveNum++;
			} 
		}
		return ball;
	}

	

    byte[] _bytesPublicInfo = new byte[1024];
    public void SyncPublicInfo()
    {
        StringManager.BeginPushData(_bytesPublicInfo);
        StringManager.PushData_Float( Main.s_Instance.GetGameStartTime() );

        //// ！---- 刺的变动信息

        // 当前处于销毁状态的刺
        List<CMonster> lstDestroyedMonsters = CClassEditor.s_Instance.GetSyncPublicList_DestroyedMonsters();
        int nNum = lstDestroyedMonsters.Count;
        StringManager.PushData_Short((short)nNum);
        for ( int i = 0; i < nNum; i++)
        {
            CMonster monster = lstDestroyedMonsters[i];
            StringManager.PushData_Int((int)monster.GetGuid());  
            StringManager.PushData_Float(monster.GetDeadOccurTime());
        }

        // 当前处于位置变动状态的刺
        Dictionary<int, CMonster> dicPushedThorns = CClassEditor.s_Instance.GetSyncPublicList_PushedMonsters();
        StringManager.PushData_Short((short)dicPushedThorns.Count);
        foreach ( KeyValuePair<int, CMonster> pair in dicPushedThorns)
        {
            StringManager.PushData_Int( (int)pair.Value.GetGuid() );
            StringManager.PushData_Float(pair.Value.GetPos().x);
            StringManager.PushData_Float(pair.Value.GetPos().y);
        }
        //// ！---- end 刺的变动信息

        photonView.RPC("RPC_SyncPublicInfo", PhotonTargets.All, _bytesPublicInfo);
    }


    [PunRPC]
    public void RPC_SyncPublicInfo( byte[] bytes )
    {
        StringManager.BeginPopData( bytes );
        Main.s_Instance.SetGameStartTime(StringManager.PopData_Float());
        Main.s_Instance.ProcessTimeLeft();
        Main.s_Instance._txtTimeLeft.gameObject.SetActive( true );

        int nNumOfDestroyedThorn = StringManager.PopData_Short();
        for ( int i = 0; i < nNumOfDestroyedThorn; i++ )
        {
            int nThornId = StringManager.PopData_Int();
            float fOccurTime = StringManager.PopData_Float();
            CClassEditor.s_Instance.RemoveThorn(nThornId, fOccurTime);
        }

        int nNumOfPushedThorns = StringManager.PopData_Short();
        for ( int i = 0; i < nNumOfPushedThorns; i++ )
        {
            int nThornId = StringManager.PopData_Int();
            float fPosX = StringManager.PopData_Float();
            float fPosY = StringManager.PopData_Float();
            vecTempPos.x = fPosX;
            vecTempPos.y = fPosY;
            CMonster monster = CClassEditor.s_Instance.GetMonsterByGuid(nThornId);
            vecTempPos.z = -monster.GetSize();
            monster.SetPos(vecTempPos);
        }

        
    }

	byte[] _bytesFullInfo = new byte[3072];
    public void SyncFullInfo()
	{
		int nPointer = 0;

        string szPlayerName = GetPlayerName ();
		byte[] bytesPlayerName = StringManager.String2Bytes ( szPlayerName );
		int nLenOfPlayerName = bytesPlayerName.Length;
		byte[] bytesLenOfPlayerName = BitConverter.GetBytes (nLenOfPlayerName);
		bytesLenOfPlayerName.CopyTo (_bytesFullInfo, nPointer);
		nPointer += sizeof(byte);
		bytesPlayerName.CopyTo (_bytesFullInfo, nPointer);
		nPointer += nLenOfPlayerName;

		/// ! -- 成长系统信息 
		byte byLevel = (byte)GetLevel();
		BitConverter.GetBytes (byLevel).CopyTo (_bytesFullInfo, nPointer);
		nPointer += sizeof(byte);
		/// ! -- end 成长系统信息 

		/// ! -- Buff 系统信息
		int byNumOfBuff = m_lstBuff.Count;
		BitConverter.GetBytes (byNumOfBuff).CopyTo (_bytesFullInfo, nPointer);
		nPointer += sizeof(int);
		for (int i = 0; i < m_lstBuff.Count; i++) {
			CBuff buff = m_lstBuff [i];
			int byBuffId = buff.GetConfig ().id;
			float sLeftTime = buff.GetTime ();
			long lBuffGuid = buff.GetGUID ();
			BitConverter.GetBytes (byBuffId).CopyTo (_bytesFullInfo, nPointer);
			nPointer += sizeof(int);
			BitConverter.GetBytes (lBuffGuid).CopyTo (_bytesFullInfo, nPointer);
			nPointer += sizeof(long);
			BitConverter.GetBytes (sLeftTime).CopyTo (_bytesFullInfo, nPointer);
			nPointer += sizeof(float);
		}
		/// ! -- end Buff 系统信息

		BitConverter.GetBytes (AccountManager.s_bObserve?1:0).CopyTo (_bytesFullInfo, nPointer);
		nPointer += sizeof(byte);

		int nNum = GetCurLiveBallsNum ();
		BitConverter.GetBytes (nNum).CopyTo (_bytesFullInfo, nPointer);
		nPointer += sizeof(byte);
		BitConverter.GetBytes (m_nCurSkinIndex).CopyTo (_bytesFullInfo, nPointer);
		nPointer += sizeof(ushort);
		BitConverter.GetBytes (IsMoving()?1:0).CopyTo (_bytesFullInfo, nPointer);
		nPointer += sizeof(byte);



		for (int i = 0; i < m_lstBalls.Count; i++) {
			Ball ball = m_lstBalls [i];
			if (ball.IsDead ()) {
				continue;
			}
			if (ball.GetIndex () != i) {
				Debug.LogError ( "从个的？？ball.GetIndex () != i" );
				continue;
			}

			

			float fSize = ball.GetSize ();
			ushort usSize = (ushort)(fSize * 100f);

			float fWorldPosX = ball.GetPos ().x;
			float fWorldPosY = ball.GetPos ().y;


			float fPosX = ball.GetPos().x;
			float fPosY = ball.GetPos().y;

			float fCurShellTime = ball.GetCurShellTime ();
			float fTotalShellTime = ball.GetTotalShellTime ();

			BitConverter.GetBytes (ball.GetIndex()).CopyTo( _bytesFullInfo, nPointer );
			nPointer += sizeof(byte);
			BitConverter.GetBytes (usSize).CopyTo( _bytesFullInfo, nPointer );
			nPointer += sizeof(ushort);
			BitConverter.GetBytes (fPosX).CopyTo( _bytesFullInfo, nPointer );
			nPointer += sizeof(float);
			BitConverter.GetBytes (fPosY).CopyTo( _bytesFullInfo, nPointer );
			nPointer += sizeof(float);
			BitConverter.GetBytes ((ushort)fCurShellTime).CopyTo( _bytesFullInfo, nPointer );
			nPointer += sizeof(ushort);
			BitConverter.GetBytes ((ushort)fTotalShellTime).CopyTo( _bytesFullInfo, nPointer );
			nPointer += sizeof(ushort);
		} // end for


		photonView.RPC ( "RPC_SyncFullInfo", PhotonTargets.All,  _bytesFullInfo );
	}

	[PunRPC]
	public void RPC_SyncFullInfo( byte[] bytes )
	{
		if (photonView.isMine) { // 全量信息是同步给其它客户端的，自己没必要再去解析一遍
			return;
		}

        if (!m_bBallInstantiated)
        {
            return;
        }

		int nPointer = 0;
		int nLenOfPlayerName = (byte)BitConverter.ToChar(bytes, nPointer);
		nPointer += sizeof (byte);
		string szPlayerName = StringManager.Bytes2String ( bytes, nPointer, nLenOfPlayerName );
		SetPlayerName ( szPlayerName );
		nPointer += nLenOfPlayerName;

		//// ! ---- 成长系统信息
		byte byLevel = (byte)BitConverter.ToChar(bytes, nPointer);
		nPointer += sizeof (byte);
		Local_SetLevel ( byLevel );
		//// ! ---- end 成长系统信息

		//// ! ---- Buff系统信息
		int nNumOfBuff = BitConverter.ToInt32(bytes, nPointer);
		nPointer += sizeof (int);
		for (int i = 0; i < nNumOfBuff; i++) {
			int nBuffId = BitConverter.ToInt32(bytes, nPointer);
			nPointer += sizeof (int);
			long lBuffGuid = BitConverter.ToInt64(bytes, nPointer);
			nPointer += sizeof (long);
			float fLeftTime = BitConverter.ToSingle(bytes, nPointer);
			nPointer += sizeof (float);
			Local_AddBuff ( nBuffId, lBuffGuid, fLeftTime );
			Debug.Log ( "add buff :" + nBuffId + " , " + fLeftTime );
		}
		//// ! ---- end Buff系统信息

		byte byObserve = (byte)BitConverter.ToChar(bytes, nPointer);
		nPointer += sizeof (byte);
		SetObserve ( byObserve == 1?true:false );

		int nNum = (byte)BitConverter.ToChar(bytes, nPointer);
		nPointer += sizeof (byte);
		m_nCurSkinIndex = (ushort)BitConverter.ToUInt16(bytes, nPointer);
		nPointer += sizeof (ushort);
		SetSkin ( m_nCurSkinIndex );
		byte byIsMoving =  (byte)BitConverter.ToChar(bytes, nPointer);
		nPointer += sizeof (byte);
		Local_SetMoving ( byIsMoving == 1?true:false );


		int nNumOfBallInGroup = 0;
		for (int i = 0; i < nNum; i++) {
			int nIndex = (byte)BitConverter.ToChar(bytes, nPointer);
			nPointer += sizeof (byte);
			if (nIndex < 0 || nIndex >= m_lstBalls.Count) {
				Debug.LogError ( "从个的？？nIndex < 0 || nIndex >= m_lstBalls.Count" );
				continue;
			}
			Ball ball = m_lstBalls [nIndex];
			ball.SetDead ( false );
			ushort usSize = BitConverter.ToUInt16(bytes, nPointer);
			nPointer += sizeof(ushort);
			float fSize = (float)usSize / 100f;
			ball.SetSize ( fSize );

			float fPosX = BitConverter.ToSingle(bytes, nPointer);
			nPointer += sizeof(float);
			float fPosY = BitConverter.ToSingle(bytes, nPointer);
			nPointer += sizeof(float);

			vecTempPos.x = fPosX;
			vecTempPos.y = fPosY;
			vecTempPos.z = -fSize;

    		ball.SetPos (vecTempPos);
	

			float fCurShellTime = BitConverter.ToUInt16(bytes, nPointer);
			nPointer += sizeof(ushort);
			float fTotalShellTime = BitConverter.ToUInt16(bytes, nPointer);
			nPointer += sizeof(ushort);

			ball.Local_SetShellInfo (fTotalShellTime, fCurShellTime);
			if ( fTotalShellTime > 0 )
			{
				ball.Local_BeginShell ();
			}
		}

		m_bInitCompleted = true;
	}

    public void SetPlayerInitCompleted(bool val)
    {
        m_bInitCompleted = val;
    }

    public bool IsPlayerInitCompleted()
    {
        return m_bInitCompleted;
    }

	bool m_bObserve = false;
	public void SetObserve( bool val )
	{
		m_bObserve = val;
		if (m_bObserve) {
			this.gameObject.SetActive ( false );
		}
	}
	/*
	void Syncing_FullInfo()
	{
		return;

		if (m_SyncFullInfoIndex >= m_lstBalls.Count) {
			m_bSyncingFullInfo = false;
			return;
		}

		if (!m_bSyncingFullInfo) {
			return;
		}

		Ball ball = m_lstBalls[m_SyncFullInfoIndex];
		if (ball && ( !ball.IsDead() ) ) {
			ball.SyncFullInfo ();
		}

		m_SyncFullInfoIndex++;
	}
	*/
	Vector3 m_vWorldCursorPos = new Vector3();
	public Vector2 m_vSpecialDirecton = new Vector2();
	public float m_fRealTimeSpeedWithoutSize;
	float m_fSyncMoveInfoCount = 0f;

	public void SyncMoveInfo( Vector3 vecWorldCursorPos, Vector2 vecSpecialDirection, float fRealTimeSpeedWithoutSize )
	{
        return;
        /*
		m_fSyncMoveInfoCount += Time.deltaTime;
		if (m_fSyncMoveInfoCount < Main.s_Instance.m_fSyncMoveCountInterval) {
			return;
		}
		m_fSyncMoveInfoCount = 0f;
        */
		m_vWorldCursorPos = vecWorldCursorPos;
		DoMove ( vecWorldCursorPos.x, vecWorldCursorPos.y, vecSpecialDirection.x, vecSpecialDirection.y, fRealTimeSpeedWithoutSize );
	}

	float m_fBaseSpeed = 0;
	public float GetBaseSpeed()
	{
		return m_fBaseSpeed;
	}
    
	public Vector3 GetWorldCursorPos ()
	{
		return m_vWorldCursorPos;
	}

	float m_fShellTimeBuffEffect = 1f;
	void UpdateBuffEffect_ShellTime()
	{
		m_fShellTimeBuffEffect = 1f;
		CBuffEditor.sBuffConfig buff_config;
		for (int i = 0; i < m_lstBuff.Count; i++) {
			CBuff buff = m_lstBuff [i];
			buff_config = buff.GetConfig ();
			if (buff_config.func == (int)CBuffEditor.eBuffFunc.shell_time) {
				m_fShellTimeBuffEffect *= ( 1f + buff_config.aryValues [0] );
			}
		}
	}

	public float GetBuffEffect_ShellTime()
	{
		return m_fShellTimeBuffEffect;
	}
    public void SetBuffEffect_ShellTime( float val )
    {
         m_fShellTimeBuffEffect = val;
    }

    void UpdateMoveSpeed()
	{
    	if (!IsMainPlayer ()) { // 移动速度由 MainPlayer实时同步，每个客户端只管维护自己的速度变化，不用管别的Player的速度实时变化
			return;
		}

        m_fBaseSpeed = Main.s_Instance.m_fBallBaseSpeed; // 这是配置的基础移动速度

        // 接下来是道具的加成
        m_fBaseSpeed += m_BaseData.fBaseSpeed;


        //// ! ---- Buff 对速度的影响
        CBuffEditor.sBuffConfig buff_config;
		for (int i = 0; i < m_lstBuff.Count; i++) {
			CBuff buff = m_lstBuff [i];
			buff_config = buff.GetConfig ();
			if (buff_config.func == (int)CBuffEditor.eBuffFunc.player_move_speed) {
				m_fBaseSpeed *= ( 1f + buff_config.aryValues [0] );
			}
		}

        // 变刺之后速度会受到影响
        if ( IsBecomeThorn() )
        {
            m_fBaseSpeed *= (1f + m_fSpeedChangePercent_BecomeThorn);
        }

        // 潜行之后速度会受到影响
        if (IsSneaking())
        {
            m_fBaseSpeed *= (1f + m_fSkillSpeedChangePercent_Sneak);
        }

        // 狂暴状态
        if ( IsHenzy() )
        {
            m_fBaseSpeed *= (1f + GetHenzySpeedChangePercent());
        }

        // 金壳状态
        if ( IsGold() )
        {
            m_fBaseSpeed *= (1f + GetSkillGoldSpeedAddPercent());
        }

        // poppin test 背景音乐的播放速度与玩家的移动速度成正比
        /*
        if (m_fBaseSpeed > Main.s_Instance.m_fBallBaseSpeed)
        {
            CAudioManager.s_Instance.audio_main_bg.pitch = 1f + (m_fBaseSpeed - Main.s_Instance.m_fBallBaseSpeed) / 500f;
        }
        else if (m_fBaseSpeed < Main.s_Instance.m_fBallBaseSpeed)
        {
            CAudioManager.s_Instance.audio_main_bg.pitch = 1f - ( Main.s_Instance.m_fBallBaseSpeed- m_fBaseSpeed) / 500f;
        }
        else
        {
            CAudioManager.s_Instance.audio_main_bg.pitch = 1f;
        }
        */

    }

	public void DoMove ( float fWorldCursorPosX, float fWorldCursorPosY, float fSpecialDirectionX, float fSpecialDirectionY, float fRealTimeSpeedWithoutSize )
	{
		if (!photonView.isMine) {
			return;
		}

		photonView.RPC ( "RPC_DoMove", PhotonTargets.All, fWorldCursorPosX, fWorldCursorPosY, fSpecialDirectionX, fSpecialDirectionY, fRealTimeSpeedWithoutSize );
	}

	// 终于领悟了帧同步的真谛：同步指令而不是同步全量数据

	static Vector2 vec2TempPos = new Vector2 ();
	static Vector3 vec3TempPos = new Vector3 ();

	[PunRPC]
	public void RPC_DoMove( float fWorldCursorPosX, float fWorldCursorPosY, float fSpecialDirectionX, float fSpecialDirectionY, float fRealTimeSpeedWithoutSize )
    {
        Local_DoMove( fWorldCursorPosX,  fWorldCursorPosY,  fSpecialDirectionX,  fSpecialDirectionY,  fRealTimeSpeedWithoutSize);
    }

    public void Local_DoMove(float fWorldCursorPosX, float fWorldCursorPosY, float fSpecialDirectionX, float fSpecialDirectionY, float fRealTimeSpeedWithoutSize)
    {
		if (photonView.isMine) {
			return;
		}

		m_fRealTimeSpeedWithoutSize = fRealTimeSpeedWithoutSize;
		m_vSpecialDirecton.x = fSpecialDirectionX;
		m_vSpecialDirecton.y = fSpecialDirectionY;
		m_vWorldCursorPos.x = fWorldCursorPosX;
		m_vWorldCursorPos.y = fWorldCursorPosY;
		vec3TempPos.x = fWorldCursorPosX;
		vec3TempPos.y = fWorldCursorPosY;
		for (int i = 0; i <  m_lstBalls.Count; i++) {
			Ball ball = m_lstBalls[i];
			if (ball.IsDead()) {
				continue;
			}
			ball.BeginMove ( vec3TempPos, m_fRealTimeSpeedWithoutSize );
		}


	
	}

	bool m_bMoving = false;
	public void SetMoving( bool val )
	{
		if (!photonView.isMine) {
			return;
		}
		photonView.RPC ( "RPC_SetMoving", PhotonTargets.All, val );
	}

	[PunRPC]
	public void RPC_SetMoving( bool val )
	{
		Local_SetMoving ( val );
	}

	public void Local_SetMoving( bool val )
	{
		if ( m_bMoving == false && val == true )
		{
			StartMove ();
		}
		m_bMoving = val;
		if (m_bMoving) {
			//EndMoveToCenter ();
		} else {
			StopMove ();
		}
	}

	public bool IsMoving()
	{
		return m_bMoving;
	}



	public void RemoveOneBall( Ball ball )
	{
		return;// No!!!!!!!!!!!!!

		m_lstBalls.Remove ( ball );
	}
		


	float m_fClearSpitTargetTotalTime = 0f;
	public void ClearSpitTarget()
	{
		for ( int i = 0; i < m_aryGunsight.Length; i++ )
        {
            CGunsight gunsight = m_aryGunsight[i];
            if (gunsight == null)
            {
                continue;
            }
            gunsight.gameObject.SetActive( false );
        }

	}

	public void BeginClearSpitTargetCount( float fTotalTime )
	{
		m_fClearSpitTargetTotalTime = fTotalTime;
	}

	void ClearSpitTargetCounting()
	{
		if (m_fClearSpitTargetTotalTime <= 0f) {
			return;
		}
		m_fClearSpitTargetTotalTime -= Time.deltaTime;
		if (m_fClearSpitTargetTotalTime <= 0f) {
			EndClearSpitTargetCount ();
		}
	}

	public void EndClearSpitTargetCount()
	{
		ClearSpitTarget ();
	}

	int m_nSpitIndex = 0;
	bool m_bSpitting = false;
/*
	public void DoSpit( int nPlatform )
	{
		Main.s_Instance.BeginCameraSizeProtect ( Main.s_Instance.m_fSpitRunTime );
		photonView.RPC ( "RPC_DoSpit", PhotonTargets.All, m_fSpitPercent, nPlatform, PhotonNetwork.time );
	}

	[PunRPC]
	public void RPC_DoSpit( float fPercent, int nPlatform, double dOccurTime )
	{
		float fDelayTime = (float)( PhotonNetwork.time - dOccurTime );
		for ( int i = 0; i < m_lstBalls.Count; i++ ) {
			Ball ball = m_lstBalls[i];
			if (ball.IsDead ()) {
				continue;
			}
			ball.CalculateChildSize ( fPercent );
			float fMotherLeftSize = 0f;
			float fChildSize = ball.GetAvailableChildSize( ref fMotherLeftSize );
			if (fChildSize < Main.BALL_MIN_SIZE || fMotherLeftSize < Main.BALL_MIN_SIZE || fMotherLeftSize < fChildSize ) {
				continue;
			}
			ball.Spit ( fDelayTime, nPlatform, fChildSize, fMotherLeftSize);
		}

		if (photonView.isMine) {
			BeginClearSpitTargetCount (Main.s_Instance.m_fSpitRunTime);
		}

	}
*/
	public bool CheckIfCanSpitBallHalf()
	{
		if (GetMP () < m_fSpitBallHalfMpCost) {
			Main.s_Instance.g_SystemMsg.SetContent (CMsgSystem.s_Instance.GetMsgContentByType( CMsgSystem.eSysMsgType.mp_not_enough ));
			return false;
		}

        bool bSpitSucceeded = false;
        for (int i = 0; i < m_lstBalls.Count; i++)
        {
            Ball ball = m_lstBalls[i];
            if (ball.IsDead())
            {
                continue;
            }

            ball.CalculateChildSize(0.5f); // 因为是“二分技能”，所以直接按50%的分球比例来预判
            float fMotherLeftSize = 0f;
            float fChildSize = ball.GetAvailableChildSize(ref fMotherLeftSize);

            if (fChildSize < Main.BALL_MIN_SIZE || fMotherLeftSize < Main.BALL_MIN_SIZE || fMotherLeftSize < fChildSize)
            {
                continue;
            }

            bSpitSucceeded = true;
            break;
        } // end for

        if (!bSpitSucceeded)
        {
            Main.s_Instance.g_SystemMsg.SetContent("队伍中所有母球当前体积都不足以二分分球。");
            return false;
        }


        return true;
	}

    public void Cancel_SpitBallHalf()
    {
        if (m_nPreCastSkill_Status_SpitHalf != 2)
        {
            return;
        }

        m_nPreCastSkill_Status_SpitHalf = 0;
        m_bCastSkillWhickNeedStopMoving = false;
        m_bPreCastSkill_SpitHalf = false;
    }

    float m_fSpitBallHalfMpCost = 0f;
    float m_fSpitBallHalfColddown = 0f;
    float m_fSpitBallHalfQianYao = 0f;
    float m_fSpitBallHalfDistance = 0f;
    float m_fSpitBallHalfRunTime = 0f;
    float m_fSpitBallHalfShellTime = 0f;
    public void PreCastSkill_SpitBallHalf(float fMpCost, float fColddown, float fQianYao, float fDistance, float fRunTime, float fShellTime)
    {
        m_fSpitBallHalfMpCost = fMpCost;
        m_fSpitBallHalfColddown = fColddown;
        m_fSpitBallHalfQianYao = fQianYao;
        m_fSpitBallHalfDistance = fDistance;
        m_fSpitBallHalfRunTime = fRunTime;
        m_fSpitBallHalfShellTime = fShellTime;

        if ( m_nPreCastSkill_Status_SpitHalf != 0 )
        {
            return;
        }

        if ( !CheckIfCanSpitBallHalf() )
        {
            RPC_CancelSpitBallHalf();
            return;
        }

        m_nPreCastSkill_Status_SpitHalf = 1;

        CSkillSystem.s_Instance.ShowProgressBar(CSkillSystem.eSkillSysProgressBarType.t_spit_ball);
        photonView.RPC("RPC_PreCastSkill_SpitBallHalf", PhotonTargets.All, fMpCost, fQianYao, fDistance, fRunTime, fShellTime);
    }

    [PunRPC]
    public void RPC_PreCastSkill_SpitBallHalf(float fMpCost, float fQianYao, float fDistance, float fRunTime, float fShellTime)
    {
        m_fSpitBallHalfMpCost = fMpCost;
        m_fSpitBallHalfQianYao = fQianYao;
        m_fSpitBallHalfDistance = fDistance;
        m_fSpitBallHalfRunTime = fRunTime;
        m_fSpitBallHalfShellTime = fShellTime;

        m_nPreCastSkill_Status_SpitHalf = 1;
        m_bPreCastSkill_SpitHalf = true;
        m_bCastSkillWhickNeedStopMoving = true;
        m_fPreCastSkill_SpitHalf_TimeCount = 0;

    }

    int m_nPreCastSkill_Status_SpitHalf = 0;
    bool m_bPreCastSkill_SpitHalf = false;
    float m_fPreCastSkill_SpitHalf_TimeCount = 0f;

    public int GetSpitBallHalfStatus()
    {
        return m_nPreCastSkill_Status_SpitHalf;
    }

    void PreCastSkill_Loop_SpitBallHalf()
    {
        if (!m_bPreCastSkill_SpitHalf)
        {
            return;
        }

        if (m_nPreCastSkill_Status_SpitHalf != 1)
        {
            return;
        }

        //// ! ---- show atrget
        ShowTarget(0.5f, m_fSpitBallHalfDistance);
        //// ! ---- end show target

        CSkillSystem.s_Instance.UpdateProgressData(m_fPreCastSkill_SpitHalf_TimeCount, m_fSpitBallHalfQianYao);

        m_fPreCastSkill_SpitHalf_TimeCount += Time.deltaTime;
        if (m_fPreCastSkill_SpitHalf_TimeCount >= m_fSpitBallHalfQianYao)
        {
            if (IsMainPlayer())
            {
                CSkillSystem.s_Instance.HideProgressBar();
                SpitBallHalf();
            }
        }
    }

    void ShowTarget( float fPercent, float fSpitBallDistance)
    {
        int nAvailableNum = GetCurAvailableBallCount();
        int nCount = 0;
        for (int i = 0; i < m_lstBalls.Count; i++)
        {
            Ball ball = m_lstBalls[i];
            if (ball.IsDead())
            {
                continue;
            }

            if (!ball.CheckIfCanForceSpit())
            {
                continue;
            }

            ball.CalculateChildSize(fPercent);
            float fMotherLeftSize = 0f;
            float fChildSize = ball.GetAvailableChildSize(ref fMotherLeftSize);
            if (fChildSize < Main.BALL_MIN_SIZE || fMotherLeftSize < Main.BALL_MIN_SIZE || fMotherLeftSize < fChildSize)
            {
                continue;
            }

            ball.ShowTarget(fSpitBallDistance);

            nCount++;
            if (nCount >= nAvailableNum)
            {
                break;
            }
        } // end for
    }

    void EndPreCastSkill_SpitBallHalf()
    {


    }

    public void SpitBallHalf()
	{
        m_nPreCastSkill_Status_SpitHalf = 0;
        m_bPreCastSkill_SpitHalf = false;

        if (!CheckIfCanSpitBallHalf ()) {
            photonView.RPC("RPC_CancelSpitBallHalf", PhotonTargets.All);
            return;
		}

		SetMP ( GetMP() - m_fSpitBallHalfMpCost );

        float fShellTime = GetRealShellTime( m_fSpitBallHalfShellTime );
		photonView.RPC ( "RPC_SpitBallHalf", PhotonTargets.All, (float)PhotonNetwork.time, fShellTime);
	}

    [PunRPC]
    public void RPC_CancelSpitBallHalf()
    {
        Main.s_Instance.ClearSkillColdDown_SpitBallHalf();
        CSkillSystem.s_Instance.HideProgressBar();
        ClearSpitTarget();
    }

	[PunRPC]
	public void RPC_SpitBallHalf( float fOccurTime, float fShellTime)
	{
        m_nPreCastSkill_Status_SpitHalf = 0;
        m_bPreCastSkill_SpitHalf = false;
        ClearSpitTarget();

        float fDelayTime = (float)PhotonNetwork.time - fOccurTime;

		for (int i = 0; i < m_lstBalls.Count; i++) {
			Ball ball = m_lstBalls [i];
			if (ball.IsDead ()) {
				continue;
			}

            float fChildArea = CyberTreeMath.SizeToArea(ball.GetSize(), Main.s_Instance.m_nShit) / 2f;
            float fChildSize = CyberTreeMath.AreaToSize(fChildArea, Main.s_Instance.m_nShit );
			if (fChildSize < Main.BALL_MIN_SIZE  ) {
				continue;
			}
			ball.SpitHalf ( fDelayTime, fChildSize, m_fSpitBallHalfDistance, m_fSpitBallHalfRunTime, fShellTime);

		} // end for

        m_bCastSkillWhickNeedStopMoving = false;
    }

	public bool CheckIfCanSpitBall()
	{
		if (GetMP () < m_fSpitBallCostMp) {
			Main.s_Instance.g_SystemMsg.SetContent (CMsgSystem.s_Instance.GetMsgContentByType(CMsgSystem.eSysMsgType.mp_not_enough));
			return false;
		}

        bool bSpitSucceeded = false;
        for (int i = 0; i < m_lstBalls.Count; i++)
        {
            Ball ball = m_lstBalls[i];
            if (ball.IsDead())
            {
                continue;
            }

            ball.CalculateChildSize_ByVolume( Main.m_fBallMinVolume ); // 按分出最小单位球来预判，看看能否成功分球
            float fMotherLeftSize = 0f;
            float fChildSize = ball.GetAvailableChildSize(ref fMotherLeftSize);

            if (fChildSize < Main.BALL_MIN_SIZE || fMotherLeftSize < Main.BALL_MIN_SIZE || fMotherLeftSize < fChildSize)
            {
                continue;
            }

            bSpitSucceeded = true;
            break;
        } // end for

        if (!bSpitSucceeded)
        {
            Main.s_Instance.g_SystemMsg.SetContent("队伍中所有母球当前体积都不足以分球。");
            return false;
        }

        return true;
	}

    int m_nWSpitStatus = 0; // 0 - None  1 - 蓄力阶段
	public void DoSpit( int nPlatform )
	{
        m_nWSpitStatus = 0;
          if (!CheckIfCanSpitBall ()) {
            photonView.RPC("RPC_CancelSpit", PhotonTargets.All );
            return;
		}
          
        SetMP( GetMP() - m_fSpitBallCostMp );

		Main.s_Instance.BeginCameraSizeProtect ( m_fSpitBallRunTime );
        CSkillSystem.s_Instance.HideProgressBar();

       float fShellTime = GetRealShellTime(m_fSpitBallShellTime);
        photonView.RPC ( "RPC_DoSpit", PhotonTargets.All, m_fSpitPercent, nPlatform, PhotonNetwork.time, Main.s_Instance._playeraction.GetWorldCursorPos().x,  Main.s_Instance._playeraction.GetWorldCursorPos().y, m_fSpitBallDistance, m_fSpitBallRunTime, fShellTime);
	}

    [PunRPC]
    public void RPC_CancelSpit()
    {
        CSkillSystem.s_Instance.HideProgressBar();
        Main.s_Instance.ClearSkillColdDown_SpitBall();
        ClearSpitTarget();
        EndForcing();
    }
    

    [PunRPC]
	public void RPC_DoSpit( float fPercent, int nPlatform, double dOccurTime, float fWorldCursorPosX, float fWorldCursorPosY, float fSpitBallRunDistance, float fSpitBallRunTime, float fShellTime)
	{


        m_nWSpitStatus = 0;

        EndForcing();

        if (!IsPlayerInitCompleted())
        {
            return;
        }

        if ( IsMainPlayer() )
        {
            CAudioManager.s_Instance.PlayAudio( CAudioManager.eAudioId.e_audio_spit );
        }

		float fDelayTime = (float)( PhotonNetwork.time - dOccurTime );

		for (int i = 0; i < m_lstBalls.Count; i++) {
			Ball ball = m_lstBalls [i];
			if (ball.IsDead ()) {
				continue;
			}

			ball.CalculateChildSize ( fPercent );
			float fMotherLeftSize = 0f;
			float fChildSize = ball.GetAvailableChildSize( ref fMotherLeftSize );

			if (fChildSize < Main.BALL_MIN_SIZE || fMotherLeftSize < Main.BALL_MIN_SIZE || fMotherLeftSize < fChildSize ) {
				continue;
			}
			ball.Spit ( fDelayTime, nPlatform, fChildSize, fMotherLeftSize, fWorldCursorPosX, fWorldCursorPosY, fSpitBallRunDistance, fSpitBallRunTime, fShellTime);

		} // end for


    	BeginClearSpitTargetCount (fSpitBallRunTime);
	}

    float m_fSpitBallCostMp = 0;
    float m_fSpitBallDistance = 0;
    float m_fSpitBallRunTime = 0;
    float m_fSpitBallShellTime = 0;
    public void BeginForceSpit( float fCostMp, float fMaxForceTime, float fShootDistance, float fShootRunTime, float fShellTime)
    {
        m_fSpitBallShellTime = fShellTime;
        m_fSpitBallCostMp = fCostMp;
        m_fSpitBallDistance = fShootDistance;
        m_fSpitBallRunTime = fShootRunTime;
        m_fForceSpitTotalTime = fMaxForceTime;
        if ( !CheckIfCanSpitBall() )
        {
            
            return;
        }

        CSkillSystem.s_Instance.ShowProgressBar(CSkillSystem.eSkillSysProgressBarType.w_spit_ball);

        photonView.RPC("RPC_BeginForceSpit", PhotonTargets.All, fCostMp, fMaxForceTime, fShootDistance, fShootRunTime, fShellTime);
    }

    public void EndForcing()
    {
        m_fForcing = false;
        m_fSpitPercent = 0f;
        m_fForceSpitCurTime = 0f;
    }

    [PunRPC]
    public void RPC_BeginForceSpit(float fCostMp, float fMaxForceTime, float fShootDistance, float fShootRunTime, float fShellTime)
    {
        m_fSpitBallShellTime = fShellTime;
        m_fSpitBallCostMp = fCostMp;
        m_fSpitBallDistance = fShootDistance;
        m_fSpitBallRunTime = fShootRunTime;
        m_fForceSpitTotalTime = fMaxForceTime;
        m_nWSpitStatus = 1;
        m_fForcing = true;
    }

    float m_fForceSpitTotalTime = 0;
    bool m_fForcing = false;
    float m_fForceSpitCurTime = 0f;
    float m_fSpitPercent = 0f;
    public void Spitting( /*float fPercent*/ )
    {
        if (!m_fForcing)
        {
            return;
        }

        if (m_nWSpitStatus != 1)
        {
            return;
        }

        if (IsMainPlayer())
        {
            CSkillSystem.s_Instance.UpdateProgressData(m_fForceSpitCurTime, m_fForceSpitTotalTime);
        }


        m_fForceSpitCurTime += Time.deltaTime;
        m_fSpitPercent = m_fForceSpitCurTime / m_fForceSpitTotalTime;
        if (m_fSpitPercent > 0.5f)
        {
            m_fSpitPercent = 0.5f;
        }
        if (m_fForceSpitCurTime >= m_fForceSpitTotalTime)
        {
            if (IsMainPlayer())
            {
                Main.s_Instance.EndSpit();
                return;
            }
        }

        // “瞄准器”机制重新设计了。因为现在是按视野优化，球球不显示的时候未必瞄准器不显示
        /*
		int nAvailableNum = GetCurAvailableBallCount ();
		int nCount = 0;
		for (int i = 0; i < m_lstBalls.Count; i++) {
			Ball ball = m_lstBalls[i];
			if (ball.IsDead ()) {
				continue;
			}

			if (!ball.CheckIfCanForceSpit ()) {
				continue;
			}

			ball.CalculateChildSize (m_fSpitPercent );
			float fMotherLeftSize = 0f;
			float fChildSize = ball.GetAvailableChildSize( ref fMotherLeftSize );
			if (fChildSize < Main.BALL_MIN_SIZE || fMotherLeftSize < Main.BALL_MIN_SIZE || fMotherLeftSize < fChildSize) {
				continue;
			}

			ball.ShowTarget ( m_fSpitBallDistance );

			nCount++;
			if (nCount >= nAvailableNum) {
				break;
			}
		} // end for
        */

        for (int i = 0; i < m_lstBalls.Count; i++)
        {
            Ball ball = m_lstBalls[i];
            if (ball.IsDead())
            {
                continue;
            }

            if (Main.s_Instance.GetPlatformType() == Main.ePlatformType.mobile)
            {
                vec2Temp = Main.s_Instance.GetSpitDirection();
                if ( vec2Temp.x == 0f && vec2Temp.y == 0)
                {
                    vec2Temp = ball.GetDir();
                    Main.s_Instance.SetSpitDirection(vec2Temp);
                }
            }
            else
            {
                vec2Temp = ball.GetDir();
            }
            ShowGunsight(ball.GetIndex(), ball.GetPos(), vec2Temp, ball.GetSize(), m_fSpitPercent, m_fSpitBallDistance);
        } // end for

    }
        // 显示瞄准器(只基于数据，不基于ball的实例，因为根据视野裁剪机制，ball可能根本就不会处在激活状态)
    CGunsight[] m_aryGunsight = new CGunsight[Main.MAX_BALL_NUM / 2];
    public void ShowGunsight( int nBallIndex, Vector2 pos, Vector2 dir, float fSize, float fPercent, float fDistanceMultiple)
    {
        if (nBallIndex >= m_aryGunsight.Length)
        {
            Debug.LogError("nBallIndex >= m_aryGunsight.Length");
            return;
        }

        CGunsight gunsight = m_aryGunsight[nBallIndex];
        if (gunsight == null)
        {
            gunsight = ResourceManager.s_Instance.NewGunsight();
            m_aryGunsight[nBallIndex] = gunsight;
        }
        float fMotherRadius = 0f;
        float fChildSize = Ball.CalculateChildSize(fSize, fPercent, ref fMotherRadius);
        if (fChildSize < Main.BALL_MIN_SIZE)
        {
            return;
        }
        gunsight.SetInfo(pos, dir, fMotherRadius, fChildSize, fDistanceMultiple);
        gunsight.gameObject.SetActive(true);
    }

    void EndSpit()
	{
		m_bSpitting = false;
	}

	float m_fPreDrawCount = 0.0f;
	void PreDrawLoop()
	{
		if (MapEditor.GetEatMode() != MapEditor.eEatMode.xiqiu) {
			return;
		}

		if (m_fPreDrawCount < 0.3f) {
			m_fPreDrawCount += Time.deltaTime;
			return;
		}
		m_fPreDrawCount = 0.0f;

		for (int i = m_lstBalls.Count - 1; i >= 0; i--) {
			Ball ball = m_lstBalls [i];
			if (ball.IsDead()) {
				continue;
			}
			ball.DoDraw ();
		}
	}

	Vector3 m_vecBalslsCenter = new Vector3();
	public Vector3 GetBallsCenter()
	{
		m_vecBalslsCenter.x = 0f;
		m_vecBalslsCenter.y = 0f;
		int nRealNum = 0;
		for (int i = 0; i < m_lstBalls.Count; i++) {
			Ball ball = m_lstBalls [i];
			if (ball.IsDead ()) {
				continue;
			}
			nRealNum++;
			vecTempPos = ball.GetPos ();
			m_vecBalslsCenter.x += vecTempPos.x;
			m_vecBalslsCenter.y += vecTempPos.y;
		}
		m_vecBalslsCenter.x /= nRealNum;
		m_vecBalslsCenter.y /= nRealNum;

		return m_vecBalslsCenter;
	}


	public void MergeAll()
	{
		photonView.RPC ( "RPC_MergeAll", PhotonTargets.All);
	}

	List<int> m_lstToMergeAllBalls = new List<int> ();
	[PunRPC]
	public void RPC_MergeAll()
	{
        DoMergeAllToTheBiggestBall();
  	}

    // 把所有的球秒合到最大的球身上去
    public void DoMergeAllToTheBiggestBall()
    {
        float fTotalVolume = 0f;
        Ball biggestBall = null;
        for ( int i = 0; i < m_lstBalls.Count; i++ )
        {
            Ball ball = m_lstBalls[i];
            if ( ball.IsDead() )
            {
                continue;
            }

            fTotalVolume += ball.GetVolume();
            if (biggestBall == null)
            {
                biggestBall = ball;
                continue;
            }
            
            if ( biggestBall.GetVolume() < ball.GetVolume())
            {
                biggestBall.SetDead( true );
                biggestBall = ball;
            }
            else
            {
                ball.SetDead(true);
            }
        }

        if (biggestBall == null)
        {
            Debug.LogError("biggestBall null");
            return;
        }

        biggestBall.SetVolume(fTotalVolume);
    }

	public void Test_AddAllBallSize( float fVal )
	{
		photonView.RPC ( "RPC_AddAllBallSize", PhotonTargets.All, fVal);
	}

	[PunRPC]
	public void RPC_AddAllBallSize( float fVal )
	{
		for (int i = 0; i < m_lstBalls.Count; i++) {
			Ball ball = m_lstBalls [i];
			if (ball.IsDead ()) {
				continue;
			}
			ball.SetSize ( ball.GetSize() + fVal );
		}
	}

	public void SetThornOfBallSize( int nIndex, float fSize )
	{
		photonView.RPC ( "RPC_SetThornOfBallSize", PhotonTargets.All, nIndex, fSize );
	}

	[PunRPC]
	public void RPC_SetThornOfBallSize( int nIndex, float fSize )
	{
		if (nIndex < 0 || nIndex >= m_lstBalls.Count) {
			Debug.LogError ( "从个的？？nIndex < 0 || nIndex >= m_lstBalls.Count" );
			return;
		}

		Ball ball = m_lstBalls [nIndex];
		if (ball.IsDead ()) {
			Debug.LogError ( "从个的？？这个球都死球了还在SetSize" );
			return;
		}

		ball.SetThornSize ( fSize );
	}
    
	public void SetBallSize( int nIndex, float fSize )
	{
		if (!photonView.isMine) {
			return;
		}

		photonView.RPC ( "RPC_SetBallSize", PhotonTargets.All, nIndex, fSize );
	}

	[PunRPC]
	public void RPC_SetBallSize( int nIndex, float fSize )
	{
		if (nIndex < 0 || nIndex >= m_lstBalls.Count) {
			Debug.LogError ( "从个的？？nIndex < 0 || nIndex >= m_lstBalls.Count" );
			return;
		}

		Ball ball = m_lstBalls [nIndex];
		if (ball.IsDead ()) {
			Debug.LogError ( "从个的？？这个球都死球了还在SetSize" );
			return;
		}

		ball.Local_SetSize ( fSize );
	}

	public void DeleteBall( int nIndex )
	{
		photonView.RPC ( "RPC_DeleteBall", PhotonTargets.All, nIndex );
	}

	[PunRPC]
	public void RPC_DeleteBall( int nIndex )
	{
		if (nIndex < 0 || nIndex >= m_lstBalls.Count) {
			Debug.LogError ( "nIndex < 0 || nIndex >= m_lstBalls.Count" );
			return;
		}

		Ball ball = m_lstBalls[nIndex];
		ball.SetDead ( true );
	}

	public struct sSplitInfo
	{
		public Vector3 pos;
		public 	Vector2 dir;
		public int my_max_split_num;
		public int my_real_split_num;
		public int split_couting;
		public float real_split_size;
		public float seg_dis;
		public bool dead_after_split;
		public Ball _ball;

		public bool CheckIfDeadAfterSplit( ref float fMotherLeftSize )
		{
			float fMotherCurSize = _ball.GetSize ();
			float fMotherCurArea = CyberTreeMath.SizeToArea ( fMotherCurSize, Main.s_Instance.m_nShit );
			float fChildArea = CyberTreeMath.SizeToArea ( real_split_size, Main.s_Instance.m_nShit );
			fMotherCurArea -= fChildArea * my_real_split_num;
			if (fMotherCurArea <= 0f) {
				dead_after_split = true;
				return dead_after_split;
			} else {
				fMotherLeftSize = CyberTreeMath.AreaToSize ( fMotherCurArea, Main.s_Instance.m_nShit );
				if (fMotherLeftSize < Main.BALL_MIN_SIZE) {
					dead_after_split = true;
					return dead_after_split;
				}
			}
			dead_after_split = false;

			return dead_after_split;
		}
	};

	List<sSplitInfo> m_lstRecycledSpiltInfo = new List<sSplitInfo>();
	sSplitInfo ReuseSplitInfo()
	{
		sSplitInfo info;
		if (m_lstRecycledSpiltInfo.Count > 0) {
			info = m_lstRecycledSpiltInfo [0];
			m_lstRecycledSpiltInfo.RemoveAt (0);
			return info;
		}
		info = new sSplitInfo ();
		return info;
	}

	void RecycleSplitInfo( sSplitInfo info )
	{
		m_lstRecycledSpiltInfo.Add ( info );
	}

	void ClearSplitInfoList()
	{
		for (int i = 0; i < m_lstSplitBalls.Count; i++) {
			RecycleSplitInfo ( m_lstSplitBalls[i] );
		}
		m_lstSplitBalls.Clear ();
	}

	public bool CheckIfCanSplit()
	{
		if (GetMP () < Main.s_Instance.m_fSplitCostMP) {
			Main.s_Instance.g_SystemMsg.SetContent (CMsgSystem.s_Instance.GetMsgContentByType(CMsgSystem.eSysMsgType.mp_not_enough));
			return false;
		}
		return true;
	}

	bool m_bSplitting = false;
	int m_nCurSplitTimes = 0;
	int m_nRealSplitNum = 0;
	List<sSplitInfo> m_lstSplitBalls = new List<sSplitInfo> ();
	byte[] _bytesSplit = new byte[90];
	public bool OneBtnSplitBall( int nPlatform, Vector3 vecWorldCursorPos, float fMpCost, float fDistance, float fSplitBallNum, float fRunTime, float fShellTime)
	{
		// 废弃以前的“R键分球”流程
		int nCurLiveBallNum = 0;
		Ball ball = GetOneLiveBall( ref nCurLiveBallNum ); // 暂定只有一个球的时候才分
		if (nCurLiveBallNum > 1) {
            Main.s_Instance.g_SystemMsg.SetContent( "只有一个球的时候才能一键分" );
			return false;
		}

		// 计算这个球可不可以分够策划配置的个数，如果不能，那么可以具体可以分多少个
		int nExpectNum =  (int)fSplitBallNum; 
		int nRealNum = 0;
		float fChildSize = 0.0f;
        float fMotherVolume = ball.GetVolume();
        float fChildVolume = 0f;
		for (nRealNum = nExpectNum; nRealNum >= 2; nRealNum--) { // 至少分为2个，最多分策划配置的个数
            fChildVolume = fMotherVolume / nRealNum;
            fChildSize = CyberTreeMath.Volume2Scale(fChildVolume);
			if (fChildSize >= Main.BALL_MIN_SIZE) {
				break;
			}
		}
		int nCurAvailableNum = GetCurAvailableBallCount ();
		if (nRealNum > nCurAvailableNum - 1) {
			nRealNum = nCurAvailableNum - 1;
		}
		if (nRealNum < 2 ) {
            Main.s_Instance.g_SystemMsg.SetContent("当前的母球体积不足以一键分球");
            return false;
		}

		if (!CheckIfCanSplit ()) {
            return false;
		}

		SetMP ( GetMP() - fMpCost);

        SetBallSize ( ball.GetIndex(), fChildSize  );
        //ball.Local_SetSize(fChildSize);

		int nChildNum = nRealNum - 1;

		Vector2 vecSplitDir = vecWorldCursorPos - ball.GetPos ();
		vecSplitDir.Normalize ();
		float fTotalDistance = Vector2.Distance ( vecWorldCursorPos, ball.GetPos () );
        if (fTotalDistance> fDistance)
        {
            fTotalDistance = fDistance;
        }

		int nPointer = 0;
		BitConverter.GetBytes (nRealNum).CopyTo (_bytesSplit, nPointer);
		nPointer += sizeof(byte);
		BitConverter.GetBytes (ball.GetIndex()).CopyTo (_bytesSplit, nPointer);
		nPointer += sizeof(byte);
		BitConverter.GetBytes (vecSplitDir.x).CopyTo (_bytesSplit, nPointer);
		nPointer += sizeof(float);
		BitConverter.GetBytes (vecSplitDir.y).CopyTo (_bytesSplit, nPointer);
		nPointer += sizeof(float);
		BitConverter.GetBytes (fTotalDistance).CopyTo (_bytesSplit, nPointer);
		nPointer += sizeof(float);
        BitConverter.GetBytes(fRunTime).CopyTo(_bytesSplit, nPointer);
        nPointer += sizeof(float);
        BitConverter.GetBytes(fShellTime).CopyTo(_bytesSplit, nPointer);
        nPointer += sizeof(float);
        BitConverter.GetBytes (fChildSize).CopyTo (_bytesSplit, nPointer);
		nPointer += sizeof(float);
		BitConverter.GetBytes (PhotonNetwork.time).CopyTo (_bytesSplit, nPointer);
		nPointer += sizeof(double);

		GetSomeLiveBallIndex ( nChildNum );
		BitConverter.GetBytes (nChildNum).CopyTo (_bytesSplit, nPointer);
		nPointer += sizeof(byte);

		if (nChildNum != m_lstSomeAvailabelBallIndex.Count) {
			Debug.LogError ( "有Bug：nChildNum != m_lstSomeAvailabelBallIndex.Count" );
		}

		for (int i = 0; i < m_lstSomeAvailabelBallIndex.Count; i++) { // 比想要分的个数(nRealNum)少一个，因为母球自己还占一个
			BitConverter.GetBytes (m_lstSomeAvailabelBallIndex[i]).CopyTo (_bytesSplit, nPointer);
			nPointer += sizeof(byte);
		}

		photonView.RPC ( "RPC_OneBtnSplitBall_New", PhotonTargets.All,  _bytesSplit );

        return true;
	}
    // right here
	[PunRPC]
	public void RPC_OneBtnSplitBall_New( byte[] bytes )
	{
        if (!IsPlayerInitCompleted())
        {
            return;
        }
        
        int nPointer = 0;
		int nRealNum = (byte)BitConverter.ToChar(bytes, nPointer);
		nPointer += sizeof (byte);
        
		int nMotherIndex = (byte)BitConverter.ToChar(bytes, nPointer);
		nPointer += sizeof (byte);
		Ball ballMother = GetBallByIndex ( nMotherIndex );
		if (ballMother == null) {
			Debug.LogError ( "出Bug了。初始化的时候每个球都是实例化了的，不可能为null" );
		}
		ballMother.SetDead ( false );

		float vecDirX = BitConverter.ToSingle(bytes, nPointer);
		nPointer += sizeof (float);
		float vecDirY = BitConverter.ToSingle(bytes, nPointer);
		nPointer += sizeof (float);
		float fTotalDistance = BitConverter.ToSingle(bytes, nPointer);
		nPointer += sizeof (float);
        float fRunTime = BitConverter.ToSingle(bytes, nPointer);
        nPointer += sizeof(float);
        float fShellTime = BitConverter.ToSingle(bytes, nPointer);
        nPointer += sizeof(float);
        float fChildSize = BitConverter.ToSingle(bytes, nPointer);
		nPointer += sizeof (float);
		double fOccureTime = BitConverter.ToDouble(bytes, nPointer);
		nPointer += sizeof (double);
		float fDelayTime = (float)( PhotonNetwork.time - fOccureTime );

		int nChildNum = (byte)BitConverter.ToChar(bytes, nPointer);
		nPointer += sizeof (byte);
		if (nChildNum != (nRealNum - 1)) {
			Debug.LogError ( "有Bug:nChildNum != (nRealNum - 1)" );
		}

		float fRealRunTime = fRunTime; /*- fDelayTime;
		if (fRealRunTime < Main.s_Instance.m_fMinRunTime) {
			fRealRunTime = Main.s_Instance.m_fMinRunTime;
		}
*/

		float fSegDis = fTotalDistance / nRealNum;
		vec2Temp.x = vecDirX;
		vec2Temp.y = vecDirY;

		bool bRealTimeFollowMouse = false;

		ballMother.SetShellEnable ( false );
		ballMother.SetSize ( fChildSize );
		ballMother.Local_SetShellInfo (GetRealShellTime(fShellTime), 0f, Ball.eShellType.r_split_ball );
		ballMother.Local_BeginShell ();

		for (int i = 0; i < nChildNum; i++) {
			int nChildIndex = (byte)BitConverter.ToChar(bytes, nPointer);  
			nPointer += sizeof (byte);

			Ball ballChild = GetBallByIndex ( nChildIndex );
			if (ballChild == null) {
				Debug.LogError ( "出Bug了。初始化的时候每个球都是实例化了的，不可能为null" );
			}
			ballChild.Local_SetPos (ballMother.GetPos());
			ballChild.RebornInit ();
			float fMyDistance = fSegDis * (i + 1);

            float fMySpeed =fMyDistance / fRealRunTime;

            ballChild.SetShellEnable ( false );
			ballChild.Local_SetSize (fChildSize);
			ballChild.SetDir (vec2Temp);
			ballChild.Local_SetShellInfo (GetRealShellTime(fShellTime), fDelayTime, Ball.eShellType.r_split_ball );
			ballChild.Local_BeginEject_New( fMySpeed, fMyDistance, false, false, 0f);

		}

	}

    public float GetRealShellTime(float fConfigShellTime)
    {
        // Buff对壳时间的影响
        fConfigShellTime *= GetBuffEffect_ShellTime();

        // 道具对壳的时间的影响
        fConfigShellTime *= ( 1 + GetDecreaseShellTime() );

        return fConfigShellTime;
    }

    public void OneBtnSplitBall( int nPlatform, float fTotalDistance )
	{
		if (m_bSplitting) { // 正在分，还没分完呢，不要进行新一轮的分
			return;
		}	
		photonView.RPC ( "RPC_OneBtnSplitBall_Mobile", PhotonTargets.All, nPlatform, fTotalDistance  );
	}




	public void PreOneBtnSplitting( float fCurOneBtnSplitDis )
	{
		for (int i = 0; i < m_lstBalls.Count; i++) {
			Ball ball = m_lstBalls [i];
			if (ball.IsDead ()) {
				continue;
			}
			ball.ShowSplitTarget ( fCurOneBtnSplitDis );
		}
	}

	public void CancelSplit()
	{
		for (int i = 0; i < m_lstBalls.Count; i++) {
			Ball ball = m_lstBalls [i];
			ball.CancelSpit ();
		}
	}

	public void PreOneBtnSplitBall( int op, float param1, float param2 )
	{
		Ball ball = null;
		int nCurAvailableBallCount = GetCurAvailableBallCount ();
		if (nCurAvailableBallCount <= 0) {
			CancelSplit ();
			return;
		}
		int nPreTotalNum = 0;
		ClearSplitInfoList ();

		float fMotherLeftSize = 0f;

		for (int i = 0; i < m_lstBalls.Count; i++) {
			ball = m_lstBalls [i];
			if (ball == null) {
				continue;
			}
			if (ball.IsDead ()) {
				continue;
			}
			sSplitInfo info = ReuseSplitInfo();
			info._ball = ball;
			info.my_max_split_num = 0;
			info.real_split_size = 0f;
			info.seg_dis = 0f;

			if (op == 0) {
				ball.CalculateRealSplitInfo (param1, param2, ref info.my_max_split_num, ref info.real_split_size, ref info.seg_dis);
			} else {
				ball.CalculateRealSplitInfo (param1, ref info.my_max_split_num, ref info.real_split_size, ref info.seg_dis);
			}
			nPreTotalNum += info.my_max_split_num;
			info.pos = ball.GetPos ();
			info.dir = ball.GetDir ();
			if ( (!photonView.isMine) && !IsMoving ()) {
				info.dir = ball.m_vecLastDirection;
			}
			info.split_couting = 0;
			info.my_real_split_num = 0;
			if (info.my_max_split_num <= 0) {
				info._ball.CanceSplit ();
			}
			//info.CheckIfDeadAfterSplit ( ref fMotherLeftSize );
			m_lstSplitBalls.Add (info);
		}

		bool bEnd = true;
		while ( nCurAvailableBallCount > 0) {
			bEnd = true;
			for (int i = 0; i < m_lstSplitBalls.Count; i++) {
				sSplitInfo info = m_lstSplitBalls [i];
				if (info.my_real_split_num < info.my_max_split_num) {
					info.my_real_split_num++;
					m_lstSplitBalls [i] = info;
					nCurAvailableBallCount--;
					bEnd = false;
				}
			}// end for
			if (bEnd) {
				break;
			}
		} // end while

	}

	[PunRPC]
	public void RPC_OneBtnSplitBall_Mobile( double dOccurTime, int nPlatform, float fTotalDistance )
	{
		float fDelayTime = (float)( PhotonNetwork.time - dOccurTime );

		PreOneBtnSplitBall ( 1, fTotalDistance, 0 );

		m_bSplitting = true;
		m_nCurSplitTimes = 0;

		while (m_bSplitting) {
			Splitting ( fDelayTime, nPlatform );
		}
	}


	[PunRPC]
	public void RPC_OneBtnSplitBall( double dOccurTime, int nPlatform, float fWorldCursorPosX, float fWorldCursorPosY )
	{
		float fDelayTime = (float)( PhotonNetwork.time - dOccurTime );

		PreOneBtnSplitBall ( 0, fWorldCursorPosX, fWorldCursorPosY );

		m_bSplitting = true;
		m_nCurSplitTimes = 0;

		while (m_bSplitting) {
			Splitting ( fDelayTime, nPlatform );
		}
	}

	void Splitting( float fDelayTime, int nPlatform )
	{
		if (!m_bSplitting) {
			return;
		}

		Ball ball = null;
		bool bEnd = true;
		for (int i = 0; i < m_lstSplitBalls.Count; i++ ) {
			sSplitInfo info  = m_lstSplitBalls [i];
			if ( info.split_couting >= info.my_real_split_num) {
				info._ball.CanceSplit ();
				continue;
			}
			info._ball.Local_SetShellInfo ( Main.s_Instance.m_fSplitShellShrinkTotalTime, 0f, Ball.eShellType.r_split_ball );
			info._ball.Local_BeginShell ();
			SplitOne ( fDelayTime, nPlatform, m_nCurSplitTimes, info);
			info.split_couting++;
			m_lstSplitBalls [i] = info;
			bEnd = false;
		}
		m_nCurSplitTimes++;

		if (bEnd) {
			EndOneBtnSplit ();
		}
	}

	public void EndOneBtnSplit()
	{
		m_bSplitting = false;
		float fMotherLeftSize = 0f;
		for (int i = 0; i < m_lstSplitBalls.Count; i++) {
			sSplitInfo info = m_lstSplitBalls [i];
			if (info.CheckIfDeadAfterSplit ( ref fMotherLeftSize )) {
				if (photonView.isMine) {
					DestroyBall( info._ball.GetIndex() );
				}
			} else {
				info._ball.Local_SetSize ( fMotherLeftSize );
				info._ball.Local_BeginShell ();
			}
		}
	}

	public void SplitOne( float fDelayTime, int nPlatform, int nTimes, sSplitInfo info )
	{
		bool bNew = false;
		Ball new_ball = ReuseOneBall (); 
		if (new_ball == null) {
			//Debug.LogError ( "SplitOne  new_ball == null" );
			return;
		}

		if (info.seg_dis < 1 ) {
			info.seg_dis = 1f;
		}
		float fDis = info.seg_dis * ( nTimes + 1 );

		float fRealRunTime =  Main.s_Instance.m_fSplitRunTime - fDelayTime;
		if (fRealRunTime < Main.s_Instance.m_fMinRunTime) {
			fRealRunTime = Main.s_Instance.m_fMinRunTime;
		}
		//float fInitSpeed = CyberTreeMath.GetV0 ( fDis, fRealRunTime  );
		//float fAccelerate = CyberTreeMath.GetA ( fDis, fRealRunTime);

		bool bRealTimeFollowMouse = false;


		Ball.RongCuo_NotPointToMyself ( ref info.dir.x, ref info.dir.y );

		float fSpeed = fDis / fRealRunTime;

		new_ball.SetShellEnable ( false );
		new_ball.Local_SetPos (info.pos);
		new_ball.Local_SetSize (info.real_split_size);
		new_ball.SetDir (info.dir);
		new_ball.Local_SetShellInfo ( Main.s_Instance.m_fSplitShellShrinkTotalTime, 0f, Ball.eShellType.r_split_ball );
		new_ball.Local_BeginEject_New( fSpeed, fDis, bRealTimeFollowMouse, true, Main.s_Instance.m_fSplitStayTime);
	}

	public struct sExplodeInfo
	{

	};

	List<int> m_lstToExplodeBalls = new List<int>();
	public void OneBtnExplode()
	{
		m_lstToExplodeBalls.Clear ();
		for (int i = 0; i < m_lstBalls.Count; i++) {
			Ball ball = m_lstBalls [i];
			if (ball.IsDead ()) {
				continue;
			}
			m_lstToExplodeBalls.Add ( ball.GetIndex() );
		}

        CMonsterEditor.sThornConfig config = CMonsterEditor.s_Instance.GetMonsterConfigById("0_4");
        for (int i = 0; i < m_lstToExplodeBalls.Count; i++) {
            Ball ball = m_lstBalls[m_lstToExplodeBalls[i]];
            ball.Explode(config);
        }
	}

	
	public int GetCurMainPlayerBallCount()
	{
		return m_lstBalls.Count;
	}

	public int GetCurBallCount()
	{
		return m_lstBalls.Count;
	}

	public int GetCurTotalBallCountIncludeRecycled()
	{
		return ( m_lstBalls.Count + m_lstRecycledBalls.Count );
	}

	public int GetAvailableBallCount()
	{
		return (int)Main.s_Instance.m_fMaxBallNumPerPlayer - GetCurBallCount ();
	}

	
	

	public Ball GetOneAvailableBall()
	{
		for (int i = 0; i < m_lstBalls.Count; i++) {
			Ball ball = m_lstBalls [i];
			if (ball && (!ball.IsDead ())) {
				return ball;
			}
		}
		return null;
	}

	public List<Ball> GetBallList()
	{
		return m_lstBalls;
	}

	public bool CheckIfCanSpitSpore()
	{
		if (GetMP () < m_fSpitSporeMpCost) {
			Main.s_Instance.g_SystemMsg.SetContent (CMsgSystem.s_Instance.GetMsgContentByType(CMsgSystem.eSysMsgType.mp_not_enough));
			return false;
		}

		return true;
	}

	float m_fUpdateBuffCount = 0f;
	const float c_UpdateBuffInterval = 1f;
	void UpdateBuffEffect()
	{
		m_fUpdateBuffCount += Time.deltaTime;
		if (m_fUpdateBuffCount < c_UpdateBuffInterval) {
			return;
		}
		m_fUpdateBuffCount = 0;

		UpdateBuffEffect_ShellTime ();
	}



	float UpdateBuffEffect_SpitSporeSpeed()
	{
        float fSpitSporeInterval = m_fSpitSporeTimeInterval;/*Main.s_Instance.m_fSpitSporeTimeInterval*/;
		CBuffEditor.sBuffConfig buff_config;
		for (int i = 0; i < m_lstBuff.Count; i++) {
			CBuff buff = m_lstBuff [i];
			buff_config = buff.GetConfig ();
			if (buff_config.func == (int)CBuffEditor.eBuffFunc.player_spit_spore_speed) {
                fSpitSporeInterval *= ( 1f + buff_config.aryValues [0] );
			}
		}

        return fSpitSporeInterval;

    }
		

	float UpdateBuffEffect_SpitSporeDis()
	{
        //m_fSpitSporeDis = Main.s_Instance.m_fSpitSporeRunDistance;
        float fSpitSporeDis = m_fSpitSporeDis;
        CBuffEditor.sBuffConfig buff_config;
		for (int i = 0; i < m_lstBuff.Count; i++) {
			CBuff buff = m_lstBuff [i];
			buff_config = buff.GetConfig ();
			if (buff_config.func == (int)CBuffEditor.eBuffFunc.player_spit_spore_distance) {
               fSpitSporeDis *= ( 1f + buff_config.aryValues [0] );
			}
		}

        return fSpitSporeDis;

    }

    string m_szSpitSporeMonsterId = "";
	float m_fSpitSporeDis = 0f;
	float m_fSpitSporeTimeInterval = 0f;
    float m_fSpitSporeMpCost = 0f;
    bool m_bSpittingSpore = false;
    int m_nCurSporeLevel = 0;
	public void BeginSpitSpore()
	{
        m_fGenerateSporeIdTimeCount = 1f;
        /*
		if (!CheckIfCanSpitSpore ()) {
			return;
		}
        */
        //// 获取该技能当前的参数（依据该技能当前累积的技能点）
        bool bCanUseSkill = true;
        CSkillSystem.sSkillParam skill_param = CSkillSystem.s_Instance.GetSkillParamById(CSkillSystem.eSkillId.q_spore, ref bCanUseSkill);
        if (bCanUseSkill)
        {
            float fMpCost = skill_param.aryValues[0];
            float fShootSpeed = skill_param.aryValues[2];
            float fShootDistance = skill_param.aryValues[3];
            string szMonsterId = skill_param.szValue;
            m_nCurSporeLevel = skill_param.nCurLevel;
            photonView.RPC("RPC_BeginSpitSpore", PhotonTargets.All, fMpCost, fShootSpeed, fShootDistance, szMonsterId);
        }
    }

    public int GetCurSporeLevel()
    {
        return m_nCurSporeLevel;
    }

	[PunRPC]
	public void RPC_BeginSpitSpore( float fMpCost, float fShootSpeed, float fShootDistance, string szMonsterId)
	{
        m_fGenerateSporeIdTimeCount = 1f;
        m_szSpitSporeMonsterId = szMonsterId;
        m_fSpitSporeDis = fShootDistance;// Main.s_Instance.m_fSpitSporeRunDistance;
        m_fSpitSporeTimeInterval = fShootSpeed;// Main.s_Instance.m_fSpitSporeTimeInterval;
        m_fSpitSporeMpCost = fMpCost;
        m_bSpittingSpore = true;
        
        // 播放吐孢子的骨骼动画
        for ( int i = 0; i < m_lstBalls.Count; i++ )
        {
            Ball ball = m_lstBalls[i];
            CSpineManager.s_Instance.PlayBallAnimation(ball, CSpineManager.eBallSpineType.spit_spore, true);
        }
	}

	public bool IsSpittingSpore()
	{
		return m_bSpittingSpore;
	}

    List<uint> m_lstSporeToSpit = new List<uint>();
    List<uint> lstSporeToSpitTemp = new List<uint>();
    float m_fSpitSporeInterval = 0f;
    float m_fSpitSporeDistance = 0f;
    float m_fSpitSporeTimeCount = 0f;
    float m_fGenerateSporeIdTimeCount = 1f;
    int m_nSporeNumPerSec = 0;
    uint m_uSporeGuid = 0;
    byte[] _bytesSyncSporeId = new byte[300];
    void GenerateSporeId()
    {
        if ( !IsMainPlayer() )
        {
            return;
        }

        if (!m_bSpittingSpore)
        {
            return;
        }

        m_fGenerateSporeIdTimeCount += Time.deltaTime;
        if (m_fGenerateSporeIdTimeCount < 0.5f)
        {
            return;
        }
        m_fGenerateSporeIdTimeCount = 0f;

        /////
        if (m_lstSporeToSpit.Count < 64)
        {
            lstSporeToSpitTemp.Clear();
            for (int i = 0; i < 64; i++)
            {
                uint uPlayerSporeId = m_uSporeGuid++;
                m_lstSporeToSpit.Add(uPlayerSporeId);
                lstSporeToSpitTemp.Add(uPlayerSporeId);
            }
            StringManager.BeginPushData(_bytesSyncSporeId);
            StringManager.PushData_Short((short)lstSporeToSpitTemp.Count);
            for (int i = 0; i < lstSporeToSpitTemp.Count; i++)
            {
                StringManager.Push_Uint(lstSporeToSpitTemp[i]);
            }
            lstSporeToSpitTemp.Clear();

            int nBlobSize = StringManager.GetBlobSize();
            byte[] bytes = StringManager.GetSuitableBlob(nBlobSize);
            Array.Copy(_bytesSyncSporeId, bytes, nBlobSize);
            photonView.RPC("RPC_SyncSporeId", PhotonTargets.Others, bytes);
        }

        //// end

    }

    [PunRPC]
    public void RPC_SyncSporeId( byte[] bytes )
    {
        if ( IsMainPlayer() )
        {
            return;
        }

        StringManager.BeginPopData(bytes);
        int num = StringManager.PopData_Short();
        for ( int i = 0; i < num; i++ )
        {
            uint uPlayerGuid = StringManager.PopData_Uint();
            m_lstSporeToSpit.Add(uPlayerGuid);
        }
    }

	public void SpittingSpore()
	{
		if (!m_bSpittingSpore) {
			return;
		}

		if (IsMainPlayer ()) {
			if (!CheckIfCanSpitSpore ()) {
				EndSpitSpore ();
				return;
			}
		}

		m_fSpitSporeInterval = UpdateBuffEffect_SpitSporeSpeed ();
        m_fSpitSporeDistance = UpdateBuffEffect_SpitSporeDis();
		//Main.s_Instance.g_SystemMsg.SetContent ("配置的射击时间间隔和距离分别是：" + "(" + Main.s_Instance.m_fSpitSporeTimeInterval + "," + Main.s_Instance.m_fSpitSporeRunDistance +") , Buff加成后是：（" + m_fSpitSporeTimeInterval + "," + m_fSpitSporeDis + ")" );

		m_fSpitSporeTimeCount += Time.deltaTime;
		if (m_fSpitSporeTimeCount < m_fSpitSporeInterval/*Main.s_Instance.m_fSpitSporeTimeInterval*/) {
			return;
		}

        if (IsMainPlayer())
        {
            CAudioManager.s_Instance.PlayAudio(CAudioManager.eAudioId.e_audio_spore);
        }
        if (m_lstSporeToSpit.Count == 0)
        {
            return;
        }

        m_fSpitSporeTimeCount = 0f;

		for (int i = 0; i < m_lstBalls.Count; i++) {
			Ball ball = m_lstBalls [i];
			if (ball.IsDead ()) {
				continue;
			}
            uint uPlayerSporeGuid = m_lstSporeToSpit[0];
            m_lstSporeToSpit.RemoveAt(0);
            bool bRet = ball.SpitBean (m_fSpitSporeDistance, m_szSpitSporeMonsterId, uPlayerSporeGuid);
            if (bRet)
            {
                if (IsMainPlayer())
                {
                    SetMP(GetMP() - m_fSpitSporeMpCost/*Main.s_Instance.m_fSpitSporeCostMP*/ );
                }
            }
		}
	}

	public void EndSpitSpore()
	{
		photonView.RPC ( "RPC_EndSpitSpore", PhotonTargets.All);
	}

	[PunRPC]
	public void RPC_EndSpitSpore()
	{
		m_bSpittingSpore = false;

        // 停止吐孢子的骨骼动画
        for (int i = 0; i < m_lstBalls.Count; i++)
        {
            Ball ball = m_lstBalls[i];
            CSpineManager.s_Instance.StopBallAnimation(ball, 0);
        }
    }

	float m_nAttenuateTime = 0;


	public void BeginBeanSprayPreDraw( bool bAll, int nSprayId = 0 )
	{
		photonView.RPC ( "RPC_BeginBeanSprayPreDraw", PhotonTargets.All, (float)PhotonNetwork.time, bAll, nSprayId );
	}

	[PunRPC]
	public void RPC_BeginBeanSprayPreDraw( float fPreDrawBeginTinme, bool bAll, int nSprayId )
	{
		MapEditor.s_Instance.BeginSprayPreDraw ( fPreDrawBeginTinme, bAll, nSprayId );
	}

	public void SprayEatBall( int nSprayGUID, float fEatSize )
	{
		photonView.RPC ( "RPC_SprayEatBall", PhotonTargets.All, nSprayGUID, fEatSize );

	}

	[PunRPC]
	public void RPC_SprayEatBall( int nSprayGUID, float fEatSize )
	{
		MapEditor.s_Instance.SprayEatBall ( nSprayGUID, fEatSize );
	}

	public static Player FindPlayerByOwnerId( int nOwnerId )
	{
		GameObject goPlayer = GameObject.Find ("Balls/player_" + nOwnerId);
		if (goPlayer == null) {
			Debug.LogError ( "player error" );
			return null;
		}
		Player player = goPlayer.GetComponent<Player> ();
		return player;
	}

	public Ball FindBallByViewId( int nViewId )
	{
		/*
		for (int i = 0; i < m_lstBalls.Count; i++) { // 这种查找效率太低，后期优化一下
			if (m_lstBalls [i] != null && m_lstBalls [i].photonView.viewID == nViewId) {
				return m_lstBalls [i];
			}
		}
		*/

		return null;
	}

	public void CaptureOneBall( int nSprayID, int nOwnerId, int nViewId )
	{
		photonView.RPC ( "RPC_CaptureOneBall", PhotonTargets.All, nSprayID, nOwnerId, nViewId );

	}

	[PunRPC]
	public void RPC_CaptureOneBall( int nSprayID, int nOwnerId, int nViewID )
	{
		Spray spray = MapEditor.s_Instance.FindSprayById ( nSprayID );
		if (spray == null) {
			Debug.LogError ( "RPC_CaptureOneBall spray null" );
			return;
		}
		Player player = Player.FindPlayerByOwnerId ( nOwnerId );
		if (player == null) {
			Debug.LogError ( "RPC_CaptureOneBall player null" );
			return;
		}
		Ball ball = player.FindBallByViewId ( nViewID );
		if ( ball == null ){
			Debug.LogError ( "RPC_CaptureOneBall ball null" );
			return;
		}
		spray.DoCaptureOneBall ( ball );
	}

	public void ReleaseOneBall( int nSprayID, int nOwnerId, int nViewId )
	{
		photonView.RPC ( "RPC_ReleaseOneBall", PhotonTargets.All, nSprayID, nOwnerId, nViewId );

	}

	[PunRPC]
	public void RPC_ReleaseOneBall( int nSprayID, int nOwnerId, int nViewID )
	{
		Spray spray = MapEditor.s_Instance.FindSprayById ( nSprayID );
		if (spray == null) {
			Debug.LogError ( "RPC_CaptureOneBall spray null" );
			return;
		}
		Player player = Player.FindPlayerByOwnerId ( nOwnerId );
		if (player == null) {
			Debug.LogError ( "RPC_CaptureOneBall player null" );
			return;
		}
		Ball ball = player.FindBallByViewId ( nViewID );
		if ( ball == null ){
			Debug.LogError ( "RPC_CaptureOneBall ball null" );
			return;
		}
		spray.DoReleaseOneBall ( ball );
	}


	List<int> m_lstLiveBall = new List<int> ();

	public void RefreshLiveBalls()
	{

	}

	public Ball GetBallByIndex( int nIndex )
	{
		if (nIndex < 0 || nIndex >= m_lstBalls.Count) {
			Debug.LogError ( "nIndex < 0 || nIndex >= m_lstBalls.Count" );
			return null;
		}
		return m_lstBalls[nIndex];
	}

	int m_nBallNum = 0;
	Ball ReallyInstantiateOneBall()
	{
		vecTempPos.x = -10000f;
		vecTempPos.y = -10000f;
		GameObject goBall = Main.s_Instance.PhotonInstantiate( Main.s_Instance.m_preBall, vecTempPos, Main.s_Instance.s_Params) ;
		goBall.transform.parent = this.transform;
		goBall.transform.position = vecTempPos;
		Ball ball = goBall.GetComponent<Ball> ();
		return ball;
	}

	public void AddOneBall( Ball ball )
	{
		int nIndex = m_lstBalls.Count;
		m_lstBalls.Add ( ball );
		ball.SetIndex ( nIndex );
		ball.transform.parent = this.transform;
		ball.SetPlayer ( this );
		ball.SetDead ( true );
		ball.SetEaten ( false );
	}

	List<int> m_lstSomeAvailabelBallIndex = new List<int>();
	public void GetSomeLiveBallIndex( int nNeedNum )
	{
		m_lstSomeAvailabelBallIndex.Clear ();
		for (int i = 0; i < m_lstBalls.Count; i++) {
			Ball ball = m_lstBalls [i];
			if (ball.IsDead ()) {
				m_lstSomeAvailabelBallIndex.Add ( ball.GetIndex() );
			}
			if (m_lstSomeAvailabelBallIndex.Count >= nNeedNum) {
				return;
			}
		}
	}

	public Ball ReuseOneBall()
	{
		Ball ball = null;
		for (int i = 0; i < m_lstBalls.Count; i++) {
			ball = m_lstBalls [i];
			if (ball.IsDead ()) {
				ball.SetDead ( false );
				ball.RebornInit ();
				return ball;
			}
		}

		Debug.LogError ( "从个的？？ReuseOneBall居然失败了！！！！！！" );
		return null;
	}

	public List<Ball> m_lstRecycledBalls = new List<Ball> ();
	public void RecycleOneBall( Ball ball )
	{
		//MapEditor.s_Instance.LogTestInfo ( ball.photonView.viewID, "RecycleOneBall" + ball.photonView.viewID );
		//MapEditor.s_Instance._inputTest.text += (ball.photonView.viewID + "\n");

		ball.SetDead ( true );
//		Debug.Log ( "RecycleOneBall: " + ball.photonView.viewID );
		bool bRet = m_lstBalls.Remove ( ball );
		if (!bRet) {
			Debug.LogError ("bool bRet = m_lstBalls.Remove ( ball );");
		} else {
//			Debug.Log ( "111111111111111111: " + ball.photonView.viewID );
		}

		m_lstRecycledBalls.Add ( ball );
	}

	public void AddOneBallToRecycled( Ball ball )
	{
		ball.SetDead ( true );
		m_lstRecycledBalls.Add ( ball );
	}


	public void EndInitAllBalls()
	{
		
	}

	bool m_bDead = false;
	public bool CheckIfPlayerDead(int nEaterOwnerId = 0)
	{
		for (int i = 0; i < m_lstBalls.Count; i++) {
			Ball ball = m_lstBalls [i];
			if (!ball.IsDead ()) { // 只要还有一个球没死，玩家就没死
				m_bDead = false;
				return m_bDead;
			}
		}
		m_bDead = true;

        return m_bDead;
	}

    int m_nEaterId = -1;
   public void AnnouceWhoKillWhom()
    {
        if (m_nEaterId > 0)
        {
            photonView.RPC("RPC_AnnouceWhoKillWhom", PhotonTargets.All, m_nEaterId, GetOwnerId());
        }
        m_nEaterId = -1;
    }

	void ProcessKillExp( int nEaterId, int nBeEatenerId )
	{
        if (Main.s_Instance.m_MainPlayer == null) // 观察者客户端木有m_MainPlayer
        {
            return;
        }

		if (Main.s_Instance.m_MainPlayer.GetOwnerId () != nEaterId) {
			return;
		}

		Player playerBeEatener = CPlayerManager.s_Instance.GetPlayer ( nBeEatenerId );
		CGrowSystem.sLevelConfig level_config = CGrowSystem.s_Instance.GetLevelConfigById ( playerBeEatener.GetLevel() );
		Main.s_Instance.m_MainPlayer.AddExp ( level_config.fKillGainExp );
        Main.s_Instance.m_MainPlayer.AddMoney(level_config.fKillGainMoney);
        Main.s_Instance.g_SystemMsg.SetContent ("击杀【" + playerBeEatener.GetLevel() +"】级玩家，获得经验值" + level_config.fKillGainExp + ",获得金钱:" + level_config.fKillGainMoney);
	}

    [PunRPC]
    public void RPC_AnnouceWhoKillWhom( int nEaterId, int nDeadMeatId)
    {
        Player playerEater = CPlayerManager.s_Instance.GetPlayer(nEaterId);
        if (playerEater == null)
        {
            Debug.LogError("有Bug! playerEater == null");
            return;
        }

        Player playerDeadMeat = CPlayerManager.s_Instance.GetPlayer(nDeadMeatId);
        if (playerDeadMeat == null)
        {
            Debug.LogError("有Bug! playerDeadMeat == null");
            return;
        }

        //Debug.Log( "【" + playerEater.GetPlayerName() + "】杀了" + "【" + playerDeadMeat.GetPlayerName() + "】" );
        Main.s_Instance.ShowWhoKillWhom(playerEater, playerDeadMeat);
        CPlayerManager.s_Instance.RecordWhoKillWhom(nEaterId, nDeadMeatId);
        CJiShaInfo.s_Instance.Show(playerEater.GetPlayerName(), playerDeadMeat.GetPlayerName(), playerEater.GetAvatar(), playerDeadMeat.GetAvatar()); 
    }


    public bool IsDead()
	{
		return m_bDead;
	}

	bool m_bReborn = false;
	public void SetRebornCompleted( bool val )
	{
		m_bReborn = val;
	}

	public bool IsRebornCompleted()
	{
		return m_bReborn;
	}

	// 主角所有的球球向中心点靠拢
	bool m_bAllBallsMovingToCenter = false;
	public void BeginMoveToCenter()
	{
		photonView.RPC ( "RPC_BeginMoveToCenter", PhotonTargets.All);
	}

    public bool IsMovingToCenter()
    {
        return m_bAllBallsMovingToCenter;
    }

    [PunRPC]
	public void RPC_BeginMoveToCenter()
	{
		GetBallsCenter ();
		m_bAllBallsMovingToCenter = true;
        for (int i = 0; i < m_lstBalls.Count; i++)
        {
            Ball ball = m_lstBalls[i];
            if (ball.IsDead())
            {
                continue;
            }
            ball.BeginMoveToDestination(m_vecBalslsCenter);
        }
    }

	void MoveToCenter()
	{

        return;
        /*
		if (!m_bAllBallsMovingToCenter) {
			return;
		}

		bool bEnd = true;
		for (int i = 0; i < m_lstBalls.Count; i++) {
			Ball ball = m_lstBalls [i];
			if (ball.IsDead ()) {
				continue;
			}
			if (ball.GetMovingToCenterStatus () == 2) {
				continue;
			}
			bEnd = false;

			if (ball.GetMovingToCenterStatus () == 0) { // 还没开始移动
				ball.SetBallsCenter ( m_vecBalslsCenter );
				ball.SetMovingToCenterStatus(1);
				ball.CalculateMoveToCenterRealSpeed ();
			}
			else if (ball.GetMovingToCenterStatus () == 1) { // 移动中
				GetBallsCenter ();
				ball.SetBallsCenter ( m_vecBalslsCenter );
				ball.CalculateMoveToCenterRealSpeed ();
				ball.DoMovingToCenter();
				//if (CyberTreeMath.CheckIfArriveDest (ball.GetPos (), m_vecBalslsCenter, 1f)) {
				//	ball.SetMovingToCenterStatus (2);
				//}
			}
		}
		if (bEnd) {
			EndMoveToCenter ();
		}
        */
	}

	public void StartMove()
	{
        EndMoveToCenter();
	}

	public void StopMove ()
	{
		if (photonView.isMine) {
			AdjustMoveInfo (1);
		}
	}

	public void EndMoveToCenter()
	{
		m_bAllBallsMovingToCenter = false;
		for (int i = 0; i < m_lstBalls.Count; i++) {
			Ball ball = m_lstBalls [i];
			ball.EndMoveToDestination ();
		}
	}

	public void UpdateIndicatorType( int type )
	{
		for (int i = 0; i < m_lstBalls.Count; i++) {
			Ball ball = m_lstBalls [i];
			if (type == CtrlMode.DIR_INDICATOR_ARROW) {
				
			} else if (type == CtrlMode.DIR_INDICATOR_LINE) {

			}
		}

	}

	public void SetPos( Vector2 pos )
	{
		m_vecPos = pos;
	}

	public Vector2 GetPos()
	{
		return m_vecPos;
	}

	int m_nTestExplodeStatus = 0; // 0 - none   1 - 正在炸开    2 - 正在收回
	float m_fExplodeTimeCount = 0f;
	float m_fMoveToCenterTimeCount = 0f;
	bool m_bTestPlayer = false;
	void TestExplode()
	{
		/*
		if (!m_bTestPlayer) {
			return;
		}

		if (m_nTestExplodeStatus == 0) {
			int nShit = (int)UnityEngine.Random.Range ( 0.0f, 50.0f );
			if (nShit == 5) {
				float fRunDistance = Main.s_Instance.m_fExplodeRunDistanceMultiple * 20.0f;
				float fInitSpeed = CyberTreeMath.GetV0 (fRunDistance, Main.s_Instance.m_fExplodeRunTime  );
				float fAccelerate = CyberTreeMath.GetA ( fRunDistance, Main.s_Instance.m_fExplodeRunTime);
				for (int i = 0; i < m_lstBalls.Count; i++) {
					Ball ball = m_lstBalls [i];
					Vector2 vecDire = MapEditor.s_Instance.GetExplodeDirection ( i );
					ball._dirx = vecDire.x;
					ball._diry = vecDire.y;
					ball.Local_SetShellInfo ( 8f, 0f, Ball.eShellType.explode_ball );
					ball.Local_BeginEject (fInitSpeed, fAccelerate, Main.s_Instance.m_fExplodeRunTime, false, false, Main.s_Instance.m_fExplodeStayTime);
				}

				m_fExplodeTimeCount = 0f;
				SetTestExplodeStatus (1);
			}
		} else if (m_nTestExplodeStatus == 1) {
			m_fExplodeTimeCount += Time.deltaTime;
			if (m_fExplodeTimeCount >= Main.s_Instance.m_fExplodeRunTime) {
				RPC_BeginMoveToCenter ();
				m_fMoveToCenterTimeCount = 0f;
				SetTestExplodeStatus (2);
			}
		} else if (m_nTestExplodeStatus == 2) {
			m_fMoveToCenterTimeCount += Time.deltaTime;
			if (m_fMoveToCenterTimeCount >= 16f ) {
				for (int i = 0; i < m_lstBalls.Count; i++) {
					Ball ball = m_lstBalls [i];
					ball.Local_SetPos ( GetPos() );
				}
				SetTestExplodeStatus (0);
			}
		}
		*/
	}

	bool CheckIfAllBallsEndMoveToCenter()
	{
		for (int i = 0; i < m_lstBalls.Count; i++) {
			Ball ball = m_lstBalls [i];
			if (ball.GetMovingToCenterStatus () != 2) {
				return false;
			}
		} // end for
		return true;
	}

	void SetTestExplodeStatus( int nStatus )
	{
		m_nTestExplodeStatus = nStatus;
	}

	public void SetTestPlayer( bool bVal )
	{
		m_bTestPlayer = bVal;
	}

	void SyncMoveInfo ()
	{
        if (Main.s_Instance == null) {
			return;
		}
		if (Main.s_Instance.m_MainPlayer == null) {
			return;
		}
        if (Main.s_Instance.m_MainPlayer.DoNotMove() || (!Main.s_Instance.m_MainPlayer.IsMoving()))
        {
            return;
        }

		Main.s_Instance.m_MainPlayer.SyncMoveInfo ( Main.s_Instance._playeraction._world_cursor.transform.position, Main.s_Instance._playeraction.m_vec2MoveInfo, Main.s_Instance._playeraction._ball_realtime_velocity_without_size); 
	}

	float m_nAdjustMoveInfoFrameCount = 0f;
	public const float c_nAdjustMoveInfoSyncInterval = 0.1f; // 多少秒校验一次
	byte[] _bytesAdjustMoveInfo = new byte[10240]; // 这个数据块的大小是可以优化的 poppin to do

    byte[] _bytestRelatedInfo = new byte[1024];
    void AdjustRelatedInfo()
    {
        StringManager.BeginPushData(_bytestRelatedInfo);
        int nNumOfBeingPushedThorn = m_lstBeingPushedThorn.Count;
        StringManager.PushData_Byte((byte)nNumOfBeingPushedThorn);
        for ( int i = m_lstBeingPushedThorn.Count - 1; i >= 0; i-- )
        {
            CMonster monster = m_lstBeingPushedThorn[i];
            CClassEditor.s_Instance.ReInsertMonsterTosBinaryList(monster);
            StringManager.PushData_Int((int)monster.GetGuid());
            StringManager.PushData_Float(monster.GetPos().x);
            StringManager.PushData_Float(monster.GetPos().y);

            if (PhotonNetwork.time - monster.GetFloatParam(0) >= 1f)
            {
                m_lstBeingPushedThorn.RemoveAt(i);
                continue;
            }

        } // end for 

        int nBlobSize = StringManager.GetBlobSize();
        byte[] bytes = StringManager.GetSuitableBlob(nBlobSize);
        Array.Copy(_bytestRelatedInfo, bytes, nBlobSize);

        photonView.RPC("RPC_AdjustRelatedInfo", PhotonTargets.Others, bytes);

    }

    [PunRPC]
    public void RPC_AdjustRelatedInfo(byte[] bytes)
    {
        if (!IsPlayerInitCompleted())
        {
            return;
        }

        if (photonView.isMine)
        { // 全量信息是同步给其它客户端的，自己没必要再去解析一遍
            return;
        }

        if (!IsInitCompleted())
        {
            return;
        }

        StringManager.BeginPopData(bytes);
        int nNumOfBeingPushedThorns = StringManager.PopData_Byte();
        for (int i = 0; i < nNumOfBeingPushedThorns; i++)
        {
            int nGuidOfThorn = StringManager.PopData_Int();
            CMonster monster = CClassEditor.s_Instance.GetMonsterByGuid((long)nGuidOfThorn);
            if ( monster == null )
            {
                Debug.LogError("RPC_AdjustRelatedInfo");
                continue;
            }
            float fPosX = StringManager.PopData_Float();
            float fPosY = StringManager.PopData_Float();
            vecTempPos.x = fPosX;
            vecTempPos.y = fPosY;
            vecTempPos.z = -monster.GetSize();
            monster.SetPos(vecTempPos);
            CClassEditor.s_Instance.ReInsertMonsterTosBinaryList(monster);
        }
    }

    // 整个帧同步的关键就是这个环节。只要牢牢的抓好这个同步流程，就能保障整个游戏正常运行下去。
    // 同时，最有可能出现性能问题的也是这个环节，后期作多用户同时在线外网测试的时候再优化
    void AdjustMoveInfo( int nDirect = 0, int nOtherOperate = 0 )
	{
        if (!photonView.isMine) {
			return;
		}

		if (!MapEditor.s_Instance.IsMapInitCompleted ()) {
			return;
		}

		if (  m_lstBalls.Count < Main.s_Instance.m_fMaxBallNumPerPlayer ) {
			return;
		}

		if (nDirect == 0) {
			m_nAdjustMoveInfoFrameCount += Time.deltaTime;
			if (m_nAdjustMoveInfoFrameCount < c_nAdjustMoveInfoSyncInterval) {
				return;
			}
			m_nAdjustMoveInfoFrameCount = 0f;
		}

		int nPointer = 2 * sizeof(int);
        int nBallNum = 0;// GetCurLiveBallsNum();

        
        BitConverter.GetBytes(PhotonNetwork.time).CopyTo(_bytesAdjustMoveInfo, nPointer);
        nPointer += sizeof(double);

        // 移动操作
        BitConverter.GetBytes(Main.s_Instance._playeraction._world_cursor.transform.position.x).CopyTo(_bytesAdjustMoveInfo, nPointer);
        nPointer += sizeof(float);
        BitConverter.GetBytes(Main.s_Instance._playeraction._world_cursor.transform.position.y).CopyTo(_bytesAdjustMoveInfo, nPointer);
        nPointer += sizeof(float);
        BitConverter.GetBytes(Main.s_Instance._playeraction._ball_realtime_velocity_without_size).CopyTo(_bytesAdjustMoveInfo, nPointer);
        nPointer += sizeof(float);
       


        // 双摇杆相关
        /*
        BitConverter.GetBytes(Main.s_Instance._playeraction.GetWorldCursorPos().x).CopyTo(_bytesAdjustMoveInfo, nPointer);
        nPointer += sizeof(float);
        BitConverter.GetBytes(Main.s_Instance._playeraction.GetWorldCursorPos().y).CopyTo(_bytesAdjustMoveInfo, nPointer);
        nPointer += sizeof(float);
        BitConverter.GetBytes(Main.s_Instance._playeraction.GetSpecialDirection().x).CopyTo(_bytesAdjustMoveInfo, nPointer);
        nPointer += sizeof(float);
        BitConverter.GetBytes(Main.s_Instance._playeraction.GetSpecialDirection().y).CopyTo(_bytesAdjustMoveInfo, nPointer);
        nPointer += sizeof(float);
        BitConverter.GetBytes(Main.s_Instance._playeraction.GetSpeed()).CopyTo(_bytesAdjustMoveInfo, nPointer);
        nPointer += sizeof(float);
        */

        BitConverter.GetBytes(GetBuffEffect_ShellTime()).CopyTo(_bytesAdjustMoveInfo, nPointer);
        nPointer += sizeof(float);
        BitConverter.GetBytes(GetDecreaseShellTime()).CopyTo(_bytesAdjustMoveInfo, nPointer);
        nPointer += sizeof(float);

        // other player的总体积同步过来，本客户端不应该去算其它客户端的总体积
        BitConverter.GetBytes(GetTotalVolume()).CopyTo(_bytesAdjustMoveInfo, nPointer);
        nPointer += sizeof(float);


        for (int i = 0; i < m_lstBalls.Count; i++) { 
			Ball ball = m_lstBalls [i];
			if (ball.IsDead ()) {
				continue;
			}

			if (ball.GetIndex () != i) {
				Debug.LogError ( "MainPlayer 从个的？？ball.GetIndex () != i" );
				continue;
			}

			if (ball.GetIndex () >= 64) {
				Debug.LogError ( "MainPlayer 从个的？？居然都超过64了" );
				continue;
			}

            if ( ball.DoNotSyncMoveInfo() )
            {
                continue;
            }

		
            float fPosX =/* byGroupId != CGroup.INVALID_GROUP_ID ? ball.GetLocalPosition().x :*/ ball.GetPos ().x;
            float fPosY =/* byGroupId != CGroup.INVALID_GROUP_ID ? ball.GetLocalPosition().y :*/ ball.GetPos ().y;

          

          //  BitConverter.GetBytes (byGroupId).CopyTo( _bytesAdjustMoveInfo, nPointer );
		//	nPointer += sizeof(byte);
			BitConverter.GetBytes (ball.GetIndex()).CopyTo( _bytesAdjustMoveInfo, nPointer );
			nPointer += sizeof(byte);
            float fSize = ball.GetSize();
            BitConverter.GetBytes(fSize).CopyTo(_bytesAdjustMoveInfo, nPointer);
			nPointer += sizeof(float);
            BitConverter.GetBytes (fPosX).CopyTo( _bytesAdjustMoveInfo, nPointer );
			nPointer += sizeof(float);
			BitConverter.GetBytes (fPosY).CopyTo( _bytesAdjustMoveInfo, nPointer );
			nPointer += sizeof(float);

            float fCurShellTime = ball.GetCurShellTime();
            float fTotalShellTime = ball.GetTotalShellTime();
            BitConverter.GetBytes((short)fCurShellTime).CopyTo(_bytesAdjustMoveInfo, nPointer);
            nPointer += sizeof(short);
            BitConverter.GetBytes((short)fTotalShellTime).CopyTo(_bytesAdjustMoveInfo, nPointer);
            nPointer += sizeof(short);


            nBallNum++;
        } // end for

    //    BitConverter.GetBytes(nGroupNum).CopyTo(_bytesAdjustMoveInfo,   0);
        BitConverter.GetBytes(nBallNum).CopyTo(_bytesAdjustMoveInfo, sizeof(int));

        short nDustNum = (short)m_lstSyncDust.Count;
        BitConverter.GetBytes(nDustNum).CopyTo(_bytesAdjustMoveInfo, nPointer);
        nPointer += sizeof(short);
        for ( int i = 0; i < m_lstSyncDust.Count; i++ )
        {
            CDust dust = m_lstSyncDust[i];
           // BitConverter.GetBytes(dust.GetId()).CopyTo(_bytesAdjustMoveInfo, nPointer);
           // nPointer += sizeof(short);
            BitConverter.GetBytes(dust.GetId()).CopyTo(_bytesAdjustMoveInfo, nPointer);
            nPointer += sizeof(short);
            BitConverter.GetBytes(dust.GetPos().x).CopyTo(_bytesAdjustMoveInfo, nPointer);
            nPointer += sizeof(float);
            BitConverter.GetBytes(dust.GetPos().y).CopyTo(_bytesAdjustMoveInfo, nPointer);
            nPointer += sizeof(float);
            BitConverter.GetBytes(dust.GetRotation()).CopyTo(_bytesAdjustMoveInfo, nPointer);
            nPointer += sizeof(float);
        }

        byte[] bytes = StringManager.GetSuitableBlob( nPointer );
        Array.Copy(_bytesAdjustMoveInfo, bytes, nPointer);

        photonView.RPC ( "RPC_AdjustMoveInfo", PhotonTargets.All, bytes/* _bytesAdjustMoveInfo*/ );

        ProcessOtherOperate ( nOtherOperate );


        AdjustRelatedInfo();

    }

    public static float s_worinigegui = 0f;
    static int[] s_nDelay = new int[7];
    static int s_nDelayCount = 0;
    static double[] s_fDelayValue = new double[7];
	[PunRPC]
	public void RPC_AdjustMoveInfo( byte[] bytes/*byte byDirect, byte byIndex, short sPosX, short sPosY*/ )
	{
        if ( !IsPlayerInitCompleted() )
        {
            return;
        }

      	if (photonView.isMine) { // 全量信息是同步给其它客户端的，自己没必要再去解析一遍
			return;
		}

		if (!IsInitCompleted ()) {
			return;
		}

		int nPointer = 0;
//        int nGroupNum = BitConverter.ToInt32(bytes, nPointer);
        nPointer += sizeof(int);
        int nBallsNum = BitConverter.ToInt32(bytes, nPointer);
        nPointer += sizeof(int);

        // poppin test
        double fTime = BitConverter.ToDouble(bytes, nPointer);
        nPointer += sizeof(double);

        
        float fWorldCursorPosX = BitConverter.ToSingle(bytes, nPointer);
        nPointer += sizeof(float);
        float fWorldCursorPosY = BitConverter.ToSingle(bytes, nPointer);
        nPointer += sizeof(float);
        float fPlayerSpeed = BitConverter.ToSingle(bytes, nPointer);
        nPointer += sizeof(float);
        Local_DoMove(fWorldCursorPosX, fWorldCursorPosY, 0, 0, fPlayerSpeed);
        

        double fTheDelay = PhotonNetwork.time - fTime;
        int nTotalNum = s_nDelay[0] + s_nDelay[1] + s_nDelay[2];

       // Debug.Log("fTheDelay= " + fTheDelay);

        // 延迟的处理
        if (fTheDelay < 0.05f) 
        {
            s_nDelay[0]++;
            s_fDelayValue[0] += fTheDelay;
        //    Debug.Log( GetOwnerId() +  ": 小于50毫秒:" + ( s_fDelayValue[0] / (float)s_nDelay[0] ) + " _" + ( (float)s_nDelay[0] / (float)nTotalNum));
        }else if (fTheDelay < 0.1f)
        {
            s_nDelay[1]++;
            s_fDelayValue[1] += fTheDelay;
        //    Debug.Log(GetOwnerId() + ": 小于100毫秒:" + (s_fDelayValue[1] / (float)s_nDelay[1]) + " _" + ((float)s_nDelay[1] / (float)nTotalNum));

        }
           else if (fTheDelay< 0.15f)
        {
            s_nDelay[2]++;
            s_fDelayValue[2] += fTheDelay;
          //  Debug.Log(GetOwnerId() + ": 小于150毫秒:" + (s_fDelayValue[2] / (float) s_nDelay[2]) + " _" + ((float) s_nDelay[2] / (float) nTotalNum));
       }
        else
        {
            s_nDelay[3]++;
            s_fDelayValue[3] += fTheDelay;
           // Debug.Log(GetOwnerId() + ": Shit:" + (s_fDelayValue[3] / (float)s_nDelay[3]) + " _" + ((float)s_nDelay[3] / (float)nTotalNum));
        }
        s_nDelayCount++;

        //Debug.Log(  (float)s_nDelay[0] / (float)s_nDelayCount + " ,  " + (float)s_nDelay[1] / (float)s_nDelayCount + " , " + (float)s_nDelay[2] / (float)s_nDelayCount);

        /*
        float fWorldCursorPosX = BitConverter.ToSingle(bytes, nPointer);
        nPointer += sizeof(float);
        float fWorldCursorPosY = BitConverter.ToSingle(bytes, nPointer);
        nPointer += sizeof(float);
        float fSpecailDiractionX = BitConverter.ToSingle(bytes, nPointer);
        nPointer += sizeof(float);
        float fSpecailDiractionY = BitConverter.ToSingle(bytes, nPointer);
        nPointer += sizeof(float);
        float fSpeed = BitConverter.ToSingle(bytes, nPointer); 
        nPointer += sizeof(float);
        Local_DoMove(fWorldCursorPosX, fWorldCursorPosY, fSpecailDiractionX, fSpecailDiractionY, fSpeed);
        */

        bool bSyncPos = true;
        if (fTheDelay > Main.s_Instance.m_fAdjustPosDelayThreshold)
        {
            bSyncPos  = false;
        }

        float fShellTime_BuffEffect = BitConverter.ToSingle(bytes, nPointer);
        nPointer += sizeof(float);
        float fShellTime_ItemEffect = BitConverter.ToSingle(bytes, nPointer);
        nPointer += sizeof(float);
        float fTotalVolume = BitConverter.ToSingle(bytes, nPointer);
        nPointer += sizeof(float);


        SetBuffEffect_ShellTime(fShellTime_BuffEffect);
        SetDecreaseShellTime(fShellTime_ItemEffect);
        SetTotalEatArea(fTotalVolume);

 
        for (int i = 0; i < nBallsNum; i++) { 
			int nIndex = (byte)BitConverter.ToChar(bytes, nPointer);
			nPointer += sizeof (byte);

            float fSize = BitConverter.ToSingle(bytes, nPointer);
            nPointer += sizeof(float);

            Ball ball = null;
            if ( nIndex < 0 || nIndex >= m_lstBalls.Count)
            {
                Debug.LogError("从个的？？nIndex < 0 || nIndex >= m_lstBalls.Count: " + nIndex + "_" + nBallsNum);
            }
            else
            {
                ball = m_lstBalls[nIndex];
            }


            if (ball)
            {
                if (ball.GetIndex() != nIndex)
                {
                    Debug.LogError("从个的？？ball.GetIndex () != nIndex :" + ball.GetIndex() + "_" + nIndex);
                }
            }				
			float fPosX = BitConverter.ToSingle(bytes, nPointer);
            s_worinigegui = fPosX;
            nPointer += sizeof(float);
			float fPosY = BitConverter.ToSingle(bytes, nPointer);
			nPointer += sizeof(float);

            float fCurShellTime = BitConverter.ToInt16(bytes, nPointer);
            nPointer += sizeof(short);
            float fTotalShellTime = BitConverter.ToInt16(bytes, nPointer);
            nPointer += sizeof(short);

            // 解决网络丢包问题的关键之关键就在这一步
            if (ball)
            {
                // 先判断它是不是在视野范围内（能同步过来的肯定不是dead的）
                float fRadius = Main.s_Instance.m_fScale2Radius * fSize;
                float fHalfRadius = fRadius / 2f;
                float fLeft = fPosX - fHalfRadius;
                vecTempPos.x = fPosX;
                vecTempPos.y = fPosY;
                vecTempPos.z = -fSize;
                ball.SetPos(vecTempPos);
                ball.SetSize(fSize);
                
                if ( Main.s_Instance.IsBallInCamView(fLeft, fPosX, fPosY ) )
                {
                    ball.SetActive( true );

                    if ( ball.IsDead() )
                    {
                        ball.SetDead(false);
                    }

                    ball.SetSize(fSize);
                   // ball.SetAdjustInfo(fPosX, fPosY, 1);


                    if (fTotalShellTime > 0 && !ball.HaveShell() )
                    {
                        ball.Local_SetShellInfo(fTotalShellTime, fCurShellTime);
                        ball.Local_BeginShell();
                    }
                    if (fTotalShellTime <= 0 && ball.HaveShell())
                    {
                        ball.Local_EndShell();
                    }
                }
                else
                {
                    ball.SetActive(false);
                }
                
            }


        } // end for

	    int nDustNum = BitConverter.ToInt16(bytes, nPointer);
        nPointer += sizeof(short);
        for ( int i = 0; i < nDustNum; i++ )
        {
            int nDustdId = BitConverter.ToInt16(bytes, nPointer);
            nPointer += sizeof(short);
            vecTempPos.x =  BitConverter.ToSingle(bytes, nPointer);
            nPointer += sizeof(float);
            vecTempPos.y = BitConverter.ToSingle(bytes, nPointer);
            nPointer += sizeof(float);
            vecTempPos.z = 0f;
            float fRotation = BitConverter.ToSingle(bytes, nPointer);
            nPointer += sizeof(float);
            CStarDust.s_Instance.SetDustPos(nDustdId, vecTempPos);
            CStarDust.s_Instance.SetDustRotation(nDustdId, fRotation);
        }
    }

    byte[] _bytesPaiHangBang = new byte[64];
    float m_fPaiHangBangSyncTimeCount = 0f;
    void SyncPaiHangBangInfo()
    {
        if (!photonView.isMine)
        {
            return;
        }

        if (!MapEditor.s_Instance.IsMapInitCompleted())
        {
            return;
        }

        m_fPaiHangBangSyncTimeCount += Time.deltaTime;
        if (m_fPaiHangBangSyncTimeCount < 15f) // 暂定15秒更新一次排行榜的“非重要信息”(而重要信息是实时更新，包括：等级、当前总体积)
        {
            return;
        }
        m_fPaiHangBangSyncTimeCount = 0f;

        StringManager.BeginPushData( _bytesPaiHangBang );
        StringManager.PushData_Short((short)GetOwnerId() );
        StringManager.PushData_Short((short)GetEatThornNum());
        StringManager.PushData_Short( (short)GetKillCount() );
        StringManager.PushData_Short((short)GetBeKilledCount());
        StringManager.PushData_Short((short)CGrowSystem.s_Instance.GetAssistAttackCount());
        StringManager.PushData_Byte((byte)CItemSystem.s_Instance.GetCurNum(0));
        StringManager.PushData_Byte((byte)CItemSystem.s_Instance.GetCurNum(1));
        StringManager.PushData_Byte((byte)CItemSystem.s_Instance.GetCurNum(2));
        StringManager.PushData_Byte((byte)CItemSystem.s_Instance.GetCurNum(3));
        int nBlobLength = StringManager.GetBlobSize();
        byte[] bytes = StringManager.GetSuitableBlob(nBlobLength);
        Array.Copy(_bytesPaiHangBang, bytes, nBlobLength);
        photonView.RPC("RPC_SyncPaiHangBangInfo", PhotonTargets.All, bytes);     
    }

    [PunRPC]
    public void RPC_SyncPaiHangBangInfo( byte[] bytes )
    {
        StringManager.BeginPopData(bytes);
        int nPlayerId = StringManager.PopData_Short();
        int nExplodeThornNum = StringManager.PopData_Short();
        int nKillCount = StringManager.PopData_Short();
        int nBeingKilledCount = StringManager.PopData_Short();
        int nAssistCount = StringManager.PopData_Short();
        int nItem0 = StringManager.PopData_Byte();
        int nItem1 = StringManager.PopData_Byte();
        int nItem2 = StringManager.PopData_Byte();
        int nItem3 = StringManager.PopData_Byte();
        SetEatThornNum(nExplodeThornNum);
        SetKillCount(nKillCount);
        SetBeKilledCount(nBeingKilledCount);
        SetAssistAttackCount(nAssistCount);
        SetItemNum(0, nItem0);
        SetItemNum(1, nItem1);
        SetItemNum(2, nItem2);
        SetItemNum(3, nItem3);
    }





    public void ProcessOtherOperate ( int nOtherOperate )
	{
		switch (nOtherOperate) {
		case 1:
			{
				EndEject ();
			}
			break;
		}
	}

	public void EndEject ()
	{
		for (int i = 0; i < m_lstBalls.Count; i++) {
			Ball ball = m_lstBalls [i];
			if (ball.IsDead ()) {
				continue;
			}
			ball.EndEject ();
		}
	}

	public void EatBall( int nPlayerPhotonID, int nBallIndex )
	{

	}

	[PunRPC]
	public void RPC_BeEaten( int nBallIndex, int nEaterOwnerId )
	{
		Ball ball = m_lstBalls[nBallIndex];

		ball.SetEaten ( true );
		if ( photonView.isMine) {
			DestroyBall ( nBallIndex, nEaterOwnerId);
		}
	}

    public float GetTotalEatArea()
    {
        return m_fTotalEatArea;
    }

    // 衰减统一由Player来操盘。之前的机制是每个球球自己去做衰减
    public float GetEatVolume()
    {
        if (m_fTotalArea == 0)
        {
            return 0;
        }
        return (m_fTotalArea - GetBornBaseTiJi());
    }

    public void SetTotalEatArea( float val )
    {
        m_fTotalEatArea = val;
    }

    public void IncKillCount()
    {
        m_nKillCount++;
        if ( IsMainPlayer() )
        {
            CGrowSystem.s_Instance.SetKillCount(m_nKillCount);
        }
    }

  

    public void IncBeKilledCount()
    {
        m_nBeKilled++;
        if (IsMainPlayer())
        {
            CGrowSystem.s_Instance.SetBeingKilledCount(m_nBeKilled);
        }
    }


    [PunRPC]
    public void RPC_EatShengFuPanDingMonster()
    {
       // Main.s_Instance.m_ShengFuPanDingMonster.SetDead( true );
        Main.s_Instance. GameOver( GetPlayerName() );
    }

    [PunRPC]
	public void RPC_EatSucceed( int nBallIndex,  float fNewSize, float fTotalEatArea, float fExplodeChildVolume, string szBecomeThornConfigId )
	{
        // 吃球操作可以由吃球者和被吃者发起。但是不能重复执行。
		if (!photonView.isMine) { // 必须由吃球者的MainPlayer来执行吃球操作，否则时序会混乱
			return;
		}

        Ball ball = GetBallByIndex(nBallIndex);
        ball.Local_SetSize(fNewSize);


        if ( fExplodeChildVolume > 0 )
        {
            CMonsterEditor.sThornConfig config = CMonsterEditor.s_Instance.GetMonsterConfigById(szBecomeThornConfigId);
            ball.Explode_CausedBySkill(config, fExplodeChildVolume);
        }
    }

    public void DestroyMonster(CMonster monster)
    {
        /*
        switch (monster.GetMonsterType())
        {
            case CMonster.eMonsterType.scene: // 场景球（相当于ARPG游戏中的野怪）
                {
                    DestroyMonster_Scene(monster);
                }
                break;
            case CMonster.eMonsterType.spit_spore:
            case CMonster.eMonsterType.spray:
                {
                    ResourceManager.s_Instance.RecycleMonster(monster); // 这一类不重生，直接销毁就好了
                }
                break;
        }
        */

        monster.SetDead(true);

        // 吃场景上的野怪（豆子、刺 等）一律同步。 但是要做一个同步队列，打包同步，不要实时同步
        if (IsMainPlayer())
        {
            if (monster.GetMonsterBuildingType() == CMonsterEditor.eMonsterBuildingType.spore)
            {
                m_lstEatSporeSyncQueue.Add( monster );
            }
            else
            {
                m_lstEatThornSyncQueue.Add(monster);
            }
        }
    }

    List<CMonster> m_lstEatThornSyncQueue = new List<CMonster>(); // 其余的刺和豆子
    List<CMonster> m_lstEatSporeSyncQueue = new List<CMonster>(); // 孢子

    byte[] _bytesEatThornSync = new byte[10240];
    float m_fEatThornSyncCount = 0f;
    void EatThornSyncQueueLoop()
    {
        m_fEatThornSyncCount += Time.deltaTime;
        if ( m_fEatThornSyncCount < 1f )
        {
            return;
        }
        m_fEatThornSyncCount = 0f;

        StringManager.BeginPushData(_bytesEatThornSync);
        StringManager.PushData_Short((short)m_lstEatThornSyncQueue.Count);
        StringManager.PushData_Float(Main.GetTime());
        for ( int i = 0; i < m_lstEatThornSyncQueue.Count; i++ )
        {
            StringManager.PushData_Int((int)m_lstEatThornSyncQueue[i].GetGuid());
        }

        StringManager.PushData_Short((short)m_lstEatSporeSyncQueue.Count);
        for (int i = 0; i < m_lstEatSporeSyncQueue.Count; i++)
        {
            StringManager.Push_Uint((uint)m_lstEatSporeSyncQueue[i].GetGuid());
            StringManager.Push_Uint((uint)m_lstEatSporeSyncQueue[i].GetPlayer().GetOwnerId());
        }

        m_lstEatThornSyncQueue.Clear();
        m_lstEatSporeSyncQueue.Clear();

        int nBlobSize = StringManager.GetBlobSize();
        byte[] bytes = StringManager.GetSuitableBlob(nBlobSize);
        Array.Copy(_bytesEatThornSync, bytes, nBlobSize);

        photonView.RPC("RPC_EatThornSync", PhotonTargets.All, bytes );


    }

    [PunRPC]
    public void RPC_EatThornSync( byte[] bytes )  
    {
        StringManager.BeginPopData(bytes);
        int num = StringManager.PopData_Short();
        float fTime = StringManager.PopData_Float();
        for ( int i = 0; i < num; i++ )
        {
            long lGuid = StringManager.PopData_Int();
            CClassEditor.s_Instance.RemoveThorn(lGuid, fTime);
        }

        num = StringManager.PopData_Short();
        for (int i = 0; i < num; i++)
        {
            uint uPlayerSporeGuid = StringManager.PopData_Uint();
            uint uPlayerId = StringManager.PopData_Uint();
            CClassEditor.s_Instance.RemoveSpore(uPlayerSporeGuid, uPlayerId);
        }
    }
    
    public void DestroyBall( int nBallIndex, int nEaterOwnerId = 0 ) // 必须由MainPlayer来执行销毁球操作，否则时序会混乱
	{
		if (!photonView.isMine) { // 必须由MainPlayer来执行销毁球操作，否则时序会混乱
			return;
		}

		photonView.RPC ( "RPC_DestroyBall", PhotonTargets.All,  nBallIndex,  nEaterOwnerId);
	}

	[PunRPC]
	public void RPC_DestroyBall( int nBallIndex, int nEaterOwnerId = 0 )
	{
		Ball ball = m_lstBalls[nBallIndex];
		ball.SetDead ( true, nEaterOwnerId);
        m_nEaterId = nEaterOwnerId;
        if (GetOwnerId() == ball._Player.GetOwnerId())
        {
			if( CheckIfPlayerDead(nEaterOwnerId))// 判断下整个玩家死没有
			{
                int nTheDeadMeatId = GetOwnerId();
                CalculateTheAssistAttackInfo(nEaterOwnerId, nTheDeadMeatId);
                ProcessKillExp( nEaterOwnerId, nTheDeadMeatId);
			}
        }

        if (nEaterOwnerId > 0)
        {
            RecordTheAssistAttackTime(nEaterOwnerId);
        }
    }

    Dictionary<int, float> m_dicAssistAttack = new Dictionary<int, float>();
    public void RecordTheAssistAttackTime(int nEaterOwnerId)
    {
        if (nEaterOwnerId == 0)
        {
            return;
        }

        m_dicAssistAttack[nEaterOwnerId] = Main.GetTime();
    }

    List<int> m_lstValidAssitPlayerId = new List<int>();
    public void CalculateTheAssistAttackInfo( int nKillerId, int nTheDeadMeatId)
    {
        m_lstValidAssitPlayerId.Clear();
        foreach ( KeyValuePair<int, float>pair in m_dicAssistAttack)
        {
            if (Main.GetTime() - pair.Value  > 30f) 
            {
                continue;
            }
            
            // 击杀者就不要算在助攻里面了
            if (pair.Key == nKillerId)
            {
                continue;
            }

            // 自己吃自己的球不算助攻
            if (nTheDeadMeatId == pair.Key)
            {
                continue;
            }

            Player player = CPlayerManager.s_Instance.GetPlayer( pair.Key );

            m_lstValidAssitPlayerId.Add(pair.Key);
            player.IncAssistAttackCount();
        }
    }

    public List<int> GetAssistPlayerIdList()
    {
        return m_lstValidAssitPlayerId;
    }


    public void IncAssistAttackCount()
    {
        m_IncAssistAttackCount++;
        if ( IsMainPlayer() )
        {
            CGrowSystem.s_Instance.SetAssistAttackCount(m_IncAssistAttackCount);
        }
    }

    public void ProcessPlayerDead()
    {
        if ( !IsMainPlayer() )
        {
            return;
        }

        float fBaseVolumeByItem = GetBaseVolumeByItem();
        fBaseVolumeByItem *= (1 + Main.s_Instance.m_fBaseVolumeyByItemDecreasedPercentWhenDead  );
        Main.s_Instance.g_SystemMsg.SetContent( "死亡导致道具加成的基础体积衰减了：" + (Main.s_Instance.m_fBaseVolumeyByItemDecreasedPercentWhenDead * 100).ToString( "f0" ) +  "%" );
        SetBaseVolumeByItem(fBaseVolumeByItem);
    }

    float m_fExplodeChildNumBuffEffect = 1f;
	float m_fExplodeMotherLeftBuffEffect = 1f;
	public float GetExplodeChildNumBuffEffect()
	{
		return m_fExplodeChildNumBuffEffect;
	}

	public float GetExplodeMotherLeftBuffEffect()
	{
		return m_fExplodeMotherLeftBuffEffect;
	}

	public void UpdateBuffEffect_Explode()
	{
		m_fExplodeChildNumBuffEffect = 1f;
		m_fExplodeMotherLeftBuffEffect = 0;
		CBuffEditor.sBuffConfig buff_config;
		for (int i = 0; i < m_lstBuff.Count; i++) {
			CBuff buff = m_lstBuff [i];
			buff_config = buff.GetConfig ();
			if (buff_config.func == (int)CBuffEditor.eBuffFunc.explode_child_num) {
				m_fExplodeChildNumBuffEffect *= ( 1f + buff_config.aryValues [0] );
			}
			if (buff_config.func == (int)CBuffEditor.eBuffFunc.explode_mother_left_percent) {
				m_fExplodeMotherLeftBuffEffect += buff_config.aryValues [0];
			} 
		}
	}

	public void ExplodeBall( int nBallIndex, string szThornConfigId, int nChildNum, float fChildSize, float fMotherLeftSize )
	{
            photonView.RPC ("RPC_WoriniGeGui", PhotonTargets.All, nBallIndex, szThornConfigId, nChildNum, fChildSize, fMotherLeftSize, PhotonNetwork.time);

            m_nEatThornNum++;
            if (IsMainPlayer())
            {
                CGrowSystem.s_Instance.SetEatThornNum(m_nEatThornNum);
            }
	}

	[PunRPC]
	public void RPC_WoriniGeGui( int nBallIndex, string szThornConfigId, int nChildNum, float fChildSize, float fMotherLeftSize, double dOccurTime )
	{

        if (  !IsPlayerInitCompleted())
        {
            return;  
        }
        Ball ball = m_lstBalls[nBallIndex];
		ball.DoExplode (szThornConfigId, nChildNum, fChildSize, fMotherLeftSize, dOccurTime );
	}

	int m_nCurSkinIndex = 0;
  
	public void ChangeSkin()
	{
		if (photonView.isMine) {
			m_nCurSkinIndex++;
			if (m_nCurSkinIndex >= Main.s_Instance.GetSpriteArrayLength()) {
				m_nCurSkinIndex = 0;
			}
			photonView.RPC ("RPC_ChangeSkin", PhotonTargets.All, m_nCurSkinIndex);
		}
	}


	[PunRPC]
	public void RPC_ChangeSkin( int nIndex )
	{
		SetSkin( nIndex );
	}


    Sprite m_sprCurAvatar = null;
	public void SetSkin( int nIndex )
	{
        m_nCurSkinIndex = nIndex;
        Sprite sprite = Main.s_Instance.GetSpriteBySpriteId ( nIndex );
        m_sprCurAvatar = sprite;

        for (int i = 0; i < m_lstBalls.Count; i++) {
			Ball ball = m_lstBalls [i];
			ball.SetSprite ( sprite );
		}
	}

	public void DoReborn()
	{
        vecTempPos = MapEditor.s_Instance.RandomGetRebornPos();
        photonView.RPC ( "RPC_DoReborn", PhotonTargets.All, vecTempPos.x, vecTempPos.y);
	}

	[PunRPC]
	public void RPC_DoReborn(float fPosX, float fPosY )
	{
        vecTempPos.x = fPosX;
        vecTempPos.y = fPosY;

        Ball ball = ReuseOneBall();
		if (ball == null) {
			return;
		}

        BeginProtect ();

        // 重生
        ball.SetSize ( CyberTreeMath.Volume2Scale(GetBornBaseTiJi() ) );
        vecTempPos.z = -ball.GetSize();
        ball.SetPos(vecTempPos);
        SetRebornCompleted ( true );

        m_bDead = false;

        ball.CheckClassStatus();

        RebornInit();
        }

    public void RebornInit()
    {
        m_dicAssistAttack.Clear();
    }

    public float GetBornBaseTiJi()
    {
        // 目前基础体积是三个部分组成：1、最原始配置的基础体积；  2、道具加成的基础体积(貌似目前策划层面 废弃了这种道具)； 3、随等级增加的基础体积
        m_fBaseArea = Main.s_Instance.m_fBornMinTiJi + m_BaseData.fBaseSize + m_fBaseVolumeByLevel;
        return m_fBaseArea;
    }

	float m_SyncPosAfterEjectTotalTime = 0f;
	public void BeginSyncPosAfterEject( float fTotalTime )
	{
		m_SyncPosAfterEjectTotalTime = fTotalTime;
	}
    
	public void BeginShell ( )
	{
        for (int i = 0; i < m_lstBalls.Count; i++)
        {
            Ball ball = m_lstBalls[i];
            ball.Local_BeginShell();
        }
    }

    public void EndShell()
    {
        for (int i = 0; i < m_lstBalls.Count; i++)
        {
            Ball ball = m_lstBalls[i];
            ball.Local_EndShell();
        }
    }

    //// 技能：魔盾
    float m_fSkillMpCost_MagicShield = 0f;
    float m_fSkillDuration_MagicShield = 0f;
    bool m_bMagicShielding = false;


    public bool CheckIfCanCastSkill_MagicShield()
    {
        if (GetMP() < m_fSkillMpCost_MagicShield)
        {
            Main.s_Instance.g_SystemMsg.SetContent(CMsgSystem.s_Instance.GetMsgContentByType(CMsgSystem.eSysMsgType.mp_not_enough));
            return false;
        }
        return true;
    }

    public bool CastSkill_MagicShield(float fMpCost, float fDuration)
    {
        m_fSkillMpCost_MagicShield = fMpCost;
        m_fSkillDuration_MagicShield = fDuration;

        if ( !CheckIfCanCastSkill_MagicShield())
        {
            return false;
        }

        // 耗蓝
        SetMP(GetMP() - m_fSkillMpCost_MagicShield);

        photonView.RPC("RPC_CastSkill_MagicShield", PhotonTargets.All,  fDuration);
        
        return true;
    }

    [PunRPC]
    public void RPC_CastSkill_MagicShield( float fDuration)
    {
        m_fSkillDuration_MagicShield = fDuration;
        m_bMagicShielding = true;
        m_fSkillLoopCount_MagicShield = 0;

        for (int i = 0; i < m_lstBalls.Count; i++)
        {
            Ball ball = m_lstBalls[i];
            ball.SetMagicShielding(true);
        }
    }

    public bool IsMagicShielding()
    {
        return m_bMagicShielding;
    }

    float m_fSkillLoopCount_MagicShield = 0;
    void SkillLoop_MagicShield()
    {
        if (!m_bMagicShielding)
        {
            return;
        }
        m_fSkillLoopCount_MagicShield += Time.deltaTime;
        if (m_fSkillLoopCount_MagicShield >= m_fSkillDuration_MagicShield)
        {
            EndSkill_MagicShield();
        }

    }

    void EndSkill_MagicShield()
    {
        m_bMagicShielding = false;

        for (int i = 0; i < m_lstBalls.Count; i++)
        {
            Ball ball = m_lstBalls[i];
            ball.SetMagicShielding(false);
        }
    }

    //// end  技能：魔盾

    /// <summary>
    /// / 技能：湮灭
    /// </summary>
    /// <returns></returns>

    float m_fSkillMpCost_Annihilate = 0f;
    float m_fSkillPercent_Annihilate = 0f;
    float m_fSkillDuration_Annihilate = 0f;
    bool m_bAnnihilating = false;

    public bool CastSkill_Annihilate(float fMpCost, float fAnnihilatePercent, float fDuration)
    {
        m_fSkillMpCost_Annihilate = fMpCost;
        m_fSkillPercent_Annihilate = fAnnihilatePercent;
        m_fSkillDuration_Annihilate = fDuration;

        if ( !CheckIfCanCastSkill_Annihilate())
        {
            return false;
        }

        // 耗蓝
        SetMP(GetMP() - m_fSkillMpCost_Annihilate);

        photonView.RPC("RPC_CastSkill_Annihilate", PhotonTargets.All, fAnnihilatePercent, fDuration);

        return true;
    }

    [PunRPC]
    public void RPC_CastSkill_Annihilate(float fAnnihilatePercent, float fDuration)
    {
        m_fSkillPercent_Annihilate = fAnnihilatePercent;
        m_fSkillDuration_Annihilate = fDuration;
        m_bAnnihilating = true;
        m_fSkillLoopCount_Annihilate = 0;

        for (int i = 0; i < m_lstBalls.Count; i++)
        {
            Ball ball = m_lstBalls[i];
            ball.SetAnnihilate(true);
        }
    }

    public bool IsAnnihilaing()
    {
        return m_bAnnihilating;
    }

    float m_fSkillLoopCount_Annihilate = 0f;
    void SkillLoop_Annihilate()
    {
        if ( !m_bAnnihilating)
        {
            return;
        }
        m_fSkillLoopCount_Annihilate += Time.deltaTime;
        if (m_fSkillLoopCount_Annihilate >= m_fSkillDuration_Annihilate)
        {
            EndSkill_Annihilate();
        }

    }

    void EndSkill_Annihilate()
    {
        m_bAnnihilating = false;

        for (int i = 0; i < m_lstBalls.Count; i++)
        {
            Ball ball = m_lstBalls[i];
            ball.SetAnnihilate(false);
        }
    }

    public  float GetAnnihilatePercent()
    {
        return m_fSkillPercent_Annihilate;
    }

    public bool CheckIfCanCastSkill_Annihilate()
    {
        if (GetMP() < m_fSkillMpCost_Annihilate)
        {
            Main.s_Instance.g_SystemMsg.SetContent(CMsgSystem.s_Instance.GetMsgContentByType(CMsgSystem.eSysMsgType.mp_not_enough));
            return false;
        }
        return true;
    }

    //// end 技能：湮灭

    //// 技能：金壳
    float m_fSkillMpCost_Gold = 0;
    float m_fSkillGold_SpeedAddPercent = 0;
    float m_fGold_Duration = 0;
    bool m_bGold = false;
    public bool CheckIfCanCastSkillGold()
    {
        if (GetMP() < m_fSkillMpCost_Gold)
        {
            Main.s_Instance.g_SystemMsg.SetContent(CMsgSystem.s_Instance.GetMsgContentByType(CMsgSystem.eSysMsgType.mp_not_enough));
            return false;
        }


        return true;
    }


    public bool CastSkill_Gold(float fMpCost, float fDuration, float fSpeedAddPercent)
    {
        m_fSkillMpCost_Gold = fMpCost;
        m_fGold_Duration = fDuration;


        if (!CheckIfCanCastSkillGold())
        {
            return false;
        }


        SetMP(GetMP() - m_fSkillMpCost_Gold);

        photonView.RPC("RPC_CastSkill_Gold", PhotonTargets.All, fDuration, fSpeedAddPercent);

        return true;
    }

    [PunRPC]
    public void RPC_CastSkill_Gold(float fDuration, float fSpeedAddPercent)
    {
        m_fSkillGold_SpeedAddPercent = fSpeedAddPercent;
        m_fGold_Duration = fDuration;
        m_bGold = true;
        m_fGoldLoopCount = 0f;
        for ( int i = 0; i < m_lstBalls.Count; i++ )
        {
            Ball ball = m_lstBalls[i];
            ball.BeginEffect( CEffect.eEffectType.gold_shell );
        }
    }

    public float GetSkillGoldSpeedAddPercent()
    {
        return m_fSkillGold_SpeedAddPercent;
    }

    float m_fGoldLoopCount = 0f;
    void SkillLoop_Gold()
    {
        if ( !m_bGold )
        {
            return;
        }

        m_fGoldLoopCount += Time.deltaTime;
        if (m_fGoldLoopCount >= m_fGold_Duration)
        {
            EndGold();
        }
    }

    void EndGold()
    {
        m_bGold = false;
        for (int i = 0; i < m_lstBalls.Count; i++)
        {
            Ball ball = m_lstBalls[i];
            ball.EndEffect(CEffect.eEffectType.gold_shell);
        }
    }

    public bool IsGold()
    {
        return m_bGold;
    }


    //// end 技能：金壳

    /// 技能：狂暴
    float m_fSkillMpCost_Henzy = 0;
    float m_fHenzy_Duration = 0;
    float m_fHenzy_SpeedChangePercent = 0;
    float m_fHenzy_WColdDown = 0;
    float m_fHenzy_EColdDown = 0;
    bool m_bHenzy = false;
    public bool CheckIfCanCastSkillHenzy()
    {
        if (GetMP() < m_fSkillMpCost_Henzy)
        {
            Main.s_Instance.g_SystemMsg.SetContent(CMsgSystem.s_Instance.GetMsgContentByType(CMsgSystem.eSysMsgType.mp_not_enough));
            return false;
        }


        return true;
    }

    public bool CastSkill_Henzy( float fCostMp, float fDuration, float fSpeedChangePercent, float fWColdDown, float fEColdDown )
    {
        m_fSkillMpCost_Henzy = fCostMp;
        m_fHenzy_Duration = fDuration;
        m_fHenzy_SpeedChangePercent = fSpeedChangePercent;
        m_fHenzy_WColdDown = fWColdDown;
        m_fHenzy_EColdDown = fEColdDown;

        if ( !CheckIfCanCastSkillHenzy())
        {
            return false;
        }

        SetMP(GetMP() - m_fSkillMpCost_MergeAll);

        photonView.RPC("RPC_CastSkill_Henzy", PhotonTargets.All, fDuration, fSpeedChangePercent, fWColdDown, fEColdDown);
        
        return true;
    }

    [PunRPC]
    public void RPC_CastSkill_Henzy(float fDuration, float fSpeedChangePercent, float fWColdDown, float fEColdDown)
    {
        m_fHenzy_Duration = fDuration;
        m_fHenzy_SpeedChangePercent = fSpeedChangePercent;
        m_fHenzy_WColdDown = fWColdDown;
        m_fHenzy_EColdDown = fEColdDown;
        m_fHenzyLoopCount = 0;
        m_bHenzy = true;

        for ( int i = 0; i < m_lstBalls.Count; i++ )
        {
            Ball ball = m_lstBalls[i];
            ball._effectHenzy.gameObject.SetActive( true );
        }
    }

    float m_fHenzyLoopCount = 0f;
    void HenzyLoop()
    {
        if ( !m_bHenzy )
        {
            return;
        }

        m_fHenzyLoopCount += Time.deltaTime;
        if (m_fHenzyLoopCount >= m_fHenzy_Duration)
        {
            EndHenzy();
        }
    }

   void EndHenzy()
    {
        m_bHenzy = false;
        for (int i = 0; i < m_lstBalls.Count; i++)
        {
            Ball ball = m_lstBalls[i];
            ball._effectHenzy.gameObject.SetActive(false);
        }
    }

    public bool IsHenzy()
    {
        return m_bHenzy;
    }

    public float GetHenzySpeedChangePercent()
    {
        return m_fHenzy_SpeedChangePercent;
    }

    public float GetHenzyWColdDown()
    {
        return m_fHenzy_WColdDown;
    }

    public float GetHenzyEColdDown()
    {
        return m_fHenzy_EColdDown;
    }

    /// end 技能：狂暴 


    /// <summary>
    /// / 技能：秒合
    /// </summary>
    /// <returns></returns>
    float m_fSkillMpCost_MergeAll = 0f;
    float m_fSkillQianYao_MergeAll = 0f;
    public bool CheckIfCanCastSkill_MergeAll()
    {
        if (GetMP() < m_fSkillMpCost_MergeAll)
        {
            Main.s_Instance.g_SystemMsg.SetContent(CMsgSystem.s_Instance.GetMsgContentByType(CMsgSystem.eSysMsgType.mp_not_enough));
            return false;
        }

        return true;
    }

    public bool CastSkill_MergeAll(float fMpCost, float fQianYao)
    {
        m_fSkillMpCost_MergeAll = fMpCost;
        m_fSkillQianYao_MergeAll = fQianYao;

        if ( !CheckIfCanCastSkill_MergeAll())
        {
            return false;
        }

        // 耗蓝
        SetMP(GetMP() - m_fSkillMpCost_MergeAll);

        CSkillSystem.s_Instance.ShowProgressBar(CSkillSystem.eSkillSysProgressBarType.i_merge_all);

        photonView.RPC("RPC_CastSkill_MergeAll", PhotonTargets.All, fQianYao);
        
        return true;
    }

    [PunRPC]
    public void RPC_CastSkill_MergeAll( float fQianYao)
    {
        m_fSkillQianYao_MergeAll = fQianYao;
        m_bMiaoHeQianYao = true;
        m_fSkillQianYaoCount_MergeAll = 0f;
    }

    bool m_bMiaoHeQianYao = false;
    float m_fSkillQianYaoCount_MergeAll = 0f;
    public bool IsMiaoHeQianYao()
    {
        return m_bMiaoHeQianYao;
    }

    void SkillLoop_MiaoHe()
    {
        if ( !m_bMiaoHeQianYao)
        {
            return;
        }
        CSkillSystem.s_Instance.UpdateProgressData(m_fSkillQianYaoCount_MergeAll, m_fSkillQianYao_MergeAll);
        m_fSkillQianYaoCount_MergeAll += Time.deltaTime;
        if (m_fSkillQianYaoCount_MergeAll >= m_fSkillQianYao_MergeAll)
        {
            DoMergeAll();
            m_bMiaoHeQianYao = false;
            CSkillSystem.s_Instance.HideProgressBar();
        }
    }
    
    public bool IsMiaoHeQiaoYao()
    {
        return m_bMiaoHeQianYao;
    }

    void DoMergeAll()
    {
        RPC_MergeAll();
    }













    //// 技能：潜行
    public bool CheckIfCanCastSkill_Sneak()
    {
        if (GetMP() < m_fSkillMpCost_Sneak)
        {
            Main.s_Instance.g_SystemMsg.SetContent(CMsgSystem.s_Instance.GetMsgContentByType(CMsgSystem.eSysMsgType.mp_not_enough));
            return false;
        }
        
        return true;
    }

    float m_fSkillMpCost_Sneak = 0f;
    float m_fSkillSpeedChangePercent_Sneak = 0f;
    float m_fSkillDuration_Sneak = 0f;
    bool m_bSneaking = false;
    public bool CastSkill_Sneak( float fMpCost, float fSpeedPercent, float fDuration )
    {
        m_fSkillMpCost_Sneak = fMpCost;
        m_fSkillSpeedChangePercent_Sneak = fSpeedPercent;
        m_fSkillDuration_Sneak = fDuration;

        if ( !CheckIfCanCastSkill_Sneak() )
        {
            return false;
        }


        // 耗蓝
        SetMP(GetMP() - m_fSkillMpCost_Sneak);

        photonView.RPC("RPC_CastSkill_Sneak", PhotonTargets.All, fSpeedPercent, fDuration );

        return true;
    }

    [PunRPC]
    public void RPC_CastSkill_Sneak( float fSpeedPercent, float fDuration )
    {
        m_fSkillSpeedChangePercent_Sneak = fSpeedPercent;
        m_fSkillDuration_Sneak = fDuration;

        SetSneaking( true );

        m_fSkillDurationCount_Sneak = 0;
    }

    public void SetSneaking( bool val )
    {
        for (int i = 0; i < m_lstBalls.Count; i++)
        {
            Ball ball = m_lstBalls[i];
            ball.SetRebornProtectEfffect(val);
        }

        m_bSneaking = val;
    }

    public bool IsSneaking()
    {
        return m_bSneaking;
    }

    float m_fSkillDurationCount_Sneak = 0;
    void SneakingLoop()
    {
        if (!m_bSneaking)
        {
            return;
        }

        m_fSkillDurationCount_Sneak += Time.deltaTime;
        if (m_fSkillDurationCount_Sneak >= m_fSkillDuration_Sneak)
        {
            EndSneak();
        }
    }

    void EndSneak()
    {
        m_bSneaking = false;
        SetSneaking( false );
    }

    //// end 【潜行】技能

    /// <summary>
    /// 技能： 变刺
    /// </summary>
    /// <returns></returns>
    float m_fMpCost_BecomeThorn = 0f;
    float m_fQianYao_BecomeThorn = 0f;
    float m_fSpeedChangePercent_BecomeThorn = 0f;
    float m_fDuration_BecomeThorn = 0f;
    string m_szExplodeConfigId_BecomeThorn = "";

    public bool CheckIfCanCastSkill_BecomeThorn()
    {
        if ( GetMP() < m_fMpCost_BecomeThorn )
        {
            Main.s_Instance.g_SystemMsg.SetContent(CMsgSystem.s_Instance.GetMsgContentByType(CMsgSystem.eSysMsgType.mp_not_enough));
            return false;
        }

        return true;
    }

    public bool CastSkill_BecomeThorn(float fMpCost_BecomeThorn, float fQianYao_BecomeThorn, float fSpeedChangePercent_BecomeThorn, float fDuration_BecomeThorn, string szExplodeConfigId)
    {
        m_fMpCost_BecomeThorn = fMpCost_BecomeThorn;

        if ( !CheckIfCanCastSkill_BecomeThorn())
        {
            return false;
        }

        // 耗蓝
        SetMP(GetMP() - m_fMpCost_BecomeThorn);

        photonView.RPC("RPC_CastSkill_BecomeThorn", PhotonTargets.All, fQianYao_BecomeThorn, fSpeedChangePercent_BecomeThorn, fDuration_BecomeThorn, szExplodeConfigId);

        return true;
    }

    [PunRPC]
    public void RPC_CastSkill_BecomeThorn(float fQianYao_BecomeThorn, float fSpeedChangePercent_BecomeThorn, float fDuration_BecomeThorn, string szExplodeConfigId)
    {
        m_fQianYao_BecomeThorn = fQianYao_BecomeThorn;
        m_fSpeedChangePercent_BecomeThorn = fSpeedChangePercent_BecomeThorn;
        m_fDuration_BecomeThorn = fDuration_BecomeThorn;
        m_szExplodeConfigId_BecomeThorn = szExplodeConfigId;

        for (int i = 0; i < m_lstBalls.Count; i++)
        {
            Ball ball = m_lstBalls[i];
            if (ball.IsDead())
            {
                continue;
            }

            ball.BecomeThorn_BeginQianYao();
        }

        BecomeThorn_BeginQianYao();
    }

    public string GetBecomeThornExplodeConfigId()
    {
        return m_szExplodeConfigId_BecomeThorn;
    }

    float m_fBecomeThornTimeCount = 0f;
    int m_nBecomeThornStatus = 0; // 0 - None  1 - QiaoYao  2 - BecomeThorn
    public void BecomeThorn_BeginQianYao()
    {
        m_nBecomeThornStatus = 1;
        m_fBecomeThornTimeCount = 0;
        for (int i = 0; i < m_lstBalls.Count; i++)
        {
            Ball ball = m_lstBalls[i];
            if (ball.IsDead())
            {
                continue;
            }
            ball._effectBecomeThorn_QianYao.gameObject.SetActive(true);
        }
    }

    public void BecomeThorn_QianYaoLoop()
    {
        if (m_nBecomeThornStatus != 1)
        {
            return;
        }
        m_fBecomeThornTimeCount += Time.deltaTime;
        if (m_fBecomeThornTimeCount < m_fQianYao_BecomeThorn)
        {
            return;
        }
        BecomeThorn_EndQianYao();
    }

    public void BecomeThorn_EndQianYao()
    {
        m_fBecomeThornTimeCount = 0;
        m_nBecomeThornStatus = 2;
        for ( int i = 0; i < m_lstBalls.Count; i++ )
        {
            Ball ball = m_lstBalls[i];
            ball._effectBecomeThorn_QianYao.gameObject.SetActive( false );
            ball._effectBecomeThorn_Become.gameObject.SetActive(true);
            ball._effectBecomeThorn_Become.Play();
            ball.SetSprite( CMonsterEditor.s_Instance.GetSpriteByMonsterType(CMonsterEditor.eMonsterBuildingType.thorn ));
            ball.SetColor( Color.red );
        }
    }

    public void BecomeThorn_ActAsThornLoop()
    {
        if (m_nBecomeThornStatus != 2)
        {
            return;
        }
        m_fBecomeThornTimeCount += Time.deltaTime;
        if (m_fBecomeThornTimeCount < m_fDuration_BecomeThorn)
        {
            return;
        }
        BecomeThorn_End();
    }

    public void BecomeThorn_End()
    {
        m_nBecomeThornStatus = 0;
        for (int i = 0; i < m_lstBalls.Count; i++)
        {
            Ball ball = m_lstBalls[i];
            ball._effectBecomeThorn_Become.gameObject.SetActive(true);
            ball._effectBecomeThorn_Become.Play();
            ball.SetColor( Color.white );
        }

        if (IsMainPlayer())
        {
            photonView.RPC("RPC_EndBecomeThorn", PhotonTargets.All);
        }
    }

    [PunRPC]
    public void RPC_EndBecomeThorn()
    {
        SetSkin( m_nCurSkinIndex );
    }

    public bool IsBecomeThorn()
    {
        return m_nBecomeThornStatus == 2;
    }

    //// end 技能：变刺

    public bool CheckIfCanUnfold()
	{
		if (GetMP () < m_fUnfoldCostMp) {
			Main.s_Instance.g_SystemMsg.SetContent (CMsgSystem.s_Instance.GetMsgContentByType(CMsgSystem.eSysMsgType.mp_not_enough));
			return false;
		}

		return true;
	}

    float m_fUnfoldCostMp = 0f;
    float m_fPreUnfoldTime = 0f;
    float m_fUnfoldScale = 0f;
    float m_fUnfoldKeepTime = 0f;
    public bool BeginPreUnfold( float fMpCost, float fMainTriggerPreUnfoldTime, float fUnfoldScale, float fUnfoldKeepTime)
	{
        m_fUnfoldCostMp = fMpCost;
        m_fPreUnfoldTime = fMainTriggerPreUnfoldTime;
        m_fUnfoldScale = fUnfoldScale;
        m_fUnfoldKeepTime = fUnfoldKeepTime;

        if (!CheckIfCanUnfold ()) {
			return false;
		}

		SetMP ( GetMP() - m_fUnfoldCostMp);

		photonView.RPC ( "RPC_BeginPreUnfold", PhotonTargets.All, fMainTriggerPreUnfoldTime, fUnfoldScale, fUnfoldKeepTime);

        return true;
	}

    float m_fCurSkillPointAffect_Unfold= 0f;
    public float GetCurSkillPointAffect_Unfold()
    {
        return m_fCurSkillPointAffect_Unfold;
    }

    [PunRPC]
	public void RPC_BeginPreUnfold( float fMainTriggerPreUnfoldTime, float fUnfoldScale, float fUnfoldKeepTime)
	{
        m_fPreUnfoldTime = fMainTriggerPreUnfoldTime;
        m_fUnfoldScale = fUnfoldScale;
        m_fUnfoldKeepTime = fUnfoldKeepTime;

        for (int i = 0; i < m_lstBalls.Count; i++) {
			Ball ball = m_lstBalls [i];
			if (ball.IsDead ()) {
				continue;
			}

			ball.BeginPreUnfold ();
		}
	}

    public float GetUnfoldSkillParam_PreUnfoldTime()
    {
        return m_fPreUnfoldTime;
    }

    public float GetUnfoldSkillParam_UnfoldScale()
    {
        return m_fUnfoldScale;
    }

    public float GetUnfoldSkillParam_UnfoldKeepTim()
    {
        return m_fUnfoldKeepTime;
    }



	public void DestroyMonster_Scene( CMonster thorn )
	{
		photonView.RPC ("RPC_DestroyMonster_Scene", PhotonTargets.All, thorn.GetGuid(), (float)PhotonNetwork.time ); 
	}

	[PunRPC]
	public void RPC_DestroyMonster_Scene( long nThornId, float fOccurTime )
	{
		CClassEditor.s_Instance.RemoveThorn (nThornId, fOccurTime);
	}

    public void RecycleBabaThorn(  int nThornIndex )
    {
        m_SkillManager.RecycleBabaThorn(nThornIndex);
    }



    // 使用“拉粑粑”技能
    public void UseSkill_Lababa()
    {
        m_SkillManager.UseSkill(CSkillManager.eSkillType.lababa);
    }

    public void CastSkill( byte[] bytes )
    {
        photonView.RPC("RPC_CastSkill", PhotonTargets.All, bytes);
    }

    [PunRPC]
    public void RPC_CastSkill(byte[] bytes)
    {
        m_SkillManager.CastSkill(bytes);
    }

    // 计算当前的总体积
    float m_fBaseArea = 0f; // 基础体积
    float m_fTotalArea = 0f;
    int m_nCurLiveBallNum = 0;
    float m_fCalcTotalAreaCount = 0f;
    public void CalculateCurTotalArea()
    {
        m_fCalcTotalAreaCount += Time.deltaTime;
        if ( m_fCalcTotalAreaCount < 3f ) // 3秒钟统计一次总体积就好了，没必要太频繁
        {
            return;
        }


        m_fTotalArea = 0f;
        for ( int i = 0; i < m_lstBalls.Count; i++ )
        {
            Ball ball = m_lstBalls[i];
            if ( ball.IsDead() )
            {
                continue;
            }
            m_fTotalArea += ball.GetVolume();
        }
    }

    public void SetCurLiveBallNum( int num )
    {
        m_nCurLiveBallNum = num;
    }

    public int GetCurLiveBallNum()
    {
        return m_nCurLiveBallNum;
    }


    /// <summary>
    /// ! ---- buff 
    /// </summary>
    List<CBuff> m_lstBuff = new List<CBuff>();
    public void AddBuff(int nBuffId)
    {
        if (!IsMainPlayer()) { // 由MainPlayer来发起Buff的生成事件
            return;
        }

        if (!CBuffEditor.s_Instance.CheckIfBuffIdValid(nBuffId)) {
            Debug.Log("buff id invalid");
            return;
        }

        CBuff buff = CreateBuff(nBuffId, m_lBuffGUID++);

        //m_lstBuff.Add ( buff );
        // Buff不能叠加，只能覆盖
        bool bCover = false;
        for (int i = 0; i < m_lstBuff.Count; i++)
        {
            CBuff the_buf = m_lstBuff[i];
            if (the_buf.GetConfig().id == nBuffId)
            {
                m_lstBuff[i] = buff;
                ResourceManager.RecycleBuff(the_buf);
                bCover = true;
                break;
            }
        }
    
        if ( ! bCover)
        {
            m_lstBuff.Add( buff );

        }

        photonView.RPC("RPC_AddBuff", PhotonTargets.All, nBuffId, (long)buff.GetGUID(), buff.GetTime());
	}

	public void Local_AddBuff( int nBuffId, long lGUID, float fLeftTime)
	{
		CBuff buff = CreateBuff ( nBuffId, lGUID );
		buff.SetTime ( fLeftTime );
        bool bCover = false;
        for (int i = 0; i < m_lstBuff.Count; i++)
        {
            CBuff the_buf = m_lstBuff[i];
            if (the_buf.GetConfig().id == nBuffId)
            {
                m_lstBuff[i] = buff;
                ResourceManager.RecycleBuff(the_buf);
                bCover = true;
                break;
            }
        }
        if ( !bCover)
        {
            m_lstBuff.Add( buff );
        }
    }

	static long m_lBuffGUID = 0;
	public static CBuff CreateBuff( int nBuffId, long lGUID )
	{
		CBuff buff = ResourceManager.ReuseBuff ( );
		buff.SetGUID ( lGUID );
		CBuffEditor.sBuffConfig config = CBuffEditor.s_Instance.GetBuffConfigById (nBuffId);
		buff.RefreshConfig (config);
		return buff;
	}


	[PunRPC]
	public void RPC_AddBuff( int nBuffId, long lGUID, float fLeftTime)
	{
		if (IsMainPlayer ()) {
			return;
		}

		Local_AddBuff ( nBuffId, lGUID, fLeftTime);
	}

	public void RemoveBuff( CBuff buff )
	{
		m_lstBuff.Remove ( buff );
		ResourceManager.RecycleBuff ( buff );

		photonView.RPC("RPC_RemoveBuff", PhotonTargets.All, buff.GetGUID());
	}

	[PunRPC]
	public void RPC_RemoveBuff( long lGUID )
	{
		if (IsMainPlayer ()) {
			return;
		}
		for (int i = 0; i < m_lstBuff.Count; i++) {
			CBuff buff = m_lstBuff[i];
			if (buff.GetGUID () == lGUID) {
				m_lstBuff.Remove (buff);
				ResourceManager.RecycleBuff ( buff );
				return;
			}
		}
	}




	void BuffLoop()
	{
		if (!IsMainPlayer ()) { // 暂定这样：Buff由 MainPlayer统一维护，实时同步给其余客户端，其余客户端只负责显示
			return;
		}
		string szContent = "";
		for (int i = m_lstBuff.Count - 1; i >= 0; i--) {
			CBuff buff = m_lstBuff [i];
			float fLeftTime = buff.GetTime ();
			fLeftTime -= Time.deltaTime;
			buff.SetTime (fLeftTime);
			if (buff.CheckIfEnd ()) {
				RemoveBuff (buff);
			}

            /*
            player_move_speed,            // 玩家移动速度
		player_spit_spore_speed,      // 吐孢子速度
		player_spit_spore_distance,   // 吐孢子距离
		explode_child_num,            // 影响炸球的分球数
		explode_mother_left_percent,  // 影响炸球时母球的保留体积
		shell_time,                   // 影响壳的持续时间
            */
            string szDesc = "";
            switch ( (CBuffEditor.eBuffFunc)buff.GetConfig().func)
            {
                case CBuffEditor.eBuffFunc.player_move_speed:
                    {
                        szDesc = "影响移动速度：" + buff.GetConfig().aryValues[0] * 100 + "%";
                    }
                    break;
                case CBuffEditor.eBuffFunc.player_spit_spore_speed:
                    {
                        szDesc = "影响吐孢子速度";
                    }
                    break;
                case CBuffEditor.eBuffFunc.player_spit_spore_distance:
                    {
                        szDesc = "影响吐孢子距离";
                    }
                    break;
                case CBuffEditor.eBuffFunc.explode_child_num:
                    {
                        szDesc = "影响分球个数";
                    }
                    break;
                case CBuffEditor.eBuffFunc.explode_mother_left_percent:
                    {
                        szDesc = "影响分球母体残留百分比";
                    }
                    break;
                case CBuffEditor.eBuffFunc.shell_time:
                    {
                        szDesc = "影响壳的时间";
                    }
                    break;

            }
            // debug info
            szContent += "Buff:" + szDesc + "(" + fLeftTime.ToString( "f0" ) + ")" + "\n";

        }  // end for
		CMsgSystem.s_Instance.ShowMsg ( 0, szContent );	
	}

	//// ! ---- end buff


	//// ! ----  Level
	/// 这些数据只是本机MainPlayer用，又何须挂在Player类里呢？就挂在成长系统（CGrowSystem）里，免得维护两份数据
	/// 
	public void SetMP( float val )
	{
		if (IsMainPlayer ()) {
			CGrowSystem.s_Instance.UpdateMP (val);
		}
	}

	public float GetMP()
	{
		return CGrowSystem.s_Instance.GetMP ();
	}


	public void AddMP( float val ){
		CGrowSystem.s_Instance.AddMP ( val );
	}

	public void AddExp( int val )
	{
		CGrowSystem.s_Instance.AddExp ( val );
	}



	int m_nLevel;
	public void SetLevel( int val )
	{
		photonView.RPC("RPC_SetLevel", PhotonTargets.All, val);
	}

	[PunRPC]
	public void RPC_SetLevel( int val )
	{
		Local_SetLevel (val);
	}

	public void Local_SetLevel( int val ){
		m_nLevel = val;
		if (IsMainPlayer ()) {
			CGrowSystem.s_Instance.UpdateLevel (m_nLevel);
		}
		for (int i = 0; i < m_lstBalls.Count; i++) {
			Ball ball = m_lstBalls [i];
			ball.SetPlayerLevel ( m_nLevel );
		}
		SetPlayerName ( GetPlayerName() );
	}

	public int GetLevel()
	{
		return m_nLevel;
	}

    ///  ! ---- end Level

    /// ! ---- Money
    public void AddMoney( int val )
    {
        CMoneySystem.s_Instance.SetMoney( GetMoney() + val );
    }

    public void CostMoney( int val )
    {
        CMoneySystem.s_Instance.SetMoney(GetMoney() - val);
    }

    public void ChangeMoney( int val )
    {
        CMoneySystem.s_Instance.SetMoney(GetMoney() + val);
    }

    public int GetMoney()
    {
        return CMoneySystem.s_Instance.GetMoney();
    }



    bool m_bCastSkillWhickNeedStopMoving = false;
    public bool IsCastSkillWhickNeedStopMoving()
    {
        return m_bCastSkillWhickNeedStopMoving;
    }

    public bool DoNotMove()
    {
        if (IsCastSkillWhickNeedStopMoving())
        {
            return true;
        }

        if ( IsMiaoHeQianYao() )
        {
            return true;
        }

        return false;
    }
   
    public void AddBaseSpeed( float val )
    {
        Main.s_Instance.g_SystemMsg.SetContent( "基础移动速度增加：" + val );
        m_BaseData.fBaseSpeed += val;
    }

    public void AddBaseVolumeByLevel( float fVolume )
    {
        m_fBaseVolumeByLevel += fVolume;
        SetBaseVolumeByLevel(m_fBaseVolumeByLevel);
        AddTotalVolume(fVolume);
    }

    public void SetBaseVolumeByLevel( float val)
    {
        m_fBaseVolumeByLevel = val;
       // LetBaseVolumeAffectRealTimeTotalVolume();
    }

    public void AddTotalVolume( float val )
    {
        int nNumOfLiveBalls = 0;
        GetTotalVolume( ref nNumOfLiveBalls);
        float fAddedVolumePerBall = val / nNumOfLiveBalls;
        for ( int i = 0; i < m_lstBalls.Count; i++ )
        {
            Ball ball = m_lstBalls[i];
            if ( ball.IsDead() )
            {
                continue;
            }
            float fVolume = ball.GetVolume();
            fVolume += fAddedVolumePerBall;
            ball.Local_SetSize( CyberTreeMath.Volume2Scale(fVolume) );
        }
    }

    // 获取当前由道具加成的基础体积值
    public float GetBaseVolumeByItem()
    {
        return m_BaseData.fBaseSize;
    }

    public void SetBaseVolumeByItem( float val )
    {
        m_BaseData.fBaseSize = val;
    }

    public void AddBaseSize( float val )
    {
        Main.s_Instance.g_SystemMsg.SetContent("道具使基础体积加了：" + val);
        m_BaseData.fBaseSize += val;
        SetBaseVolumeByItem(m_BaseData.fBaseSize);
        AddTotalVolume( val );
    }

    public float GetTotalVolume( ref int nNumOfLiveBalls )
    {
        float fTotalVolume = 0f;
        for (int i = 0; i < m_lstBalls.Count; i++)
        {
            Ball ball = m_lstBalls[i];
            if (ball.IsDead())
            {
                continue;
            }
            fTotalVolume += ball.GetVolume();
            nNumOfLiveBalls++;
        }
        return fTotalVolume;
    }

    public float GetTotalVolume()
    {
        return m_fTotalArea;
    }

    public void AddBaseMP( float val )
    {
        Main.s_Instance.g_SystemMsg.SetContent("基础蓝增加：" + val);
        m_BaseData.fBaseMp += val;

        CGrowSystem.s_Instance.UpdateMaxMP();
    }

    public float GetBaseMp()
    {
        return m_BaseData.fBaseMp;
    }

    public void AddBaseSpitDistance( float val )
    {
        Main.s_Instance.g_SystemMsg.SetContent("基础分球距离：" + val);
        m_BaseData.fBaseSpitDistance += val;
    }

    public float GetBaseSpitDistance()
    {
        return m_BaseData.fBaseSpitDistance;
    }

    public void DecreaseShellTime( float val )
    {
        Main.s_Instance.g_SystemMsg.SetContent("壳时间变为原来的：" + val);
        m_BaseData.fBaseShellTime = val;
    }

    public float GetDecreaseShellTime()
    {
     
         return m_BaseData.fBaseShellTime;
    }

    public void SetDecreaseShellTime( float val )
    {
        m_BaseData.fBaseShellTime = val;
    }

    public void PushThorn( long lThornGuid, Vector3 pos, Vector2 dir, Vector2 sporeDir )
    {
        photonView.RPC("RPC_PushThorn", PhotonTargets.All, GetCurSporeLevel(), lThornGuid, pos.x, pos.y, dir.x, dir.y, sporeDir.x, sporeDir.y);
    }

    [PunRPC]
    public void RPC_PushThorn(int nSporeLevel, long lThornGuid, float fPosX, float fPosY, float fDirX, float fDirY, float fSporeDirX, float fSporeDirY)
    {
        CMonster monster = CClassEditor.s_Instance.GetMonsterByGuid(lThornGuid);

        if ( IsMainPlayer() )
        {
            AddBeingPushedThorn(monster);
        }

        // 根据孢子等级，计算出推刺距离
        const float PUSH_THORN_BASE_DIS = 5f;
        float fDis = nSporeLevel * PUSH_THORN_BASE_DIS; // 规则暂定：推刺距离与孢子的等级呈线性正比
        
        vec2Temp.x = fDirX;
        vec2Temp.y = fDirY;

        float fRotationSpeed = 0;
        
       if (fDirX > 0 && fDirY > 0)
       {
          if ( ( fSporeDirX > 0 && fSporeDirY < 0 ) || fSporeDirX > 0 && fSporeDirY > 0)
          {
                fRotationSpeed = 2;
          }
          else
          {
                fRotationSpeed = -2;
          }
       }
       else if (fDirX > 0 && fDirY < 0)
       {
          if ((fSporeDirX > 0 && fSporeDirY < 0) || fSporeDirX < 0 && fSporeDirY < 0)
            {
                fRotationSpeed = 2;
            }
            else
            {
                fRotationSpeed = -2;
            }
        }
       else if (fDirX < 0 && fDirY < 0)
       {
            if ((fSporeDirX < 0 && fSporeDirY > 0) || fSporeDirX < 0 && fSporeDirY < 0)
            {
                fRotationSpeed = 2;
            }
            else
            {
                fRotationSpeed = -2;
            }
        }
       else if (fDirX < 0 && fDirY > 0)
       {
            if ((fSporeDirX > 0 && fSporeDirY > 0) || fSporeDirX < 0 && fSporeDirY > 0)
            {
                fRotationSpeed = 2;
            }
            else
            {
                fRotationSpeed = -2;
            }
        }

       

        monster.SetDir(vec2Temp);
        monster.BeginEject(fDis, 0.5f, fRotationSpeed);

        CClassEditor.s_Instance.AddToPushedThornList(monster);
    }

    List<CMonster> m_lstBeingPushedThorn = new List<CMonster>();
    public void AddBeingPushedThorn( CMonster monster )
    {
        for ( int i  = 0; i < m_lstBeingPushedThorn.Count; i++)
        {
            if (m_lstBeingPushedThorn[i].GetGuid() == monster.GetGuid())
            {
                m_lstBeingPushedThorn[i].SetFloatParam( 0, (float)PhotonNetwork.time );
                return;
            }
        }
        monster.SetFloatParam(0, (float)PhotonNetwork.time);
        m_lstBeingPushedThorn.Add(monster);
    }

   
    public void PublishMessage( string szContent )
    {
        photonView.RPC("RPC_PublishMessage", PhotonTargets.All, szContent );
    }

    [PunRPC]
    public void RPC_PublishMessage( string szContent )
    {
        CChatSystem.s_Instance.ReceiveMessage( m_szPlayerName , szContent );
    }

    public void SyncDustFullInfo( byte[] bytes )
    {
        photonView.RPC("RPC_SyncDustFullInfo", PhotonTargets.Others, bytes);
    }

    [PunRPC]
    public void RPC_SyncDustFullInfo(byte[] bytes)
    {
        StringManager.BeginPopData(bytes);
        int nShit = StringManager.PopData_Short();
        float fTime = StringManager.PopData_Float();
        float fTheDelay = Main.GetTime() - fTime;
        int nNum = StringManager.PopData_Short();
        for ( int i = nShit; i < nNum; i+=1  )
        {
            vecTempPos.x = StringManager.PopData_Float();
            vecTempPos.y = StringManager.PopData_Float();
            CStarDust.s_Instance.SetDustPos(i, vecTempPos);
        }
    }

    List<CDust> m_lstSyncDust = new List<CDust>();
    public void AddDustToSyncPool(CDust dust )
    {
        for ( int i = 0; i < m_lstSyncDust.Count; i++ )
        {
            CDust node_dust = m_lstSyncDust[i];
            if (node_dust.GetId() == dust.GetId())
            {
                node_dust.SetCollisionTime( Main.GetTime() );
                return;
            }
        }

        dust.SetCollisionTime(Main.GetTime());
        m_lstSyncDust.Add( dust );
    }

    void DustCollisonLoop()
    {
        for ( int i = m_lstSyncDust.Count - 1; i >= 0; i-- )
        {
            CDust dust = m_lstSyncDust[i];
            if ( Main.GetTime() - dust.GetCollisionTime() >= 0.5f )
            {
                m_lstSyncDust.RemoveAt( i );
                continue;
            }
        }
    }

    // 衰减    
    public void Attenuate()
    {
        if ( !IsMainPlayer() )
        {
            return;
        }

        // 只把吃进去的体积拿来衰减。基础体积不参与衰减
        // 基础体积分为三个部分：游戏初始时赋予的 + 道具加成的（貌似目前这种道具废弃了） + 升级时加成的
        float fPlayerEatVolume = GetEatVolume();
        if ( fPlayerEatVolume == 0f)
        {
            return;
        }

        float fEatVolumePer = fPlayerEatVolume / GetCurLiveBallNum();
        for ( int i = 0; i < m_lstBalls.Count; i++ )
        {
            Ball ball = m_lstBalls[i];
            if ( ball.IsDead() )
            {
                continue;
            }
            ball.DoAttenuate(fEatVolumePer );
        }
    }


} // end Player
