﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CSpineManager : MonoBehaviour {

    static Quaternion vecTempRotation = new Quaternion();
    static Vector3 vecTempPos1 = new Vector3();

    public static CSpineManager s_Instance;

    public enum eBallSpineType
    {
        spit_spore, // 吐孢子
        split,      // 分球
        unfold,     // 扩张

    };

    private void Awake()
    {
        s_Instance = this;
    }

    // Use this for initialization
    void Start () {
		
	}
	
	// Update is called once per frame
	void Update () {
		
	}

    // 旋转
    public void SetBallAnimationRotation( Ball ball, float fRotationAngle )
    {
        ball._skeletonAnimatnion.transform.localRotation = Quaternion.identity;
        ball._skeletonAnimatnion.transform.Rotate (0.0f, 0.0f, fRotationAngle);
    }

    public void PlayBallAnimation( Ball ball, eBallSpineType aniType, bool bLoop = false, float fTimeScale = 1f, int nTrack = 0 )
    {
        ball._skeletonAnimatnion.state.SetAnimation(nTrack, GetBallAnimationNameByType( aniType ), bLoop );
        ball._skeletonAnimatnion.state.TimeScale = fTimeScale; // 控制播放速度
    }

    public void StopBallAnimation( Ball ball, int nTrack )
    {
        ball._skeletonAnimatnion.state.SetEmptyAnimation(nTrack, 0);
    }

    public void SetBallSkin( Ball ball )
    {
        //        ball._skeletonAnimatnion.skeleton.SetSkin();

        
    }


    string GetBallAnimationNameByType(eBallSpineType aniType)
    {
        string szAnimationName = "";
        switch(aniType)
        {
            case eBallSpineType.spit_spore:
                {
                    szAnimationName = "ani03";
                }
                break;
            case eBallSpineType.split:
                {
                    szAnimationName = "ani01";
                }
                break;
            case eBallSpineType.unfold:
                {
                    szAnimationName = "ani02";
                }
                break;

        }

        return szAnimationName;
    }
}
