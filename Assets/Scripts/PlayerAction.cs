using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.EventSystems;

public class PlayerAction : MonoBehaviour
{
    public Canvas _canvas;
    public GameObject _global;
    public GameObject _center;
    public GameObject _world_cursor;
    public GameObject _screen_cursor;

    public Vector2 _cursor_direction = Vector2.zero;
    public Vector2 _joystick_movement = Vector2.zero;


    float _cursor_velocity = 8.0f;
    float _cursor_realtime_velocity = 8.0f;
    
    float _cursor_radius = 0.0f;
    float _cursor_move_time_velocity = 0.0f;
    public const float CURSOR_MOVE_TIME = 0.5f;

    public float _ball_velocity = 0.2f;
    float _ball_realtime_velocity = 0.2f;
	public float _ball_realtime_velocity_without_size = 0.2f;

    public Vector3 _center_move_velocity = Vector3.zero;

    public const float CENTER_SLOW_MOVE_TIME = 10.0f;
    public const float CENTER_FAST_MOVE_TIME = 1.0f;

    public float _center_move_time_velocity = 0.0f;
    public float _center_move_time = CENTER_FAST_MOVE_TIME;
    
    bool _release_joystick_keep_position = false;
    bool _release_joystick_fast_stop = false;        
    bool _release_joystick_fast_rotation = false;    
    
    Vector2 _touch_screen_position = Vector2.zero;
    Vector3 _touch_world_position  = Vector3.zero;

    const float _touch_stop_interval = 0.25f;
    float _touch_start_time = 0.0f;

    public static PlayerAction s_Instance = null;

    void Awake()
    {
        s_Instance = this;
    }

    void Start()
    {
		Reset ();
    }

    void OnDestroy()
    {

    }

	public void Reset()
	{
		_ball_realtime_velocity = 0f;
		_ball_realtime_velocity_without_size = 0f;
	}

    public Vector3 GetWorldCursorPos()
    {
        return this._world_cursor.transform.position;
    }

    public Vector2 GetSpecialDirection()
    {
        return m_vec2MoveInfo;
    }

    public float GetSpeed()
    {
        return _ball_realtime_velocity_without_size;
    }


    float m_nFrameCount = 0;
	const float c_nPlayerMoveInfoSyncInterval = 0.5f; // 500ms同步一次，一秒钟大概可以同步2次
	public Vector2 m_vec2MoveInfo = new Vector2();


    void FixedUpdate()
    {
        DoUpdate();
    }

    void Update()
    {
        
    }

    void DoUpdate()
    { 
		if ( Main.s_Instance == null || Main.s_Instance.m_MainPlayer == null) {
			return;
		}

		if (!Main.s_Instance || !Main.s_Instance.m_MainPlayer/* || !Main.s_Instance.m_MainPlayer.IsMoving ()*/) {
			return;
		}

        if ( AccountManager.m_eSceneMode != AccountManager.eSceneMode.Game )
        {
            return;
        }

		if (AccountManager.s_bObserve) {
			return;
		}

        //this.UpdateIndicators();
        this.UpdateCursorDirection();
		if (/*CtrlMode.GetCtrlMode() != CtrlMode.CTRL_MODE_NONE*/true) {
            this.UpdateCursorPosition();
            Vector3 balls_center_position = Vector3.zero;
            Vector3 balls_min_position = Vector3.zero;
            Vector3 balls_max_position = Vector3.zero;
            this.UpdateBallsPositions(ref balls_center_position,
                                      ref balls_min_position,
                                      ref balls_max_position);
            this.UpdateCenterPosition(balls_center_position);
            CtrlMode.UpdateCameraViewPort(balls_min_position,
                                          balls_max_position);
        }
        else {
            this.CheckPlayingStatus();
        }
	}

    void CheckPlayingStatus()
    {
        SceneAction scene_action =
            this._global.GetComponent<SceneAction>();
        GUIAction gui_action =
            this._global.GetComponent<GUIAction>();
        if (this.IsMouseDown() &&
            scene_action.GetSceneStatus() == SceneStatus.SceneStatusPlaying) {
            CtrlMode.SetCtrlMode(CtrlMode.CTRL_MODE_HAND);
            CtrlMode.SetDirIndicatorType(CtrlMode.DIR_INDICATOR_LINE);
            gui_action.UpdateCtrlModeDropDown(CtrlMode.CTRL_MODE_HAND);
            gui_action.UpdateDirIndicatorDropDown(CtrlMode.DIR_INDICATOR_LINE);
            this._center_move_time = PlayerAction.CENTER_SLOW_MOVE_TIME;
        }
    }
    
    public void UpdateIndicators()
    {
        switch (CtrlMode.GetCtrlMode()) {
			/*
            case CtrlMode.CTRL_MODE_NONE:
                this._world_cursor.GetComponent<Renderer>().enabled = false;
                this._screen_cursor.SetActive(false);
                this._center.GetComponent<Renderer>().enabled = false;                
                break;
			*/
           case CtrlMode.CTRL_MODE_HAND:
                this._world_cursor.GetComponent<Renderer>().enabled = false;
                this._screen_cursor.SetActive(true);
                this._center.GetComponent<Renderer>().enabled = false;
                break;
            case CtrlMode.CTRL_MODE_JOYSTICK:
                this._world_cursor.GetComponent<Renderer>().enabled = false;
                this._screen_cursor.SetActive(false);
                this._center.GetComponent<Renderer>().enabled = false;
                break;
            case CtrlMode.CTRL_MODE_WORLD_CURSOR:
                this._world_cursor.GetComponent<Renderer>().enabled = true;
                this._screen_cursor.SetActive(false);                
                this._center.GetComponent<Renderer>().enabled = false;
                break;                
            case CtrlMode.CTRL_MODE_SCREEN_CURSOR:                
                this._world_cursor.GetComponent<Renderer>().enabled = false;
                this._screen_cursor.SetActive(true);                                
                this._center.GetComponent<Renderer>().enabled = false;
                break;
            default:
                this._world_cursor.GetComponent<Renderer>().enabled = false;
                this._screen_cursor.SetActive(true);
                this._center.GetComponent<Renderer>().enabled = true;
                break;
        }
    }

    
    public void UpdateCursorPosition()
    {
        if (CtrlMode.GetCtrlMode() == CtrlMode.CTRL_MODE_HAND ||
            CtrlMode.GetCtrlMode() == CtrlMode.CTRL_MODE_SCREEN_CURSOR &&
            this._release_joystick_keep_position) {

            this._world_cursor.transform.position =
                new Vector3(this._touch_world_position.x,
                            this._touch_world_position.y,
                            this._world_cursor.transform.position.z);                            

            Vector2 canvas_point = Vector2.zero;
            if (RectTransformUtility.ScreenPointToLocalPointInRectangle(this._canvas.GetComponent<RectTransform>(),
                                                                        this._touch_screen_position,
                                                                        this._canvas.worldCamera,
                                                                        out canvas_point)) {
                this._screen_cursor.GetComponent<RectTransform>().anchoredPosition = canvas_point;
            }
	
        }
        else if (CtrlMode.GetCtrlMode() == CtrlMode.CTRL_MODE_JOYSTICK) { // 摇杆模式下，点击一下屏幕就是“急停”操作
            Vector3 position = Vector3.zero;
            if (this._release_joystick_fast_stop) { // 
                position = this._center.transform.position;
                this._joystick_movement = Vector2.zero;
                this._cursor_realtime_velocity = 0.0f;
                this._release_joystick_fast_stop = false;
				Main.s_Instance.m_MainPlayer.SetMoving ( false ); // 停止移动
				Main.s_Instance.m_MainPlayer.BeginMoveToCenter(); // 主角所有的球球向中心点靠拢
            }
            else {
                float radius = Mathf.Lerp(0.0f,
                                          Mathf.Max(CtrlMode._world_port_h, CtrlMode._world_port_v) * 0.5f,
                                          this._joystick_movement.magnitude);
                this._cursor_radius =
                    Mathf.SmoothDamp(this._cursor_radius,
                                     radius,
                                     ref this._cursor_move_time_velocity,
                                     PlayerAction.CURSOR_MOVE_TIME);

                this._cursor_radius /= 1.2f; // 之前之所以所有球球的运行轨迹都是平行的，就是这个“鼠标点”太远太远了。稍微缩短点就好了

                position = 
                    this._center.transform.position +
                    new Vector3(this._cursor_direction.x,
                                this._cursor_direction.y, 0) * this._cursor_radius;
           }
            this._world_cursor.transform.position = position;

        }        
        else if (CtrlMode.GetCtrlMode() == CtrlMode.CTRL_MODE_WORLD_CURSOR) {
            float joystick_magnitude = this._joystick_movement.magnitude;
            this._cursor_realtime_velocity = Mathf.Lerp(0.0f,
                                                        this._cursor_velocity,
                                                        joystick_magnitude);
            Vector3 position =
                this._world_cursor.transform.position + 
                new Vector3(this._cursor_direction.x,
                            this._cursor_direction.y, 0) *
                this._cursor_realtime_velocity * Time.fixedDeltaTime;
            position.z = -1;
            position.x = Mathf.Min(position.x, this._center.transform.position.x + CtrlMode._world_port_h * 0.5f);
            position.x = Mathf.Max(position.x, this._center.transform.position.x - CtrlMode._world_port_h * 0.5f);
            position.y = Mathf.Min(position.y, this._center.transform.position.y + CtrlMode._world_port_v * 0.5f);
            position.y = Mathf.Max(position.y, this._center.transform.position.y - CtrlMode._world_port_v * 0.5f);
            this._world_cursor.transform.position = position;
        }
        else if (CtrlMode.GetCtrlMode() == CtrlMode.CTRL_MODE_SCREEN_CURSOR) {
            float joystick_magnitude = this._joystick_movement.magnitude;
            this._cursor_realtime_velocity = Mathf.Lerp(0.0f,
                                                        this._cursor_velocity,
                                                        joystick_magnitude);
            this._touch_world_position =
                this._world_cursor.transform.position + 
                new Vector3(this._cursor_direction.x,
                            this._cursor_direction.y, 0) *
                this._cursor_realtime_velocity * Time.fixedDeltaTime;
            this._touch_world_position.x =
                Mathf.Min(this._touch_world_position.x,
                          this._center.transform.position.x + CtrlMode._world_port_h * 0.5f);
            this._touch_world_position.x =
                Mathf.Max(this._touch_world_position.x,
                          this._center.transform.position.x - CtrlMode._world_port_h * 0.5f);
            this._touch_world_position.y =
                Mathf.Min(this._touch_world_position.y,
                          this._center.transform.position.y + CtrlMode._world_port_v * 0.5f);
            this._touch_world_position.y =
                Mathf.Max(this._touch_world_position.y,
                          this._center.transform.position.y - CtrlMode._world_port_v * 0.5f);
            this._world_cursor.transform.position = this._touch_world_position;
            this._touch_screen_position = Camera.main.WorldToScreenPoint(this._touch_world_position);
            Vector2 canvas_point = Vector2.zero;
            if (RectTransformUtility.ScreenPointToLocalPointInRectangle(this._canvas.GetComponent<RectTransform>(),
                                                                        this._touch_screen_position,
                                                                        this._canvas.worldCamera,
                                                                        out canvas_point)) {
                this._screen_cursor.GetComponent<RectTransform>().anchoredPosition = canvas_point;
            }
        }
    }
    /*
    public void UpdateBallsPositions(ref Vector3 balls_center_position,
                                 ref Vector3 balls_min_position,
                                 ref Vector3 balls_max_position)
    {
        balls_center_position.x = 0.0f;
        balls_center_position.y = 0.0f;
        balls_min_position.x = 0.0f;
        balls_min_position.y = 0.0f;
        balls_max_position.x = 0.0f;
        balls_max_position.y = 0.0f;
        int nCount = 0;
        float fTotalWeight = 0.0f;

        bool bFirst = true;

        foreach (Transform child in Main.s_Instance.m_goMainPlayer.transform)
        {
            GameObject goBall = child.gameObject;
            Ball ball = goBall.GetComponent<Ball>();
            if ( ball.IsEjecting() )
            {
                continue;
            }

            if (bFirst)
            {
                balls_min_position.x = ball.transform.position.x;
                balls_max_position.x = ball.transform.position.x;
                balls_min_position.y = ball.transform.position.x;
                balls_max_position.y = ball.transform.position.x;
            }
            else
            {
                balls_min_position.x = Mathf.Min(balls_min_position.x, ball.transform.position.x);
                balls_max_position.x = Mathf.Max(balls_max_position.x, ball.transform.position.x);
                balls_min_position.y = Mathf.Min(balls_min_position.y, ball.transform.position.y);
                balls_max_position.y = Mathf.Max(balls_max_position.y, ball.transform.position.y);
            }

            float fWeight = ball.GetSize() * ball.GetSize() * ball.GetSize(); // 体积作为权重，影响中心点计算
            balls_center_position.x += ball.transform.position.x * fWeight;
            balls_center_position.y += ball.transform.position.y * fWeight;
            fTotalWeight += fWeight;
            nCount++;

            bFirst = false;

            BallAction ball_action = ball.GetComponent<BallAction>();
            if (CtrlMode.GetCtrlMode() == CtrlMode.CTRL_MODE_JOYSTICK)
            {
                this._ball_realtime_velocity =
                    Mathf.Lerp(0, Main.s_Instance.m_fBallBaseSpeed, this._joystick_movement.magnitude);
            }
            else
            {
                this._ball_realtime_velocity = Main.s_Instance.m_fBallBaseSpeed;
            }

            

            ball_action.UpdatePosition(this._world_cursor.transform.position, this._ball_realtime_velocity);

        } // end foreach
        balls_center_position.x = balls_center_position.x / nCount / fTotalWeight;
        balls_center_position.y = balls_center_position.y / nCount / fTotalWeight;
    }
    */

	public Vector3 m_vecBallsCenter = new Vector3();
	public Vector3 GetBallsCenter()
	{
		return m_vecBallsCenter;
	}

	public Dictionary<int, float> dicTempGroupInGrass = new Dictionary<int, float> ();

    public void UpdateBallsPositions(ref Vector3 balls_center_position,
                                     ref Vector3 balls_min_position,
                                     ref Vector3 balls_max_position)
    {
        if (Main.s_Instance == null || Main.s_Instance.m_goMainPlayer == null) {
			return;
		}


        //GameObject[] balls = GameObject.FindGameObjectsWithTag("Ball");
        //for (int i=0; i<balls.Length; ++i) {
		bool bFirst = true;
		bool bShitFirst = true;
        float fTotalWeight = 0.0f;
        //balls_center_position = Vector3.zero;

		m_lstMoveOrderX.Clear();
		m_lstMoveOrderY.Clear();
		List<Ball> lst = Main.s_Instance.m_MainPlayer.GetBallList();
        int nLiveBallNum = 0;
		for ( int i = 0; i < lst.Count; i++ ) // 散户球的移动
		{
            //GameObject ball = balls[i];
			//GameObject ball = child.gameObject;
			//Ball the_ball = ball.GetComponent<Ball> ();
			Ball the_ball = lst[i];
			if (the_ball == null) {
				Debug.LogError ( "ball null" );
				continue;
			}

			if (the_ball.IsDead ()) {
				continue;
			}

            nLiveBallNum++;

            float fWeight = the_ball.GetSize() * the_ball.GetSize() * the_ball.GetSize(); // 体积作为权重，影响中心点计算
			if (bShitFirst/*balls_center_position == Vector3.zero*/) {
				balls_center_position = the_ball.transform.position;
				bShitFirst = false;
            }
            else {
                float k = fWeight / fTotalWeight;
                //balls_center_position.x = (ball.transform.position.x * k  + balls_center_position.x * ( 1 - k )  ) ;
                //balls_center_position.y = (ball.transform.position.y * k + balls_center_position.y * ( 1 - k )  );
				balls_center_position.x = (the_ball.transform.position.x  + balls_center_position.x  ) / 2.0f ;
				balls_center_position.y = (the_ball.transform.position.y  + balls_center_position.y ) / 2.0f;

            }
            
            fTotalWeight += fWeight;


            if (bFirst) {
				balls_min_position.x = the_ball.transform.position.x -	the_ball.transform.localScale.x * CtrlMode._ball_scale_zoom_factor;
				balls_min_position.y = the_ball.transform.position.y -	the_ball.transform.localScale.y * CtrlMode._ball_scale_zoom_factor;
				balls_max_position.x = the_ball.transform.position.x +	the_ball.transform.localScale.x * CtrlMode._ball_scale_zoom_factor;
				balls_max_position.y = the_ball.transform.position.y +	the_ball.transform.localScale.y * CtrlMode._ball_scale_zoom_factor;
				bFirst = false;
			}
			balls_min_position.x = Mathf.Min((the_ball.transform.position.x -
				the_ball.transform.localScale.x * CtrlMode._ball_scale_zoom_factor),
                                                 balls_min_position.x);



			balls_min_position.y = Mathf.Min((the_ball.transform.position.y -
				the_ball.transform.localScale.y * CtrlMode._ball_scale_zoom_factor),
                                                 balls_min_position.y);


			balls_max_position.x = Mathf.Max((the_ball.transform.position.x +
				the_ball.transform.localScale.x * CtrlMode._ball_scale_zoom_factor),
                                                 balls_max_position.x);
			balls_max_position.y = Mathf.Max((the_ball.transform.position.y +
				the_ball.transform.localScale.y * CtrlMode._ball_scale_zoom_factor),
                                                 balls_max_position.y);

			BallAction ball_action = the_ball.GetComponent<BallAction>();

			float fPlayerbaseSpeed = Main.s_Instance.m_MainPlayer.GetBaseSpeed ();

            if (CtrlMode.GetCtrlMode() == CtrlMode.CTRL_MODE_JOYSTICK) {
				this._ball_realtime_velocity_without_size =
					Mathf.Lerp(0, fPlayerbaseSpeed/*Main.s_Instance.m_fBallBaseSpeed*/, this._joystick_movement.magnitude); 
            }
            else {
				this._ball_realtime_velocity_without_size = fPlayerbaseSpeed/*Main.s_Instance.m_fBallBaseSpeed*/;
            }

			if (the_ball.IsEjecting ()) {
				continue;
			}

			if (Main.s_Instance.m_MainPlayer.IsMoving ()  && (!Main.s_Instance.m_MainPlayer.DoNotMove())) {
								ball_action.UpdatePosition (this._world_cursor.transform.position, this._ball_realtime_velocity_without_size ); 
				
			}
        } //// end foreach
		m_vecBallsCenter = balls_center_position;
        Main.s_Instance.m_MainPlayer.SetCurLiveBallNum(nLiveBallNum);
		

		/*
		// 排序
		SortMoveOrder( ref m_lstMoveOrderX, lst, 0 );
		SortMoveOrder( ref m_lstMoveOrderY, lst, 1 );

		// 往X轴的负方向运动
		for (int i = 0; i < m_lstMoveOrderX.Count; i++) {
			int nIndex = m_lstMoveOrderX [i];
			Ball ball = lst [nIndex];
			if (ball.GetDir ().x >= 0) {
				continue;
			}
			ball._ba.UpdatePosition (this._world_cursor.transform.position, this._ball_realtime_velocity_without_size, 0);
		}

		// 往X轴的正方向运动
		for (int i = m_lstMoveOrderX.Count - 1; i >= 0; i--) {
			int nIndex = m_lstMoveOrderX [i];
			Ball ball = lst [nIndex];
			if (ball.GetDir ().x <= 0) {
				continue;
			}
			ball._ba.UpdatePosition (this._world_cursor.transform.position, this._ball_realtime_velocity_without_size, 0);
		}

		// 往Y轴的负方向运动
		for (int i = 0; i < m_lstMoveOrderY.Count; i++) {
			int nIndex = m_lstMoveOrderY [i];
			Ball ball = lst [nIndex];
			if (ball.GetDir ().y >= 0) {
				continue;
			}
			ball._ba.UpdatePosition (this._world_cursor.transform.position, this._ball_realtime_velocity_without_size, 1);
		}

		// 往Y轴的正方向运动
		for (int i = m_lstMoveOrderY.Count - 1; i >= 0; i--) {
			int nIndex = m_lstMoveOrderY [i];
			Ball ball = lst [nIndex];
			if (ball.GetDir ().y <= 0) {
				continue;
			}
			ball._ba.UpdatePosition (this._world_cursor.transform.position, this._ball_realtime_velocity_without_size, 1);
		}
		*/
    }

	// nAxis: 0 - X    1 - Y
	void SortMoveOrder( ref List<int> lst, List<Ball> lstBalls, int nAxis )
	{
		int nTemp = 0;
		for (int i = 0; i < lst.Count - 1; i++) {
			for (int j = i + 1; j < lst.Count; j++) {
				Ball ball1 = lstBalls [i];
				Ball ball2 = lstBalls [j];
				float val1 = (nAxis == 0?ball1.GetPos().x:ball1.GetPos().y);
				float val2 = (nAxis == 0?ball2.GetPos().x:ball2.GetPos().y);
				if (val1 > val2) {
					nTemp = lst [i];
					lst [i] = lst[j];
					lst [j] = nTemp;
				}
 			}
		}
	}
	List<int> m_lstMoveOrderX = new List<int>();
	List<int> m_lstMoveOrderY = new List<int>();
    

	bool IsMouseDown()
    {
        return Input.GetMouseButton(0) &&
            !this._global.GetComponent<GUIAction>().Intersect(Input.mousePosition);
    }
    
    public void UpdateCursorDirection()
    {
        if (this.IsMouseDown()) {
            if (this._release_joystick_keep_position) {
                this._touch_start_time = Time.time;
            }
            this._release_joystick_keep_position = false;
        }
        if (CtrlMode.GetCtrlMode() == CtrlMode.CTRL_MODE_HAND ||
            CtrlMode.GetCtrlMode() == CtrlMode.CTRL_MODE_SCREEN_CURSOR &&
            this._release_joystick_keep_position) {
            if (true/*this.IsMouseDown()*/) {
				if (Main.s_Instance.m_nPlatform == 0) {
					this._touch_screen_position = Input.mousePosition;
				} else {
					Touch touch;
					if (Input.touchCount >= 1) {
							if (!Main.s_Instance.IsForceSpitting ()) {
								touch = Input.GetTouch (0);
							} else {
								if (Main.s_Instance.GetSpitFingerID () == 0) {
									touch = Input.GetTouch (1);
								} else {
									touch = Input.GetTouch (0);
								}
							}
							this._touch_screen_position = touch.position;
						}
	
				}
				//MapEditor.s_Instance._inputTest.text = Input.mousePosition.ToString ();
            }
            Vector3 position = Camera.main.ScreenToWorldPoint(this._touch_screen_position);
            this._touch_world_position.x = position.x;
            this._touch_world_position.y = position.y;
            Vector2 direction = this._touch_world_position - this._center.transform.position;
            direction.Normalize();
            this._cursor_direction = direction;
        }
        else if (CtrlMode.GetCtrlMode() == CtrlMode.CTRL_MODE_JOYSTICK ||
                 CtrlMode.GetCtrlMode() == CtrlMode.CTRL_MODE_WORLD_CURSOR ||
                 CtrlMode.GetCtrlMode() == CtrlMode.CTRL_MODE_SCREEN_CURSOR) {
            this._cursor_direction = this._joystick_movement.normalized;

        }
    }

    public void UpdateCenterPosition(Vector3 balls_center_position)
    {
		if (AccountManager.s_bObserve) {
			return;
		}

        if ( Main.s_Instance.m_MainPlayer.IsDead() )
        {
            return;
        }
        /*
        if (!Mathf.Approximately(this._center_move_time, PlayerAction.CENTER_FAST_MOVE_TIME)) {
            this._center_move_time =
                Mathf.SmoothDamp(this._center_move_time,
                                 PlayerAction.CENTER_FAST_MOVE_TIME,
                                 ref this._center_move_time_velocity,
                                 PlayerAction.CENTER_SLOW_MOVE_TIME);
        }
        */
        this._center.transform.position = 
                    Vector3.SmoothDamp(this._center.transform.position,
                               balls_center_position,
                               ref this._center_move_velocity,
                               CtrlMode._zoom_time/*this._center_move_time*/);

        Main.s_Instance.m_camWarFog.transform.position = Camera.main.transform.position;
        Main.s_Instance.m_camWarFog.orthographicSize = Camera.main.orthographicSize;
    }
    
    public void OnMoveStart()
    {
        // Debug.Log("OnMoveStart");
    }

    public void OnMove(Vector2 axis_vals)
    {
        this._joystick_movement = axis_vals;
    }
    
    public void OnMoveSpeed(Vector2 axis_speed_vals)
    {
        // Debug.Log("OnMoveSpeed: " + axis_speed_vals);
    }
    
    public void OnMoveEnd()
    {
        // Debug.Log("OnMoveEnd");
    }

    public void OnTouchStart()
    {
		Main.s_Instance.m_MainPlayer.SetMoving ( true );

	}

    public void OnTouchEnd()
    {  
	   this._release_joystick_keep_position = true;
        if (CtrlMode.GetCtrlMode() != CtrlMode.CTRL_MODE_JOYSTICK) {
            this._joystick_movement = Vector2.zero;
            this._cursor_realtime_velocity = 0.0f;
        }
        else {
            float now = Time.time;
			if (now - this._touch_start_time < PlayerAction._touch_stop_interval) {
				this._release_joystick_fast_stop = true;
			}
        }
    }

    public void OnDownUp()
    {
        // Debug.Log("OnDownUp");        
    }
    
    public void OnDownDown()
    {
        // Debug.Log("OnDownDown");
    }

    public void OnDownRight()
    {
        // Debug.Log("OnDownRight");
    }

    public void OnDownLeft()
    {
        // Debug.Log("OnDownLeft");
    }

    public void OnPressUp()
    {
        // Debug.Log("OnPressUp");        
    }
    
    public void OnPressDown()
    {
        // Debug.Log("OnPressDown");
    }

    public void OnPressRight()
    {
        // Debug.Log("OnPressRight");
    }

    public void OnPressLeft()
    {
        // Debug.Log("OnPressLeft");
    }    
};
