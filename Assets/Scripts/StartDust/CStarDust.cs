﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System.Xml;
using System.Text;
using System;
using UnityEngine.UI;

public class CStarDust : MonoBehaviour {

    public InputField _inputDustNum;
    public InputField _inputDustMaxSize;
    public InputField _inputDustMinSize;
    public InputField _inputDustBeltSize;
    public InputField _inputDustAngularVelocity;
    public InputField _inputDustBackToOrbitVelocity;
    public InputField _inputDustMass;

    public GameObject[] m_aryDustPrefabs;

    static Vector3 vecTempPos = new Vector3();
    static Vector2 vec2TempPos = new Vector2();

    public Vector2 m_vecOrbitCenter = new Vector2();
    public float m_fOrbitRaduis;
    public float m_fAngularVElocity;
    public float m_fMoveToOrbitSpeed;

    public float m_fAngularVElocityX;
    public float m_fAngularVElocityY;

    public static CStarDust s_Instance;

    List<CDust> m_lstDust = new List<CDust>();

    XmlNode m_xmlNode = null;

    public CDust m_ShitDust0;
    public CDust m_ShitDust1;

    private void Awake()
    {
        s_Instance = this;
    }

    // Use this for initialization
    void Start () {

   
        /*
        m_fAngularVElocityX = Mathf.Cos(m_fAngularVElocity);
        m_fAngularVElocityY = Mathf.Sin(m_fAngularVElocity);


        for ( int i = 0; i < 300; i++ )
        {
            int nPreIdx = UnityEngine.Random.Range(0, 4);
            CDust dust = GameObject.Instantiate(m_aryDustPrefabs[nPreIdx]).GetComponent<CDust>();
            float fRadius = (float)UnityEngine.Random.Range(180, 220);
            dust.SetRadius(fRadius);
            float fRotation = (float)UnityEngine.Random.Range(0, 360);
            vec2TempPos.x = fRadius * Mathf.Cos(fRotation);
            vec2TempPos.y = fRadius * Mathf.Sin(fRotation);
            dust.SetStartPos(vec2TempPos, fRotation);
            dust.SetSize((float)UnityEngine.Random.Range(3f, 10f));
            m_lstDust.Add(dust);
        }
        */

    }
	
	// Update is called once per frame
	void Update () {

        GenerateDust();
        Move();


    }

    float m_fCheckInCamViewTimeCount = 0f;
    void Move()
    {
        if ( !m_bGenerated )
        {
            return;
        }

        float fDeltaTime = 0f;
        if (AccountManager.m_eSceneMode == AccountManager.eSceneMode.Game)
        {
            fDeltaTime = Main.GetTime() - Main.s_Instance.GetGameStartTime();
        }
        else if(AccountManager.m_eSceneMode == AccountManager.eSceneMode.MapEditor)
        {
            fDeltaTime = Time.time;
        }
        float dDeltaRotation = fDeltaTime * m_fDustAngularVelocity;


        bool bCheckIfInCamView = true;
        m_fCheckInCamViewTimeCount += Time.deltaTime;
        if (m_fCheckInCamViewTimeCount >= 0.3f)
        {
            m_fCheckInCamViewTimeCount = 0f;
            bCheckIfInCamView = true;
        }
        else
        {
            bCheckIfInCamView = false;
        }
        for (int i = 0; i < m_lstDust.Count; i++)
        {
            CDust dust = m_lstDust[i];
            dust.Move(dDeltaRotation, bCheckIfInCamView);
           // dust.MoveToSupposedPos(bCheckIfInCamView);
        }
    }

    void CalculateOrbitPos()
    {

    }

    /*
    void MoveToOrbitPos()
    {
        vecTempDirection = m_vecPosInOrbit - dust.transform.position;
        
        vecTempDirection.Normalize();
        float fDeltaX = m_fMoveToOrbitSpeed * vecTempDirection.x;
        float fDeltaY = m_fMoveToOrbitSpeed * vecTempDirection.y;
        vecTempPos = dust.transform.position;
        vecTempPos.x += fDeltaX;
        vecTempPos.y += fDeltaY;
        vec2TempPos= vecTempPos;
        dust.MoveToPosition(vec2TempPos);
    }
    */


    public void SaveDust(XmlDocument xmlDoc, XmlNode node, float fClassCircleRadius)
    {
        XmlNode nodeNum = StringManager.CreateNode(xmlDoc, node, "Num");
        nodeNum.InnerText = m_nDustNum.ToString();

        XmlNode nodeDustMaxSize = StringManager.CreateNode(xmlDoc, node, "DustMaxSize");
        nodeDustMaxSize.InnerText = m_fDustMaxSize.ToString( "f1" );
        XmlNode nodeDustMinSize = StringManager.CreateNode(xmlDoc, node, "DustMinSize");
        nodeDustMinSize.InnerText = m_fDustMinSize.ToString("f1");

        XmlNode nodeDustBeltSize = StringManager.CreateNode(xmlDoc, node, "DustBeltSize");
        nodeDustBeltSize.InnerText = m_fDustBeltSize.ToString("f1");

        XmlNode nodeAngularVelocit = StringManager.CreateNode(xmlDoc, node, "DustAngularVelocity");
        nodeAngularVelocit.InnerText = m_fDustAngularVelocity.ToString("f4");

        XmlNode nodeBackToOrbitVelocity = StringManager.CreateNode(xmlDoc, node, "DustBackToOrbitVelocity");
        nodeBackToOrbitVelocity.InnerText = m_fDustBackToOrbitVelocity.ToString("f4");

        XmlNode nodeMass = StringManager.CreateNode(xmlDoc, node, "Mass");
        nodeMass.InnerText = m_fDustMass.ToString("f1");


        XmlNode nodeDusts = StringManager.CreateNode(xmlDoc, node, "Dusts");
        for (int i = 0; i < m_nDustNum; i++)
        {
            int nPreIdx = UnityEngine.Random.Range(0, 4);
            //CDust dust = GameObject.Instantiate(m_aryDustPrefabs[nPreIdx]).GetComponent<CDust>();
            float fRadius = (float)UnityEngine.Random.Range(fClassCircleRadius - m_fDustBeltSize, fClassCircleRadius + m_fDustBeltSize);
            float fSize = (float)UnityEngine.Random.Range(m_fDustMinSize, m_fDustMaxSize);
            //dust.SetRadius(fRadius);
            float fRotation = (float)UnityEngine.Random.Range(0, 360);
            vec2TempPos.x = fRadius * Mathf.Cos(fRotation);
            vec2TempPos.y = fRadius * Mathf.Sin(fRotation);
            //dust.SetStartPos(vec2TempPos, fRotation);
            //dust.SetSize( fSize);
            //m_lstDust.Add(dust);
            string szValue = nPreIdx + "," + fRadius.ToString("f1") + "," + fRotation.ToString("f1") + "," + fSize.ToString("f1") + "," + vec2TempPos.x.ToString("f1") + "," + vec2TempPos.y.ToString("f1");
            StringManager.CreateNode(xmlDoc, nodeDusts, "D", szValue );
            
        }
    }

    public void RecordDustNode(XmlNode nodeStarDust)
    {
        m_xmlNode = nodeStarDust;
        m_bMapConfigLoaded = true;
    }

    public void MarkGameStartTimeSet()
    {
        m_bGameStartTimeSet = true;
    }

    public void GenerateDust_NotLoadXml()
    {
        if (!m_bGenerated || !m_bMapConfigLoaded)
        {
            return;
        }

        if (AccountManager.m_eSceneMode != AccountManager.eSceneMode.MapEditor)
        {
            return;
        }

        for ( int i = 0; i < m_lstDust.Count; i++ )
        {
            GameObject.Destroy(m_lstDust[i].gameObject);
        }
        m_lstDust.Clear();
        for (int i = 0; i < m_nDustNum; i++)
        {
            int nPreIdx = UnityEngine.Random.Range(0, 4);
            CDust dust = GameObject.Instantiate(m_aryDustPrefabs[nPreIdx]).GetComponent<CDust>();
            float fRadius = (float)UnityEngine.Random.Range(CClassEditor.s_Instance.GetClassCircleRadius() - m_fDustBeltSize, CClassEditor.s_Instance.GetClassCircleRadius() + m_fDustBeltSize);
            float fSize = (float)UnityEngine.Random.Range( m_fDustMinSize, m_fDustMaxSize);
            dust.SetRadius(fRadius);
            float fRotation = (float)UnityEngine.Random.Range(0, 360);
            vec2TempPos.x = fRadius * Mathf.Cos(fRotation);
            vec2TempPos.y = fRadius * Mathf.Sin(fRotation);
            dust.SetStartPos(vec2TempPos, fRotation);
            dust.SetSize( fSize);
            dust.SetMass( m_fDustMass );
            m_lstDust.Add(dust);
        }


    }

    bool m_bGenerated = false;
    bool m_bMapConfigLoaded = false;
    bool m_bGameStartTimeSet = false;
    public void GenerateDust()
    {
        if (m_bGenerated || (!m_bMapConfigLoaded) )
        {
            return;
        }

        if ( AccountManager.m_eSceneMode == AccountManager.eSceneMode.Game )
        {
            if(!m_bGameStartTimeSet)
            {
                return;
            }
        }
        else if (AccountManager.m_eSceneMode == AccountManager.eSceneMode.MapEditor )
        {

        }
      

        

        if (m_xmlNode == null)
        {
            return;
        }

        m_bGenerated = true;

        for ( int i = 0; i < m_lstDust.Count; i++ )
        {
            CDust dust = m_lstDust[i];
            GameObject.Destroy( dust.gameObject );
        }
        m_lstDust.Clear();


        for (int i = 0; i < m_xmlNode.ChildNodes.Count; i++)
        {
            XmlNode node = m_xmlNode.ChildNodes[i];
            if (node.Name == "Num")
            {
                if ( !int.TryParse( node.InnerText, out m_nDustNum ) )
                {
                    m_nDustNum = 300;
                }

             }
            else if (node.Name == "DustMaxSize")
            {
                if (!float.TryParse(node.InnerText, out m_fDustMaxSize))
                {
                    m_fDustMaxSize = 10f;
                }
            }
            else if (node.Name == "DustMinSize")
            {
                if (!float.TryParse(node.InnerText, out m_fDustMinSize))
                {
                    m_fDustMinSize = 10f;
                }
            }
            else if (node.Name == "DustBeltSize")
            {
                if (!float.TryParse(node.InnerText, out m_fDustBeltSize))
                {
                    m_fDustBeltSize = 40f;
                }
            }
            else if (node.Name == "DustAngularVelocity")
            {
                if (!float.TryParse(node.InnerText, out m_fDustAngularVelocity))
                {
                    m_fDustAngularVelocity = 0.05f;
                }
            }
            else if (node.Name == "DustBackToOrbitVelocity")
            {
                if (!float.TryParse(node.InnerText, out m_fDustBackToOrbitVelocity))
                {
                    m_fDustBackToOrbitVelocity = 0.05f;
                }
            }
            else if (node.Name == "Mass")
            {
                if (!float.TryParse(node.InnerText, out m_fDustMass))
                {
                    m_fDustMass = 1f;
                }
            }
            else if (node.Name == "Dusts")
            {
                for (int j = 0; j < node.ChildNodes.Count; j++)
                {
                     XmlNode node_dust = node.ChildNodes[j];
                    string[] aryValues = node_dust.InnerText.Split(',');
                    int nPreIdx = int.Parse(aryValues[0]);
                    float fRadius = float.Parse(aryValues[1]);
                    float fRotation = float.Parse(aryValues[2]);
                    float fSize = float.Parse(aryValues[3]);
                    vec2TempPos.x = float.Parse(aryValues[4]);
                    vec2TempPos.y = float.Parse(aryValues[5]);
                    CDust dust = GameObject.Instantiate(m_aryDustPrefabs[nPreIdx]).GetComponent<CDust>();
                    dust.SetId( j );
                    dust.SetRadius(fRadius);
                    dust.SetSize(fSize);
                    dust.SetStartPos(vec2TempPos, fRotation);
                    dust.SetMass(m_fDustMass);
                    float fDeltaTime = Main.GetTime() - Main.s_Instance.GetGameStartTime();
                    float dDeltaRotation = fDeltaTime * m_fAngularVElocity;
                    dust.MoveDirectlly(dDeltaRotation);
                    m_lstDust.Add(dust);
                    dust.transform.SetParent( this.transform );
                }
            }
        } // end for i
        
        UpdateUI();
    }

    void UpdateUI()
    {
        _inputDustAngularVelocity.text = m_fDustAngularVelocity.ToString( "f4" );
        _inputDustBeltSize.text = m_fDustBeltSize.ToString( "f1" );
        _inputDustMaxSize.text = m_fDustMaxSize.ToString( "f1" );
        _inputDustMinSize.text = m_fDustMinSize.ToString("f1");
        _inputDustNum.text = m_nDustNum.ToString();
        _inputDustBackToOrbitVelocity.text = m_fDustBackToOrbitVelocity.ToString( "f4" );
        _inputDustMass.text = m_fDustMass.ToString("f1");
    }

    byte[] _bytesDustFullInfo = new byte[10240];
    float m_fSycnTimeCount = 0f;
    int m_nShit = 0;
    void SyncDustFullInfo()
    {
        return;

        if (!Main.IsMasterClient() || Main.s_Instance == null || Main.s_Instance.m_MainPlayer == null)
        {
            return;
        }
        m_fSycnTimeCount += Time.deltaTime;
        if (m_fSycnTimeCount < 1f)
        {
            return;
        }
        m_fSycnTimeCount = 0f;
        StringManager.BeginPushData(_bytesDustFullInfo);
        StringManager.PushData_Short((short)m_nShit);
        StringManager.PushData_Float( Main.GetTime() );
        StringManager.PushData_Short((short)m_lstDust.Count);
        for ( int i = m_nShit; i < m_lstDust.Count; i+=1 )
        {
            CDust dust = m_lstDust[i];
            StringManager.PushData_Float( dust.GetPos().x );
            StringManager.PushData_Float(dust.GetPos().y  );
        }
        int nBlobSize = StringManager.GetBlobSize();
        byte[] bytes = StringManager.GetSuitableBlob(nBlobSize);
        Array.Copy(_bytesDustFullInfo, bytes, nBlobSize);
        Main.s_Instance.m_MainPlayer.SyncDustFullInfo(bytes);
        m_nShit++;
        if (m_nShit > 0)
        {
            m_nShit = 0;
        }
    }

    public void SetDustRotation(int idx, float fRotation)
    {
        if (idx >= m_lstDust.Count)
        {
            return;
        }
        CDust dust = m_lstDust[idx];
        dust.SetRotation(fRotation);
    }

    public void SetDustPos( int idx, Vector3 pos )
    {
        if ( idx >= m_lstDust.Count )
        {
            return;
        }
        CDust dust = m_lstDust[idx];
        dust.SetPos(pos);
        dust.SetPosInOrbitDirectlly( pos );
    }

    public void SetDustBeingSynced(int idx )
    {
        if (idx >= m_lstDust.Count)
        {
            return;
        }
        CDust dust = m_lstDust[idx];
        dust.SetSyncedTime(0.5f);
    }

    public void RefreshStarDust()
    {
        GenerateDust_NotLoadXml();
    }

    int m_nDustNum = 300;
    float m_fDustMaxSize = 10f;
    float m_fDustMinSize = 4f;
    float m_fDustBeltSize = 40f;
    float m_fDustAngularVelocity = 0.05f;
    float m_fDustMass = 1.0f;
    float m_fDustBackToOrbitVelocity = 1f;

    public void OnInputValueChanged_DustNum()
    {
        int val = 300;
        if (!int.TryParse(_inputDustNum.text, out val))
        {
            val = 300;
        }
        m_nDustNum = val;
        RefreshStarDust();
    }

    public void OnInputValueChanged_DustMaxSize()
    {
        float val = 10f;
        if (!float.TryParse(_inputDustMaxSize.text, out val))
        {
            val = 10f;
        }
        m_fDustMaxSize = val;
        RefreshStarDust();
    }

    public void OnInputValueChanged_DustMinSize()
    {
        float val = 4f;
        if (!float.TryParse(_inputDustMinSize.text, out val))
        {
            val = 4f;
        }
        m_fDustMinSize = val;
        RefreshStarDust();
    }

    public void OnInputValueChanged_DustBeltSize()
    {
        float val = 40f;
        if (!float.TryParse(_inputDustBeltSize.text, out val))
        {
            val = 40f;
        }
        m_fDustBeltSize = val;
        RefreshStarDust();
    }

    public void OnInputValueChanged_DustAngularVelocity()
    {
        float val = 0.05f;
        if (!float.TryParse(_inputDustAngularVelocity.text, out val))
        {
            val = 0.05f;
        }
        m_fDustAngularVelocity = val;
        RefreshStarDust();
    }

    public void OnInputValueChanged_DustBackToOrbitVelocity()
    {
        float val = 0.05f;
        if (!float.TryParse(_inputDustBackToOrbitVelocity.text, out val))
        {
            val = 0.05f;
        }
        m_fDustBackToOrbitVelocity = val;
        RefreshStarDust();
    }

    public void OnInputValueChanged_DustMass()
    {
        float val = 1f;
        if (!float.TryParse(_inputDustMass.text, out val))
        {
            val = 1f;
        }
        m_fDustMass = val;
        RefreshStarDust();
    }

    public float GetBackToOrbitSpeed()
    {
        return m_fDustBackToOrbitVelocity;
    }
}
