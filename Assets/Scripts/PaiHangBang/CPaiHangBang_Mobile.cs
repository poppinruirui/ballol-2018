﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class CPaiHangBang_Mobile : MonoBehaviour {

    public static CPaiHangBang_Mobile s_Instance;

    public GameObject m_preItem;

    public GameObject m_goHighLight;

    public GameObject goList;
    public GameObject goItemContainer;


    public float m_fMoveSpeed = 1f;
    public float m_fVerticalSpace = 36;
    public int m_nTotalNum = 10;
    public float m_fListHeight = 600;
    

    bool m_bMovingDown = false;
    bool m_bMovingUp = false;



    static Vector3 vecTemp = new Vector3();

    List<CPaiHangBangItem_Mobile> m_lstItems = new List<CPaiHangBangItem_Mobile>();
    public  CPaiHangBangItem_Mobile[] m_aryItems;

    private void Awake()
    {
        s_Instance = this;
    }

    // Use this for initialization
    void Start () {
        
        /*
        for ( int i = 0; i < m_nTotalNum; i++ )
        {
            CPaiHangBangItem_Mobile item = GameObject.Instantiate(m_preItem).GetComponent<CPaiHangBangItem_Mobile>();
            vecTemp.x = 0f;
            vecTemp.y = -m_fVerticalSpace * i;
            vecTemp.z = 0f;
            item.transform.SetParent(goList.transform );
            item.SetPos(vecTemp);
            m_lstItems.Add( item );
        }
        */
	}
	
	// Update is called once per frame
	void Update () {
        Moving_Down();
        Moving_Up();
        RefreshItemsInfo();
    }

    public void OnClickButton_ShowPaiHangBang()
    {
        BeginMove_Down();
    }

    public void OnClickButton_HidePaiHangBang()
    {
        BeginMove_Up();
    }

    public void BeginMove_Down()
    {
        m_bMovingDown = true;

        EndMove_Up();
    }

    void Moving_Down()
    {
        if ( !m_bMovingDown)
        {
            return;
        }

        vecTemp = goList.transform.localPosition;
        vecTemp.y -= m_fMoveSpeed * Time.deltaTime;
        if (vecTemp.y <= 0f )
        {
            vecTemp.y = 0f;
            EndMove_Down();
        }
       goList.transform.localPosition = vecTemp;
    }

    void EndMove_Down()
    {
        m_bMovingDown = false;
    }

    public void BeginMove_Up()
    {
        m_bMovingUp = true;

        EndMove_Down();
    }

    void Moving_Up()
    {
        if (!m_bMovingUp)
        {
            return;
        }

        vecTemp = goList.transform.localPosition;
        vecTemp.y += m_fMoveSpeed * Time.deltaTime;
        if (vecTemp.y >= m_fListHeight)
        {
            vecTemp.y = m_fListHeight;
            EndMove_Up();
        }
        goList.transform.localPosition = vecTemp;

    }

    void EndMove_Up()
    {
        m_bMovingUp = false;
    }


    public struct sPlayerAccount
    {
        public string szName;
        public int nOwnerId;
        public float fTotalVolume;
        public int nLevel;
        public int nEatThornNum;
        public int nKillNum;
        public int nBeKilledNum;
        public int nAssistNum;
        public int nItem0Num;
        public int nItem1Num;
        public int nItem2Num;
        public int nItem3Num;
    };
    float c_fRefreshPlayerListInterval = 3f;
    float m_fRefreshPlayerListTimeCount = 0f;
    List<sPlayerAccount> lstTempPlayerAccount = new List<sPlayerAccount>();

    void RefreshItemsInfo()
    {
        if (Main.s_Instance.m_nPlatform != 1)
        {
            return;
        }

        m_fRefreshPlayerListTimeCount += Time.deltaTime;
        if (m_fRefreshPlayerListTimeCount < c_fRefreshPlayerListInterval)
        {
            return;
        }
        m_fRefreshPlayerListTimeCount = 0f;


        lstTempPlayerAccount.Clear();

        for (int i = 0; i < m_aryItems.Length; i++)
        {
            CPaiHangBangItem_Mobile item = m_aryItems[i];
            item.gameObject.SetActive(false);
        }

     //   m_goHighLight.SetActive( false );

        for (int i = 0; i < PhotonNetwork.playerList.Length; i++)
        {
            PhotonPlayer photon_player = PhotonNetwork.playerList[i];
            sPlayerAccount info;
            info.szName = photon_player.NickName;
            info.nOwnerId = photon_player.ID;
            Player player = CPlayerManager.s_Instance.GetPlayer(photon_player.ID);
            if (player == null)
            {
                continue;
            }
            int nNumOfLiveBalls = 0;
            info.fTotalVolume = player.GetTotalVolume(ref nNumOfLiveBalls);
            info.nLevel = player.GetLevel();
            info.nEatThornNum = player.GetEatThornNum();
            info.nKillNum = player.GetKillCount();
            info.nBeKilledNum = player.GetBeKilledCount();
            info.nAssistNum = player.GetAssistAttackCount();
            info.nItem0Num = player.GetItemNum(0);
            info.nItem1Num = player.GetItemNum(1);
            info.nItem2Num = player.GetItemNum(2);
            info.nItem3Num = player.GetItemNum(3);
            // insert sort
            bool bInserted = false;
            for (int j = 0; j < lstTempPlayerAccount.Count; j++)
            {
                if (info.fTotalVolume > lstTempPlayerAccount[j].fTotalVolume)
                {
                    lstTempPlayerAccount.Insert(j, info);
                    bInserted = true;
                    break;
                }
            } // end j
            if (!bInserted)
            {
                lstTempPlayerAccount.Add(info);
            }

        } // end i

        int nMainPlayerRank = 0;
        
        for (int i = 0; i < lstTempPlayerAccount.Count; i++)
        {
            if (i >= m_aryItems.Length)
            {
                continue;
            }
            if (Main.s_Instance && Main.s_Instance.m_MainPlayer)
            {
                if (lstTempPlayerAccount[i].nOwnerId == Main.s_Instance.m_MainPlayer.GetOwnerId())
                {
                    nMainPlayerRank = i + 1;
                   // vecTemp = m_goHighLight.transform.localPosition;
                   // vecTemp.y = -i * m_fVerticalSpace;
                   // m_goHighLight.SetActive(true);
                }
            }
            CPaiHangBangItem_Mobile item = m_aryItems[i];
            item.gameObject.SetActive(true);
            item.SetPlayerName(lstTempPlayerAccount[i].szName);
        }
        

        CGrowSystem.s_Instance.SetRank(nMainPlayerRank, lstTempPlayerAccount.Count);
    }
}
