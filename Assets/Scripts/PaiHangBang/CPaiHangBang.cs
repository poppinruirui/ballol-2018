﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CPaiHangBang : MonoBehaviour {

    public GameObject m_goPanel;

    public GameObject m_prePaiHangBangItem;

    public float m_fItemYGap;
    public float m_fItemColGap;
    public int m_nNumOfCol;

    public GameObject m_goContainer;
    static Vector3 vecTempPos = new Vector3();
    static Vector3 vecTempScale = new Vector3();

    public static CPaiHangBang s_Instance = null;

    List<CPaiHangBangItem> m_lstItems = new List<CPaiHangBangItem>();

    private void Awake()
    {
        s_Instance = this;
    }

    // Use this for initialization
    void Start()
    {
        Init();
    }

    // Update is called once per frame
    void Update()
    {

    }

    public void SetVisible( bool bVisible )
    {
        m_goPanel.SetActive( bVisible );
        if ( bVisible )
        {
            RefreshItemsInfo();
        }
    }

    void Init()
    {
        vecTempScale.x = 1;
        vecTempScale.y = 1;
        vecTempScale.z = 1;

        // 第一列
        for (int i = 0; i < m_nNumOfCol; i++)
        {
            CPaiHangBangItem item = GameObject.Instantiate(m_prePaiHangBangItem).GetComponent<CPaiHangBangItem>();
            item.transform.SetParent(m_goContainer.transform);
            vecTempPos.x = 0f;
            vecTempPos.y = -m_fItemYGap * i;
            vecTempPos.z = 0f;
            item.transform.localPosition = vecTempPos;
            item.transform.localScale = vecTempScale;
            m_lstItems.Add(item);
            if ( i == 0 )
            {
                item.SetNo( "1st" );
            }
            else if (i == 1)
            {
                item.SetNo("2nd");
            }
            else if (i == 2)
            {
                item.SetNo("3rd");
            }
            else
            {
                item.SetNo( (i+1).ToString());
            }


        }

        // 第二列
        for (int i = 0; i < m_nNumOfCol; i++)
        {
            CPaiHangBangItem item = GameObject.Instantiate(m_prePaiHangBangItem).GetComponent<CPaiHangBangItem>();
            item.transform.SetParent(m_goContainer.transform);
            vecTempPos.x = m_fItemColGap;
            vecTempPos.y = -m_fItemYGap * i;
            vecTempPos.z = 0f;
            item.transform.localPosition = vecTempPos;
            item.transform.localScale = vecTempScale;
            m_lstItems.Add(item);
        }
    }

    public struct sPlayerAccount
    {
        public string szName;
        public int nOwnerId;
        public float fTotalVolume;
        public int nLevel;
        public int nEatThornNum;
        public int nKillNum;
        public int nBeKilledNum;
        public int nAssistNum;
        public int nItem0Num;
        public int nItem1Num;
        public int nItem2Num;
        public int nItem3Num;
    };
    float c_fRefreshPlayerListInterval = 3f;
    float m_fRefreshPlayerListTimeCount = 0f;
    List<sPlayerAccount> lstTempPlayerAccount = new List<sPlayerAccount>();

    void RefreshItemsInfo()
    {
        lstTempPlayerAccount.Clear();

        for ( int i = 0; i < m_lstItems.Count; i++ )
        {
            CPaiHangBangItem item = m_lstItems[i];
            item.gameObject.SetActive( false );
        }

        for (int i = 0; i < PhotonNetwork.playerList.Length; i++)
        {
            PhotonPlayer photon_player = PhotonNetwork.playerList[i];
            sPlayerAccount info;
            info.szName = photon_player.NickName;
            info.nOwnerId = photon_player.ID;
            Player player = CPlayerManager.s_Instance.GetPlayer(photon_player.ID);
            if (player == null)
            {
                Debug.LogError("RefreshPlayerList() bug, player null!");
                continue;
            }
            int nNumOfLiveBalls = 0;
            info.fTotalVolume = player.GetTotalVolume(ref nNumOfLiveBalls);
            info.nLevel = player.GetLevel();
            info.nEatThornNum = player.GetEatThornNum();
            info.nKillNum = player.GetKillCount();
            info.nBeKilledNum = player.GetBeKilledCount();
            info.nAssistNum = player.GetAssistAttackCount();
            info.nItem0Num = player.GetItemNum(0);
            info.nItem1Num = player.GetItemNum(1);
            info.nItem2Num = player.GetItemNum(2);
            info.nItem3Num = player.GetItemNum(3);
            // insert sort
            bool bInserted = false;
            for (int j = 0; j < lstTempPlayerAccount.Count; j++)
            {
                if (info.fTotalVolume > lstTempPlayerAccount[j].fTotalVolume)
                {
                    lstTempPlayerAccount.Insert(j, info);
                    bInserted = true;
                    break;
                }
            } // end j
            if (!bInserted)
            {
                lstTempPlayerAccount.Add(info);
            }

        } // end i

        for ( int i = 0; i < lstTempPlayerAccount.Count; i++ )
        {
            if ( i >= m_lstItems.Count)
            {
                return;
            }
            CPaiHangBangItem item = m_lstItems[i];
            item.gameObject.SetActive( true );
            item.SetName(lstTempPlayerAccount[i].szName);
            item.SetTotalVolume(lstTempPlayerAccount[i].fTotalVolume.ToString("f0"));
            item.SetLevel(lstTempPlayerAccount[i].nLevel );
            item.SetJiShaInfo(lstTempPlayerAccount[i].nKillNum, lstTempPlayerAccount[i].nBeKilledNum, lstTempPlayerAccount[i].nAssistNum);
            item.SetEatThornCount(lstTempPlayerAccount[i].nEatThornNum);
            item.SetItemNum(lstTempPlayerAccount[i].nItem0Num, lstTempPlayerAccount[i].nItem1Num, lstTempPlayerAccount[i].nItem2Num, lstTempPlayerAccount[i].nItem3Num);
        }
    }
}
