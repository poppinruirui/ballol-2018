﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class SystemMsg : MonoBehaviour {

	static Color cTempColor = new Color ();

	public Text _txtContent;

	const float c_fShowTime = 5f;
	const float c_fFadeTime = 5f;
	float m_fCouting = 0f;
	bool m_bShowing = false;
	bool m_bFadinging = false;

	// Use this for initialization
	void Start () {
		
	}
	
	// Update is called once per frame
	void Update () {
		Fadind ();
		Counting ();
	}

	public void SetContent( string szContent )
	{
		_txtContent.text = szContent;
		BeginCount ();
	}

	void SetAlpha( float val )
	{
		cTempColor = _txtContent.color;
		cTempColor.a = val;
		_txtContent.color = cTempColor;
	}

	float GetAlpha()
	{
		return _txtContent.color.a;
	}

	void BeginCount()
	{
		m_bShowing = true;
		SetAlpha ( 1f );
		m_fCouting = 0;
	}

	void Counting()
	{
		if (!m_bShowing) {
			return;
		}
		m_fCouting += Time.deltaTime;
		if (m_fCouting >= c_fShowTime) {
			EndCounting ();
		}
	}

	void EndCounting()
	{
		m_bShowing = false;
		BeginFade ();
	}

	void BeginFade()
	{
		m_bFadinging = true;
	}

	void Fadind()
	{
		if (!m_bFadinging) {
			return;
		}

		float fAlpha = GetAlpha ();
		fAlpha -= Time.deltaTime;
		SetAlpha ( fAlpha );
		if (fAlpha <= 0f) {
			EndFade ();
		}
	}

	void EndFade()
	{
		m_bFadinging = false;
	}

}
