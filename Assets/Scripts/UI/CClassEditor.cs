﻿/*
 * 阶级圈编辑器
*/
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using System.Xml;

public class CClassEditor : MonoBehaviour {

	public CMonsterEditor _MonsterEditor;

	static Vector3 vecTempScale = new Vector3();
	static Vector3 vecTempPos = new Vector3();
    static Vector3 vecTempPos1 = new Vector3();
    static Vector2 vec2Temp = new Vector2();

    //public GameObject _goOutterClassCircle;
    //public GameObject _goInnerClassCircle;
    public GameObject _goClassCircle; // 废弃了多层的阶级圈机制。采用只有一层的阶级圈
    public CMonster _cShengFuPanDingBall;

    public GameObject _goContainer_MannualMonster;

    public InputField _inputTimeOfOneGame;
    //public InputField _inputOutterClassCircleRadius;
    //public InputField _inputInnerClassCircleRadius;
    public InputField _inputClassCircleRadius;
    public InputField _inputShengFuPanDingBallSize;
	public Dropdown _dropArea;
	public Dropdown _dropThornType;
    public Dropdown _dropThornId;
    public InputField _inputSizeThreshold;
	public InputField _inputDensity;
	public InputField _inputRebornTime;
    public InputField _inputRebornTimeMannual;
    public InputField _inputNormalAttenuate;
	public InputField _inputInvasionAttenuate;
    public InputField _inputInvasionSpeedSlowDown;
    public InputField _inputWarFogRadius;

    public Button _btnAddOneMonster;
    public Button _btnDeleteOneMonster;
    public Text _txtMonsterNum;

    float m_fTimeOfOneGame = 0f;
    //float m_fOutterClassCircleRadius = 0f;
    //float m_fInnerClassCircleRadius = 0f;
    float m_fClassCircleRadius = 0f;
    float m_fShengFuPanDingBallSize;

    float m_fLowThreshold = 0f;
    float m_fMiddleThreshold = 0f;
	float m_fHighThreshold = 0f;

    float m_fWarFogRadius = 0f;

	public struct sClassConfig
	{
		public int nId;
		public float fSizeThreshold;
		public float fNormalAttenuate;
		public float fInvasionAttenuate;
        public float fInvasionSpeedSlowdown;
        public List<sThornOfThisClassConfig > lstThorns;
	};
	Dictionary<int, sClassConfig> m_dicClassConfig = new Dictionary<int, sClassConfig>();


	sClassConfig m_CurConfig;
	sThornOfThisClassConfig m_CurThornConfig;


    public struct sThornOfThisClassConfig
	{
		public string szThornId;
		public float fDensity;
		public float fRebornTime;
	};

    public struct sMannualMonsterConfig
    {
        public string szMonsterId;
        public float fRebornTime;
        public List<Vector3> lstPos;
    };
    sMannualMonsterConfig m_CurMannualMonsterConfig;
    Dictionary<string, sMannualMonsterConfig> m_dicMannualMonsterConfig = new Dictionary<string, sMannualMonsterConfig>();
    sMannualMonsterConfig tempMannualMonsterConfig;

    public static CClassEditor s_Instance;

    CMonster m_CurMovingMonster = null;
    CMonster m_CurSelectedMonster = null;

    void Awake()
	{
		s_Instance = this;
	}

	// Use this for initialization
	void Start () {
		InitDropdown_ClassType();
		InitDropdown_ThornType ();
        InitDropdown_ThornID();
        string szThornId = "0_0";
        m_CurMannualMonsterConfig = GetMannualMonsterConfigById(szThornId);
        UpdateUiContent();
	}
	
    public void SelectMonster( CMonster monster )
    {
        m_CurMovingMonster = monster;
        m_CurSelectedMonster = monster;
    }

    bool m_bMouseDown = false;
    Vector3 m_vecLastMouseWorldPos = new Vector3();
    void ProcessKeyInput()
    {
        if (AccountManager.m_bInGame)
        {
            return;
        }

        if ( Input.GetKeyDown( KeyCode.Escape ) )
        {
            m_CurSelectedMonster = null;
        }
    }

    void ProcessMouseInput()
    {
        if ( AccountManager.m_bInGame )
        {
            return;
        }

        if ( UIManager.IsPointerOverUI() )
        {
            return;
        }

        if ( Input.GetMouseButton(0) ) // 鼠标左键持续按下状态
        {
            if (m_CurMovingMonster)
            {
                vecTempPos = Camera.main.ScreenToWorldPoint(Input.mousePosition);
                m_CurMovingMonster.SetPos(vecTempPos);
            }
        }

        if ( Input.GetMouseButtonDown(0) ) // 点下鼠标左键
        {
            m_bMouseDown = true;
        }

        if( Input.GetMouseButtonUp(0) ) // 放开鼠标左键
        {
            m_CurMovingMonster = null;
        }
    }

	// Update is called once per frame
	void Update () {
		ToRebornLoop();
        ProcessMouseInput();
        CheckInCamViewLoop();
    }

	public void InitDropdown_ClassType()
	{
		List<string> showNames = new List<string>();

		showNames.Add( "0 - 菜鸟区");
		showNames.Add( "1 - 高手区");

		UIManager.UpdateDropdownView( _dropArea, showNames);
	}

	public void InitDropdown_ThornType()
	{
		List<string> showNames = new List<string>();

		showNames.Add("0 - 刺");
        showNames.Add("1 - 豆");
        showNames.Add("2 - 球");
        showNames.Add("3 - 孢");
        showNames.Add("4 - 技");

        UIManager.UpdateDropdownView( _dropThornType, showNames);
    }

    public void InitDropdown_ThornID()
    {
        List<string> showNames = new List<string>();

        showNames.Add("0");
        showNames.Add("1");
        showNames.Add("2");
        showNames.Add("3");
        showNames.Add("4");
        showNames.Add("5");
        showNames.Add("6");
        showNames.Add("7");
        showNames.Add("8");
        showNames.Add("9");
        showNames.Add("10");
        showNames.Add("11");
        showNames.Add("12");
        showNames.Add("13");
        showNames.Add("14");
        showNames.Add("15");
        showNames.Add("16");
        showNames.Add("17");
        showNames.Add("18");
        showNames.Add("19");
        showNames.Add("20");
        showNames.Add("21");
        showNames.Add("22");
        showNames.Add("23");
        showNames.Add("24");
        showNames.Add("25");
        showNames.Add("26");
        showNames.Add("27");
        showNames.Add("28");
        showNames.Add("29");
        showNames.Add("30");
        showNames.Add("31");
        UIManager.UpdateDropdownView(_dropThornId, showNames);
    }

    public sClassConfig GetClassConfigById( int nId )
	{
		sClassConfig config;
		if (!m_dicClassConfig.TryGetValue (nId, out config)) {
			config = new sClassConfig ();
			config.nId = nId;
			m_dicClassConfig [nId] = config;
		}
		return config;
	}

    public sMannualMonsterConfig GetMannualMonsterConfigById( string szId )
    {
        if ( !m_dicMannualMonsterConfig.TryGetValue(szId, out tempMannualMonsterConfig ) )
        {
            tempMannualMonsterConfig = new sMannualMonsterConfig();
            tempMannualMonsterConfig.szMonsterId = szId;
            tempMannualMonsterConfig.lstPos = new List<Vector3>();
            m_dicMannualMonsterConfig[tempMannualMonsterConfig.szMonsterId] = tempMannualMonsterConfig;

        }
        return tempMannualMonsterConfig;
    }


    sThornOfThisClassConfig tempThornConfig;
	public sThornOfThisClassConfig GetThornConfigById( sClassConfig classConfig, string szId )
	{
		if (classConfig.lstThorns == null) {
			classConfig.lstThorns = new List<sThornOfThisClassConfig> ();
		}
		bool bFound = false;
		for (int i = 0; i < classConfig.lstThorns.Count; i++) {
			tempThornConfig = classConfig.lstThorns [i];
			if (tempThornConfig.szThornId == szId) {
				bFound = true;
				break;
			}
		}
		if (!bFound) {
			tempThornConfig = new sThornOfThisClassConfig ();
			tempThornConfig.szThornId = szId;
			classConfig.lstThorns.Add( tempThornConfig);
            m_dicClassConfig[classConfig.nId] = classConfig;

        }
		return tempThornConfig;
	}

	public void OnDropDownValueChanged_ClassType()
	{
		m_CurConfig = GetClassConfigById ( _dropArea.value );
        string szMonsterId = _dropThornType.value + "_" + _dropThornId.value;
        m_CurThornConfig = GetThornConfigById(m_CurConfig, szMonsterId);
        m_CurMannualMonsterConfig = GetMannualMonsterConfigById(szMonsterId);
		UpdateUiContent ();
	}

	public void OnDropDownValueChanged_ThornType()
	{
        string szMonsterId = _dropThornType.value + "_" + _dropThornId.value;
        m_CurThornConfig = GetThornConfigById( m_CurConfig, szMonsterId);
        m_CurMannualMonsterConfig = GetMannualMonsterConfigById(szMonsterId);
        UpdateUiContent ();
	}

    public void OnDropDownValueChanged_ThornId()
    {
        string szMonsterId = _dropThornType.value + "_" + _dropThornId.value;
        m_CurThornConfig = GetThornConfigById(m_CurConfig, szMonsterId);
        m_CurMannualMonsterConfig = GetMannualMonsterConfigById(szMonsterId);
        UpdateUiContent();
    }


    void UpdateUiContent()
	{
		_inputTimeOfOneGame.text = m_fTimeOfOneGame.ToString ();
        //_inputOutterClassCircleRadius.text = m_fOutterClassCircleRadius.ToString ();
        //_inputInnerClassCircleRadius.text = m_fInnerClassCircleRadius.ToString ();
        _inputClassCircleRadius.text = m_fClassCircleRadius.ToString();
        _inputShengFuPanDingBallSize.text = m_fShengFuPanDingBallSize.ToString ();
			

		_inputSizeThreshold.text = m_CurConfig.fSizeThreshold.ToString ();
		_inputDensity.text = m_CurThornConfig.fDensity.ToString ();
		_inputRebornTime.text = m_CurThornConfig.fRebornTime.ToString();
		_inputNormalAttenuate.text = m_CurConfig.fNormalAttenuate.ToString ();
		_inputInvasionAttenuate.text = m_CurConfig.fInvasionAttenuate.ToString ();
        _inputInvasionSpeedSlowDown.text = m_CurConfig.fInvasionSpeedSlowdown.ToString();
        _inputRebornTimeMannual.text = m_CurMannualMonsterConfig.fRebornTime.ToString();

        RefreshMonsterNum();
    }

	public void OnInputValueChanged_TimeOfOneGame()
	{
		m_fTimeOfOneGame = float.Parse ( _inputTimeOfOneGame.text );
	}
    /*
	public void OnInputValueChanged_OutterClassCircleRadius()
	{
		SetClassCircleRadius( _goOutterClassCircle, float.Parse ( _inputOutterClassCircleRadius.text ), ref m_fOutterClassCircleRadius );
	}

	public void OnInputValueChanged_InnerClassCircleRadius()
	{
		SetClassCircleRadius( _goInnerClassCircle, float.Parse ( _inputInnerClassCircleRadius.text ), ref m_fInnerClassCircleRadius );
	}
    */
    public void OnInputValueChanged_ClassCircleRadius()
    {
        SetClassCircleRadius(_goClassCircle, float.Parse(_inputClassCircleRadius.text), ref m_fClassCircleRadius);
    }

    public void SetClassCircleRadius( GameObject circle, float val, ref float radius )
	{
		radius = val;

		// 所见即所得，在画面上实时反映圈的尺寸的变化(半径值乘以2，就正好是transform.localScale需要的值)
		float fScale = val * 2f;
		vecTempScale.x = fScale;
		vecTempScale.y = fScale;
		vecTempScale.z = 1f;
		circle.transform.localScale = vecTempScale;

        RefreshMonsterNum();
	}
    /*
	public float GetOutterClassCircleRadius( ){
		return m_fOutterClassCircleRadius;
	}

	public float GetInnerClassCircleRadius( ){
		return m_fInnerClassCircleRadius;
	}
    */
    public float GetClassCircleRadius()
    {
        return m_fClassCircleRadius;
    }

    public void OnInputValueChanged_ShengFuPanDingBallSize()
	{
		SetShengFuPanDingBallSize ( float.Parse( _inputShengFuPanDingBallSize.text ) );
	}

	public void SetShengFuPanDingBallSize( float val )
	{
		m_fShengFuPanDingBallSize = val;
		_cShengFuPanDingBall.SetSize ( CyberTreeMath.Radius2Scale( val ) );
	}

    public float GetShengFuPanDingBallRadius()
    {
        return m_fShengFuPanDingBallSize;
    }

    public void OnInputValueChanged_ClassThresholdSize()
	{
		m_CurConfig.fSizeThreshold = float.Parse ( _inputSizeThreshold.text );
		m_dicClassConfig [m_CurConfig.nId] = m_CurConfig;
	}

	public void SetThornConfigById( ref sClassConfig classConfig, string szId, ref sThornOfThisClassConfig config  )
	{
		if (classConfig.lstThorns == null) {
			classConfig.lstThorns = new List<sThornOfThisClassConfig> ();
		}

		bool bFound = false;
		for (int i = 0; i < classConfig.lstThorns.Count; i++) {
			tempThornConfig = classConfig.lstThorns [i];
			if (tempThornConfig.szThornId == szId) {
				classConfig.lstThorns[i] = config;
				bFound = true;
			}
		}
		if (!bFound) {
			classConfig.lstThorns.Add ( config );
		}
		m_dicClassConfig [classConfig.nId] = classConfig;
	}

	public void OnInputValueChanged_ThornDensity()
	{
		m_CurThornConfig.fDensity = float.Parse ( _inputDensity.text );
		SetThornConfigById( ref m_CurConfig, m_CurThornConfig.szThornId, ref m_CurThornConfig );
		m_dicClassConfig [m_CurConfig.nId] = m_CurConfig;

        // 密度变动的时候计算出预期刷出数量
        RefreshMonsterNum();

    }

    public void RefreshMonsterNum()
    {
        float fHighArea = Mathf.PI * m_fClassCircleRadius * m_fClassCircleRadius; // 高级区面积(高级区面积就是内圈面积　)
        float fWorldSizeX = MapEditor.s_Instance.GetWorldSizeX();
        float fWorldSizeY = MapEditor.s_Instance.GetWorldSizeY();
        float fWorldArea = MapEditor.s_Instance.GetWorldArea(); // 整个场景的面积
        float fLowArea = fWorldArea - fHighArea; //  整个场景面积减去圈面积(高级区面积)，就是低级区的面积
        int nNum = 0;
        if( m_CurConfig.nId == 0 ) // 低级区
        {
            nNum = (int)(fLowArea * m_CurThornConfig.fDensity);
        }
        else if (m_CurConfig.nId == 1) // 高级区
        {
            nNum = (int)(fHighArea * m_CurThornConfig.fDensity);
        }

        if ( nNum == 0 )
        {
            nNum = 1;
        }
        _txtMonsterNum.text = nNum.ToString();
    }

	public void OnInputValueChanged_RebornTime()
	{
		m_CurThornConfig.fRebornTime = float.Parse ( _inputRebornTime.text );
		SetThornConfigById(ref m_CurConfig, m_CurThornConfig.szThornId, ref m_CurThornConfig );
		m_dicClassConfig [m_CurConfig.nId] = m_CurConfig;
	}

    public void OnInputValueChanged_RebornTimeMannual()
    {
        m_CurMannualMonsterConfig.fRebornTime = float.Parse(_inputRebornTimeMannual.text);
        m_dicMannualMonsterConfig[m_CurMannualMonsterConfig.szMonsterId] = m_CurMannualMonsterConfig;
    }

    public void OnInputValueChanged_NormalAttenuate()
	{
		m_CurConfig.fNormalAttenuate = float.Parse ( _inputNormalAttenuate.text );
		m_dicClassConfig [m_CurConfig.nId] = m_CurConfig;
	}

    public void OnInputValueChanged_InvasionAttenuate()
    {
        m_CurConfig.fInvasionAttenuate = float.Parse(_inputInvasionAttenuate.text);
        m_dicClassConfig[m_CurConfig.nId] = m_CurConfig;
    }

    public void OnInputValueChanged_InvasionSpeedDown()
    {
        m_CurConfig.fInvasionSpeedSlowdown = float.Parse(_inputInvasionSpeedSlowDown.text);
        m_dicClassConfig[m_CurConfig.nId] = m_CurConfig;
    }

    public void GenerateMannualMonster( XmlNode node)
    {
        if (node == null)
        {
            return;
        }

        for ( int i = 0; i < node.ChildNodes.Count; i++ )
        {
            XmlNode sub_node = node.ChildNodes[i];
            string szMonsterId = sub_node.Name.Substring( 1, sub_node.Name.Length - 1) ;
            tempMannualMonsterConfig = GetMannualMonsterConfigById(szMonsterId);
            for ( int j = 0; j < sub_node.ChildNodes.Count; j++ )
            {
                XmlNode config_node = sub_node.ChildNodes[j];
                if ( config_node.Name == "RebornTime")
                {
                    tempMannualMonsterConfig.fRebornTime = float.Parse( config_node.InnerText );
                }
                else  if (config_node.Name == "PosLst")
                {
                    string[] aryPosLst = config_node.InnerText.Split( ',' );
                    for ( int k = 0; k < aryPosLst.Length; k+=2  )
                    {
                        vecTempPos.x = float.Parse(aryPosLst[k]);
                        vecTempPos.y = float.Parse(aryPosLst[k+1]);

                        tempMannualMonsterConfig.lstPos.Add(vecTempPos);

                        CMonster monster = ResourceManager.s_Instance.ReuseMonster();
                        monster.SetConfigId(szMonsterId);
                        monster.SetRebornTime(tempMannualMonsterConfig.fRebornTime);
                        monster.SetMonsterType(CMonster.eMonsterType.scene);
                        AddMonster(  monster ); // 加入到“怪”的总表中，便于统一管理

                        // poppin test
                        if (monster.GetMonsterBuildingType() ==   CMonsterEditor.eMonsterBuildingType.scene_ball)
                        {
                            vecTempPos.z = 500f;
                        }
                        else
                        {
                            vecTempPos.z = -monster.GetSize();
                        }

                        monster.SetPos(vecTempPos);
                        monster.SetInitPos(vecTempPos);
                        monster.transform.parent = _goContainer_MannualMonster.transform;
                    }
                }
            } // end j
            m_dicMannualMonsterConfig[tempMannualMonsterConfig.szMonsterId] = tempMannualMonsterConfig;
        } // end i

        m_CurMannualMonsterConfig = GetMannualMonsterConfigById("0_0");
        UpdateUiContent();
    }

    // 手工摆的怪单独保存，不要跟阶级区的信息混在一起
    public void SaveMannualMonster(XmlDocument xmlDoc, XmlNode node)
    {
        foreach (KeyValuePair<string, sMannualMonsterConfig> pair in m_dicMannualMonsterConfig)
        {
            if (pair.Value.lstPos!= null )
            {
                pair.Value.lstPos.Clear();
            }
        }


        foreach ( Transform child in _goContainer_MannualMonster.transform )
        {
            CMonster monster = child.gameObject.GetComponent<CMonster>();
            if ( !m_dicMannualMonsterConfig.TryGetValue(monster.GetConfigId(), out tempMannualMonsterConfig) )
            {
                tempMannualMonsterConfig = NewMannualMonsterConfig(monster.GetConfigId());
            }
            tempMannualMonsterConfig.lstPos.Add( monster.GetPos() );
        }

        foreach( KeyValuePair<string, sMannualMonsterConfig> pair in m_dicMannualMonsterConfig )
        {
            if (pair.Value.lstPos == null || pair.Value.lstPos.Count == 0)
            {
                continue;
            }

            XmlNode monster_node = StringManager.CreateNode(xmlDoc, node, "M" + pair.Key);
            StringManager.CreateNode(xmlDoc, monster_node, "RebornTime", pair.Value.fRebornTime.ToString());
            string szPosLst = "";
            for ( int i = 0; i < pair.Value.lstPos.Count; i++ )
            {
                if ( i != 0 )
                {
                    szPosLst += ",";
                }
                szPosLst += ( pair.Value.lstPos[i].x.ToString("f1") + "," + pair.Value.lstPos[i].y.ToString("f1"));

            } // end for i
            StringManager.CreateNode(xmlDoc, monster_node, "PosLst", szPosLst);
        } // end foreach
    }

    sMannualMonsterConfig NewMannualMonsterConfig( string szConfigId )
    {
        tempMannualMonsterConfig = new sMannualMonsterConfig();
        tempMannualMonsterConfig.szMonsterId = szConfigId;
        tempMannualMonsterConfig.lstPos = new List<Vector3>();
        m_dicMannualMonsterConfig[szConfigId] = tempMannualMonsterConfig;
        return tempMannualMonsterConfig;
    }

    public void SaveClass(XmlDocument xmlDoc, XmlNode node )
	{
        StringManager.CreateNode (xmlDoc, node, "TimeOfOneGame",  m_fTimeOfOneGame.ToString() );
        //StringManager.CreateNode (xmlDoc, node, "OutterCirlceRadius",  m_fOutterClassCircleRadius.ToString() );
        //StringManager.CreateNode (xmlDoc, node, "InnerCirlceRadius",  m_fInnerClassCircleRadius.ToString() );
        StringManager.CreateNode(xmlDoc, node, "CirlceRadius", m_fClassCircleRadius.ToString());
        StringManager.CreateNode(xmlDoc, node, "WarFogRadius", m_fWarFogRadius.ToString());
        StringManager.CreateNode (xmlDoc, node, "ShengFuSize",  m_fShengFuPanDingBallSize.ToString() );
		foreach (KeyValuePair<int, sClassConfig> pair in m_dicClassConfig) {

            if ( pair.Key == 2 )
            {
                continue;
            }

			XmlNode class_node = StringManager.CreateNode (xmlDoc, node, "Class" + pair.Key ); 
			StringManager.CreateNode (xmlDoc, class_node, "ClassId",  pair.Value.nId.ToString() );
			StringManager.CreateNode (xmlDoc, class_node, "Threshold",  pair.Value.fSizeThreshold.ToString() );
			StringManager.CreateNode (xmlDoc, class_node, "NormalAttenuate",  pair.Value.fNormalAttenuate.ToString() );
			StringManager.CreateNode (xmlDoc, class_node, "InvasionAttenuate",  pair.Value.fInvasionAttenuate.ToString() );
            StringManager.CreateNode(xmlDoc, class_node, "InvasionSpeedDown", pair.Value.fInvasionSpeedSlowdown.ToString());
            //foreach (KeyValuePair<int, sThornOfThisClassConfig> pairthorn in pair.Value.dicThorns )
            if (pair.Value.lstThorns != null)
            {
                for (int i = 0; i < pair.Value.lstThorns.Count; i++)
                {
                    tempThornConfig = pair.Value.lstThorns[i];
                    if (tempThornConfig.fDensity <= 0f)
                    {
                        continue;
                    }
                    XmlNode thorn_node = StringManager.CreateNode(xmlDoc, class_node, "Thorn" + tempThornConfig.szThornId);
                    StringManager.CreateNode(xmlDoc, thorn_node, "ThornId", tempThornConfig.szThornId.ToString());
                    StringManager.CreateNode(xmlDoc, thorn_node, "Density", tempThornConfig.fDensity.ToString());
                    StringManager.CreateNode(xmlDoc, thorn_node, "RebornTime", tempThornConfig.fRebornTime.ToString());
                } // end fori
            }
		}


	}

	public void SaveFakeRandomThornPos(XmlDocument xmlDoc, XmlNode node )
	{
		// 同时就要用“伪随机”机制把所有刺生成好.
		float fHighArea = Mathf.PI * m_fClassCircleRadius * m_fClassCircleRadius; // 高级区面积(高级区面积就是内圈面积　)
		float fWorldSizeX = MapEditor.s_Instance.GetWorldSizeX();
		float fWorldSizeY = MapEditor.s_Instance.GetWorldSizeY();
		float fWorldArea = MapEditor.s_Instance.GetWorldArea(); // 整个场景的面积
		float fLowArea = fWorldArea - fHighArea; //  整个场景面积减去圈面积(高级区面积)，就是低级区的面积
		float fArea = 0f;
		foreach (KeyValuePair<int, sClassConfig> pair in m_dicClassConfig) {
            if (pair.Key == 2)
            {
                continue;
            }

            if (pair.Key == 0)
            { // 低级区
                fArea = fLowArea;
            }
            else if (pair.Key == 1)
            { // 高级区
                fArea = fHighArea;
            }
			XmlNode class_node = StringManager.CreateNode (xmlDoc, node, "Class" + pair.Key );
            if (pair.Value.lstThorns != null)
            {
                for (int i = 0; i < pair.Value.lstThorns.Count; i++)
                {
                    tempThornConfig = pair.Value.lstThorns[i];
                    if (tempThornConfig.fDensity <= 0f)
                    {
                        continue;
                    }
                    float fNum = fArea * tempThornConfig.fDensity;
                    if ( fNum <= 0f )
                    {
                        fNum = 1;
                    }
                    XmlNode thorn_node = StringManager.CreateNode(xmlDoc, class_node, "T" + tempThornConfig.szThornId);
                    string szContent = "";
                    for (int j = 0; j < (int)fNum; j++)
                    {
                            if (pair.Key == 0)
                            { // 低级区
                                vecTempPos = GetRandomPos_LowArea();
                            }
                            else if (pair.Key == 1)
                            { // 高级区
                                vecTempPos = GetRandomPos_HighArea();
                            }
                    
                     

                            if (j != 0)
                            {
                                szContent += ",";
                            }
                            szContent += vecTempPos.x.ToString("f1") + "," + vecTempPos.y.ToString("f1");
                            thorn_node.InnerText = szContent;
                    } // end for j
                } // end for i
            } // end if
		} // end dic
	}

    // (伪)随机位置怪
	public void GenerateFakeRandonThorns( XmlNode nodeFakeRandom )
	{
		for (int i = 0; i < nodeFakeRandom.ChildNodes.Count; i++) {
			XmlNode class_node = nodeFakeRandom.ChildNodes [i];
			int nClassId = int.Parse( class_node.Name.Substring ( 5, 1 ) );
			sClassConfig class_config = GetClassConfigById ( nClassId );
			for (int j = 0; j < class_node.ChildNodes.Count; j++) {
				XmlNode thorn_node = class_node.ChildNodes [j];
				string  szThornConfigId = thorn_node.Name.Substring( 1, thorn_node.Name.Length - 1 );

				// 一个刺的自身配置
				CMonsterEditor.sThornConfig thorn_buildin_config = _MonsterEditor.GetMonsterConfigById (szThornConfigId);

				// 一个刺在应用场景中的配置
				sThornOfThisClassConfig thorn_in_class_config = GetThornConfigById( class_config, szThornConfigId );

				string[] aryPos = thorn_node.InnerText.Split ( ',' );
				for (int k = 0; k < aryPos.Length; k += 2) {
					vecTempPos.x = float.Parse( aryPos [k] );
					vecTempPos.y = float.Parse( aryPos [k+1] );
					vecTempPos.z = 0f;
                    CMonster thorn = ResourceManager.s_Instance.ReuseMonster().GetComponent<CMonster> ();
					thorn.SetDead (false);
					thorn.SetRebornTime ( thorn_in_class_config.fRebornTime );
					thorn.SetConfigId ( szThornConfigId );
					thorn.SetMonsterType (CMonster.eMonsterType.scene);
					thorn.SetSize (CyberTreeMath.Radius2Scale(thorn_buildin_config.fSelfSize) );
					thorn.transform.parent = Main.s_Instance.m_goThorns.transform;
                    if (thorn.GetMonsterBuildingType() == CMonsterEditor.eMonsterBuildingType.scene_ball)
                    {
                        vecTempPos.z = 500f; // 暂时这样写死，便于测试。今后规则敲定了再说
                    }
                    else
                    {
                        vecTempPos.z = -thorn.GetSize();
                    }
                    thorn.SetPos(vecTempPos);
                    thorn.SetInitPos(vecTempPos);
                    AddMonster ( thorn );
                    

                }
			}


		} // end i


    }

    // 吐的孢子和场景上的刺分开处理
    Dictionary<string, CMonster> m_dicAllSpores = new Dictionary<string, CMonster>(); // 场景中所有的孢子，每个孢子都有唯一的ID号
    Dictionary<long, CMonster> m_dicAllThorns = new Dictionary<long, CMonster>(); // 场景中所有的刺，每个刺都有唯一的ID号


    // 这两个列表纯粹为了位置排序(便于二分查找)
    List<CMonster> m_lstMonstersX = new List<CMonster>(); 
    List<CMonster> m_lstMonstersY = new List<CMonster>();  

    long m_lGuidCount = 1; // 0号怪是决胜球。其余一切怪都从1号开始编号
    public const int INVALID_MONSTER_GUID = -1;
	public void AddMonster( CMonster monster )
	{
        monster.SetGuid( m_lGuidCount++ );
		m_dicAllThorns [monster.GetGuid ()] = monster;
        InsertSortMonster(monster, 0, ref m_lstMonstersX);
        InsertSortMonster(monster, 1, ref m_lstMonstersY);
        //monster.SetActive( false );
    }

    public void ReInsertMonsterTosBinaryList( CMonster monster )
    {
        m_lstMonstersX.Remove( monster ) ;
        m_lstMonstersY.Remove( monster );
        int idx =  binary_greater_than_key(ref m_lstMonstersX, 0, monster.GetPos().x);
        if ( idx == -1 )
        {
            idx = 0;
        }
        m_lstMonstersX.Insert(idx, monster );
        idx = binary_greater_than_key(ref m_lstMonstersY, 1, monster.GetPos().y);
        if (idx == -1)
        {
            idx = 0;
        }
        m_lstMonstersY.Insert(idx, monster);
    }

    void InsertSortMonster( CMonster monster, int x_or_y, ref List<CMonster> lst )
    {
        bool bFound = false;
        for (int i = 0; i < lst.Count; i++)
        {
            CMonster node_monster = lst[i];
            float new_val = 0f;
            float node_val = 0f;
            if (x_or_y == 0 )
            {
                new_val = monster.GetPos().x;
                node_val = node_monster.GetPos().x;
            }
            else
            {
                new_val = monster.GetPos().y;
                node_val = node_monster.GetPos().y;
            }
            if (new_val < node_val)
            {
                lst.Insert(i, monster);
                bFound = true;
                break;
            }
        }
        if (!bFound)
        {
            lst.Add(monster);
        }
    }

    float m_fCheckInCamViewTimeCount = 0f;
    Dictionary<int, int> dicTemp = new Dictionary<int, int>();
    List<CMonster> m_lstActiveMonster = new List<CMonster>();
    void CheckInCamViewLoop()
    {
        return;

        if ( !MapEditor.s_Instance.IsMapInitCompleted() )
        {
            return;
        }

        if (m_lstMonstersX.Count == 0 || m_lstMonstersY.Count == 0 )
        {
            return;
        }

        m_fCheckInCamViewTimeCount += Time.deltaTime;
        if (m_fCheckInCamViewTimeCount < 0.5f)
        {
            return;
        }
        m_fCheckInCamViewTimeCount = 0f;

        CMonster monster = null;

        int idx_greater_than_screen_x_0 = binary_greater_than_key(ref m_lstMonstersX, 0, -50);
        int idx_less_than_screen_x_width = binary_less_than_key(ref m_lstMonstersX, 0, Screen.width + 50);
        int idx_greater_than_screen_y_0 = binary_greater_than_key(ref m_lstMonstersY, 1, -50);
        int idx_less_than_screen_y_height = binary_less_than_key(ref m_lstMonstersY, 1, Screen.height + 50);

        int nVal = 0;
        int nGuid = 0;
        dicTemp.Clear();
        dicTempActiveMonsterOfThisRound.Clear();
        for ( int i = idx_greater_than_screen_x_0; i <= idx_less_than_screen_x_width; i++ )
        {
            if ( i < 0 || i >= m_lstMonstersX.Count)
            {
                continue;
            }
            monster = m_lstMonstersX[i];
            if (monster == null)
            {
                continue;
            }
            if ( monster.IsDead() ) // 已死亡的野怪无论在不在视野范围内都不显示
            {
                continue;
            }
            nGuid = (int)monster.GetGuid();
            dicTemp[nGuid] = 1;
        }
        for ( int i = idx_greater_than_screen_y_0; i <= idx_less_than_screen_y_height; i++ )
        {
            if (i < 0 || i >= m_lstMonstersY.Count)
            {
                continue;
            }
            monster = m_lstMonstersY[i];
            if (monster == null)
            {
                continue;
            }
            if (monster.IsDead()) // 已死亡的野怪无论在不在视野范围内都不显示
            {
                continue;
            }
            nGuid = (int)monster.GetGuid();
            if ( dicTemp.TryGetValue(nGuid, out nVal) )
            {
                //AddActiveMonster( monster );
                dicTempActiveMonsterOfThisRound[(int)monster.GetGuid()] = monster;
            }
        }

        for ( int i = m_lstActiveMonster.Count - 1; i >= 0; i-- ) // 遍历上一轮的Active MOnster，看看这轮它是已经消失还是继续保持活跃
        {
            monster = m_lstActiveMonster[i];
            if ( monster == null )
            {
                m_lstActiveMonster.RemoveAt(i);
                Debug.LogError("anni?");
                continue;
            }
            nGuid = (int)monster.GetGuid();
            CMonster temp_monster = null;
            if (dicTempActiveMonsterOfThisRound.TryGetValue(nGuid, out temp_monster)) // 本轮继续活跃
            {
                dicTempActiveMonsterOfThisRound.Remove(nGuid);
            }
            else // 已消失
            {
                RemoveActiveMonster(monster);
            }
        }

        foreach ( KeyValuePair<int, CMonster> pair in dicTempActiveMonsterOfThisRound)
        {
            if (pair.Value == null)
            {
                continue;
            }
            AddActiveMonster(pair.Value);
        }
    }

    Dictionary<int, CMonster> dicTempActiveMonsterOfThisRound = new Dictionary<int, CMonster>();

    void AddActiveMonster( CMonster monster )
    {
        m_lstActiveMonster.Add( monster );
        monster.SetActive( true );
    }

    void RemoveActiveMonster(CMonster monster)
    {
        m_lstActiveMonster.Remove(monster);
        monster.SetActive(false);
    }

    //// 二分查找法
    float GetScreenPosOfMonster( ref List<CMonster> lst, int idx, int x_or_y )
    {
        if (idx >= lst.Count)
        {
            return -10000;
        }

        CMonster monster = lst[idx];
        if ( monster == null ) // ??
        {
            return -10000;
        }
        vecTempPos = monster.GetPos();
        vecTempPos = Camera.main.WorldToScreenPoint(vecTempPos);
        return (x_or_y == 0 ? vecTempPos.x : vecTempPos.y);
    }

    // 小于key值的最大元素
    int binary_less_than_key(ref List<CMonster> lst, int x_or_y, float key)
    {
        int low = 0;
        int high = lst.Count;
        float node_val = 0f;
        
        while (low < high)
        {
            int mid = (low + high + 1) / 2;
            node_val = GetScreenPosOfMonster(ref lst, mid, x_or_y);

            if (node_val > key)
            {
                high = mid - 1;
            }
            else
            {
                low = mid;
            }
        }

        node_val = GetScreenPosOfMonster(ref lst, low, x_or_y);
        if (node_val <= key)
        {
            return low;
        }
        else
        {
            return -1;
        }
    }

    // 大于key值的最小元素
    int binary_greater_than_key(ref List<CMonster> lst, int x_or_y, float key)
    {
        int low = 0;
        int high = lst.Count;
        float node_val = 0f;

        while (low < high)
        {
            int mid = (low + high) / 2;
            node_val = GetScreenPosOfMonster(ref lst, mid, x_or_y);
            if (node_val < key)
            {
                low = mid + 1;
            }
            else
            { //a[mid] >= key  
                high = mid;
            }
        }

        node_val = GetScreenPosOfMonster(ref lst, high, x_or_y);
        if (node_val >= key)
        {
            return high;
        }
        else
        {
            return -1;
        }
    }
    //// end 二分查找法


    public CMonster GetMonsterByGuid(long lThornId)
    {
        CMonster thorn = null;
        if (!m_dicAllThorns.TryGetValue(lThornId, out thorn))
        {
            Debug.LogError("bug! 没找到该GUID号的野怪");
            return null;
        }
        return thorn;
    }

	public void RemoveThorn( long lThornId, float fOccurTime )
	{
		CMonster thorn = null;
		if (!m_dicAllThorns.TryGetValue (lThornId, out thorn)) {
			Debug.LogError ("bug! Removethorn时没找到该GUID号的野怪");
			return;
		}
        

        // 不用重生的（比如吐的孢子）就直接销毁掉，需要重生的（比如场景豆子、场景刺）就加入重生等待列表
        if (thorn.IsRebornable())
        {
            AddToRebornList(thorn, fOccurTime);
        }
        else
        {
            ResourceManager.RecycleMonster(thorn);
        }
	}

    public void RemoveSpore( uint uPlayerSporeGuid, uint uPlayerId)
    {
        CMonster monster = null;
        string szKey = uPlayerId + "_" + uPlayerSporeGuid;
        if (!m_dicAllSpores.TryGetValue(szKey, out monster))
        {
            Debug.LogError("RemoveSpore 没找到相关ID号的孢子：" + szKey);
            return;
        }
        m_dicAllSpores.Remove(szKey);
        ResourceManager.RecycleMonster( monster );
    }

    public void AddSpore(uint uPlayerSporeGuid, uint uPlayerId, CMonster monster)
    {
        string szKey = uPlayerId + "_" + uPlayerSporeGuid;
        m_dicAllSpores[szKey] = monster;
    }

    List<CMonster> m_lstSyncPublicList = new List<CMonster>();
   
    public List<CMonster> GetSyncPublicList_DestroyedMonsters()
    {
        return m_lstSyncPublicList;
    }

    public Dictionary<int, CMonster> GetSyncPublicList_PushedMonsters()
    {
        return m_dicPushedThornList;
    }

    List<CMonster> m_lstToBeReborn = new List<CMonster>();
	void AddToRebornList(CMonster thorn, float fOccurTime)
	{
		thorn.SetDead ( true );
		thorn.SetDeadOccurTime (fOccurTime);
		m_lstToBeReborn.Add ( thorn );

        if (thorn.GetMonsterBuildingType() == CMonsterEditor.eMonsterBuildingType.thorn)
        {
            m_lstSyncPublicList.Add(thorn);
            RemoveMonsterFromPushedThornList(thorn);
        }
    }

	float c_RebornInterval = 1f; // 多少秒判断一次有没有刺可以重生了
	float m_fRebornCount = 0f;
	void ToRebornLoop()
	{
		m_fRebornCount += Time.deltaTime;
		if (m_fRebornCount < c_RebornInterval) {
			return;
		}
		m_fRebornCount = 0f;

		for (int i = m_lstToBeReborn.Count - 1; i >= 0; i--) { // 凡是在遍历途中要删除节点的，都要反向遍历
			CMonster thorn = m_lstToBeReborn[i];
			if (PhotonNetwork.time - thorn.GetDeadOccurTime () >= thorn.GetRebornTime ()) {
				thorn.SetDead ( false );
                thorn.SetPos( thorn.GetInitPos() );
                m_lstToBeReborn.Remove(thorn);
                if (thorn.GetMonsterBuildingType() == CMonsterEditor.eMonsterBuildingType.thorn)
                {
                    m_lstSyncPublicList.Remove(thorn); 
                }
            }
		}
	}

    Dictionary<int, CMonster> m_dicPushedThornList = new Dictionary<int, CMonster>();
    public void AddToPushedThornList( CMonster monster )
    {
        m_dicPushedThornList[(int)monster.GetGuid()] = monster;
    }

    public void RemoveMonsterFromPushedThornList(CMonster monster)
    {
        m_dicPushedThornList.Remove( (int)monster.GetGuid() );
    }

    // 在低级区随机找一个位置
    static int s_LowCount = 0;
    /*
	public Vector3 GetRandomPos_LowArea()
    { 
		float fWorldSizeX = MapEditor.s_Instance.GetWorldSizeX();
		float fWorldSizeY = MapEditor.s_Instance.GetWorldSizeY();

		int nRet = s_LowCount % 4;
		s_LowCount++;
        if (s_LowCount >= 4)
        {
            s_LowCount = 0;
        }
		float fDisX = 0f;
		float fDisY = 0f;
		if (nRet == 0) {
			fDisX = (float)UnityEngine.Random.Range (-fWorldSizeX / 2, fWorldSizeX / 2);
			fDisY = (float)UnityEngine.Random.Range (m_fClassCircleRadius, fWorldSizeY / 2);
		}else if (nRet == 1) {
			fDisX = (float)UnityEngine.Random.Range (-fWorldSizeX / 2, fWorldSizeX / 2);  
			fDisY = (float)UnityEngine.Random.Range (-fWorldSizeY / 2, -m_fClassCircleRadius);
		}else if (nRet == 2) {
			fDisX = (float)UnityEngine.Random.Range (-fWorldSizeX / 2, -m_fClassCircleRadius);
			fDisY = (float)UnityEngine.Random.Range (-m_fClassCircleRadius, m_fClassCircleRadius);   
		}
		else if (nRet == 3) {
			fDisX = (float)UnityEngine.Random.Range (m_fClassCircleRadius, fWorldSizeX / 2);  
			fDisY = (float)UnityEngine.Random.Range (-m_fClassCircleRadius, m_fClassCircleRadius);
		}
		vecTempPos.x = fDisX;
		vecTempPos.y = fDisY;
		vecTempPos.z = 0f;

       
		return vecTempPos;


	}
    */
    public Vector3 GetRandomPos_LowArea()
    {
        float fLeft = MapEditor.s_Instance.GetWorldLeft();
        float fRight = MapEditor.s_Instance.GetWorldRight();
        float fTop = MapEditor.s_Instance.GetWorldTop();
        float fBottom = MapEditor.s_Instance.GetWorldBottom();
        float fAngle = (float)UnityEngine.Random.Range(0f, 360f);
        float fRadius = CClassEditor.s_Instance.GetClassCircleRadius();

        /*
        float fRadian = CyberTreeMath.Angle2Radian( fAngle );
        float fDis = (float)UnityEngine.Random.Range(m_fClassCircleRadius, fMaxDis);
        vecTempPos.x = fDis * Mathf.Cos(fRadian);
        vecTempPos.y = fDis * Mathf.Sin(fRadian);
        vecTempPos.z = 0f;
        */
        float x = 0;
        float y = 0;
        float x2 = 0;
        float y2 = 0;
        float fCita = 0;
        if (fAngle == 0)
        {
            x2 = fRadius;
            y2 = 0;

            x = (float)UnityEngine.Random.Range(fRadius, fRight);
            y = 0;
        }
        else if ( fAngle > 0 && fAngle < 90f)
        {
            fCita = CyberTreeMath.Angle2Radian(fAngle);
            x2 = Mathf.Cos(fCita) * fRadius;
            y2 = Mathf.Sin(fCita) * fRadius;

            if (fAngle < 45 )
            {
                x = (float)UnityEngine.Random.Range(x2, fRight);
                y = CyberTreeMath.LineY(x, x2, y2);
            }
            else
            {
                y = (float)UnityEngine.Random.Range(y2, fTop);
                x = CyberTreeMath.LineX(y, x2, y2);
            }

        }
        else if ( fAngle == 90 )
        {
            x2 = 0;
            y2 = fRadius;

            x = 0;
            y = (float)UnityEngine.Random.Range(fRadius, fTop);
        }
        else if (fAngle > 90 && fAngle < 180)
        {
            fCita = CyberTreeMath.Angle2Radian( 180 - fAngle);
            x2 = -Mathf.Cos(fCita) * fRadius;
            y2 = Mathf.Sin(fCita) * fRadius;

            if ( fAngle < 135 )
            {
                y = (float)UnityEngine.Random.Range(y2, fTop);
                x = CyberTreeMath.LineX(y, x2, y2);
            }
            else
            {
                x = (float)UnityEngine.Random.Range(fLeft, x2 ); // x2含负号
                y = CyberTreeMath.LineY(x, x2, y2);
            }
        }
        else if (fAngle == 180)
        {
            x2 = -fRadius;
            y2 = 0;
        }
        else if (fAngle > 180 && fAngle < 270)
        {
            fCita = CyberTreeMath.Angle2Radian(fAngle - 180f);
            x2 = -Mathf.Cos(fCita) * fRadius;
            y2 = -Mathf.Sin(fCita) * fRadius;

            if (fAngle < 225 )
            {
                x = (float)UnityEngine.Random.Range(fLeft, x2); // x2含负号
                y = CyberTreeMath.LineY(x, x2, y2);
            }
            else
            {
                y = (float)UnityEngine.Random.Range(fBottom, y2); // y2含负号
                x = CyberTreeMath.LineX(y, x2, y2);
            }

        }
        else if (fAngle == 270)
        {
            x2 = 0;
            y2 = -fRadius;
        }
        else if (fAngle > 270 && fAngle < 360)
        {
            fCita = CyberTreeMath.Angle2Radian( 360f - fAngle);
            x2 = Mathf.Cos(fCita) * fRadius;
            y2 = -Mathf.Sin(fCita) * fRadius;

            if ( fAngle < 315 )
            {
                y = (float)UnityEngine.Random.Range(fBottom, y2); // y2含负号
                x = CyberTreeMath.LineX(y, x2, y2);
            }
            else
            {
                x = (float)UnityEngine.Random.Range(x2, fRight); // x2含负号
                y = CyberTreeMath.LineY(x, x2, y2);
            }
        }

        vecTempPos.x = x;
        vecTempPos.y = y;
        vecTempPos.z = 0f;
        

        return vecTempPos;
    }
            

    /*
	public Vector3 GetRandomPos_MiddleArea()
	{
		float fDis = (float)UnityEngine.Random.Range ( m_fInnerClassCircleRadius, m_fOutterClassCircleRadius );
		float fAngle = (float)UnityEngine.Random.Range ( 0f, 360f );
		float fRadian = CyberTreeMath.c_fAngleToRadian * fAngle;
		vecTempPos.x = fDis * Mathf.Cos ( fRadian );
		vecTempPos.y = fDis * Mathf.Sin ( fRadian );
		vecTempPos.z = 0f;
		return vecTempPos;
	}
    */

	public Vector3 GetRandomPos_HighArea()
	{
		float fDis = (float)UnityEngine.Random.Range ( 0, m_fClassCircleRadius );
		float fAngle = (float)UnityEngine.Random.Range ( 0f, 360f );
		float fRadian = CyberTreeMath.Angle2Radian( fAngle );
		vecTempPos.x = fDis * Mathf.Cos ( fRadian );
		vecTempPos.y = fDis * Mathf.Sin ( fRadian );
		vecTempPos.z = 0f;
		return vecTempPos;
	}

	public void GenerateClass(XmlNode node)
	{
		if (node == null) {
			return;
		}

		for (int i = 0; i < node.ChildNodes.Count; i++) {
			XmlNode sub_node = node.ChildNodes [i];
			if (sub_node.Name == "TimeOfOneGame") {
				m_fTimeOfOneGame = float.Parse( sub_node.InnerText);
			}
            /*
			if (sub_node.Name == "OutterCirlceRadius") {
				SetClassCircleRadius( _goOutterClassCircle, float.Parse( sub_node.InnerText ), ref m_fOutterClassCircleRadius );
			}
			if (sub_node.Name == "InnerCirlceRadius") {
				SetClassCircleRadius( _goInnerClassCircle, float.Parse( sub_node.InnerText ), ref m_fInnerClassCircleRadius );
			}
            */
            if (sub_node.Name == "CirlceRadius")
            {
                SetClassCircleRadius(_goClassCircle, float.Parse(sub_node.InnerText), ref m_fClassCircleRadius);
            }
            if (sub_node.Name == "ShengFuSize") {
				SetShengFuPanDingBallSize (float.Parse( sub_node.InnerText ));
			}
            if (sub_node.Name == "WarFogRadius")
            {
                m_fWarFogRadius = 1f;
                if ( !float.TryParse(sub_node.InnerText, out m_fWarFogRadius) )
                {
                    m_fWarFogRadius = 1f;
                }
                _inputWarFogRadius.text = m_fWarFogRadius.ToString();
            }
            if (sub_node.Name.Substring(0, 5 ) == "Class" ) {
				int nClassId = int.Parse (sub_node.ChildNodes [0].InnerText );
				sClassConfig classConfig = GetClassConfigById ( nClassId );
				for (int j = 0; j < sub_node.ChildNodes.Count; j++) {
					XmlNode class_node = sub_node.ChildNodes [j];
					if (class_node.Name == "Threshold") {
						classConfig.fSizeThreshold = float.Parse ( class_node.InnerText );
						if (nClassId == 1) {
							m_fMiddleThreshold =  classConfig.fSizeThreshold;
						}
						if (nClassId == 0) {
                            m_fLowThreshold = classConfig.fSizeThreshold;
						}
					}
					if (class_node.Name == "NormalAttenuate") {
						classConfig.fNormalAttenuate = float.Parse ( class_node.InnerText );
					}
					if (class_node.Name == "InvasionAttenuate") {
						classConfig.fInvasionAttenuate = float.Parse ( class_node.InnerText );
					}
                    if (class_node.Name == "InvasionSpeedDown")
                    {
                        classConfig.fInvasionSpeedSlowdown = float.Parse(class_node.InnerText);
                    }

                    if (class_node.Name.Substring (0, 5) == "Thorn") {
						string szThornId = class_node.ChildNodes[0].InnerText;
						sThornOfThisClassConfig thornConfig = GetThornConfigById (classConfig, szThornId);
						for (int k = 0; k < class_node.ChildNodes.Count; k++) {
							XmlNode thorn_node = class_node.ChildNodes[k];
							if (thorn_node.Name == "Density") {
								thornConfig.fDensity = float.Parse ( thorn_node.InnerText );
							}
							if (thorn_node.Name == "RebornTime") {
								thornConfig.fRebornTime = float.Parse ( thorn_node.InnerText );
							}

						} // end k
						SetThornConfigById(ref classConfig, thornConfig.szThornId,ref  thornConfig ); // 修改完了值别忘了立刻回灌
					}
				} // end j


				m_dicClassConfig [nClassId] = classConfig;
			}
		}// end i

		m_CurConfig = GetClassConfigById ( _dropArea.value );
        string szMonsterId = _dropThornType.value + "_" + _dropThornId.value;
		m_CurThornConfig = GetThornConfigById ( m_CurConfig, szMonsterId);
		UpdateUiContent ();   
	}

    public float GetLowThreshold()
    {
        return m_fLowThreshold;
    }

    public float GetMiddleThreshold()
	{
		return m_fMiddleThreshold;
	}

	public float GetHighThreshold()
	{
		return m_fHighThreshold;
	}

	public float GetTimeOfOneGame()
	{
		return m_fTimeOfOneGame;
	}

    public void OnButtonClick_AddOneMonster()
    {
        CMonster monster = ResourceManager.s_Instance.ReuseMonster();
        monster.SetConfigId(m_CurMannualMonsterConfig.szMonsterId);
        vecTempPos.x = Screen.width / 2f;
        vecTempPos.y = Screen.height / 2f;
        vecTempPos = Camera.main.ScreenToWorldPoint(vecTempPos);
        monster.SetPos(vecTempPos);
        monster.transform.parent = _goContainer_MannualMonster.transform;
    }

    public void OnButtonClick_DeleteOneMonster()
    {
        if (m_CurSelectedMonster)
        {
            GameObject.Destroy(m_CurSelectedMonster.gameObject);
        }
    }



    /// <summary>
    ///  Scene Ball
    /// </summary>
    public const int SHENGFU_SCENE_BALL_GUID = 0; // 胜负判定球的GUID是0
    long m_lSceneBallGuid = 1; // 胜负判定球的GUID是0。所以其余所有场景球的GUID都是从1开始的。
    Dictionary<long, CMonster> m_dicSceneBalls = new Dictionary<long, CMonster>();
    public void AddOneSceneBall(CMonster scene_ball)
    {
        m_dicSceneBalls[scene_ball.GetGuid()] = scene_ball;
    }

    
    public void RemoveOneSceneBall(int nGuid)
    {
        
    }

    //// !---- end Scene Ball

    public float GetWarFogRadius()
    {
        return m_fWarFogRadius;
    }

    public void OnInputValueChanged_WarFogRadius()
    {
        m_fWarFogRadius = 1f;
        if ( !float.TryParse( _inputWarFogRadius.text, out m_fWarFogRadius) )
        {
            m_fWarFogRadius = 1f;
        }
    }

}
