﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CGunsight : MonoBehaviour {

    static Vector3 vecTempScale = new Vector3();
    static Vector3 vecTempPos= new Vector3();
    static Vector2 vecTempDir = new Vector2();

    public GameObject _dickJoint;
    public GameObject _dickStick;
    public GameObject _circle;

   
	// Use this for initialization
	void Start () {
		
	}
	
	// Update is called once per frame
	void Update () {
		
	}


    public void SetInfo( Vector3 pos, Vector2 dir, float fMotherRadius, float fChildSize, float fDistanceMultiple )
    {
        float fAngle = CyberTreeMath.Dir2Angle(dir.x, dir.y );

        this.transform.position = pos;
        
        vecTempScale.x = fChildSize;
        vecTempScale.y = fChildSize;
        vecTempScale.z = 1f;
        _circle.transform.localScale = vecTempScale;

        float fDistance = fDistanceMultiple * fMotherRadius;
        vecTempPos.x = dir.x * fDistance;
        vecTempPos.y = dir.y * fDistance;
        vecTempPos.z = 0f;
        _circle.transform.localPosition = vecTempPos;

        float fJointLength = fChildSize * 2.59f;
        float fStickLength = fDistance - fJointLength;
        float fSticScaleY =( fStickLength + 0.05f ) / 7.77f;
        vecTempScale.y = fSticScaleY;
        vecTempScale.x = fChildSize;
        vecTempScale.z = 1f;
        _dickStick.transform.localScale = vecTempScale;

        vecTempScale.y = fChildSize;
        vecTempScale.x = fChildSize;
        vecTempScale.z = 1f;
        _dickJoint.transform.localScale = vecTempScale;

        float fJointDistance = fDistance - fJointLength;
        vecTempPos.x = dir.x * fJointDistance;
        vecTempPos.y = dir.y * fJointDistance;
        vecTempPos.z = 0f;
        _dickJoint.transform.localPosition = vecTempPos;


        CyberTreeMath.RotateGameObjByAngle(_dickStick, fAngle - 90);
        CyberTreeMath.RotateGameObjByAngle(_dickJoint, fAngle - 90);
    }
}
