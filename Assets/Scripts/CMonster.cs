﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CMonster : MonoBehaviour {

	CMonsterEditor.sThornConfig m_Config;

    public bool m_bIsJueSheng = false;

    static Vector2 vec2Temp = new Vector2();
	static Vector3 vecTempPos = new Vector3();
	static Vector3 vecTempScale = new Vector3();
	static Color cTempColor = new Color();

    public BallTrigger_New m_Trigger;

    public TextMesh _tmText;

    public Rigidbody2D _rigid;

    // “怪”的种类(逻辑功能层面)
    public enum eMonsterType
	{
        scene,         // 场景上统一生成的（要同步的）
		spit_spore,  // 吐的孢子
		spray,          // 喷泉
		la_baba,      // 拉的粑粑
	};

	long m_nGUID = 0; // 独一无二的实例编号
	string m_szConfigId = ""; // 配置数据编号

    public CircleCollider2D _Trigger;

	public SpriteRenderer _srMain;

	float m_fSize = 0f;

	int m_nBuffId = 0; // 这个怪会造成哪个Buff.  0表示没有buff
	public eMonsterType m_eMonsterType = eMonsterType.scene;
    public CMonsterEditor.eMonsterBuildingType  m_eMonsterBuildingType;

    int[] m_aryIntParam = new int[4];
	float[] m_aryFloatParam = new float[4];

	bool m_bDead = false;
	float m_fDeadOccurTime = 0f;
	float m_fRebornTime = 0f;

	Vector2 m_Dir = new Vector2();
	Vector3 m_Pos = new Vector3();

	Vector2 m_vEjectSpeed = new Vector2();
	float m_fjectTotalDis = 0f;
	bool m_bEjecting = false;
	Vector3 m_vEjectStartPos = new Vector3();

    Player m_Player = null;
    Ball m_Ball = null;

	// Use this for initialization
	void Start () {
		
	}
	
	// Update is called once per frame
	void Update () {
		EjectingLoop ();
	}

    public void Reset()
    {
        SetPlayer(null);
        SetBall( null );
        m_bEjecting = false;
        m_vEjectSpeed = Vector2.zero;
        m_fRotationSpeed = 0;
    }

    public void OnMouseDown()
    {
        CClassEditor.s_Instance.SelectMonster( this );
    }

    public void EatBall(Ball ball)
    {
        ball.SetEaten(true);
        ball._Player.DestroyBall(ball.GetIndex());
    }


    public void SetActive( bool val )
    {
        this.gameObject.SetActive( val );
    }


    bool m_bAutoReborn = false;
	public void SetAutoReborn( bool val )
	{
		m_bAutoReborn = val;
	}

	public bool GetAutoReborn()
	{
		return m_bAutoReborn;
	}

	public void SetGuid( long nId )
	{
		m_nGUID = nId;
	}

	public long GetGuid()
	{
		return m_nGUID;
	}

	public void SetConfigId( string szConfigId )
	{
		m_szConfigId = szConfigId;
		m_Config = CMonsterEditor.s_Instance.GetMonsterConfigById (m_szConfigId);
		SetSize ( CyberTreeMath.Radius2Scale( m_Config.fSelfSize ) );
		SetColor ( m_Config.szColor );
        SetMonsterBuildingType((CMonsterEditor.eMonsterBuildingType)m_Config.nType);
        SetSprite ( CMonsterEditor.s_Instance.GetSpriteByMonsterType((CMonsterEditor.eMonsterBuildingType)m_Config.nType) );
        
        if ( GetMonsterBuildingType() == CMonsterEditor.eMonsterBuildingType.pick_skill )
        {
            //_tmText.gameObject.SetActive( true );
            //_tmText.text = CSkillSystem.s_Instance.GetPickSkillNameById(m_Config.nSkillId);
            SetPickSkillInfo(m_Config.nSkillId);
        }

    }

    void SetPickSkillInfo( int nSkillId)
    {
        _srMain.sprite = CSkillSystem.s_Instance.GetSkillSprite(nSkillId);

        // 暂定
        _Trigger.radius *= 0.3f;

    }

    public CMonsterEditor.sThornConfig GetConfig()
    {
        return m_Config;
    }

    public string GetConfigId()
	{
		return m_szConfigId;
	}

	public void SetMonsterType( eMonsterType val )
	{
		m_eMonsterType = val;
	}

	public eMonsterType GetMonsterType()
	{
		return m_eMonsterType;
	}

    public void SetMonsterBuildingType( CMonsterEditor.eMonsterBuildingType type  )
    {
        m_eMonsterBuildingType = type;
    }

    public CMonsterEditor.eMonsterBuildingType GetMonsterBuildingType()
    {
        return m_eMonsterBuildingType;
    }

    public void SetDead( bool val )
	{
		m_bDead = val;
		this.gameObject.SetActive ( !m_bDead );
	}

	public bool IsDead()
	{
		return m_bDead;
	}

	// 设置死亡发生时间，重生的时候好判断
	public void SetDeadOccurTime( float val )
	{
		m_fDeadOccurTime = val;
	}

	public float GetDeadOccurTime()
	{
		return m_fDeadOccurTime;
	}

	public void SetRebornTime( float val )
	{
		m_fRebornTime = val;
	}

	public float GetRebornTime()
	{
		return m_fRebornTime;
	}

	public void SetIntParam( int idx, int val )
	{
		m_aryIntParam [idx] = val;
	}

	public int GetIntParam( int idx )
	{
		return m_aryIntParam [idx];
	}

	public void SetFloatParam( int idx, float val )
	{
		m_aryFloatParam [idx] = val;
	}

	public float GetFloatParam( int idx )
	{
		return m_aryFloatParam [idx];
	}

	public void SetSize( float val )
	{
		m_fSize = val;
		vecTempScale.x = val;
		vecTempScale.y = val;
		vecTempScale.z = 1f;
		this.transform.localScale = vecTempScale;
	}

    public float GetTriggerSize()
    {
        return _Trigger.bounds.size.x;
    }

    public float GetSize()
	{
		return m_fSize;
	}

	public void SetSprite( Sprite sprite )
	{
		_srMain.sprite = sprite;
	}

	public void SetColor( Color color )
	{
		_srMain.color = color;
	}

	public void SetColor( string szColor )
	{
		Color color = Color.white;
		if ( !ColorUtility.TryParseHtmlString("#" + szColor, out color)) {
			color = Color.white;
		}
		_srMain.color = color;
	}

	public void SetDir( Vector2 dir )
	{
		m_Dir = dir;
	}

	public Vector2 GetDir()
	{
		return m_Dir;
	}

	public void SetPos( Vector3 pos )
	{
		m_Pos = pos;
		this.transform.position = pos;
	}

	public Vector3 GetPos()
	{
        return this.transform.position;
    }

	public float GetRadius()
	{
		return _srMain.bounds.size.x / 2.0f;
	}

	public float GetDiameter()
	{
		return _srMain.bounds.size.x;
	}

    public void BeginEject_BySpeed( float fSpeed, float fRunTime)
    {
        float fDistance = fSpeed * fRunTime;
        m_vEjectSpeed.x = fSpeed * m_Dir.x;
        m_vEjectSpeed.y = fSpeed * m_Dir.y;
        m_fjectTotalDis = fDistance;
        m_bEjecting = true;
        m_vEjectStartPos = GetPos();
    }


    float m_fRotation = 0;
    float m_fRotationSpeed = 0;
    public void BeginEject( float fDistance, float fRunTime, float fRotationSpeed = 0)
	{
        float fSpeed = fDistance / fRunTime;
		m_vEjectSpeed.x += fSpeed * m_Dir.x;
		m_vEjectSpeed.y += fSpeed * m_Dir.y;
		m_fjectTotalDis = fDistance;
		m_bEjecting = true;
		m_vEjectStartPos = GetPos ();
        m_fRotation = 0;
        m_fRotationSpeed = fRotationSpeed;
    }
    

    void EjectingLoop()
	{
		if (!m_bEjecting) {
			return;
		}

        if (m_fRotationSpeed != 0f)
        {
            m_fRotation += m_fRotationSpeed;
            _rigid.MoveRotation(m_fRotation);
        }
       

        float fDeltaX = m_vEjectSpeed.x * Time.deltaTime;
		float fDeltaY = m_vEjectSpeed.y * Time.deltaTime;
		vecTempPos = GetPos ();
		vecTempPos.x += fDeltaX;
		vecTempPos.y += fDeltaY;
		SetPos ( vecTempPos );
		if (Vector2.Distance (vecTempPos, m_vEjectStartPos) >= m_fjectTotalDis) {
			EndEject ();
		}
	}

	public bool IsEjecting()
	{
		return m_bEjecting;
	}

	public void EndEject()
	{
		m_bEjecting = false;
        m_vEjectSpeed.x = 0;
        m_vEjectSpeed.y = 0;

    }

	bool m_bNeedSync = false;
	public void SetNeedSync( bool val )
	{
		m_bNeedSync = val;
	}

	public bool GetNeedSync()
	{
		return m_bNeedSync;
	}

    public void SetTriggerType( BallTrigger_New.eTriggerType type )
    {
        m_Trigger._type = type;
    }
    public BallTrigger_New.eTriggerType GetTriggerType( )
    {
        return m_Trigger._type;
    }

    public void PushThorn( CMonster monster )
    {

    }

    public float GetSpeed()
    {
        return Mathf.Sqrt(m_vEjectSpeed.x * m_vEjectSpeed.x + m_vEjectSpeed.y * m_vEjectSpeed.y);
    }

    public void SetPlayer( Player player )
    {
        m_Player = player;
    }

    public Player GetPlayer()
    {
        return m_Player;
    }

    public void SetBall( Ball ball )
    {
        m_Ball = ball;
    }

    public Ball GetBall()
    {
        return m_Ball;
    }

    Vector3 m_vecInitPos = new Vector3();
    public void SetInitPos( Vector3 pos )
    {
        m_vecInitPos = pos;
    }

    public Vector3 GetInitPos()
    {
        return m_vecInitPos;
    }

    public bool IsRebornable()
    {
        switch( this.GetMonsterBuildingType() )
        {
            case CMonsterEditor.eMonsterBuildingType.bean:
            case CMonsterEditor.eMonsterBuildingType.pick_skill:
            case CMonsterEditor.eMonsterBuildingType.scene_ball:
            case CMonsterEditor.eMonsterBuildingType.thorn:
                {
                    return true;
                }
                break;

            case CMonsterEditor.eMonsterBuildingType.spore:
                {
                    return false;
                }
                break;
        }

        return false;
    }
}
