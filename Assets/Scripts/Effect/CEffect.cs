﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CEffect : MonoBehaviour {

    public enum eEffectType
    {
        gold_shell, // 金壳
    };

    static Vector3 vecTempPos = new Vector3();

	// Use this for initialization
	void Start () {
		
	}
	
	// Update is called once per frame
	void Update () {
        AnimationCircleLoop();

    }


    float m_fAnimationCircleRadius = 0;
    float m_fRotation = 0;
    float m_fX = 0f;
    float m_fY = 0f;
    public void SetAnimationCircleParams( float fRadius, float fRotation, float fX, float fY )
    {
        m_fAnimationCircleRadius = fRadius;
        m_fRotation = fRotation;
        m_fX = fX;
        m_fY = fY;
    }

    bool m_bAnimationCircle = true;
    public void BeginAnimationCircle()
    {
        m_bAnimationCircle = true;
    }

    public void EndAnimationCircle()
    {
        m_bAnimationCircle = false;
    }

 
    void AnimationCircleLoop()
    {
        if ( !m_bAnimationCircle)
        {
            return;
        }

        vecTempPos.x = m_fX;
        vecTempPos.y = m_fY;
        vecTempPos.z = -1;
        this.gameObject.transform.localPosition = vecTempPos;
    }
}
