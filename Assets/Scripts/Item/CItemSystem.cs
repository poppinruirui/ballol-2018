﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using System.Xml;

public class CItemSystem : MonoBehaviour {

    public static CItemSystem s_Instance;

    public const int ITEM_NUM = 4;
    public const int ITEM_MAX_LEVEL = 6;

    // UI
    public Dropdown _dropdownFunc;
    public Dropdown _dropdownLevel;
    public InputField _inputName;
    public InputField _inputDesc;
    public InputField _inputSpriteId;
    public InputField _inputShortKey;
    public InputField _inputPrice;
    public InputField _inputValue;
    
    public Sprite[] m_aryItemSprite;
    public UIItem[] m_aryUiItem;
    public UIItem[] m_aryUiItem_PC;
    public UIItem[] m_aryUiItem_Mobile;

    public enum eItemFunc
    {
        base_size,        // 基础体积
        base_mp,         // 基础魔法
        base_speed,     // 基础速度
        shell_time,       // 壳的时间
        spit_distance,   // 分身距离
    };

    public struct sItemConfig
    {
        public int nConfigId;
        public int nShortcutKey;
        public string szName;
        public string szDesc;
        public int nFunc;
        public int nSpriteId;
        public Dictionary<int, sValueByLevel> dicValueByLevel;
    };
    sItemConfig m_CurItemConfig;

    public struct sValueByLevel
    {
        public int nPrice;     // 金钱消耗
        public float fValue;   // 核心数值
    };
    sValueByLevel tempValueByLevel;

    // 第一个int表示道具ID，第二个int表示等级
    Dictionary<int, sItemConfig> m_dicItemConfig = new Dictionary<int, sItemConfig>();
    sItemConfig tempItemConfig;
    int m_nCurConfigId = 0;
    int m_nCurLevel = 1;

    Dictionary<int, int> m_dicItemNum = new Dictionary<int, int>();

    void Awake()
    {
        s_Instance = this;
    }

    // Use this for initialization
    void Start () {
        InitDropdown_Func();
        InitDropdown_Level();
        bool bLevelExist = true;
        m_CurItemConfig = GetItemConfigByIdAndLevel( 0, 1, ref bLevelExist );

    }

    void InitDropdown_Func()
    {
        List<string> showNames = new List<string>();

        showNames.Add("影响基础体积");
        showNames.Add("影响基础魔法");
        showNames.Add("影响基础速度");
        showNames.Add("影响壳的时间");
        //showNames.Add("加分身距离");

        UIManager.UpdateDropdownView(_dropdownFunc, showNames);
    }

    void InitDropdown_Level()
    {
        List<string> showNames = new List<string>();

        showNames.Add("1级");
        showNames.Add("2级");
        showNames.Add("3级");
        showNames.Add("4级");
        showNames.Add("5级");
        showNames.Add("6级");
        showNames.Add("7级");
        showNames.Add("8级");
        showNames.Add("9级");
        showNames.Add("10级");
        showNames.Add("11级");
        showNames.Add("12级");
        showNames.Add("13级");
        showNames.Add("14级");
        showNames.Add("15级");
        showNames.Add("16级");
        showNames.Add("17级");
        showNames.Add("18级");
        showNames.Add("19级");
        showNames.Add("20级");

        UIManager.UpdateDropdownView(_dropdownLevel, showNames);
    }

    /*
    public sItemConfig GetItemConfigByIdAndLevel( int nConfigId )
    {
        if ( !m_dicItemConfig.TryGetValue(nConfigId, out tempItemConfig )  )
        {
            tempItemConfig = new sItemConfig();
            tempItemConfig.nConfigId = nConfigId;
            m_dicItemConfig[tempItemConfig.nConfigId] = tempItemConfig;
        }
        return tempItemConfig;
    }
    */

    public sItemConfig GetItemConfigById(int nConfigId)
    {
        if (!m_dicItemConfig.TryGetValue(nConfigId, out tempItemConfig))
        {
            tempItemConfig = new sItemConfig();
            tempItemConfig.nConfigId = nConfigId;
            tempItemConfig.dicValueByLevel = new Dictionary<int, sValueByLevel>();
            tempItemConfig.nShortcutKey = nConfigId + 1;
            tempItemConfig.nSpriteId = nConfigId;
        }
        return tempItemConfig;
    }


    public sItemConfig GetItemConfigByIdAndLevel(int nConfigId, int nLevel, ref bool bLevelExist)
    {
        if ( !m_dicItemConfig.TryGetValue(nConfigId, out tempItemConfig) )
        {
            tempItemConfig = new sItemConfig();
            tempItemConfig.nConfigId = nConfigId;
            tempItemConfig.dicValueByLevel = new Dictionary<int, sValueByLevel>();
            tempItemConfig.nShortcutKey = nConfigId + 1;
            tempItemConfig.nSpriteId = nConfigId;
        }
        bLevelExist = true;
        if ( !tempItemConfig.dicValueByLevel.TryGetValue(nLevel, out tempValueByLevel))
        {
            tempValueByLevel = new sValueByLevel();
            tempItemConfig.dicValueByLevel[nLevel] = tempValueByLevel;
            bLevelExist = false;
        }
        if (tempValueByLevel.fValue == 0 )
        {
            bLevelExist = false;
        }

        m_dicItemConfig[tempItemConfig.nConfigId] = tempItemConfig;
        return tempItemConfig;
    }

    public void OnDropdownValueChange_Func()
    {
        _dropdownLevel.value = 0;
        m_nCurConfigId = _dropdownFunc.value;
        bool bLevelExist = true;
        m_CurItemConfig = GetItemConfigByIdAndLevel(m_nCurConfigId, m_nCurLevel, ref bLevelExist);
        UpdateUiContent();
    } 

    public void OnDropdownValueChange_Level()
    {
        m_nCurLevel = _dropdownLevel.value + 1;
        bool bLevelExist = true;
        m_CurItemConfig = GetItemConfigByIdAndLevel(m_nCurConfigId, m_nCurLevel,ref bLevelExist);
        
        UpdateUiContent();
    }

    void UpdateUiContent()
    {
        _inputName.text = m_CurItemConfig.szName;
        _inputDesc.text = m_CurItemConfig.szDesc;
        _inputSpriteId.text = m_CurItemConfig.nSpriteId.ToString();
        _inputShortKey.text = m_CurItemConfig.nShortcutKey.ToString();
        _inputPrice.text = m_CurItemConfig.dicValueByLevel[m_nCurLevel].nPrice.ToString();
        _inputValue.text = m_CurItemConfig.dicValueByLevel[m_nCurLevel].fValue.ToString();

    }

    public void OnInputValueChanged_Name()
    {
        m_CurItemConfig.szName = _inputName.text;
        m_dicItemConfig[m_CurItemConfig.nConfigId] = m_CurItemConfig;
    }

    public void OnInputValueChanged_Desc()
    {
        m_CurItemConfig.szDesc = _inputDesc.text;
        m_dicItemConfig[m_CurItemConfig.nConfigId] = m_CurItemConfig;
    }

    public void OnInputValueChanged_SpriteId()
    {
        m_CurItemConfig.nSpriteId = int.Parse( _inputSpriteId.text );
        m_dicItemConfig[m_CurItemConfig.nConfigId] = m_CurItemConfig;
    }

    public void OnInputValueChanged_ShortKey()
    {
        m_CurItemConfig.nShortcutKey = int.Parse( _inputShortKey.text );
        m_dicItemConfig[m_CurItemConfig.nConfigId] = m_CurItemConfig;
    }

    public void OnInputValueChanged_Price()
    {
        m_CurItemConfig.dicValueByLevel.TryGetValue(m_nCurLevel, out tempValueByLevel);
        if ( !int.TryParse( _inputPrice.text, out tempValueByLevel.nPrice) )
        {
            tempValueByLevel.nPrice = 0;
        }
        m_CurItemConfig.dicValueByLevel[m_nCurLevel] = tempValueByLevel;
        m_dicItemConfig[m_CurItemConfig.nConfigId] = m_CurItemConfig;
    }

    public void OnInputValueChanged_Value()
    {
        m_CurItemConfig.dicValueByLevel.TryGetValue(m_nCurLevel, out tempValueByLevel);
        if ( !float.TryParse(_inputValue.text, out tempValueByLevel.fValue ))
        {
            tempValueByLevel.fValue = 0f;
        }
        m_CurItemConfig.dicValueByLevel[m_nCurLevel] = tempValueByLevel;
        m_dicItemConfig[m_CurItemConfig.nConfigId] = m_CurItemConfig;
    }
   
    public void InitItemSystem()
    {

        foreach (KeyValuePair<int, sItemConfig> pair in m_dicItemConfig)
        {
            if (m_aryUiItem[pair.Key] == null)
            {
                continue; ;
            }
               m_aryUiItem[pair.Key].InitItem(pair.Value, 1);
        } // end foreach
    }

    void RefreshItemInfo(sItemConfig config, int nLevel )
    {
        bool bLevelExist = true;
        m_aryUiItem[config.nConfigId].InitItem(config, nLevel + 1);
    }

    // Update is called once per frame
    void Update () {
        ProcessKeyInput();

        InitUI();
	}

    bool m_bUiInited = false;
    void InitUI()
    {
        if (m_bUiInited)
        {
            return;
        }

        if (Main.s_Instance == null)
        {
            return;
        }

        for (int i = 0; i < ITEM_NUM; i++)
        {
            if (Main.s_Instance.GetPlatformType() == Main.ePlatformType.pc)
            {
                m_aryUiItem[i] = m_aryUiItem_PC[i];
            }
            else if (Main.s_Instance.GetPlatformType() == Main.ePlatformType.mobile)
            {
                m_aryUiItem[i] = m_aryUiItem_Mobile[i];
            }
        }

        m_bUiInited = true;
    }

    public void ExecuteItem( int nShortKeyId )
    {
        int nConfigId = nShortKeyId; // 道具ID是从0开始的，快捷键ID是从1开始的
        int nCurNum = GetCurNum(nConfigId); // “道具等级”就是当前拥有的道具数量
        int nNextLevel = nCurNum + 1;
        bool bLevelExist = true;
        m_CurItemConfig = GetItemConfigByIdAndLevel(nConfigId, nNextLevel, ref bLevelExist);
        if ( !bLevelExist)
        {
            Main.s_Instance.g_SystemMsg.SetContent("该物品已满级了");
            return; // 该物品满级了，不能再使用了
        }
        if ( !AccountManager.m_bInGame )
        {
            return;
        }

        int nCurMoney = Main.s_Instance.m_MainPlayer.GetMoney();
        int nNextLevelNeedPrice = m_CurItemConfig.dicValueByLevel[nNextLevel].nPrice;
        if (nCurMoney < nNextLevelNeedPrice)
        {
            Main.s_Instance.g_SystemMsg.SetContent( CMsgSystem.s_Instance.GetMsgContentByType( CMsgSystem.eSysMsgType.money_not_enough ) );
            return;
        }

        // 判断能不能用
        bool bCanUse = UseItem(tempItemConfig);
        if ( !bCanUse)
        {
            return;
        }

        Main.s_Instance.m_MainPlayer.CostMoney(nNextLevelNeedPrice);


        RefreshAddPointButton();
    }

    public void RefreshAddPointButton()
    {
        for ( int i = 0; i < m_aryUiItem.Length; i++ )
        {
            if (m_aryUiItem[i] == null)
            {
                continue;
            }

            int nConfigId = i;            // 索引号恰好是道具的ID号
            int nCueLevel = GetCurNum(i);   //  道具的当前个数就是当前被动技能的等级
            m_aryUiItem[i].RefreshLevel(nCueLevel);
            int nNextLevel = nCueLevel + 1;
            bool bLevelExist = true;
            bool bCanUseItem = false;
            m_CurItemConfig = GetItemConfigByIdAndLevel(nConfigId, nNextLevel, ref bLevelExist);
            m_aryUiItem[i].SetAddPointButtonVisible( false );
            if ( bLevelExist) // 还有可以加的级
            {
                if (Main.s_Instance.m_MainPlayer.GetMoney() >= m_CurItemConfig.dicValueByLevel[nNextLevel].nPrice)
                {
                    m_aryUiItem[i].gameObject.SetActive( true );
                    m_aryUiItem[i].SetAddPointButtonVisible(true);
                    m_aryUiItem[i].SetItemInfo(m_CurItemConfig, nCueLevel, nNextLevel);
                    bCanUseItem = true;
                }
            }

            if (!bCanUseItem)
            {
                m_aryUiItem[i].gameObject.SetActive(false);
            }
        }
    }

    void UpdateItemNum( int nId, int nNum )
    {
        m_aryUiItem[nId].UpdateNum( nNum );
    }

    public int GetCurNum( int nConfigId )
    {
        int nCurNum = 0;
        if (!m_dicItemNum.TryGetValue(nConfigId, out nCurNum))
        {
            nCurNum = 0;
        }
        return nCurNum;
    }

    public bool UseItem(sItemConfig config )
    {
        int nCurNum = 0;
        if (  !m_dicItemNum.TryGetValue(config.nConfigId, out nCurNum) )
        { 

        }
        nCurNum++;
        m_dicItemNum[config.nConfigId] = nCurNum;
        UpdateItemNum(config.nConfigId, nCurNum);
        int nCurLevel = nCurNum;
        float fValue = 0f;
        if ( config.dicValueByLevel.TryGetValue(nCurLevel, out tempValueByLevel) )
        {
            fValue = tempValueByLevel.fValue;
        }
        if (fValue == 0)
        {
            Main.s_Instance.g_SystemMsg.SetContent( "该道具已满级了" );
            return false;
        }
        switch ( (eItemFunc)config.nFunc )
        {
            case eItemFunc.base_size:
                {
                    Main.s_Instance.m_MainPlayer.AddBaseSize(fValue);
                }
                break;
            case eItemFunc.base_mp:
                {
                    Main.s_Instance.m_MainPlayer.AddBaseMP(fValue);
                }
                break;
            case eItemFunc.base_speed:
                {
                    
                    Main.s_Instance.m_MainPlayer.AddBaseSpeed(fValue);
                    
                }
                break;
            case eItemFunc.shell_time:
                {
                    Main.s_Instance.m_MainPlayer.DecreaseShellTime(fValue);
                }
                break;
            case eItemFunc.spit_distance:
                {
                    //Main.s_Instance.m_MainPlayer.AddBaseSpitDistance(config.fValue);
                }
                break;

        }

       // RefreshItemInfo(config, nCurLevel);



        return true;
    }

    void ProcessKeyInput()
    {
        if (CChatSystem.s_Instance && CChatSystem.s_Instance.IsChatBarFocus())
        {
            return;
        }

        if ( !AccountManager.m_bInGame )
        {
            return;
        }

        if ( Input.GetKeyDown( KeyCode.Alpha1 ) )
        {
            ExecuteItem(0);
        }
        else if (Input.GetKeyDown(KeyCode.Alpha2))
        {
            ExecuteItem(1);
        }
        else if (Input.GetKeyDown(KeyCode.Alpha3))
        {
            ExecuteItem(2);
        }
        else if (Input.GetKeyDown(KeyCode.Alpha4))
        {
            ExecuteItem(3);
        }
        else if (Input.GetKeyDown(KeyCode.Alpha5))
        {
            ExecuteItem(4);
        }

    }

    public void SaveItem(XmlDocument xmlDoc, XmlNode node)
    {
        foreach (KeyValuePair<int, sItemConfig> pair in m_dicItemConfig)
        {
            tempItemConfig = pair.Value;
            XmlNode item_node = StringManager.CreateNode(xmlDoc, node, "I" + pair.Key);
            StringManager.CreateNode(xmlDoc, item_node, "ConfigId", pair.Value.nConfigId.ToString());
            StringManager.CreateNode(xmlDoc, item_node, "Func", pair.Value.nFunc.ToString());
            StringManager.CreateNode(xmlDoc, item_node, "Name", pair.Value.szName);
            StringManager.CreateNode(xmlDoc, item_node, "Desc", pair.Value.szDesc);
            XmlNode level_node = StringManager.CreateNode(xmlDoc, item_node, "Level" );
            foreach (KeyValuePair<int, sValueByLevel> level_pair in pair.Value.dicValueByLevel )
            {
                StringManager.CreateNode(xmlDoc, level_node, "L" + level_pair.Key, level_pair.Value.nPrice + "," + level_pair.Value.fValue);
            }
        } // end foreach

    } // end SaveItem

    public void GenerateItem(XmlNode node)
    {

        if (node == null)
        {
            return;
        }
        for (int i = 0; i < node.ChildNodes.Count; i++)
        {
            XmlNode sub_node = node.ChildNodes[i];
            for (int j = 0; j < sub_node.ChildNodes.Count; j++)
            {
                XmlNode node_item = sub_node.ChildNodes[j];
                switch (node_item.Name)
                {
                    case "ConfigId":
                        {
                            int nConfigId = int.Parse(node_item.InnerText);
                            m_CurItemConfig = GetItemConfigById(nConfigId);
                            m_CurItemConfig.nFunc = nConfigId;
                        }
                        break;
                    case "Name":
                        {
                            m_CurItemConfig.szName = node_item.InnerText;
                        }
                        break;
                    case "Desc":
                        {
                            m_CurItemConfig.szDesc = node_item.InnerText;
                        }
                        break;
                    case "Level":
                        {
                            for ( int k = 0; k < node_item.ChildNodes.Count; k++ )
                            {
                                XmlNode node_level = node_item.ChildNodes[k];
                                int nLevel = int.Parse ( node_level.Name.Substring(1, node_level.Name.Length - 1) );
                                string[] aryValues = node_level.InnerText.Split( ',' );
                                int nPrice = int.Parse(aryValues[0]);
                                float fValue = float.Parse(aryValues[1]);
                                tempValueByLevel.nPrice = nPrice;
                                tempValueByLevel.fValue = fValue;
                                m_CurItemConfig.dicValueByLevel[nLevel] = tempValueByLevel;
                            }
                        }
                        break;


                        /*
                    case "Price":
                        {
                            tempItemConfig.nPrice = int.Parse(node_item.InnerText);
                        }
                        break;
                    case "Value":
                        {
                            tempItemConfig.fValue = float.Parse(node_item.InnerText);
                        }
                        break;
                        */
                } // end switch
            } // end for j
            m_dicItemConfig[m_CurItemConfig.nConfigId] = m_CurItemConfig;
        } // end for i

        bool bLevelExist = true;
        m_CurItemConfig = GetItemConfigByIdAndLevel( 0, 1, ref bLevelExist);
        UpdateUiContent();
      

    } // GenerateItem
}
