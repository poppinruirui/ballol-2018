﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.EventSystems;

public class UIItem : MonoBehaviour, IPointerEnterHandler, IPointerExitHandler
 {

    public static UIItem s_Instance;

    public Button _btnAddPoint;
    public Text _txtNum;
    public Text _txtName;
    public Text _txtDescValue;
    public Text _txtDesc;
    public Text _txtPrice;
    public Text _txtNextLevelDesc;
    public Image _imgMain;
    public GameObject _tips;
    public Image _imgLevel;

    int m_nShortcuteKey = 0;

    CItemSystem.sItemConfig m_Config;

    void Awake()
    {
        s_Instance = this;
    }

    // Use this for initialization
    void Start () {
        _txtNum.text = "(LV." + 1 + ")";
    }
	
	// Update is called once per frame
	void Update () {
		
	}

    int m_nCurLevel = 0;
    public void SetItemInfo( CItemSystem.sItemConfig config, int nCurLevel, int nLevel)
    {
        m_nCurLevel = nCurLevel;
        m_Config = config;
        _txtName.text = "· " + config.szName;
        switch (config.nFunc)
        {
            case (int)CItemSystem.eItemFunc.shell_time:
                {
                    _txtDesc.text = config.szDesc;
                    _txtDescValue.text = config.dicValueByLevel[nLevel].fValue * 100 + "%";
                }
                break;

            case (int)CItemSystem.eItemFunc.base_mp:
                {
                    _txtDesc.text = config.szDesc;
                    _txtDescValue.text = config.dicValueByLevel[nLevel].fValue.ToString();
                }
                break;

            case (int)CItemSystem.eItemFunc.base_size:
                {
                    _txtDesc.text = config.szDesc;
                    _txtDescValue.text = config.dicValueByLevel[nLevel].fValue.ToString();
                }
                break;
            case (int)CItemSystem.eItemFunc.base_speed:
                {
                    _txtDesc.text = config.szDesc;
                    _txtDescValue.text = config.dicValueByLevel[nLevel].fValue.ToString();

                }
                break;
        }

    }

    public void InitItem( CItemSystem.sItemConfig config, int nLevel )
    {
        /*
        _txtName.text = config.szName;
        bool bThisLevelExist = false;
        bool bNextLevelExist = false;
        CItemSystem.sValueByLevel valueOfThisLevel;
        CItemSystem.sValueByLevel valueOfNextLevel;
        if (config.dicValueByLevel.TryGetValue(nLevel, out valueOfThisLevel))
        {
            if (valueOfThisLevel.nPrice != 0 && valueOfThisLevel.fValue != 0f)
            {
                bThisLevelExist = true;
            }
        }
        if (config.dicValueByLevel.TryGetValue(nLevel + 1, out valueOfNextLevel))
        {
            if (valueOfNextLevel.nPrice > 0 && valueOfNextLevel.fValue > 0f)
            {
                bNextLevelExist = true;
            }
        }
        if (!bThisLevelExist)
        {
            _txtDescValue.text = "";
            _txtPrice.text = "";
            _txtNextLevelDesc.text = "";
            return;
        }
        string szNextLevelDescContent = "";
        switch (config.nFunc)
        {
            case (int)CItemSystem.eItemFunc.shell_time:
                {
                    _txtDesc.text = config.szDesc;
                    _txtDescValue.text = "" + valueOfThisLevel.fValue * 100 + "%";
                    szNextLevelDescContent = "" + valueOfNextLevel.fValue * 100 + "%";
                }
                break;

            case (int)CItemSystem.eItemFunc.base_mp:
                {
                    _txtDesc.text = config.szDesc;
                    _txtDescValue.text = "+" + valueOfThisLevel.fValue + "点";
                    szNextLevelDescContent = "+" + valueOfNextLevel.fValue + "点";
                }
                break;

            case (int)CItemSystem.eItemFunc.base_size:
                {
                    _txtDesc.text = config.szDesc;
                    _txtDescValue.text = "+" + valueOfThisLevel.fValue + "吨";
                    szNextLevelDescContent = "+" + valueOfNextLevel.fValue + "吨";

                }
                break;
            case (int)CItemSystem.eItemFunc.base_speed:
                {
                    _txtDesc.text = config.szDesc;
                    _txtDescValue.text = "+" + valueOfThisLevel.fValue + "点";
                    szNextLevelDescContent = "+" + valueOfNextLevel.fValue + "点";
                }
                break;
        }
        if (bNextLevelExist)
        {
            _txtNextLevelDesc.text = "下一级 " + szNextLevelDescContent;
        }
        else
        {
            _txtNextLevelDesc.text = "(没有下一级了)";
        }


        _txtPrice.text = "- " + valueOfThisLevel.nPrice + "G";
        _imgMain.sprite = CItemSystem.s_Instance.m_aryItemSprite[config.nSpriteId ];
        m_nShortcuteKey = config.nShortcutKey;
        */
    }
    

    public void UpdateNum( int nNum )
    {
        //_txtNum.text = nNum.ToString();
        _txtNum.text = "(LV." + nNum + ")";
    }

    public void OnPointerEnter(PointerEventData eventData)
    {
        //_tips.SetActive(true);
    }

    public void OnPointerExit(PointerEventData eventData)

    {
        //_tips.SetActive(false);
    }

    public void OnClickButton_Add()
    {
        CItemSystem.s_Instance.ExecuteItem(m_nShortcuteKey - 1);
    }

    public void SetAddPointButtonVisible( bool bVisible )
    {
        //_btnAddPoint.gameObject.SetActive(bVisible);
        _tips.gameObject.SetActive(bVisible);
    }

    public void OnClickButton_UseItem()
    {
        if ( m_Config.dicValueByLevel == null )
        {
            return;
        }

        CItemSystem.s_Instance.ExecuteItem( m_Config.nConfigId );
    }

    public void RefreshLevel(int nCurLevel)
    {
        float fFillAmount = (float)nCurLevel / (float)CItemSystem.ITEM_MAX_LEVEL;
        if (_imgLevel)
        {
            _imgLevel.fillAmount = fFillAmount;
        }
    }
}
