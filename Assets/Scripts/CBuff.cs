﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CBuff : MonoBehaviour {

	CBuffEditor.sBuffConfig m_Config;
	float m_fTime = 0f;

	public enum eBuffType
	{
		
	};

	// Use this for initialization
	void Start () {
		
	}
	
	// Update is called once per frame
	void Update () {
		
	}

	public void RefreshConfig( CBuffEditor.sBuffConfig config )
	{
		m_Config = config; 
		SetTime ( m_Config.time );
	}

	public CBuffEditor.sBuffConfig GetConfig()
	{
		return m_Config;
	}

	public float GetTime()
	{
		return m_fTime;
	}

	public void SetTime( float val )
	{
		m_fTime = val;
	}

	public bool CheckIfEnd()
	{
		return m_fTime <= 0f;
	}

	long m_lGUID = 0;
	public void SetGUID( long val )
	{
		m_lGUID = val;
	}

	public long GetGUID()
	{
		return m_lGUID;
	}
}
