﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using System.Xml;
public class CSkillSystem : MonoBehaviour {

    public const int MAIN_SKILL_MAX_POINT = 5;
    public const int SELECT_SKILL_MAX_POINT = 3;

    public int m_nMax_R_Num;

    public Sprite[] _arySkillLevelUpSprites;
    public Sprite[] _arySkillArrowAppearSprites;
    public Sprite[] _arySkillArrowBreatheSprites;
    public Sprite[] _arySkillSelectSkillSprites;

    public Button _btnPrevBtn; // 上一页
    public Button _btnNextBtn; // 下一页

    int m_nCurPageIndex = 0;
    public GameObject[] _aryPages;

    public const int SELECTABLE_SKILL_START_ID = 3;
    public const int INVALID_SKILL_ID = -1;
    int m_nPickSkillId = INVALID_SKILL_ID;

    public const int SKILL_NUM = 5; // 总共有多少种技能
    public enum eSkillId
    {
        q_spore = 0,
        w_spit,
        e_unfold, // 猥琐扩展
        r_sneak, // 潜行
        t_become_thorn, // 变刺
        y_annihilate, // 湮灭
        u_magicshield, // 魔盾
        i_merge,     // 秒合
        o_fenzy,     // 狂暴
        p_gold,      // 金
    };

    public struct sSkillParam
    {
        public int nCurLevel;
        public bool bAvailable; 
        public float[] aryValues; // 每个level的数值
        public string szValue;
    };
    sSkillParam tempSkillParam;

    public struct sSkillConfig
    {
        public int nSkillId;
        public string szName;
        public string szDesc;
        public int m_nMaxPoint; // 天赋点最高值
        // ???? 待定
    };

    public struct sSkillInfo
    {
        public int nSkillId;
        public int nMaxPoint;
        public int nCurPoint;
    };
    sSkillInfo tempSkillInfo;
    Dictionary<int, sSkillInfo> m_dicSkillInfo = new Dictionary<int, sSkillInfo>();

    public enum eSkillSysProgressBarType
    {
        none,
        t_spit_ball,  // 二分分球
        w_spit_ball, // 蓄力分球
        i_merge_all,  // 秒合
    };
    eSkillSysProgressBarType m_eCurSkillType = eSkillSysProgressBarType.none;

    public CProgressBar _progressBar_PC;
    public CProgressBar _progressBar_MOBILE;
    CProgressBar _progressBar;

    public GameObject _panelThis;

    public Text _txtProgressBar;
    public Image _imgProgressBar;

    public Text _txtCurTotalPoints;

    public static  CSkillSystem s_Instance;

    int m_nPoints = 0; // 当前拥有的总点数

    /// <summary>
    /// UI
    /// </summary>
    public CUISkill[] _arySkillCounter;

    public UISkillConfig[] _arySkillParamsConfig;
    public UISkillCastButton[] _arySkillCastButton;


    void Awake()
    {
        s_Instance = this;
    }

    // Use this for initialization
    void Start () {
        InitAllSkills();
        SetTotalPoint(0);
    }

    bool m_bUiInited = false;
    void InitUI()
    {
        if (m_bUiInited)
        {
            return;
        }

        if ( Main.s_Instance == null )
        {
            return;
        }

        if ( Main.s_Instance.GetPlatformType() == Main.ePlatformType.pc )
        {
            _progressBar = _progressBar_PC;
        }else if (Main.s_Instance.GetPlatformType() == Main.ePlatformType.mobile)
        {
            _progressBar = _progressBar_MOBILE;
        }

        m_bUiInited = true;
    }

    // Update is called once per frame
    float m_fRefreshCastButtonTimeCount = 0f;
	void Update () {

        InitUI();

        if (m_fRefreshCastButtonTimeCount >= 0.5f)
        {
            RefreshCastButtonAvailable();
            m_fRefreshCastButtonTimeCount = 0f;
        }
        m_fRefreshCastButtonTimeCount += Time.deltaTime;
    }

    // poppin test
    public void InitAllSkills()
    {
        /*
        int nIndex = (int)eSkillId.q_spore;
        tempSkillInfo = new sSkillInfo();
        tempSkillInfo.nSkillId = nIndex;
        tempSkillInfo.nMaxPoint = 5;
        tempSkillInfo.nCurPoint = 0;
        m_dicSkillInfo[tempSkillInfo.nSkillId] = tempSkillInfo;
        // _arySkillCounter[nIndex].SetMaxNum(tempSkillInfo.nMaxPoint);
        _arySkillCounter[nIndex].Init(tempSkillInfo);
        _arySkillCastButton[nIndex].Init(tempSkillInfo);

        nIndex = (int)eSkillId.w_spit;
        tempSkillInfo = new sSkillInfo();
        tempSkillInfo.nSkillId = nIndex;
        tempSkillInfo.nMaxPoint = 5;
        tempSkillInfo.nCurPoint = 0;
        m_dicSkillInfo[tempSkillInfo.nSkillId] = tempSkillInfo;
        // _arySkillCounter[nIndex].SetMaxNum(tempSkillInfo.nMaxPoint);
        _arySkillCounter[nIndex].Init(tempSkillInfo);
        _arySkillCastButton[nIndex].Init(tempSkillInfo);

        nIndex = (int)eSkillId.e_unfold;
        tempSkillInfo = new sSkillInfo();
        tempSkillInfo.nSkillId = nIndex;
        tempSkillInfo.nMaxPoint = 5;
        tempSkillInfo.nCurPoint = 0;
        m_dicSkillInfo[tempSkillInfo.nSkillId] = tempSkillInfo;
        //_arySkillCounter[nIndex].SetMaxNum(tempSkillInfo.nMaxPoint);
        _arySkillCounter[nIndex].Init(tempSkillInfo);
        _arySkillCastButton[nIndex].Init(tempSkillInfo);

        nIndex = (int)eSkillId.r_split;
        tempSkillInfo = new sSkillInfo();
        tempSkillInfo.nSkillId = nIndex;
        tempSkillInfo.nMaxPoint = 5;
        tempSkillInfo.nCurPoint = 0;
        m_dicSkillInfo[tempSkillInfo.nSkillId] = tempSkillInfo;
        //_arySkillCounter[nIndex].SetMaxNum(tempSkillInfo.nMaxPoint);
        _arySkillCounter[nIndex].Init(tempSkillInfo);
        _arySkillCastButton[nIndex].Init(tempSkillInfo);

        nIndex = (int)eSkillId.t_half_spit;
        tempSkillInfo = new sSkillInfo();
        tempSkillInfo.nSkillId = nIndex;
        tempSkillInfo.nMaxPoint = 5;
        tempSkillInfo.nCurPoint = 0;
        m_dicSkillInfo[tempSkillInfo.nSkillId] = tempSkillInfo;
        // _arySkillCounter[nIndex].SetMaxNum(tempSkillInfo.nMaxPoint);
        _arySkillCounter[nIndex].Init(tempSkillInfo);
        _arySkillCastButton[nIndex].Init(tempSkillInfo);
        */
    }

    public string GetPickSkillNameById( int nSkillId )
    {
        switch (nSkillId)
        {
            case 3:
                {
                    return "潜";
                }
                break;
            case 4:
                {
                    return "刺";
                }
                break;
            case 5:
                {
                    return "湮";
                }
                break;
            case 6:
                {
                    return "免";
                }
                break;
            case 7:
                {
                    return "秒";
                }
                break;
            case 8:
                {
                    return "暴";
                }
                break;
            case 9:
                {
                    return "金";
                }
                break;

        } // end switch

        return "日";
    }

    public void ShowProgressBar(eSkillSysProgressBarType type )
    {
        
        _progressBar.gameObject.SetActive(true);
        m_eCurSkillType = type;
        switch (m_eCurSkillType)
        {
            case eSkillSysProgressBarType.t_spit_ball:
                {
                    _progressBar.SetCaption( "二分分球前摇" );
                }
                break;
            case eSkillSysProgressBarType.w_spit_ball:
                {
                    _progressBar.SetCaption( "力度分球时限" );
                }
                break;
            case eSkillSysProgressBarType.i_merge_all:
                {
                    _progressBar.SetCaption( "秒合前摇" );
                }
                break;
        }
        
    }

    public void UpdateProgressData( float fCurValue, float fTotalValue )
    {
        if (fTotalValue == 0)
        {
            return;
        }

        _progressBar.SetfillAmount( fCurValue / fTotalValue );
    }

    public void HideProgressBar()
    {
        _progressBar.gameObject.SetActive( false );
    }

    public sSkillInfo GetSkillInfoById( int nSkillId )
    {
        return m_dicSkillInfo[nSkillId];
    }

    public void SetPickSkillId( int nPickSkillId)
    {
        m_nPickSkillId = nPickSkillId;
    }

    public int GetPickSkillId()
    {
        return m_nPickSkillId;
    }

    public int GetSelectSkillCurPoint()
    {
        return m_nSelectSkillCurPoint;
    }

    public void PickSkill( int nSkillId )
    {
        SetPickSkillId(nSkillId);
        m_dicSkillInfo.TryGetValue(nSkillId, out tempSkillInfo);
        _arySkillCastButton[SELECTABLE_SKILL_START_ID].SetPickSkillCounterInfo(tempSkillInfo);

    }

    public void AddPickSkillPoint()
    {
        if ( GetPickSkillId() == INVALID_SKILL_ID )
        {
            Main.s_Instance.g_SystemMsg.SetContent("技能ID无效");
            return;
        }

        AddPoint(GetPickSkillId());
    }

    int m_nSelectSkillCurPoint = 0;
    public void AddPoint(int nSkillId, bool bDirectly = false )
    {
        if (!bDirectly)
        {
            if (m_nPoints <= 0)
            {
                Main.s_Instance.g_SystemMsg.SetContent("当前已经没有点数了，不能为技能加点");
                return;
            }
        }
        tempSkillInfo = GetSkillInfoById(nSkillId);
        bool bFull = false;
        if (nSkillId < SELECTABLE_SKILL_START_ID) // 基础技能
        {
            if (tempSkillInfo.nCurPoint >= MAIN_SKILL_MAX_POINT)
            {
                bFull = true;
            }
        }
        else // 可选技能（R技能）
        {
            if (tempSkillInfo.nCurPoint >= SELECT_SKILL_MAX_POINT)
            {
                bFull = true;
            }
        }

        if (bFull)
        {
            Main.s_Instance.g_SystemMsg.SetContent("该技能已加满，不能再加了！");
            return;
        }
        

        tempSkillInfo.nCurPoint++;
        if ( nSkillId >= SELECTABLE_SKILL_START_ID )
        {
            m_nSelectSkillCurPoint++;
        }

        m_dicSkillInfo[tempSkillInfo.nSkillId] = tempSkillInfo;

        if (!bDirectly)
        {
            SetTotalPoint(GetTotalPoint() - 1); // 消耗1点的“技能点数”
        }

        if (nSkillId < SELECTABLE_SKILL_START_ID)
        {
            _arySkillCastButton[nSkillId].SetCurLevel(tempSkillInfo.nCurPoint);
        }
        else
        {
            _arySkillCastButton[SELECTABLE_SKILL_START_ID].SetCurLevel(m_nSelectSkillCurPoint);
        }

        RefreshAddPointButton();
    }

   


    public bool CheckIfCanAddPoint( int nSkillId)
    {
        if (m_nPoints <= 0)
        {
            Main.s_Instance.g_SystemMsg.SetContent("当前已经没有点数了，不能为技能加点");
            return false;
        }

        tempSkillInfo = GetSkillInfoById(nSkillId);
        if (tempSkillInfo.nCurPoint >= tempSkillInfo.nMaxPoint)
        {
            Main.s_Instance.g_SystemMsg.SetContent("该技能已加满，不能再加了！");
            return false;
        }

        return true;
    }

    public void SetTotalPoint( int val )
    {
        m_nPoints = val;
        _txtCurTotalPoints.text = m_nPoints.ToString();

        RefreshAddPointButton();
    }

    public void RefreshAddPointButton()
    {
        for (int i = 0; i < _arySkillCastButton.Length; i++)
        {
            if (_arySkillCastButton[i] == null)
            {
                continue;
            }

            if (m_nPoints > 0)
            {
                if (i >= SELECTABLE_SKILL_START_ID) // 可选技能
                {
                    if (m_nSelectSkillCurPoint < SELECT_SKILL_MAX_POINT)
                    {
                        _arySkillCastButton[i].SetAddPointButtonVisible(true);
                    }
                }
                else
                {
                    tempSkillInfo = GetSkillInfoById(i);
                    if (tempSkillInfo.nCurPoint < MAIN_SKILL_MAX_POINT )
                    {
                        _arySkillCastButton[i].SetAddPointButtonVisible(true);
                    }
                }
            }
            else
            {
                _arySkillCastButton[i].SetAddPointButtonVisible(false);

            }
        } // end for
    }

    public void SetSkillColdownActive( eSkillId id, bool bActive)
    {
        if ( (int)id < SELECTABLE_SKILL_START_ID )
        {
            _arySkillCastButton[(int)id].SetSkillColdownActive(bActive);
        }
        else
        {
            _arySkillCastButton[SELECTABLE_SKILL_START_ID].SetSkillColdownActive(bActive);
        }
    }


    public void SetSkillColdownInfo(eSkillId id, float fFillAmount, string szLeftTime )
    {
        if ((int)id < SELECTABLE_SKILL_START_ID)
        {
            _arySkillCastButton[(int)id].SetSkillColdownInfo(fFillAmount, szLeftTime);
        }
        else
        {
            _arySkillCastButton[SELECTABLE_SKILL_START_ID].SetSkillColdownInfo(fFillAmount, szLeftTime);
        }
    }


    /*
    void SetAddPointButtonVisible( bool bVisible )
    {
        for (int i = 0; i < _arySkillCastButton.Length; i++)
        {
            if (_arySkillCastButton[i] == null)
            {
                continue;
            }
            _arySkillCastButton[i].SetAddPointButtonVisible(bVisible);
        }
    }
    */

    public int GetTotalPoint()
    {
        return m_nPoints;
    }

    public float GetSkillPointAffectValue( eSkillId nSkillId )
    {
        float val = 0;

        switch(nSkillId)
        {
            case eSkillId.e_unfold: // 猥琐扩张
                {
                    tempSkillInfo = GetSkillInfoById( (int)nSkillId);
                    val = 0.3f * tempSkillInfo.nCurPoint; // poppin test
                }
                break;

        } // end switch

        return val;
    }

    public void OnButtonClick_Close()
    {
        _panelThis.gameObject.SetActive( false );
    }

    public void SaveSkillParamConfig(XmlDocument xmlDoc, XmlNode node)
    {
        for ( int i = 0; i < _arySkillParamsConfig.Length; i++ )
        {
            if (_arySkillParamsConfig[i] == null)
            {
                continue;
            }
            StringManager.CreateNode(xmlDoc, node, "S" + i, _arySkillParamsConfig[i].GetConfigContent());
        } // end i
    }

    public void GenerateSkillParamConfig( XmlNode node)
    {
        if ( node == null )
        {
            return;
        }


        for ( int i = 0; i < node.ChildNodes.Count; i++ )
        {
            string[] aryValues = node.ChildNodes[i].InnerText.Split( ',' );
            _arySkillParamsConfig[i].SetConfigContent(aryValues, (CSkillSystem.eSkillId)i);
            
            tempSkillInfo = new sSkillInfo();
            tempSkillInfo.nSkillId = i;
            tempSkillInfo.nMaxPoint = _arySkillParamsConfig[i].GetMaxPoint();
            tempSkillInfo.nCurPoint = 0;
            m_dicSkillInfo[tempSkillInfo.nSkillId] = tempSkillInfo;
            if (i < _arySkillCounter.Length && _arySkillCounter[i])
            {
                _arySkillCounter[i].Init(tempSkillInfo);
            }

            if (i < SELECTABLE_SKILL_START_ID)
            {
                _arySkillCastButton[i].Init(tempSkillInfo);
            }
            else
            {
                _arySkillCastButton[SELECTABLE_SKILL_START_ID].Init(tempSkillInfo);
            }
        } // end i

        // 暂定
        AddPoint((int)eSkillId.q_spore, true); // 一出来“吐孢子”技能就可用
        PickSkill( (int)eSkillId.r_sneak);     // 一出来就捡到“潜行”技能
    }

    void UpdateSkillPointUI()
    {

    }

    public sSkillParam GetSkillParamById( eSkillId eSkillId, ref bool bCanUse )
    {
        int nSkillId = (int)eSkillId;
        sSkillInfo skill_point_info = GetSkillInfoById(nSkillId );
        int nCurPoint = nSkillId < CSkillSystem.SELECTABLE_SKILL_START_ID ? skill_point_info.nCurPoint : CSkillSystem.s_Instance.GetSelectSkillCurPoint() ;
        if (nCurPoint == 0)
        {
            Main.s_Instance.g_SystemMsg.SetContent( "该技能还未激活，请用技能加点激活" );
            bCanUse = false;
            return tempSkillParam;
        }
        bCanUse = true;
        if (nCurPoint > skill_point_info.nMaxPoint)
        {
            nCurPoint = skill_point_info.nMaxPoint;
        }
        return _arySkillParamsConfig[nSkillId].GetSkillParamByLevel(nCurPoint);
    }

    public void OnClickButton_PrevPage()
    {
        if (  m_nCurPageIndex == 0 )
        {
            return;
        }
        _aryPages[m_nCurPageIndex].SetActive( false );
        m_nCurPageIndex--;
        _aryPages[m_nCurPageIndex].SetActive(true);
    }

    public void OnClickButton_NextPage()
    {
        if (m_nCurPageIndex == _aryPages.Length - 1)
        {
            return;
        }
        _aryPages[m_nCurPageIndex].SetActive(false);
        m_nCurPageIndex++;
        _aryPages[m_nCurPageIndex].SetActive(true);

    }

    public void RefreshCastButtonAvailable()
    {
        for ( int i = 0; i < _arySkillCastButton.Length; i++ )
        {
            if (_arySkillCastButton[i] == null)
            {
                continue;
            }
            _arySkillCastButton[i].RefreshStatus();
        }
    }

    public bool IsSkillColdDowning( eSkillId id )
    {
        return Main.s_Instance.IsSkillColdDowning(id);
    }

    public Sprite GetSkillSprite( int nSkillId )
    {
        if (nSkillId >= _arySkillSelectSkillSprites.Length)
        {
            return null;
        }

        return _arySkillSelectSkillSprites[nSkillId];

    }
}
