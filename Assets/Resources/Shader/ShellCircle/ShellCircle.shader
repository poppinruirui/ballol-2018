﻿// Upgrade NOTE: replaced 'mul(UNITY_MATRIX_MVP,*)' with 'UnityObjectToClipPos(*)'

// Upgrade NOTE: replaced '_Object2World' with 'unity_ObjectToWorld'

Shader "Custom/ShellCircle" {
	Properties 
	{
		_Color ("Color", Color) = (1,1,1,1)
		_Width("RoundWidth", int) = 100
		_Percent( "Perent", float ) = 0
	}
	SubShader 
	{
	//此shader是根据面片顶点,需要是一2个三角面合成的正方形面片
		Pass 
		{
			ZTest Off
			ZWrite Off
			ColorMask 0 ////////////ColorMask GB 只显示R通道,即红色
		}
		Pass 
		{
			Blend SrcAlpha OneMinusSrcAlpha
			CGPROGRAM
			#pragma vertex vert
			#pragma fragment frag
			#include "UnityCG.cginc"
			struct v2f 
			{
				float4 pos : SV_POSITION;
				float4 oPos : TEXCOORD1;
			};

			fixed4 _Color;
			int _Width;
			float4 _MainTex_ST;
			float _Percent;
			v2f vert (appdata_base v)
			{
				v2f o;
				o.pos = UnityObjectToClipPos(v.vertex);
				o.oPos = v.vertex;
				return o;
			}
			fixed4 frag(v2f i) : COLOR
			{
			
				float dis = sqrt(i.oPos.x * i.oPos.x + i.oPos.y * i.oPos.y);
				float maxDistance = 0.5;
				if(dis > maxDistance)
				{
					discard;
				}
				else
				{
					/*
					float ringWorldRange = unity_ObjectToWorld[0][0];
					float minDistance = (ringWorldRange * 0.5 - _Width)/ringWorldRange;
					if(dis < minDistance)
					{
						discard;
					}
					_Color.a = 1;//(dis - minDistance)/(0.5 - minDistance);
					*/
					if ( _Percent < 0.25 ) // i.oPos.x > 0 && i.oPos.y > 0
					{
						if ( i.oPos.x > 0 && i.oPos.y > 0 ) 
						{
							float p = _Percent / 0.25;
							if ( atan( i.oPos.x / i.oPos.y ) < p * 1.571 )
							{
								discard;		
							}
							else
							{
							
							}
						}
						else
						{
						
						}
					}
					else if ( _Percent < 0.5 ) 
					{
						if ( i.oPos.x > 0 && i.oPos.y > 0 ) 
						{
							discard;
						}
						else if ( i.oPos.x > 0 && i.oPos.y < 0 )
						{
							float p = ( _Percent - 0.25 ) / 0.25;
							if ( atan( abs(i.oPos.y / i.oPos.x) ) < p * 1.571 )
							{
								discard;		
							}
							else
							{
							
							}
						}
						else
						{
						
						}
					}
					else if ( _Percent < 0.75 ) 
					{
						if ( (i.oPos.x > 0 && i.oPos.y > 0 ) || (i.oPos.x > 0 && i.oPos.y < 0 ) ) 
						{
							discard;
						}
						else if ( i.oPos.x < 0 && i.oPos.y < 0 )
						{
							float p = ( _Percent - 0.5 ) / 0.25;
							if ( atan( i.oPos.x / i.oPos.y ) < p * 1.571 )
							{
								discard;		
							}
							else
							{
							
							}
						}
						else
						{
						
						}
					}
					else  
					{
						if ( (i.oPos.x > 0 && i.oPos.y > 0 ) || (i.oPos.x > 0 && i.oPos.y < 0 ) || (i.oPos.x < 0 && i.oPos.y < 0 ) ) 
						{
							discard;
						}
						else if ( i.oPos.x < 0 && i.oPos.y > 0 )
						{
							float p = ( _Percent - 0.75 ) / 0.25;
							if ( atan( abs( i.oPos.y / i.oPos.x ) ) < p * 1.571 )
							{
								discard;		
							}
							else
							{
							
							}
						}
						else
						{
						
						}
					}
				}
				return _Color;
			}
			ENDCG
		}
	}
	FallBack "Diffuse"
}
