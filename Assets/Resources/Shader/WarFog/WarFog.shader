﻿// Upgrade NOTE: replaced 'mul(UNITY_MATRIX_MVP,*)' with 'UnityObjectToClipPos(*)'

// Upgrade NOTE: replaced '_Object2World' with 'unity_ObjectToWorld'

Shader "Custom/WarFog" {
	Properties 
	{
		_Color ("Color", Color) = (1,1,1,1)
		_Width("RoundWidth", int) = 100
		_Percent( "Perent", float ) = 0
	}
	SubShader 
	{
	//此shader是根据面片顶点,需要是一2个三角面合成的正方形面片
		Pass 
		{
			ZTest Off
			ZWrite Off
			ColorMask 0 ////////////ColorMask GB 只显示R通道,即红色
		}
		Pass 
		{
	   	      
	

			Blend SrcAlpha OneMinusSrcAlpha
			CGPROGRAM
			#pragma vertex vert
			#pragma fragment frag
			#include "UnityCG.cginc"

			uniform float4 _ball[64]; 

			struct v2f 
			{
				float4 pos : SV_POSITION;
				float4 oPos : TEXCOORD1;
			};

			fixed4 _Color;
			int _Width;
			float4 _MainTex_ST;
			float _Percent;
			float _ClassCircleRadius;
			int _CurLiveBallNum;
			float _aryBallsRadius[64];
			float _aryBallsPosX[64];
			float _aryBallsPosY[64];
			
			v2f vert (appdata_base v)
			{
				v2f o;
				o.pos = UnityObjectToClipPos(v.vertex);
				o.oPos = v.vertex;
				return o;
			}
			fixed4 frag(v2f p) : COLOR
			{
		
				float dis = sqrt(p.oPos.x * p.oPos.x + p.oPos.y * p.oPos.y);
				if(dis < _ClassCircleRadius)
				{
					discard;
				}
				else
				{
				
						for ( int i = 0; i < _CurLiveBallNum; i++ )
						{
							float dis_to_ball = sqrt( ( p.oPos.x  - _aryBallsPosX[i] ) * ( p.oPos.x  - _aryBallsPosX[i] ) + ( p.oPos.y  - _aryBallsPosY[i] ) * ( p.oPos.y  - _aryBallsPosY[i] ) );
							if ( dis_to_ball < _aryBallsRadius[i] )
							{
								discard;
							}
						}
						
				}
				
	
					
				return _Color;
			}
			ENDCG
		}
	}
	FallBack "Diffuse"
}
