﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class CMiniMap : MonoBehaviour {

    public static CMiniMap s_Instance;

    public Image _imgRealBg;
    public GameObject _goJueShengBall;
    public GameObject _goCircle;

    public GameObject _preMiniBall;
    List<GameObject> m_lstMiniBalls = new List<GameObject>();
    List<GameObject> m_lstRecycledMiniBalls = new List<GameObject>();

    static Vector3 vecTempPos = new Vector3();
    static Vector3 vecTempScale = new Vector3();

    const float MINI_MAP_WIDTH = 300f;
    const float MINI_MAP_HEIGHT = 300f;

    float m_fRatioX = 1f;
    float m_fRatioY = 1f;

    // Use this for initialization
    void Start () {
		
	}
	
	// Update is called once per frame
	void Update () {
        RefreshMiniBall();
    }

    private void Awake()
    {
        s_Instance = this;
    }

    public void Init()
    {
        float fWorldSizeX = MapEditor.s_Instance.GetWorldSizeX();
        float fWorldSizeY = MapEditor.s_Instance.GetWorldSizeY();
        vecTempScale.x = 1f;
        vecTempScale.y = 1f;
        vecTempScale.z = 1f;
        if (fWorldSizeX > fWorldSizeY)
        {
            m_fRatioY = vecTempScale.y = fWorldSizeY / fWorldSizeX;
        }
        else if (fWorldSizeX < fWorldSizeY)
        {
            m_fRatioX = vecTempScale.x = fWorldSizeX / fWorldSizeY;
        }
        _imgRealBg.transform.localScale = vecTempScale;

        float fJueShengQiuRadius = CClassEditor.s_Instance.GetShengFuPanDingBallRadius();
        float fJueShengQiuScale = CyberTreeMath.Radius2Scale(fJueShengQiuRadius);
        vecTempScale.x = fJueShengQiuScale;
        vecTempScale.y = fJueShengQiuScale;
        vecTempScale.z = 1f;
        _goJueShengBall.transform.localScale = vecTempScale;

        float fWorldSize = fWorldSizeX > fWorldSizeY ? fWorldSizeX : fWorldSizeY;
        float fCircleRadius = CClassEditor.s_Instance.GetClassCircleRadius();
        float fCircleScale = ( fCircleRadius * 2 )* MINI_MAP_WIDTH / fWorldSize;
        vecTempScale.x = fCircleScale;
        vecTempScale.y = fCircleScale;
        vecTempScale.z = 1f;
        _goCircle.transform.localScale = vecTempScale;
    }

    void RefreshMiniBall()
    {
        if ( !MapEditor.s_Instance.IsMapInitCompleted() )
        {
            return;
        }

        ClearMiniBalls();
        List<Ball> lstBalls = Main.s_Instance.m_MainPlayer.GetBallList();
        for ( int i = 0; i < lstBalls.Count; i++ )
        {
            Ball ball = lstBalls[i];
            if ( ball.IsDead() )
            {
                continue;
            }
            GameObject goMiniBall = ReuseMiniBall();
            m_lstMiniBalls.Add(goMiniBall);
            vecTempPos = ball.GetPos();
            float fScale = ball.GetSize();
            float fWorldSizeX = MapEditor.s_Instance.GetWorldSizeX();
            float fWorldSizeY = MapEditor.s_Instance.GetWorldSizeY();
            vecTempPos.x = vecTempPos.x / fWorldSizeX * MINI_MAP_WIDTH * m_fRatioX;
            vecTempPos.y = vecTempPos.y / fWorldSizeY * MINI_MAP_HEIGHT * m_fRatioY;
            goMiniBall.transform.localPosition = vecTempPos;
            vecTempScale.x = fScale;
            vecTempScale.y = fScale;
            vecTempScale.z = 1f;
            goMiniBall.transform.localScale = vecTempScale;
        }
    }

    void ClearMiniBalls()
    {
        for ( int i = 0; i < m_lstMiniBalls.Count; i++ )
        {
            RecycleMiniBall( m_lstMiniBalls[i] );
        }
        m_lstMiniBalls.Clear();
    }

    void RecycleMiniBall( GameObject goMiniBall )
    {
        goMiniBall.SetActive( false );
        m_lstRecycledMiniBalls.Add( goMiniBall );
    }

    GameObject ReuseMiniBall()
    {
        GameObject goMiniBall = null;
        if (m_lstRecycledMiniBalls.Count == 0)
        {
            goMiniBall = GameObject.Instantiate( _preMiniBall );
            goMiniBall.transform.SetParent( this.transform );
        }
        else
        {
            goMiniBall = m_lstRecycledMiniBalls[0];
            goMiniBall.SetActive( true );
            m_lstRecycledMiniBalls.RemoveAt(0);
        }
        return goMiniBall;
    }
}
