/*
 * 一个PhotonView就是一个跟服务器端的长连接。如果每个球球身上都带一个photonView，这势必无谓地占用大量带宽资源，造成网络拥堵。
 * 
*/
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Spine.Unity;
using UnityEngine.UI;

public class Ball : MonoBehaviour
{
    public SkeletonAnimation _skeletonAnimatnion;

    public MeshRenderer _mrShellCircle;

    public GameObject _shell;
    public GameObject _thorn;

    public CircleCollider2D _Trigger;
    public CircleCollider2D _Collider;
    public CircleCollider2D _ColliderTemp;
    public CircleCollider2D _ColliderDust;

    public GameObject _Mouth;
    public SpriteRenderer _srMouth;
    public SpriteRenderer _srOutterRing;
    public GameObject _main; // 暂不废弃ƒ
    public Rigidbody2D _rigid;


    bool m_bIsEjecting = false;

    public static Vector3 vecTempSize = new Vector3();
    public static Vector3 vecTempPos = new Vector3();
    public static Vector3 vecTempPos2 = new Vector3();
    public static Vector3 vecTempScale = new Vector3();
    public static Vector2 vec2Temp = new Vector2();
    public static Vector2 vec2Temp1 = new Vector2();
    public static Color cTempColor = new Color();

    Vector2 _direction = new Vector2();
    Vector2 _speed = new Vector2();
    Vector2 _vecSpitDir = new Vector2();

    

    public Text _txtPlayerName;
    

    public static Vector3 s_vecShellInitScale = new Vector3();

    public HTSpriteSheet _effectBecomeThorn_QianYao;
    public HTSpriteSheet _effectBecomeThorn_Become;
    public HTSpriteSheet _effectAnnihilate;
    public HTSpriteSheet _effectMagicShield;
    public HTSpriteSheet _effectHenzy;
    public HTSpriteSheet _effectGoldShell;

    public CEffect _effectPreUnfold;


    /*
	{
		set {  
			if (_ba == null) {
				_direction.x = value;
			} else {
				_ba._direction.x = value; 
			}

		}
		get { 
			if (_ba == null) {
				return _direction.x;
			} else {
				return _ba._direction.x; 
			}
		}
	}
	*/

    public float _diry = 0.0f;
    /*
	{
		set {  
			if (_ba == null) {
				_direction.y = value;
			} else {
				_ba._direction.y = value; 
			}

		}
		get { 
			if (_ba == null) {
				return _direction.y;
			} else {
				return _ba._direction.y; 
			}
		}
	}
	*/

    public GameObject _obsceneCircle;
    float m_fSpeed = 0.0f;

   


    public Player _Player = null; // 本Ball所属的Player
    public GameObject _player = null; // 这个 ball所属的player

    eBallType m_eBallType = eBallType.ball_type_ball;
    public eBallType _balltype
    {
        set { m_eBallType = value; }
        get { return m_eBallType; }
    }

    public enum eBallType
    {
        ball_type_ball,          // 常规的球
        ball_type_thorn,         // 刺
        ball_type_bean,          // 豆子
        ball_type_spore,         // 孢子 
    };

    public BallAction _ba; // 小熊那边做的BallAction模块


    /*
	[PunRPC]
	public void RPC_DestroyBall()
	{
		if (photonView.isMine) {
			Main.s_Instance.PhotonDestroy (this.gameObject);
		} 
	}
	*/

    protected bool m_bDead = false;
    public void Die()
    {
        /*
		m_bDead = true;
		
		if (_relateTarget) {
			DestroyTarget ( _relateTarget );
		}

		gameObject.SetActive ( false );

		if (_Trigger) {
			_Trigger.enabled = false;
		}

		if (_Collider) {
			_Collider.enabled = false;
		}
        */
    }

    public bool IsDead()
    {
        return m_bDead;
    }

    // [poppin youhua]
    public static void DestroyTarget(SpitBallTarget target)
    {
        //GameObject.Destroy ( target.gameObject );
        if (target)
        {
            target.SetVisible(false);
        }
    }


    SpriteRenderer m_sr;
    void Awake()
    {
        Init();

        m_sr = this.gameObject.GetComponent<SpriteRenderer>();

    }

    void Start()
    {

    }

    public bool IsMine()
    {
        return _Player.photonView.isMine;
    }

    public void SetPlayer(Player player)
    {
        _Player = player;
        SetSprite( Main.s_Instance.GetSpriteByPhotonOwnerId(_Player.photonView.ownerId) );
    }

    Vector3 m_vecObsCircleInitScale = new Vector3();
    public virtual void Init()
    {
        GameObject go;

        if (_shell)
        {
            s_vecShellInitScale = _shell.transform.localScale;
        }
        SetShellEnable(false);

        /*
        if (_player)
        {
            _Player = _player.GetComponent<Player>();
			if (!photonView.isMine) {
				_Player.AddOneBall (this);

			}

            if (photonView.isMine)
            {
                DarkForest.s_Instance.AddBall(this);
            }

            MiniMap.s_Instance.AddBall(this, photonView.isMine);

        }
	*/
        /*
                Transform transThorn = this.gameObject.transform.Find ("Thorn"); 
                if (transThorn) {
                    _thorn = transThorn.gameObject;
                    SpriteRenderer sr = _thorn.GetComponent<SpriteRenderer> ();
                    if (_Player)
                    {
                        sr.color = _Player._color_poison;
                    }
                    transThorn.localScale = Vector3.zero;
                } else {
                    Debug.LogError ( "transThorn empty" );
                    return;
                }
        */

        /*
                Transform transMouth = this.gameObject.transform.Find ("Mouth"); 
                if (transMouth) {
                    _Mouth = transMouth.gameObject;
                    _srMouth = _Mouth.GetComponent<SpriteRenderer> ();
                    if (photonView) {
                        _srMouth.sprite = Main.s_Instance.GetSpriteByPhotonOwnerId (photonView.ownerId);
                    } else {
                        _srMouth.sprite = Main.s_Instance.GetSpriteByPhotonOwnerId (3);
                    }
                    if (_Player)
                    {
                       // _srMouth.color = _Player._color_inner;  // 用贴图了，颜色机制暂时废弃
                    }
                } else {
                    Debug.LogError ( "transMouth empty" );
                    return;
                }
        */

        /*
		GameObject goObsceneMask = this.gameObject.transform.Find ( "ObsceneCircle" ).gameObject;
		_obsceneCircle = goObsceneMask.GetComponent<ObsceneMaskShader> ();
		_obsceneCircle.gameObject.SetActive ( false );
		m_vecObsCircleInitScale = _obsceneCircle.gameObject.transform.localScale;
        if (AccountManager.m_eSceneMode == AccountManager.eSceneMode.Game)
        {
       
        }
        else
        {
            goObsceneMask.SetActive( false );
        }
		_ba = this.gameObject.GetComponent<BallAction> ();

		go = this.gameObject.transform.Find ( "Text" ).gameObject;
		_text = go.GetComponent<TextMesh> ();

		*/

    }

    public void SetSprite(Sprite sprite)
    {
        _srMouth.sprite = sprite;
    }

    public void SetColor(Color color)
    {
        _srMouth.color = color;
    }

    public void SetShellEnable(bool val)
    {
        if (_shell)
        {
            _shell.gameObject.SetActive(val);
        }
    }

    // 上面的Init()是初生Init，由于我们是内存池机制，球球删除时并不真正销毁，待会儿会拿出来复用，所以再专门设置一个重生的Init()
    public void RebornInit()
    {
        _Trigger.radius = Main.s_Instance.m_fBallTriggerBasrRadius;
        m_bIsMovingToDest = false;
        m_fShellShrinkCurTime = 01f;
        SetShellEnable(false);
        Local_EndShell();
        SetDead(false);
        SetEaten(false);
        m_nMovingToCenterStatus = 0;
        vecTempScale.x = 1f;
        vecTempScale.y = 1f;
        vecTempScale.z = 1f;
        _Mouth.transform.localScale = vecTempScale;
        m_nUnfoldStatus = 0;
        _obsceneCircle.gameObject.SetActive(false);
        SetDir(Vector2.zero);
        _direction = Vector2.zero;
        _speed = Vector2.zero;
        _vecSpitDir = Vector2.zero;
        m_fMoveSpeedX = 0.0f;
        m_fMoveSpeedY = 0.0f;
        m_bIsEjecting = false;
        m_bIsEjecting_2 = false;
        m_vecdirection = Vector2.zero;
        BallSizeChange();

        CheckClassStatus();
        m_nSizeClassId = -1;
        ChangeSizeClassId();

        SetPlayerName(_Player.GetPlayerName());

        m_lstGrass.Clear();

        ClearSpitTarget();

        _effectBecomeThorn_Become.gameObject.SetActive(false);
        _effectBecomeThorn_QianYao.gameObject.SetActive(false);
    }

    // Update is called once per frame
    public virtual void Update()
    {
        PreUnfolding();
        Unfolding();
        Shell();
        //Move ();


        //Ejecting ();
        Ejecting_New();
        Ejecting_New_2();

        ProcessColliderTemp();
        Staying();
        UpdateSpitDir();
        UpdateSplitTarget();

        ShowSize();

        //Attenuate();

        StayOnClassCircleLoop();

        UpdateDickDir();

        // poppin test
        _rigid.velocity = Vector2.zero;
        _rigid.rotation = 0;

        TempShellLOop();
        DustCollierLoop();

        MovingToDest();


        // poppin test
        /*
        if (_Player && !IsDead() )
        {
            vec2Temp.x = 0f;
            vec2Temp.y = 1f;
            _Player.ShowGunsight(GetIndex(), GetPos(), vec2Temp, GetSize(), 0.5f, 3f  );
        }
        */
    }

    public void UpdateDickDir()
    {
        /*
        if (_goDick == null)
        {
            return;
        }

        float fDirX = 0f;
        float fDirY = 0f;
        if (_Player.IsMainPlayer())
        {

            fDirX = _dirx;
            fDirY = _diry;


        }
        else
        {
            fDirX = m_vecdirection.x;
            fDirY = m_vecdirection.y;
        }

        float fAngle = 0f;
        if (fDirX == 0)
        {
            if (fDirY > 0)
            {
                fAngle = 90f;
            }
            else if (fDirY < 0)
            {
                fAngle = -90f;
            }
        }
        else
        {
            float fTan = fDirY / fDirX;
            fAngle = Mathf.Atan(fTan) / CyberTreeMath.c_fAngleToRadian;
            if (fDirX < 0)
            {

                fAngle = 180f + fAngle;

            }
        }

        _goDick.transform.localRotation = Quaternion.identity;
        _goDick.transform.Rotate(0.0f, 0.0f, fAngle);
        */
    }

    byte m_byPlayerLevel = 1;
    public void SetPlayerName(string szPlayerName)
    {
        _txtPlayerName.text = "[" + m_byPlayerLevel + "级]" + szPlayerName;
        //_txtPlayerNameOutLine.text = _txtPlayerName.text;

    }

    public void SetPlayerLevel(int nPlayerLevel)
    {
        m_byPlayerLevel = (byte)nPlayerLevel;
    }

    void FixedUpdate()
    {
        Move();
    }

    bool m_bNeedSyncSize = false;
    float m_fLastSyncSizeTime = 0f;
    public void SetSize(float val)
    {
        Local_SetSize(val);
    }

    [PunRPC]
    public void RPC_SetSize(float val)
    {
        Local_SetSize(val);
    }

    float m_fDestSize = 0f;
    float m_fSizeUpdateSpeed = 0f;
    public void Local_SetSize(float val)
    {

        vecTempSize.x = val;
        vecTempSize.y = val;
        vecTempSize.z = 1.0f;
        this.transform.localScale = vecTempSize;

        // 遮挡是由z坐标值决定。
        vecTempPos = GetPos();
        vecTempPos.z = -val;
        SetPos(vecTempPos);

        BallSizeChange();
    }

    void UpdateSize()
    {
        return;

        if (m_fSizeUpdateSpeed == 0f)
        {
            return;
        }

        float fCurSize = GetRealTimeScale();
        fCurSize += m_fSizeUpdateSpeed * Time.deltaTime;
        if (m_fSizeUpdateSpeed > 0 && fCurSize >= m_fDestSize)
        {
            fCurSize = m_fDestSize;
            m_fSizeUpdateSpeed = 0;
        }
        if (m_fSizeUpdateSpeed < 0 && fCurSize <= m_fDestSize)
        {
            fCurSize = m_fDestSize;
            m_fSizeUpdateSpeed = 0;
        }

        Local_Directly_SetSize(fCurSize);
    }

    public void Local_Directly_SetSize(float val)
    {
        vecTempSize.x = val;
        vecTempSize.y = val;
        vecTempSize.z = 1.0f;
        this.transform.localScale = vecTempSize;

        // 遮挡是由z坐标值决定。
        vecTempPos = GetPos();
        vecTempPos.z = -val;
        SetPos(vecTempPos);

    }


    float m_fSize2CiFang = 0.0f;
    float m_fSize3CiFang = 0.0f;
    float m_fSize2KaiGenHao = 0.0f;
    float m_fSize3KaiGenHao = 0.0f;
    float m_fTiJi = 0f;
    float m_fVolume = 0f;
    float m_fRadius = 0f;
    // 这里要作一个性能优化，没必要每帧都计算。（吃球判断时有专门的计算流程，实时性很高，不靠这个）
    void BallSizeChange()
    {
        if ( IsDead() )
        {
            return;
        }

        float fSize = GetSize();

        m_fRadius = CyberTreeMath.Scale2Radius( fSize );

        CalculateMassBySize(GetSize());
    }

    void UpdateClassStatus()
    {
        if (_Player)
        {
            CheckClassStatus();
            ChangeSizeClassId();
        }
    }
    
    public float GetVolume()
    {
        return CyberTreeMath.Scale2Volume( GetSize(), ref m_fRadius);
    }

    // without base-volume
    public float GetEatVolume()
    {
        //float fEatVolume = m_fVolume - 
        return 0;
    }

    public float GetRadius()
    {
        return m_fRadius;
    }

    void CalculateMassBySize(float fSize)
    {
        if (_rigid == null) {
            return;
        }
        _rigid.mass = fSize;
    }


    public float GetTriggerSize()
    {
        return _Trigger.bounds.size.x;
    }

    public float GetSize()
    {
        return this.transform.localScale.x;
    }

    public float GetRealTimeScale()
    {
        return this.transform.localScale.x;
    }

    float m_fColliderTempTime = 0f;
    void ProcessColliderTemp()
    {
        /*
		if (IsDead ()) {
			return;
		}

		if ( _ColliderTemp == null || _ColliderTemp.enabled == false )
		{
			return;
		}
		m_fColliderTempTime -= Time.deltaTime;
		if (m_fColliderTempTime <= 0f) {
			_ColliderTemp.enabled = false;
		}
		*/
    }

    public float GetEjectingSpeed()
    {
        return m_fInitSpeed;
    }

    public bool IsEjecting()
    {
        return m_bIsEjecting || m_bIsEjecting_2;
    }

    public void SlowDownEject()
    {
        m_fInitSpeed *= 0.9f;
    }


    float m_fEjectInitialSpeed = 0.0f;
    float m_fEjectSpeed = 0.0f;
    float m_fEjectAccelerate = 0.0f;
    float m_fStartPosX = 0.0f;
    float m_fStartPosY = 0.0f;
    float m_fTimeClapse = 0.0f;
    float m_fEjectingTotalTime = 0f;
    public void BeginEject(float fInitialSpeed, float fAccelerate, float fEjectingTotalTime, bool bRealTimeFollowMouse = false, bool bUseColliderTemp = false, float fStayTime = 0f)
    {
        //photonView.RPC ( "RPC_BeginEject", PhotonTargets.All, fInitialSpeed, fAccelerate, fEjectingTotalTime, bRealTimeFollowMouse, bUseColliderTemp,fStayTime );
    }

    [PunRPC]
    public void RPC_BeginEject(float fInitialSpeed, float fAccelerate, float fEjectingTotalTime, bool bRealTimeFollowMouse = false, bool bUseColliderTemp = false, float fStayTime = 0f)
    {
        Local_BeginEject(fInitialSpeed, fAccelerate, fEjectingTotalTime, bRealTimeFollowMouse, bUseColliderTemp, fStayTime);
    }

    bool m_bRealTimeFollowMouse = false;
    bool m_bUseColliderTemp = false;
    float m_fStayTimeWhenEjectEnd = 0f;
    public void Local_BeginEject(float fInitialSpeed, float fAccelerate, float fEjectingTotalTime, bool bRealTimeFollowMouse = false, bool bUseColliderTemp = false, float fStayTime = 0f)
    {
        m_fStayTimeWhenEjectEnd = fStayTime;
        m_bUseColliderTemp = bUseColliderTemp;
        m_fEjectingTotalTime = fEjectingTotalTime;
        m_bRealTimeFollowMouse = bRealTimeFollowMouse;
        m_bIsEjecting = true;
        m_fEjectInitialSpeed = fInitialSpeed;
        m_fEjectSpeed = fInitialSpeed;
        m_fEjectAccelerate = fAccelerate;
        m_fStartPosX = this.gameObject.transform.position.x;
        m_fStartPosY = this.gameObject.transform.position.y;
        m_fTimeClapse = 0.0f;
    }

    float m_fTotalEjectDisX = 0f;
    float m_fTotalEjectDisY = 0f;
    float m_fCurEjectDisX = 0f;
    float m_fCurEjectDisY = 0f;
    public void Local_BeginEject_New(float fSpeed, float fDis, bool bRealTimeFollowMouse = false, bool bUseColliderTemp = false, float fStayTime = 0f)
    {
        m_fStayTimeWhenEjectEnd = fStayTime;
        m_bUseColliderTemp = bUseColliderTemp;
        m_bRealTimeFollowMouse = bRealTimeFollowMouse;
        m_bIsEjecting = true;

        m_fEjectSpeed = fSpeed;

        m_fStartPosX = this.gameObject.transform.position.x;
        m_fStartPosY = this.gameObject.transform.position.y;
        m_fTimeClapse = 0.0f;

        m_fTotalEjectDisX = fDis * GetDir().x;
        m_fTotalEjectDisY = fDis * GetDir().y;
        m_fCurEjectDisX = 0f;
        m_fCurEjectDisY = 0f;



        // poppin test
        if (m_fEjectSpeed > Main.s_Instance.m_fMaxMoveDistancePerFrame)
        {
            m_fEjectSpeed = Main.s_Instance.m_fMaxMoveDistancePerFrame;
        }


        // 播放球体的喷射动画
        CSpineManager.s_Instance.SetBallAnimationRotation(this, CyberTreeMath.Dir2Angle(GetDir().x, GetDir().y));
        CSpineManager.s_Instance.PlayBallAnimation(this, CSpineManager.eBallSpineType.split, false, 1.5f);
    }

    float m_fInitSpeed = 0f;
    float m_fAccelerate = 0f;
    bool m_bIsEjecting_2 = false;
    public void Local_BeginEject_New_2(float fExpectDistance, float fExpectTime, float fStayTime = 0f)
    {
        m_fStayTimeWhenEjectEnd = fStayTime;
        m_fInitSpeed = CyberTreeMath.GetV0(fExpectDistance, fExpectTime);
        m_fAccelerate = CyberTreeMath.GetA(fExpectDistance, fExpectTime);
        m_bIsEjecting_2 = true;

        // 播放球体的喷射动画
        CSpineManager.s_Instance.SetBallAnimationRotation(this, CyberTreeMath.Dir2Angle( GetDir().x, GetDir().y) );
        CSpineManager.s_Instance.PlayBallAnimation(this, CSpineManager.eBallSpineType.split, false, 3f);
    }

    public float GetCurEjectSpeed()
    {
        return m_fInitSpeed;
    }

    public void SetEjectSpeed( float val)
    {
        m_fInitSpeed = val;
    }

    void Ejecting_New_2()
    {
        if (!m_bIsEjecting_2)
        {
            return;
        }

        if (IsDead())
        {
            return;
        }

        float fDeltaX = Time.deltaTime * m_fInitSpeed * GetDir().x;
        float fDeltaY = Time.deltaTime * m_fInitSpeed * GetDir().y;

        m_fInitSpeed += m_fAccelerate * Time.deltaTime;
        
        if (m_fInitSpeed <= 0f)
        {
            End_Eject_New_2();
        }

        vecTempPos = GetPos();

        bool bCanMoveX = true;
        bool bCanMoveY = true;
        MapEditor.s_Instance.CheckIfWillExceedWorldBorder(this, fDeltaX, fDeltaY, ref bCanMoveX, ref bCanMoveY);

        if (bCanMoveX)
        {
            vecTempPos.x += fDeltaX;
        }

        if (bCanMoveY)
        {
            vecTempPos.y += fDeltaY;
        }

        if (!bCanMoveX && !bCanMoveY)
        {
            End_Eject_New_2();
            return;
        }

        SetPos(vecTempPos);
    }

    void End_Eject_New_2()
    {
        m_bIsEjecting_2 = false;

        if (m_fStayTimeWhenEjectEnd > 0f)
        {
            BeginStay();
        }
        else
        {
            Local_BeginShell();
        }
    }

    public bool CheckNeedCollider()
    {
        if (!m_bShell)
        {
            return false;
        }

        if (Main.s_Instance.m_fShellShrinkTotalTime == 0f)
        {
            return false;
        }

        return true;
    }

    public void EndEject()
    {
        //SyncBaseInfo();

        m_bIsEjecting = false;


        if (m_bUseColliderTemp)
        {
            //_ColliderTemp.enabled = true;
            m_fColliderTempTime = 0.5f;
        }

        if (m_bShell)
        {

        }

        if (m_fStayTimeWhenEjectEnd > 0f)
        {
            BeginStay();
        }
        else
        {
            Local_BeginShell();
        }
    }

    bool m_bStaying = false;
    public void SetStayTime(float val)
    {
        m_fStayTimeWhenEjectEnd = val;
    }

    public void BeginStay()
    {
        m_bStaying = true;
    }

    void Staying()
    {
        if (IsDead())
        {
            return;
        }

        if (m_bStaying == false)
        {
            return;
        }

        m_fStayTimeWhenEjectEnd -= Time.deltaTime;
        if (m_fStayTimeWhenEjectEnd <= 0f)
        {
            EndStay();
        }
    }

    public bool IsStaying()
    {
        return m_bStaying;
    }

    public void EndStay()
    {
        m_bStaying = false;
        if (m_fShellTotalTime > 0)
        {
            Local_BeginShell();
        }
    }



    public void Ejecting_New()
    {
        if (IsDead())
        {
            return;
        }

        if (!m_bIsEjecting)
        {
            return;
        }

        float fDeltaX = Time.deltaTime * m_fEjectSpeed * GetDir().x;
        float fDeltaY = Time.deltaTime * m_fEjectSpeed * GetDir().y;

        vecTempPos = GetPos();

        if (((m_fTotalEjectDisX == 0f) || (m_fTotalEjectDisX > 0 && m_fCurEjectDisX >= m_fTotalEjectDisX) || (m_fTotalEjectDisX < 0 && m_fCurEjectDisX <= m_fTotalEjectDisX)) &&
            ((m_fTotalEjectDisY == 0f) || (m_fTotalEjectDisY > 0 && m_fCurEjectDisY >= m_fTotalEjectDisY) || (m_fTotalEjectDisY < 0 && m_fCurEjectDisY <= m_fTotalEjectDisY))
        )
        {
            EndEject();
            return;
        }

        bool bCanMoveX = true;
        bool bCanMoveY = true;
        MapEditor.s_Instance.CheckIfWillExceedWorldBorder(this, fDeltaX, fDeltaY, ref bCanMoveX, ref bCanMoveY);
        /*
        if (IsStayOnClassCircle() && IsSizeReachClassCircleThreshold())
        {
            MapEditor.s_Instance.CheckIfCanCrossClassCircle(this, fDeltaX, fDeltaY, ref bCanMoveX, ref bCanMoveY);
        }
        */
        if (bCanMoveX)
        {
            vecTempPos.x += fDeltaX;
        }

        if (bCanMoveY)
        {
            vecTempPos.y += fDeltaY;
        }

        if (!bCanMoveX && !bCanMoveY)
        {
            EndEject();
            return;
        }

        SetPos(vecTempPos);

        m_fCurEjectDisX += fDeltaX;
        m_fCurEjectDisY += fDeltaY;
    }

    // nAxis    0 - X    1 - Y
    public bool DoEjectingRealMove(int nAxis, float fDelta, float fTotalEjectDis, ref float fCurEjectDis)
    {
        fCurEjectDis += fDelta;

        if (
             (fTotalEjectDis == 0f) ||
             (fTotalEjectDis > 0 && fCurEjectDis >= fTotalEjectDis) ||
             (fTotalEjectDis < 0 && fCurEjectDis <= fTotalEjectDis)
           )
        {
            EndEject();
            return true;
        }

        vecTempPos = _rigid.position;
        bool bCanMoveX = true;
        bool bCanMoveY = true;
        if (nAxis == 0)
        { // X轴
            MapEditor.s_Instance.CheckIfWillExceedWorldBorder(this, fDelta, 0f, ref bCanMoveX, ref bCanMoveY);
            bCanMoveY = false;
        }
        else if (nAxis == 1)
        {
            MapEditor.s_Instance.CheckIfWillExceedWorldBorder(this, 0f, fDelta, ref bCanMoveX, ref bCanMoveY);
            bCanMoveX = false;
        }

        if (bCanMoveX)
        {
            vecTempPos.x += fDelta;
        }
        if (bCanMoveY)
        {
            vecTempPos.y += fDelta;
        }

        SetPos(vecTempPos);

        //_rigid.MovePosition( vecTempPos );
        //_rigid.MoveRotation ( 0 );


        return false;
    }

    public void Ejecting()
    {
        if (IsDead())
        {
            return;
        }

        if (!m_bIsEjecting)
        {
            return;
        }



        if (m_fTimeClapse >= m_fEjectingTotalTime/*Main.s_Instance.m_fSpitRunTime*/)
        {
            EndEject();
            return;
        }

        if (m_bRealTimeFollowMouse)
        {
            vec2Temp = _Player.GetWorldCursorPos() - this.GetPos();


            vec2Temp.Normalize();
            SetDir( vec2Temp );
        }

        /*
		m_fTimeClapse += Time.fixedDeltaTime;
		vecTempPos.x = m_fStartPosX;
		vecTempPos.y = m_fStartPosY;
		vecTempPos.z = this.gameObject.transform.position.z;
		float S = CyberTreeMath.CalculateDistance( m_fEjectInitialSpeed, m_fEjectAccelerate, m_fTimeClapse ); //m_fEjectSpeed * m_fTimeClapse + 0.5f * m_fAccelerate * m_fTimeClapse * m_fTimeClapse;
		vecTempPos.x += S * _dirx;
		vecTempPos.y += S * _diry;
		*/
        m_fTimeClapse += Time.deltaTime;
        vecTempPos = GetPos();
        float fDeltaX = Time.deltaTime * m_fEjectInitialSpeed * GetDir().x;
        float fDeltaY = Time.deltaTime * m_fEjectInitialSpeed * GetDir().y;

        float fRealNextX = 0f;
        float fRealNextY = 0f;
        MapEditor.s_Instance.ClampMoveToWorldBorder(GetRadius(), vecTempPos.x, vecTempPos.y, fDeltaX, fDeltaY, ref fRealNextX, ref fRealNextY);

        vecTempPos.x = fRealNextX;
        vecTempPos.y = fRealNextY;

        this.transform.position = vecTempPos;

        m_fEjectInitialSpeed += Time.deltaTime * m_fEjectAccelerate;
        if (m_fEjectInitialSpeed <= 0f)
        {
            EndEject();
        }

        //	SetPos ( vecTempPos );

        bool bRet = CyberTreeMath.ClampBallPosWithinSCreen(vecTempPos, ref vecTempPos2);
        this.gameObject.transform.position = vecTempPos2;
        if (bRet)
        {
            EndEject();

        }

        SetDirLineVisible(false);
    }

    public void SetDirLineVisible(bool bVisible)
    {
        if (_ba == null)
        {
            return;
        }

        _ba._dir_line.gameObject.SetActive(bVisible);
    }

    int m_nUnfoldStatus = 0;

    /// <summary>
    /// /  技能：变刺
    /// </summary>
    public void BecomeThorn_BeginQianYao()
    {
        _effectBecomeThorn_QianYao.gameObject.SetActive(true);
    }


    float m_fPreUnfoldTimeCount = 0f;
    public void BeginPreUnfold()
    {
        float fScale = _Player.GetUnfoldSkillParam_UnfoldScale();
        vecTempScale.x = fScale;
        vecTempScale.y = fScale;
        vecTempScale.z = 1f;
        _obsceneCircle.transform.localScale = vecTempScale;
        _obsceneCircle.gameObject.SetActive(true);
        //		_obsceneCircle.Begin ( Main.s_Instance.m_fMainTriggerPreUnfoldTime );
        m_nUnfoldStatus = 1;
        m_fPreUnfoldTimeCount = 0f;
    }

    void PreUnfolding()
    {
        if (m_nUnfoldStatus != 1 && m_nUnfoldStatus != 2 )
        {
            return;
        }

        if (IsDead())
        {
            return;
        }

        m_fPreUnfoldTimeCount += Time.deltaTime;

        if (m_fPreUnfoldTimeCount >= _Player.GetUnfoldSkillParam_PreUnfoldTime() - 0.2f) // 播放骨骼动画
        {
            if (m_nUnfoldStatus == 1)
            {
                CSpineManager.s_Instance.PlayBallAnimation(this, CSpineManager.eBallSpineType.unfold, false, 2f);
                m_nUnfoldStatus = 2;
            }
            
        }
    
        if (m_fPreUnfoldTimeCount >= _Player.GetUnfoldSkillParam_PreUnfoldTime())
        {
            _obsceneCircle.gameObject.SetActive(false);
            vecTempScale = _obsceneCircle.transform.localScale;
            vecTempScale.x = 1f;
            vecTempScale.y = 1f;
            vecTempScale.z = 1f;
            _obsceneCircle.transform.localScale = vecTempScale;
            BeginUnfold();
        }
    }

    float m_fUnfoldLeftTime = 0.0f;

    public bool IsUnfolding()
    {
        return (m_nUnfoldStatus == 3);
    }

    void BeginUnfold()
    {
        if (_Player.IsMainPlayer())
        {
            CAudioManager.s_Instance.PlayAudio(CAudioManager.eAudioId.e_audio_unfold);
        }

        m_nUnfoldStatus = 3;
        m_fUnfoldLeftTime = _Player.GetUnfoldSkillParam_UnfoldKeepTim();
        float fScale = _Player.GetUnfoldSkillParam_UnfoldScale();
        vecTempSize.x = fScale;
        vecTempSize.y = fScale;
        vecTempSize.z = 1.0f;

        // _Mouth.transform.localScale = vecTempSize;
        _Trigger.radius = Main.s_Instance.m_fBallTriggerBasrRadius * fScale;
    }


    void Unfolding()
    {
        if (m_nUnfoldStatus != 3 )
        {
            return;
        }

        if (IsDead())
        {
            return;
        }

        m_fUnfoldLeftTime -= Time.deltaTime;
        if (m_fUnfoldLeftTime <= 0.0f)
        {
            EndUnfold();
        }

    }

    public void EndUnfold()
    {
        m_nUnfoldStatus = 0;
        vecTempScale.x = 1f;
        vecTempScale.y = 1f;
        vecTempScale.z = 1f;

        // poppin test
        // _Mouth.transform.localScale = vecTempScale;
        _Trigger.radius = Main.s_Instance.m_fBallTriggerBasrRadius;
    }



    public void ProcessPk_Monster(CMonster monster)
    {
        if (this._Player.IsProtect())
        {
            return;
        }
        
        if (!this._Player.IsMainPlayer()) // 只运算自己控制的球球的吃怪情况，不管别的客户端的球的吃怪情况
        {
            return;
        }
        

        /*
        if (this._Player.IsProtect()) // 处于保护期的球球不能吃怪
        {
            return;
        }
        */
        if (monster.GetMonsterBuildingType() == CMonsterEditor.eMonsterBuildingType.spore)
        {
            if (monster.IsEjecting())
            {
                if (monster.GetBall() == this)
                {
                    return;
                }
            }
        }


        if (GetTriggerSize() > monster.GetTriggerSize())
        {
            if (CheckIfPartiallyCover(this._Trigger, monster._Trigger, MapEditor.s_Instance.m_fYaQiuBaiFenBi))
            {
                this.EatThorn(monster);
            }
        }
        else if (GetTriggerSize() < monster.GetTriggerSize() && !this._Player.IsProtect()) // 处在保护期不能被刺吃
        {
            if (monster.GetMonsterBuildingType() == CMonsterEditor.eMonsterBuildingType.scene_ball) // 场景球可以反吃玩家球。普通怪不能反吃玩家球
            {
                if (CheckIfPartiallyCover(monster._Trigger, this._Trigger, MapEditor.s_Instance.m_fYaQiuBaiFenBi))
                {
                    monster.EatBall(this);
                }
            }
        }
        else
        {
            return;
        }

    }

    public void ProcessPk_SceneBall(CMonster scene_ball)
    {
        // 这个流程废弃
        return;

        if (!this._Player.IsMainPlayer()) // 只运算自己控制的球球的吃怪情况，不管别的客户端的球的吃怪情况
        {
            return;
        }

        if (this._Player.IsProtect()) // 处于保护期的球球不能吃怪
        {
            return;
        }

        if (GetSize() > scene_ball.GetSize())
        {
            if (CheckIfPartiallyCover(this._Trigger, scene_ball._Trigger, MapEditor.s_Instance.m_fYaQiuBaiFenBi))
            {
                Ball_Eat_SceneBall(scene_ball);
            }
        }
        else if (GetSize() < scene_ball.GetSize())
        {
            if (CheckIfPartiallyCover(scene_ball._Trigger, this._Trigger, MapEditor.s_Instance.m_fYaQiuBaiFenBi))
            {
                scene_ball.EatBall(this);
            }

        }
        else
        {
            return;
        }
    }


    public void Ball_Eat_SceneBall(CMonster scene_ball)
    {
        if (scene_ball.m_bIsJueSheng)
        {
            this._Player.photonView.RPC("RPC_EatShengFuPanDingMonster", PhotonTargets.All);
        }

        scene_ball.SetActive(false); // 避免重复吃球

        /*
        float fSceneBallArea = scene_ball.GetConfig().fFoodSize;
        float fCurArea = CyberTreeMath.SizeToArea( GetSize(), Main.s_Instance.m_nShit );
        float fNewArea = fCurArea + fSceneBallArea;
        float fNewSize = CyberTreeMath.AreaToSize(fNewArea, Main.s_Instance.m_nShit);
        _Player.SetBallSize( this.GetIndex(), fNewSize);
        _Player.DestroyMonster(scene_ball);
        */
        float fSceneBallVolume = scene_ball.GetConfig().fFoodSize;
        float fCurVolume = GetVolume();
        float fNewVolume = fSceneBallVolume + fCurVolume;
        float fNewSize = CyberTreeMath.Volume2Scale(fNewVolume);
        Local_SetSize( fNewSize);
        _Player.DestroyMonster(scene_ball);
    }

    public void ProcessPk(Ball ballOpponent)
    {
        if (this._balltype != eBallType.ball_type_ball)
        {
            return;
        }

        // 跟路人甲无关，只跟主吃方和被吃方这两个客户端相关
        // 如果主吃球和被吃球都不是我这个客户端的，那关我毛线事，我的客户端不去运算
        if ((!this._Player.photonView.isMine) && (!ballOpponent._Player.photonView.isMine))
        {
            return;
        }

        if (this._Player.IsProtect() || ballOpponent._Player.IsProtect())
        {
            return;
        }

        if ( ( this._Player.IsSneaking() || ballOpponent._Player.IsSneaking() ) && ( this._Player.GetOwnerId() != ballOpponent._Player.GetOwnerId() ))
        {
            return;
        }


        if (this._Player.photonView.ownerId == ballOpponent._Player.photonView.ownerId)// 队友
        { 
            if (this.IsEjecting() || ballOpponent.IsEjecting())
            {
                return;
            }
            if (this.HaveShell() && ballOpponent.HaveShell())
            {
                return;
            }
        }

        if ( this._Player.IsGold() || ballOpponent._Player.IsGold())
        {
             ballOpponent.Local_Begin_Shell_Temp();
             this.Local_Begin_Shell_Temp();
        }


        if (IsDead() || ballOpponent.IsDead())
        {
            return;
        }

        if ((this._Player != null && this._Player.IsProtect()) || (ballOpponent._Player != null && ballOpponent._Player.IsProtect()))
        {
            return;
        }

        int nRet = Ball.WhoIsBig(this, ballOpponent);
        if (this._Player.photonView.ownerId == ballOpponent._Player.photonView.ownerId)
        {
            if (nRet >= 0)
            {
                this.EatTeammate(ballOpponent);
            }
        }
        else
        {
            if (nRet == 1)
            {
                this.PreEat(ballOpponent);
            }
        }

    } 

    public void EatTeammate(Ball ballOpponent)
    {
        if (!CheckIfPartiallyCover(this._Trigger, ballOpponent._Trigger, MapEditor.s_Instance.m_fYaQiuBaiFenBi_Self))
        {
            return;

        }

        this.Eat(ballOpponent);
    }

   


    void PreEat(Ball ballOpponent)
    {
        if (this._balltype != eBallType.ball_type_ball)
        {
            return;
        }

        if (!CheckIfPartiallyCover(this._Trigger, ballOpponent._Trigger, MapEditor.s_Instance.m_fYaQiuBaiFenBi))
        {
            return;

        }

        this.Eat(ballOpponent);
    }

    public void DoDraw()
    {
        /*
        float fCurArea = GetArea();
        float fAreaDelta = m_fPreDrawDeltaTime * MapEditor.s_Instance.m_fXiQiuSuDu;
        m_fPreDrawDeltaTime = 0.0f;
        float fNewArea = fCurArea + fAreaDelta;
        if (fNewArea <= 0.0f) {
            SetDead(true);
            DestroyBall();
            return;
        }
        Local_SetSize(CyberTreeMath.AreaToSize(fNewArea, Main.s_Instance.m_nShit));
        SetNeedSyncSize();
        */
    }

    void DrawBall(Ball ballOpponent)
    {
        /*
		if (this.photonView.ownerId == ballOpponent.photonView.ownerId) {
			if (this.m_bShell && ballOpponent.m_bShell) {
				return;
			}
		}
		*/
        if (ballOpponent.IsDead())
        {
            return;
        }

        // 因为被吸的是绝对数量而不是自身体积的百分比，因此非常好计算。
        this.PreDraw(Time.deltaTime);
        ballOpponent.PreDraw(-Time.deltaTime);
    }

    float m_fPreDrawDeltaTime = 0.0f;
    public void PreDraw(float fDeltaTime)
    {
        m_fPreDrawDeltaTime += fDeltaTime;

    }

    public void AddArea(float fAreaToAdd)
    {

        float fCurArea = CyberTreeMath.SizeToArea(GetSize(), Main.s_Instance.m_nShit);
        float fNewArea = fCurArea + fAreaToAdd;
        float fNewSize = CyberTreeMath.AreaToSize(fNewArea, Main.s_Instance.m_nShit);
        SetSize(fNewSize);

    }

    public void AddSize(float fSizeToAdd)
    {
        if (IsUnfolding())
        {

        }
        else
        {
            SetSize(CyberTreeMath.CalculateNewSize(GetSize(), fSizeToAdd, Main.s_Instance.m_nShit));
        }
    }

    void Eat(Ball ballOpponent)
    {
        EatBall(ballOpponent);
    }

    public void EatBean(CMonster bean)
    {
        if (bean.IsDead())
        { // 避免重复吃豆子(Unity的很多操作都要下一贞才真正执行，所以完全有可能被吃的东西没有立刻消失，又被吃一次)
            return;
        }

        if (_Player.photonView.isMine)
        { // 吃球者只在自己那个客户端（MainPlayer端）涨体积，在别人的客户端吃了豆子就不涨了，只涨一边
            /*
            float fCurSize = GetSize();
			float fBeanSize = bean.GetFloatParam(0);//Main.s_Instance.m_fBeanSize;
            float fAreaToAdd = CyberTreeMath.SizeToArea(fBeanSize, Main.s_Instance.m_nShit);
            float fCurArea = CyberTreeMath.SizeToArea(fCurSize, Main.s_Instance.m_nShit);
            float fNewArea = fCurArea + fAreaToAdd;
            float fNewSize = CyberTreeMath.AreaToSize(fNewArea, Main.s_Instance.m_nShit);
            _Player.SetBallSize(GetIndex(), fNewSize);
            */
            float fBeanVolume = bean.GetConfig().fFoodSize;
            float fCurVolume = GetVolume();
            float fNewVolume = fBeanVolume + fCurVolume;
            float fNewSize = CyberTreeMath.Volume2Scale(fNewVolume);
            Local_SetSize( fNewSize );
        }

        ResourceManager.RecycleMonster(bean);
    }


    Thorn.ColorThornParam m_paramColorThorn = new Thorn.ColorThornParam();
    public void EatThorn(CMonster thorn)
    {
        if (thorn.IsDead())
        { // 避免重复吃刺
            return;
        }

        if (!_Player.IsMainPlayer()) // 非本客户端MainPlayer的球球执行吃刺动作
        {
            /*
            if (thorn.GetMonsterBuildingType() == CMonsterEditor.eMonsterBuildingType.bean || thorn.GetMonsterBuildingType() == CMonsterEditor.eMonsterBuildingType.spore)
            {
                _Player.DestroyMonster(thorn);
            }
            */
            return;
        }

        CMonsterEditor.sThornConfig config = CMonsterEditor.s_Instance.GetMonsterConfigById(thorn.GetConfigId());

        // 已经不能爆球了就别吃刺
        /*
        if (thorn.GetMonsterBuildingType() == CMonsterEditor.eMonsterBuildingType.thorn)
        {
            // 判断其能不能爆球
            _Player.UpdateBuffEffect_Explode();
            int nChildNum = 0;
            float fChildSize = 0f;
            float fMotherLeftSize = 0;
            if (!CheckIfReallyCanExplode(config, _Player.GetExplodeChildNumBuffEffect(), _Player.GetExplodeMotherLeftBuffEffect(), ref nChildNum, ref fChildSize, ref fMotherLeftSize))
            {
                return;
            }
        }
        */

        // 拉粑粑技能先屏蔽掉了
        //float fThornSelfSize = thorn
        /*
        if ( thorn.GetThornType() == Thorn.eThornType.baba )
        {
            if ( thorn.GetOwnerId() == _Player.GetOwnerId() ) // 不吃自己拉的粑粑
            {
                return;
            }
		}
		*/

        thorn.SetDead(true);

        // 吃刺也会增加球本身的总体积。但如果当前队伍已经不能爆刺了，则吃刺不增加球的体积
        // 先执行“增加体积”，再炸。以免不小心炸死了就没有增加体积
        // poppin to youhua 吃一个刺居然发了这么多条网络协议，看看有没有可以优化
        if (_Player.IsMainPlayer())
        {
            float fCurTiJi = GetVolume();
            float fNewTiJi = fCurTiJi + config.fFoodSize; // 这里的fFoodSize是指体积值
            float fNewSize = CyberTreeMath.Volume2Scale(fNewTiJi);
            // _Player.SetBallSize(GetIndex(), fNewSize); 
            Local_SetSize(fNewSize);
           

            if (config.fBuffId > 0)
            { // 这个刺带Buff
                _Player.AddBuff((int)config.fBuffId);
            }

            // 经验值
            if (config.fExp > 0f)
            {
                _Player.AddExp((int)config.fExp);
            }

            // 金钱
            if (config.fMoney > 0)
            {
                _Player.AddMoney((int)config.fMoney);
            }

            // 吃刺直接炸，没有“刺环”的概念了
            if ((int)config.fExplodeChildNum > 0 && ( !_Player.IsMagicShielding() ) )
            { // 如果配置的炸球个数等于0，则不炸球，这种刺可以当豆子用
                Explode(config);
            }

            if ( (CMonsterEditor.eMonsterBuildingType)config.nType == CMonsterEditor.eMonsterBuildingType.pick_skill ) // 是捡拾类技能 
            {
                CSkillSystem.s_Instance.PickSkill(config.nSkillId);
            }
        }

        _Player.DestroyMonster(thorn);
        if (thorn.m_bIsJueSheng)
        {
            this._Player.photonView.RPC("RPC_EatShengFuPanDingMonster", PhotonTargets.All);
        }
    }

    public void SetThornSize(float fThornSize)
    {
        Local_SetThornSize(fThornSize);
    }

    [PunRPC]
    public void RPC_SetThornSize(float fThornSize)
    {
        Local_SetThornSize(fThornSize);
    }

    float m_fThornSize = 0.0f;
    public void Local_SetThornSize(float fThornSize)
    {   // “刺环”概念废弃
        /*
        if (_thorn == null) {
            return;
        }

        m_fThornSize = fThornSize;
        float fBallSize = GetSize();
        if (m_fThornSize > fBallSize) {
            m_fThornSize = fBallSize;
        }
        if (fBallSize == 0) {
            return;
        }
        float fThornRealScaleSize = m_fThornSize / fBallSize;
        vecTempScale.x = fThornRealScaleSize;
        vecTempScale.y = fThornRealScaleSize;
        vecTempScale.z = 1.0f;
        _thorn.transform.localScale = vecTempScale;
        //_thornForAlpha.transform.localScale = vecTempScale;
        if (_Player.photonView.isMine && CheckIfExplode()) {
            Explode();
        }
		*/
    }

    public void SetVolume( float fVolume )
    {
        float fNewSize = CyberTreeMath.Volume2Scale(fVolume);
        Local_SetSize( fNewSize );
    }

    public float GetThornSize()
    {
        return m_fThornSize;
    }

    bool CheckIfExplode()
    {
        float fBallSize = GetSize();
        if (m_fThornSize < fBallSize)
        {
            return false;
        }

        if (fBallSize < 2.0f)
        {
            return false;
        }

        return true;
    }

    public static Vector2[] s_aryBallExplodeDirection = {
        new Vector2( 0.0f, 1.0f ),
        new Vector2( 0.707f, 0.707f ),
        new Vector2( 1.0f, 0.0f ),
        new Vector2( 0.707f, -0.707f ),
        new Vector2( 0, -1.0f ),
        new Vector2( -0.707f, -0.707f ),
        new Vector2( -1.0f, 0.0f ),
        new Vector2( -0.707f, 0.707f ),

    };

    /*
	 * 爆裂的规则：
	 * 1、准备爆裂的母球，半径最小为2。不然没法爆，因为爆出的小球最小半径得是1（一个孢子的尺寸），并且爆了之后母球剩下的半径至少也得是1.
	 * 2、母球分一半的半径出来爆。
	 * 3、能爆成8等分尽量8等分（确保爆出来的每个小球的半径大于等于1）；不够8等分则能爆几个算几个。
	 * */
    public float GetMinExplodeSize()
    {
        return Main.BALL_MIN_SIZE;
    }

    float m_fShitExplodeTime = 0.0f;


    public void Explode(CMonsterEditor.sThornConfig config)
    {
        if (!_Player.photonView.isMine)
        {
            return;
        }

        _Player.UpdateBuffEffect_Explode();

        int nChildNum = 0;
        float fMotherLeftSize = 0f;
        float fChildSize = 0f;
        if (!CheckIfReallyCanExplode(config, _Player.GetExplodeChildNumBuffEffect(), _Player.GetExplodeMotherLeftBuffEffect(), ref nChildNum, ref fChildSize, ref fMotherLeftSize))
        {
            return;
        }

        Main.s_Instance.BeginCameraSizeProtect(config.fExplodeRunTime);
        _Player.ExplodeBall(GetIndex(), config.szId, nChildNum, fChildSize, fMotherLeftSize);
    }

    public void Explode_CausedBySkill(CMonsterEditor.sThornConfig config, float fChildVolume)
    {
        _Player.UpdateBuffEffect_Explode();

        int nChildNum = 0;
        float fMotherLeftSize = 0f;
        float fChildSize = 0f;
        if (!CheckIfReallyCanExplode_CausedBySkill(config, _Player.GetExplodeChildNumBuffEffect(), fChildVolume, ref nChildNum, ref fChildSize, ref fMotherLeftSize))
        {
            return;
        }
        Main.s_Instance.BeginCameraSizeProtect(config.fExplodeRunTime);
        _Player.ExplodeBall(GetIndex(), config.szId, nChildNum, fChildSize, fMotherLeftSize);
    }

    public bool CheckIfReallyCanExplode_CausedBySkill(CMonsterEditor.sThornConfig config, float fChildNumBuffEffect, float fChildVolume, ref int nChildNum, ref float fChildSize, ref float fMotherLeftSize)
    {
        int nAvailableNum = _Player.GetCurAvailableBallCount();
        if (nAvailableNum < Main.s_Instance.m_nExplodeMinNum)// 至少得有1个空位的余额才行，不然炸个屁的球啊
        {
            return false;
        }

        float fMotherCurVolume = GetVolume();
        int nExpectChildNum = (int)config.fExplodeChildNum;
        nExpectChildNum = (int)(nExpectChildNum * fChildNumBuffEffect);

        if (nExpectChildNum < Main.s_Instance.m_nExplodeMinNum)
        {
            return false;
        }

        bool bFound = false;
        for (int i = nExpectChildNum; i >= Main.s_Instance.m_nExplodeMinNum; i--)
        {
            fMotherCurVolume -= i * fChildVolume;
            if (fMotherCurVolume >= 0)
            {
                nChildNum = i;
                bFound = true;
                break;
            }
        } // end for i
        if (!bFound)
        {
            return false;
        }

        fChildSize = CyberTreeMath.Volume2Scale(fChildVolume);
        fMotherLeftSize = CyberTreeMath.Volume2Scale(fMotherCurVolume);

        return true;
    }

    public bool CheckIfReallyCanExplode(CMonsterEditor.sThornConfig config, float fChildNumBuffEffect, float fMotherLeftBuffEffect, ref int nChildNum, ref float fChildSize, ref float fMotherLeftSize)
    {
        int nAvailableNum = _Player.GetCurAvailableBallCount();
        if (nAvailableNum < Main.s_Instance.m_nExplodeMinNum)
        { // 至少得有1个空位的余额才行，不然炸个屁的球啊
            return false;
        }

        float fMotherCurArea = GetVolume();

        // buff可能会影母体存留百分比
        float fRealMotherLeftPercent = config.fExplodeMotherLeftPercent + fMotherLeftBuffEffect;
        //Debug.Log( "母体存留百分比：" + config.fExplodeMotherLeftPercent + " , " +  fRealMotherLeftPercent);

        float fExplodePercent = 1f - fRealMotherLeftPercent;
        if (fExplodePercent <= 0f)
        {
            return false;
        }
        float fChildsArea = fMotherCurArea * fExplodePercent; // (预期母球分出来的体积，即子球的总体积)

        // buff可能会影预期分球数
        int nExpectNumConfig = (int)config.fExplodeChildNum; // 预期分球个数，如果不够预期，则能分几个算几个（这是原始配置中的个数）
        int nExpectNum = (int)(nExpectNumConfig * fChildNumBuffEffect); // （配置和个数乘以Buff的加成数，就是实际打算分的个数）
                                                                        //Debug.Log( "分球数：" + nExpectNumConfig + "," + nExpectNum );

        int nRealNum = 0;
        if (nExpectNum > nAvailableNum)
        {
            nExpectNum = nAvailableNum;
        }
        float fRealChildArea = 0f;// (实际子球的总面积)
        for (nRealNum = nExpectNum; nRealNum >= Main.s_Instance.m_nExplodeMinNum; nRealNum--)
        {
            fChildSize = CyberTreeMath.Volume2Scale(fChildsArea / nRealNum);
            if (fChildSize >= Main.BALL_MIN_SIZE)
            {
                fRealChildArea = fChildsArea;
                break;
            }
        }

        if (nRealNum < Main.s_Instance.m_nExplodeMinNum)
        {
            return false;
        }

        // 这里再结合Buff



        nChildNum = nRealNum;

        float fMotherLeftArea = fMotherCurArea - fRealChildArea;
        fMotherLeftSize = CyberTreeMath.Volume2Scale(fMotherLeftArea);
        if (fMotherLeftSize < Main.BALL_MIN_SIZE)
        { // 如果母球剩余半径太小（小于本游戏规定的半径最小值），就直接让它死掉
            fMotherLeftSize = 0f;
        }


        return true;
    }

    public void DoExplode(string szThornConfigId, int nChildNum, float fChildSize, float fMotherLeft, double dOccurTime)
    {
        if (IsDead())
        {
            Debug.LogError("DoExplode   IsDead ()");
            return;
        }

        int nAvailableNum = _Player.GetCurAvailableBallCount();
        if (nAvailableNum <= Main.s_Instance.m_nExplodeMinNum)
        {
            return;
        }

        if (IsEjecting())
        {
            EndEject();
        }

        // CAudioManager.s_Instance.audio_explode.Play();

        float fDelayTime = (float)(PhotonNetwork.time - dOccurTime);
        CMonsterEditor.sThornConfig config = CMonsterEditor.s_Instance.GetMonsterConfigById(szThornConfigId);
        float fRealRunTime = config.fExplodeRunTime - fDelayTime;
        if (fRealRunTime < Main.s_Instance.m_fMinRunTime)
        {
            fRealRunTime = Main.s_Instance.m_fMinRunTime;
        }
        // 生成小球
        float fRadiusMother = GetRadius();
        if (IsUnfolding())
        {
            fRadiusMother /= Main.s_Instance.m_fUnfoldSale;
        }

        if (_Player.IsMainPlayer())
        {
            CAudioManager.s_Instance.PlayAudio(CAudioManager.eAudioId.e_audio_explode);
        }

        vecTempPos = GetPos();
        Ball new_ball = null;
        float fRunDistance = config.fExplodeRunDistance * fRadiusMother;
        if (config.fExplodeFormationId == 0)
        {
            List<Vector2> lstDir = MapEditor.s_Instance.GetExplodeDirList(nChildNum);
            for (int i = 0; i < nChildNum; i++)
            {
                Vector2 vecDir = lstDir[i];
                new_ball = ExplodeChildBall_0(config, vecTempPos.x, vecTempPos.y, fRealRunTime, fChildSize, fRunDistance, vecDir);
            }
        }
        else if (config.fExplodeFormationId == 1)
        {
            float nNumOfEveryDir = nChildNum / 4;
            if (nChildNum == 0)
            {
                int nShit = 123;

            }

            if (nNumOfEveryDir > 0)
            {
                float fSegDistance = fRunDistance / nNumOfEveryDir;
                for (int i = 0; i < nChildNum; i++)
                {
                    new_ball = ExplodeChildBall_1(i, config, vecTempPos.x, vecTempPos.y, fRealRunTime, fChildSize, fRunDistance, fSegDistance);
                }
            }
        }


        // 母球剩余

        if (fMotherLeft <= 0f)
        {
            _Player.DestroyBall(GetIndex());
        }
        else
        {
            Local_SetSize(fMotherLeft);
            Local_SetShellInfo(config.fExplodeShellTime, 0f, eShellType.explode_ball);
            Local_BeginShell();
        }
    }

    // 圆形布阵
    Ball ExplodeChildBall_0(CMonsterEditor.sThornConfig config, float fExplodePosX, float fExplodePosY, float fRealRunTime, float fChildSize, float fRunDistance, Vector2 vecDire)
    {
        Ball new_ball = _Player.ReuseOneBall(); // 这种逻辑会不会有隐患？在网络丢包的情况下。 稍后再探讨，暂时这样
        if (new_ball == null)
        {
            Debug.LogError("ExplodeChildBall_0");
            return null;
        }


        vecTempPos = GetPos();
        vecTempPos.x = fExplodePosX;
        vecTempPos.y = fExplodePosY;
        new_ball.SetShellEnable(false);

        float fSpeed = fRunDistance / fRealRunTime;
        new_ball.Local_SetPos(vecTempPos);
        new_ball.Local_SetSize(fChildSize);
        new_ball.SetDir( vecDire );
        new_ball.Local_SetShellInfo(_Player.GetRealShellTime(config.fExplodeShellTime), 0f, eShellType.explode_ball);
        new_ball.Local_BeginEject_New(fSpeed, fRunDistance, false, false, config.fExplodeStayTime);


        return new_ball;
    }

    // 十字布阵
    Ball ExplodeChildBall_1(int idx, CMonsterEditor.sThornConfig config, float fExplodePosX, float fExplodePosY, float fRealRunTime, float fChildSize, float fRunDistance, float fSegDistance)
    {
        int nDeShu = idx / 4;
        int nYuShu = idx % 4;
        if (nYuShu == 0)
        {
            vec2Temp.x = 0f;
            vec2Temp.y = 1f;
        }
        else if (nYuShu == 1)
        {
            vec2Temp.x = 0f;
            vec2Temp.y = -1f;
        }
        else if (nYuShu == 2)
        {
            vec2Temp.x = -1f;
            vec2Temp.y = 0f;
        }
        else if (nYuShu == 3)
        {
            vec2Temp.x = 1f;
            vec2Temp.y = 0f;
        }
        float fRealDis = fRunDistance - nDeShu  * fSegDistance;
        if (fRealDis < 0)
        {
            fRealDis = 0;
        }
        Ball new_ball = _Player.ReuseOneBall(); // 这种逻辑会不会有隐患？在网络丢包的情况下。 稍后再探讨，暂时这样
        if (new_ball == null)
        {
            Debug.LogError("ExplodeChildBall_1");
            return null;
        }

        vecTempPos = GetPos();
        vecTempPos.x = fExplodePosX;
        vecTempPos.y = fExplodePosY;
        new_ball.SetShellEnable(false);

        float fSpeed = fRealDis / fRealRunTime;
        
        new_ball.Local_SetPos(vecTempPos);
        new_ball.Local_SetSize(fChildSize);
        new_ball.SetDir(vec2Temp);
        new_ball.Local_SetShellInfo(_Player.GetRealShellTime(config.fExplodeShellTime), 0f, eShellType.explode_ball);
        new_ball.Local_BeginEject_New(fSpeed, fRealDis, false, false, config.fExplodeStayTime);

        return new_ball;
    }

    public struct EatNode
    {
        public Ball eatenBall;
        public float eatenSize;
    };

    List<EatNode> m_lstEaten = new List<EatNode>();

    void AddEatenNode(EatNode node)
    {
        m_lstEaten.Add(node);
    }

    void ProcessEatenNode()
    {
        for (int i = m_lstEaten.Count - 1; i >= 0; i--)
        {
            EatNode node = m_lstEaten[i];

            if (node.eatenBall == null)
            {
                float fMotherSize = GetSize();
                float fNewSize = CyberTreeMath.CalculateNewSize(fMotherSize, node.eatenSize, Main.s_Instance.m_nShit);
                SetSize(fNewSize);
                m_lstEaten.Remove(node);
            }
        }
    }

    bool CheckIfInEatenList(Ball the_eater, Ball ballOpponent)
    {
        for (int i = m_lstEaten.Count - 1; i >= 0; i--)
        {
            EatNode node = m_lstEaten[i];
            if (node.eatenBall == the_eater || node.eatenBall == ballOpponent)
            {
                return true;
            }
        }

        return false;
    }

    bool m_bEaten = false;
    public void SetEaten(bool val)
    {
        m_bEaten = val;
    }

    public void SetVisible(bool val)
    {
        this.gameObject.SetActive(val);
    }

    public bool IsEaten()
    {
        return m_bEaten;
    }

    public virtual void SetDead(bool val, int nEaterOwnerId = 0)
    {
        m_bDead = val;
        this.gameObject.SetActive(!m_bDead);

        if (m_bDead)
        {
            ProcessGrassWhenDead(); // ??
        }
        else
        {
        }
    }

 

    CGrass m_Grass = null;
    float m_fEnterGrassTime = 0f;

    public CGrass GetGrass()
    {
        return m_Grass;
    }

    void ProcessGrassWhenDead()
    {
        LeaveGrass();
    }

    List<CGrass> m_lstGrass = new List<CGrass>();
    public void LeaveGrass()
    {
        if (m_Grass)
        {
            m_Grass.RemoveBall(this);
        }
        m_Grass = null;
    }

    public void EnterGrass(CGrass grass)
    {
        m_Grass = grass;
        grass.AddBall(this);
        if (m_Grass)
        {
            m_fEnterGrassTime = (float)PhotonNetwork.time; // 进入草丛的时间
        }
    }


    public void Hide()
    {
        vecTempPos = GetPos();
        vecTempPos.x = -10000;
        vecTempPos.y = -10000;
        Local_SetPos(vecTempPos);
    }

    public static bool IsSelf( Ball ball1, Ball ball2 )
    {
        return ball1._Player.GetOwnerId() == ball2._Player.GetOwnerId();
    }

    public static bool IsTeammate(Ball ball1, Ball ball2)
    {
        return false;
    }

    // 发起“吃球”事件。发起该事件的客户端可能是主吃方，也可能是被吃方，以那边先触发为准
    public void EatBall(Ball ballOpponent)
    {
        if ((!this._Player.photonView.isMine) && (!ballOpponent._Player.photonView.isMine))
        { // 发起吃球的客户端必须是MainPlayer
            return;
        }

        if (ballOpponent.IsEaten())
        { // 避免重复吃球 
            return;
        }

        if (ballOpponent.IsDead())
        {
            Debug.LogError("已经死了从个还在吃哦");// 避免重复吃球 
            return;
        }

        float fEaterVolume = GetVolume();
        float fPoorthingVolume = ballOpponent.GetVolume();// CyberTreeMath.SizeToArea(fPoorthingSize, Main.s_Instance.m_nShit);

        // 湮灭状态
        if (ballOpponent._Player.IsAnnihilaing() && ( !IsSelf( this, ballOpponent) ) && ( !IsTeammate(this, ballOpponent) ) )  
        {
            fPoorthingVolume *= ballOpponent._Player.GetAnnihilatePercent();
        }

        float fNewVolume = fEaterVolume + fPoorthingVolume;

        if (fNewVolume < 0)
        {
            fNewVolume = 0;
        }
        float fNewSize = CyberTreeMath.Volume2Scale(fNewVolume);// CyberTreeMath.AreaToSize(fNewArea, Main.s_Instance.m_nShit);
                                                                // Debug.Log( "吃球：" + fEaterSize + "," + fPoorthingSize + ","  + fNewSize);
        if (fNewSize < Main.BALL_MIN_SIZE)
        {
            fNewSize = Main.BALL_MIN_SIZE;
        }
        SetSize(fNewSize);
        float fTotalEatArea = this._Player.GetTotalEatArea();
        if (ballOpponent._Player.GetOwnerId() != this._Player.GetOwnerId()) // 吃自己的球不算哦，必须吃别人的球。另外，场景球、豆子、孢子都是走的Monster流程而不是Ball流程，所以不用担心和这里混淆起来
        {
            fTotalEatArea += fPoorthingVolume;
        }

        float fExplodeChildVolume = 0f;
        if (ballOpponent._Player.IsBecomeThorn())
        {
            fExplodeChildVolume = ballOpponent.GetVolume();
        }
        this._Player.photonView.RPC("RPC_EatSucceed", PhotonTargets.All, this.GetIndex(), fNewSize, fTotalEatArea, fExplodeChildVolume, ballOpponent._Player.GetBecomeThornExplodeConfigId());


        // 先把被吃者状态置为“已吃”，但还不能直接SetDead，Dead状态必须由该球的MainPlayer来发起.不然会引起时序混乱
        ballOpponent.SetEaten(true);
        ballOpponent.SetVisible(false);
        ballOpponent._Player.photonView.RPC("RPC_BeEaten", PhotonTargets.All, ballOpponent.GetIndex(), this._Player.GetOwnerId());
    }

    public void DestroyBall()
    {

    }




    void FrameInterpolation(Vector3 dest)
    {
        Local_SetPos(dest.x, dest.y, dest.z);
    }


    public void Terminate()
    {
        //if (this.photonView.isMine) {
        //	PhotonNetwork.Destroy ( this.gameObject );
        //}
    }

    public static int WhoIsBig(Ball ball1, Ball ball2)
    {
        float fSize1 = ball1.GetRadius();// CyberTreeMath.FloatTrim(ball1._Trigger.bounds.size.x);
        float fSize2 = ball2.GetRadius();// CyberTreeMath.FloatTrim(ball2._Trigger.bounds.size.x);
        if (fSize1 == fSize2)
        {
            return 0;
        }
        else if (fSize1 > fSize2)
        {
            return 1;
        }
        else
        {
            return -1;
        }
    }

    bool CheckIfTotallyCover(CircleCollider2D circle1, CircleCollider2D circle2)
    {
        float r1 = circle1.bounds.size.x / 2.0f;
        float r2 = circle2.bounds.size.x / 2.0f;

        float deltaX = circle1.bounds.center.x - circle2.bounds.center.x;
        float deltaY = circle1.bounds.center.y - circle2.bounds.center.y;
        float dis = Mathf.Sqrt(deltaX * deltaX + deltaY * deltaY);
        if (dis <= Mathf.Abs(r1 - r2))
        {
            return true;
        }

        return false;
    }


    bool CheckIfPartiallyCover(CircleCollider2D c1, CircleCollider2D c2, float fYaQiuBaiFenBi)
    {
        // CircleCollider2D c1 = this._Trigger;
        //  CircleCollider2D c2 = ballOpponent._Trigger;

        float d;
        float s, s1, s2, s3, angle1, angle2;
        float r1 = c1.bounds.size.x / 2f; // this ball radius
        float r2 = c2.bounds.size.x / 2f;        // opponent radius
        float r1_2cifang = r1 * r1;
        float r2_2cifang = r2 * r2;

        d = Mathf.Sqrt((c1.bounds.center.x - c2.bounds.center.x) * (c1.bounds.center.x - c2.bounds.center.x) + (c1.bounds.center.y - c2.bounds.center.y) * (c1.bounds.center.y - c2.bounds.center.y));
        float d_2cifang = d * d;
        if (d >= (r1 + r2))
        {//两圆相离
            return false;
        }
        if ((r1 - r2) >= d)
        {//两圆内含,c1大
            return true;
        }

        angle1 = Mathf.Acos((r1_2cifang + d_2cifang - r2_2cifang) / (2 * r1 * d));
        angle2 = Mathf.Acos((r2_2cifang + d_2cifang - r1_2cifang) / (2 * r2 * d));

        s1 = angle1 * r1_2cifang;
        s2 = angle2 * r2_2cifang;
        s3 = r1 * d * Mathf.Sin(angle1);
        s = s1 + s2 - s3;

        float fOpponentMianJi = Mathf.PI * r2_2cifang;
        float fCurPercent = s / fOpponentMianJi;

        if (fCurPercent >= fYaQiuBaiFenBi/*MapEditor.s_Instance.m_fYaQiuBaiFenBi*/)
        {
            return true;
        }

        return false;
    }

    public bool CheckIfCanForceSpit()
    {
        if (IsEjecting())
        {
            return false;
        }

        return true;
    }

    public bool SpitBean(float fRealTimeSpitSporeDistance, string szMonsterId, uint uPlayerSporeGuid)
    {
        CMonsterEditor.sThornConfig config = CMonsterEditor.s_Instance.GetMonsterConfigById(szMonsterId/*Main.s_Instance.m_szSpitSporeBeanType*/ );

        float fMotherSize = GetSize();
        float fMotherCurVolume = GetVolume();
        float fMotherLeftVolume = fMotherCurVolume - config.fFoodSize; // foodsize是体积値
        if (fMotherLeftVolume <= 0)
        {
            return false;
        }
        float fMotherLeftSize = CyberTreeMath.Volume2Scale(fMotherLeftVolume);
        if (fMotherLeftSize < Main.BALL_MIN_SIZE)
        {
            return false;
        }

        CMonster spore = ResourceManager.s_Instance.ReuseMonster(); // 孢子没必要具有网络属性，就是每个客户端的单机版
        if (spore == null)
        {
            return false;
        }

        spore.SetGuid((long)uPlayerSporeGuid);
        spore.SetPlayer(_Player);
        spore.SetMonsterBuildingType(CMonsterEditor.eMonsterBuildingType.spore);
        spore.SetTriggerType(BallTrigger_New.eTriggerType.spore);
        spore.SetConfigId(szMonsterId);
        spore.SetAutoReborn(false);
        spore.SetMonsterType(CMonster.eMonsterType.spit_spore);
        spore.transform.parent = Main.s_Instance.m_goSpores.transform;
        Local_SetSize(fMotherLeftSize);
        spore.SetDir(GetDir());
        vecTempPos = GetPos(); // 孢子从球球的正中心出来
                               //vecTempPos.x += (GetRadius() + spore.GetDiameter()) * GetDir().x;
                               //vecTempPos.y += (GetRadius() + spore.GetDiameter()) * GetDir().y;
        vecTempPos.z = 0f;
        spore.SetPos(vecTempPos);
        fRealTimeSpitSporeDistance += GetRadius();
        spore.BeginEject(fRealTimeSpitSporeDistance/*Main.s_Instance.m_fSpitSporeRunDistance*/, 0.5f/*Main.s_Instance.m_fSpitRunTime*/ );
        spore.SetBall(this);
        CClassEditor.s_Instance.AddSpore(uPlayerSporeGuid, (uint)_Player.GetOwnerId(), spore);


        return true;
    }

    // 废弃。没有“孢子”的概念了。就吐豆子SpitBean
    /*
	public Spore SpitSpore()
	{
		float fMotherSize = GetSize();
		float fMotherLeftSize = 0.0f;
		if (!CheckIfCanSpitBall (fMotherSize, Main.BALL_MIN_SIZE, ref fMotherLeftSize)) {
			return null;
		}
		Main.s_Instance.s_Params [0] = Main.eInstantiateType.Spore;
		Main.s_Instance.s_Params [1] = 444;
		GameObject goSpore = Main.s_Instance.PhotonInstantiate (Main.s_Instance.m_preSpore, Vector3.zero, Main.s_Instance.s_Params );//GameObject.Instantiate( Main.s_Instance.m_preSpore );
		Spore spore = goSpore.GetComponent<Spore>();
		goSpore.transform.parent = Main.s_Instance.m_goSpores.transform;
		spore._srMain.color = this._srMain.color;
		SetSize ( fMotherLeftSize );
        spore.SetDir( _dirx, _diry );
        vecTempPos = GetPos();
        vecTempPos.x += ( GetRadius() + spore.GetRadius() ) * _dirx;
        vecTempPos.y += ( GetRadius() + spore.GetRadius() ) * _diry;
        vecTempPos.z = -spore.GetSize();
        spore.SetPos(vecTempPos);
        float fSpitBallInitSpeed = CyberTreeMath.GetV0 ( Main.s_Instance.m_fSpitSporeRunDistance, Main.s_Instance.m_fSpitRunTime  );
        float fSpitBallAccelerate = CyberTreeMath.GetA (  Main.s_Instance.m_fSpitSporeRunDistance, Main.s_Instance.m_fSpitRunTime);
        spore.BeginEject ( fSpitBallInitSpeed, fSpitBallAccelerate, Main.s_Instance.m_fSpitRunTime );
		return spore;

	}
	*/



    public float GetBoundsSize()
    {
        return _srMouth.bounds.size.x; // ;_Trigger.bounds.size.x;
    }

    public Vector2 GetDir()
    {
        return m_vecdirection;
    }


    public void SetDir(Vector2 dir)
    {
        m_vecdirection = dir;
    }

    public Vector3 GetPos()
    {
        return this.gameObject.transform.position;
    }

    public void GetPos(ref float fX, ref float fY)
    {
        fX = this.gameObject.transform.position.x;
        fY = this.gameObject.transform.position.y;
    }

    public float GetPosX()
    {
        return GetPos().x;
    }

    public float GetPosY()
    {
        return GetPos().y;
    }

    public void SetLocalPosition(Vector3 pos)
    {
        this.transform.localPosition = pos;
    }

    public Vector3 GetLocalPosition()
    {
        return this.transform.localPosition;
    }

    public void SetPos(Vector3 pos)
    {
        Local_SetPos(pos);
    }


    public void Local_SetPos(float fX, float fY, float fZ)
    {
        vecTempPos.x = fX;
        vecTempPos.y = fY;
        vecTempPos.z = fZ;
        this.gameObject.transform.position = vecTempPos;
    }

    public void Local_SetPos(Vector3 pos)
    {
        this.gameObject.transform.position = pos;
    }
    
    float m_fChildSize = 0.0f;
    float m_fMotherLeftSize = 0.0f;
    float m_fChildThornSize = 0.0f;
    float m_fMotherLeftThornSize = 0.0f;
    public void CalculateChildSize(float fForceSpitPercent)
    {
        float fMotherVolume = GetVolume();
        float fChildVolume = fForceSpitPercent * fMotherVolume;
        float fChildSize = CyberTreeMath.Volume2Scale(fChildVolume);
        float fMotherLeftVolume = fMotherVolume - fChildVolume;
        if (fMotherLeftVolume < 0)
        {
            fMotherLeftVolume = 0;
        }
        float fMotherLeftSize = CyberTreeMath.Volume2Scale(fMotherLeftVolume);
        m_fChildSize = fChildSize;
        m_fMotherLeftSize = fMotherLeftSize;
        if (m_fChildSize < Main.BALL_MIN_SIZE || m_fMotherLeftSize < Main.BALL_MIN_SIZE || m_fMotherLeftSize < m_fChildSize)
        {
            CalculateChildSize_ByVolume(Main.m_fBallMinVolume);
        }
    }

    public void CalculateChildSize_ByVolume(float fMinVolume)
    {
        float fMotherVolume = GetVolume();
        float fChildVolume = fMinVolume;
        float fChildSize = CyberTreeMath.Volume2Scale(fChildVolume);
        float fMotherLeftVolume = fMotherVolume - fChildVolume;
        if (fMotherLeftVolume < 0)
        {
            fMotherLeftVolume = 0;
        }
        float fMotherLeftSize = CyberTreeMath.Volume2Scale(fMotherLeftVolume);
        m_fChildSize = fChildSize;
        m_fMotherLeftSize = fMotherLeftSize;
    }

    public static float CalculateChildSize( float fMotherSize, float fPercent, ref float fRadius)
    {
        float fMotherVolume = CyberTreeMath.Scale2Volume(fMotherSize, ref fRadius);
        float fChildVolume = fMotherVolume * fPercent;
        float fChildSize = CyberTreeMath.Volume2Scale(fChildVolume);
        return fChildSize;
    }

    public void ClearAvailableChildSize()
    {
        m_fChildSize = 0.0f;
    }

    public float GetAvailableChildSize(ref float fMotherLeftSize)
    {
        fMotherLeftSize = m_fMotherLeftSize;
        return m_fChildSize;
    }

    bool m_bShell = false;
    float m_fShellShrinkCurTime = -1f;
    float m_fShellTotalTime = 0f;

    public void SetShellInfo(float fTotalTime, float fStarTime)
    {
        if (_shell == null)
        {
            Debug.LogError("if (_shell == null) ");
            return;
        }

        /*
		if ( photonView.isMine )
		{
			photonView.RPC ( "Local_SetShellInfo", PhotonTargets.All, fTotalTime, fStarTime );
		}
		*/
    }

    public void Local_SetShellInfo(float fTotalTime, float fStarTime, eShellType type = (eShellType)0)
    {
        m_fShellTotalTime = fTotalTime;
        m_fShellShrinkCurTime = fStarTime;
        SetCurShellType(type);
    }

    public enum eShellType
    {
        none,
        w_spilt_ball, // w键吐球
        r_split_ball, // r键分球
        explode_ball, // 炸球
    };
    eShellType m_eShellType = eShellType.none;

    public eShellType GetCurShellType()
    {
        return m_eShellType;
    }

    public void SetCurShellType(eShellType type)
    {
        m_eShellType = type;
    }

    public float GetCurShellTime()
    {
        return m_fShellShrinkCurTime;
    }

    public float GetTotalShellTime()
    {
        return m_fShellTotalTime;
    }

    public void BeginShell()
    {
        if (_shell == null)
        {
            return;
        }

        /*
		if ( photonView.isMine )
		{
			photonView.RPC ( "RPC_BeginShell", PhotonTargets.All );
		}
		*/
    }

    [PunRPC]
    public void RPC_BeginShell()
    {
        Local_BeginShell();
    }

    float m_fShellUnfoldScale = 0.1f;
    public void Local_BeginShell()
    {
        if (m_fShellTotalTime <= 0f)
        {
            return;
        }

        _mrShellCircle.gameObject.SetActive( true );
        _mrShellCircle.material.SetFloat("Perent", 0f );

        m_bShell = true;

//        vecTempScale.x = s_vecShellInitScale.x * (1f + m_fShellUnfoldScale);
//        vecTempScale.y = s_vecShellInitScale.x * (1f + m_fShellUnfoldScale);
//        vecTempScale.z = 1.0f;
//        SetShellSize(vecTempScale);
        _shell.gameObject.SetActive(true);
        _Collider.enabled = true;
    }

    // 临时启用壳
    float m_fTempShellCount = 0f;
    public void Local_Begin_Shell_Temp()
    {
        _ColliderTemp.enabled = true;
        m_fTempShellCount = 0.5f;
    }

    void TempShellLOop()
    {
        if (m_fTempShellCount == 0f)
        {
            return;
        }

        m_fTempShellCount -= Time.deltaTime;
        if (m_fTempShellCount <= 0f)
        {
            _ColliderTemp.enabled = false;
            m_fTempShellCount = 0f;
            return;
        }

    }

    public bool HaveShell()
    {
        return m_bShell;
    }

    void Shell()
    {
        if (m_fShellTotalTime <= 0)
        {
            return;
        }

        if (_Player && _Player.IsGold() ) // “金壳”状态下由Player统一计时，单个球不必Loop
        { 
            return;
        }

        if (_shell == null)
        {
            return;
        }

        if (!m_bShell)
        {
            return;
        }

        if (IsDead())
        {
            return;
        }

        if (_shell)
        {
            _shell.gameObject.SetActive(true);
        }
        m_fShellShrinkCurTime += Time.deltaTime;

        float fPercent = m_fShellShrinkCurTime / m_fShellTotalTime;
        _mrShellCircle.material.SetFloat("_Percent", fPercent);


        //float fLeftSize = 1.1f - 0.1f * m_fShellShrinkCurTime / m_fShellTotalTime/*Main.s_Instance.m_fShellShrinkTotalTime*/;
        //        float k = m_fShellUnfoldScale * (1f - m_fShellShrinkCurTime / m_fShellTotalTime);
        if (m_fShellShrinkCurTime >= m_fShellTotalTime)
        {
            Local_EndShell();
            return;
        }
//        k += 1f;
//        vecTempScale.x = s_vecShellInitScale.x * k;
//        vecTempScale.y = s_vecShellInitScale.y * k;
//        vecTempScale.z = 1f;

//        SetShellSize(vecTempScale);

        // 有一种草丛可以令壳消失
        if (m_Grass)
        {
            if (m_Grass.GetConfig().nFunc == (int)CGrassEditor.eGrassFunc.shell)
            {
                //Debug.Log ( "呆了几秒了：" + (PhotonNetwork.time - m_fEnterGrassTime)+ "|" + m_Grass.GetConfig ().aryValue [0] );
                if (PhotonNetwork.time - m_fEnterGrassTime >= m_Grass.GetConfig().aryValue[0])
                {
                    Local_EndShell();
                }
            }
        }
    }

    public float GetCurLeftShellTime()
    {
        return m_fShellTotalTime - m_fShellShrinkCurTime;
    }

    [PunRPC]
    public void RPC_EndShell()
    {
        Local_EndShell();
    }

    void EndShell()
    {
        //photonView.RPC ( "RPC_EndShell", PhotonTargets.All );
    }

    public void Local_EndShell()
    {
        _Collider.enabled = false;
        m_bShell = false;
        m_fShellShrinkCurTime = 0f;
        m_fShellTotalTime = 0f;

        vecTempScale.x = s_vecShellInitScale.x;
        vecTempScale.y = s_vecShellInitScale.x;
        vecTempScale.z = 1f;
        //_shell.transform.localScale = vecTempScale;
        SetShellSize(vecTempScale);

        /*
		cTempColor = _srShellForAlpha.color;
		cTempColor.a = 0.0f;
		_srShellForAlpha.color = cTempColor;
		*/
        if (_shell)
        {
            _shell.gameObject.SetActive(false);
        }

        _mrShellCircle.gameObject.SetActive(false);

        m_eShellType = eShellType.none;
    }

    void SetShellSize(Vector3 vecScale)
    {
        if (!_shell)
        {
            return;
        }
        _shell.transform.localScale = vecScale;
        //_shellForAlpha.transform.localScale = vecScale;
    }

    public float m_fSpitBallInitSpeed = 5.0f;           // 分球的初速度
    public float m_fSpitBallAccelerate = 0.0f;         // 吐孢子的加速度(这个值通过距离、初速度、时间 算出来，不用配置) 

    public float m_fExplodeInitSpeed = 0.0f;
    public float m_fExplodeInitAccelerate = 0.0f;

    SpitBallTarget m_SpitBallTarget = null;
    float m_fSpitBallTargetParam = 0f;
    bool IfCanShowTarget()
    {
        if (IsEjecting())
        {
            return false;
        }


        return true;
    }

    public static void RongCuo_NotPointToMyself(ref float dirx, ref float diry)
    {
        // 容错，不准往自己身上吐
        if (dirx == 0 && diry == 0f)
        {
            dirx = 0.5f;
            diry = 0.5f;
        }
    }

    bool m_bTargeting = false;
    public void ClearSpitTarget()
    {
        /*
        m_bTargeting = false;

        if (m_SpitBallTarget)
        {
            m_SpitBallTarget.SetBall(null);
            m_SpitBallTarget.SetVisible(false);
        }

        _Dick.gameObject.SetActive(false);
        */
    }

    public void ShowTarget(float fSpitBallDistance)
    {
        /*
        if (!IfCanShowTarget())
        {
            return;
        }

        if (m_SpitBallTarget == null)
        {
            m_SpitBallTarget = ResourceManager.ReuseTarget();
            m_SpitBallTarget.gameObject.transform.parent = Main.s_Instance.m_goSpitBallTargets.transform;
        }
        m_SpitBallTarget.gameObject.SetActive(true);
        m_SpitBallTarget.SetBall(this);

        float fMotherSize = GetSize();
        float fRadiusMother = GetBoundsSize() / 2.0f;

        float fMotherLeftSize = 0.0f;
        float fChildSize = GetAvailableChildSize(ref fMotherLeftSize);
        if (fChildSize < Main.BALL_MIN_SIZE || fMotherLeftSize < Main.BALL_MIN_SIZE || fMotherLeftSize < fChildSize)
        {
            m_SpitBallTarget.gameObject.SetActive(false);
            return;
        }
        else
        {
            m_SpitBallTarget.gameObject.SetActive(true);
        }

        m_SpitBallTarget.SetSize(fChildSize);
        float dirx = 0f;
        float diry = 0f;
        if (Main.s_Instance.m_nPlatform == 0)
        { // PC
            if (_Player.IsMainPlayer())
            {
                dirx = _dirx;
                diry = _diry;
            }
            else
            {
                dirx = m_vecdirection.x;
                diry = m_vecdirection.y;
            }
        }
        else if (Main.s_Instance.m_nPlatform == 1)
        { // Mobile Phone
            if (_Player.IsMainPlayer())
            {
                dirx = Main.s_Instance.GetSpitDirection().x;
                diry = Main.s_Instance.GetSpitDirection().y;
            }
            else
            {
                dirx = _Player.m_vSpecialDirecton.x;
                diry = _Player.m_vSpecialDirecton.y;
            }
        }
        if (dirx == 0f && diry == 0f)
        {
            dirx = _dirx;
            diry = _diry;
        }

        RongCuo_NotPointToMyself(ref dirx, ref diry);

        vecTempPos = GetPos();
        float fDis = fSpitBallDistance * fRadiusMother;
        vecTempPos.x += fDis * dirx;
        vecTempPos.y += fDis * diry;
        m_SpitBallTarget.SetPos(vecTempPos);

        _goDick.SetActive(true);
        vecTempScale.x = fDis / Main.s_Instance.m_fPixel2Scale / GetSize();
        vecTempScale.y = fChildSize / GetSize();
        vecTempScale.z = 1f;
        _Dick._goCyliner.transform.localScale = vecTempScale;

        vecTempScale.x = fChildSize / GetSize();
        vecTempScale.y = fChildSize / GetSize();
        vecTempScale.z = 1f;
        //_Dick._goCircle.transform.position = vecTempPos;
        vecTempPos2.x = fDis / GetSize();
        vecTempPos2.y = 0;// fDis * diry /   GetSize();
        vecTempPos2.z = 0f;
        _Dick._goCircle.transform.localScale = vecTempScale;
        _Dick._goCircle.transform.localPosition = vecTempPos2;

        m_bTargeting = true;
        */
    }

    public bool IsTargeting()
    {
        return m_bTargeting;
    }

    SpitBallTarget m_SplitBallTarget = null;
    public SpitBallTarget GetSplitTarget()
    {
        return m_SplitBallTarget;
    }

    public void UpdateSplitTarget()
    {

    }

    public void ShowSplitTarget(float fCurOneBtnSplitDis)
    {
        if (m_SpitBallTarget == null)
        {
            m_SpitBallTarget = ResourceManager.ReuseTarget();
            m_SpitBallTarget.gameObject.transform.parent = Main.s_Instance.m_goSpitBallTargets.transform;
        }
        m_SpitBallTarget.gameObject.SetActive(true);

        float fSize = GetSize();
        vecTempScale.x = fSize;
        vecTempScale.y = fSize;
        vecTempScale.z = 1.0f;
        m_SpitBallTarget.gameObject.transform.localScale = vecTempScale;

        float dirx = 0f;
        float diry = 0f;
        dirx = GetDir().x;
        diry = GetDir().y;
        RongCuo_NotPointToMyself(ref dirx, ref diry);


        vecTempPos = GetPos();
        vecTempPos.x += fCurOneBtnSplitDis * dirx;
        vecTempPos.y += fCurOneBtnSplitDis * diry;
        //	Debug.Log (  fCurOneBtnSplitDis);

        m_SpitBallTarget.gameObject.transform.localPosition = vecTempPos;
        //	MapEditor.s_Instance._txtDebugInfo.text = m_SplitBallTarget.gameObject.transform.localPosition.ToString() + " _ " + vecTempPos.ToString ();

    }

    public void CalculateSpitBallRunParams(float fRadiusMother, float fRadiusChild, float fMultiple, ref float fInitSpeed, ref float fAccelerate)
    {
        float fRunDistance = fMultiple * fRadiusMother;
        fInitSpeed = CyberTreeMath.GetV0(fRunDistance, Main.s_Instance.m_fSpitRunTime);
        fAccelerate = CyberTreeMath.GetA(fRunDistance, Main.s_Instance.m_fSpitRunTime);
    }

    public SpitBallTarget _relateTarget = null;
    public void RelateTarget(Ball ball)
    {
        return; // 废弃

        ball._relateTarget = m_SpitBallTarget;
    }

    public SpitBallTarget _relateSplitTarget = null;
    public void RelateSplitTarget(SpitBallTarget target)
    {
        _relateSplitTarget = target;
    }

    public void RefeshPosDueToOutOfScreen()
    {
        /*		
                if (_balltype == eBallType.ball_type_thorn && _isSpited) {
                    return;
                }
        */
        if (!CheckIfOutOfScreen())
        {
            return;
        }

        this.gameObject.transform.position = Main.s_Instance.RandomPosWithinScreen();
    }

    public bool CheckIfOutOfScreen()
    {
        Vector3 vecScreenPos = Camera.main.WorldToScreenPoint(this.gameObject.transform.position);

        if (vecScreenPos.x < 0 || vecScreenPos.x > Screen.width || vecScreenPos.y < 0 || vecScreenPos.y > Screen.height)
        {
            return true;
        }

        return false;
    }



    public  void DoAttenuate( float fEatVolumePer)
    {
        float fAttenuate = m_fAttenuateSpeed * fEatVolumePer * Time.deltaTime; // 每个球的衰减率可能不一样。因为就算是同一个玩家的球，也可能出在不同的衰减区
        if (fAttenuate == 0f)
        {
            return;
        }
        float fCurVolume = GetVolume();
        fCurVolume += fAttenuate;
        float fCurSize = CyberTreeMath.Volume2Scale(fCurVolume);
        if (fCurSize < Main.BALL_MIN_SIZE)
        {
            fCurSize = Main.BALL_MIN_SIZE;
        }
        Local_SetSize(fCurSize);
    }

    void ShowSize()
    {
        /*
        if (_text)
        {
            string szInfo = "半径" + GetRadius().ToString("f2") + "米\n";
            szInfo += "速度:" + _ba.GetRealTimeVelocity().ToString("f2");

            _text.text = szInfo;

            // _text.text = m_nIndex + " : " + GetPos().x.ToString("f1") + " , " + GetPos().y.ToString("f1");


        }
        */

    }


    public void EatGrassSeed(int nGrassGUID)
    {
        //  photonView.RPC("RPC_EatGrassSeed", PhotonTargets.AllBuffered, this.photonView.ownerId, nGrassGUID, PhotonNetwork.time );
    }

    [PunRPC]
    public void RPC_EatGrassSeed(int nOwnerId, int nGrassGUID, double dCurTime)
    {
        Local_EatGrassSeed(nOwnerId, nGrassGUID, dCurTime);
    }

    public void Local_EatGrassSeed(int nOwnerId, int nGrassGUID, double dCurTime)
    {
        MapEditor.s_Instance.ProcessGrassSeed(nOwnerId, nGrassGUID, dCurTime);
    }

   

    public bool DoNotSyncMoveInfo()
    {
        if (IsEjecting())
        {
            return true;
        }

        return false;
    }


    public bool DoNotMove()
    {
        if (IsStaying())
        {
            return true;
        }

        if (IsEjecting())
        {
            return true;
        }



        if (IsDead())
        {
            return true;
        }

        if (IsEaten())
        {
            return true;
        }

        if (GetMovingToCenterStatus() == 1)
        {
            return true;
        }

        return false;
    }

    public void SetAdjustInfo_InGroup(float fPosX, float fPosY)
    {
        vecTempPos.x = fPosX;
        vecTempPos.y = fPosY;
        vecTempPos.z = -GetSize();
        SetLocalPosition(vecTempPos);
    }

    float m_fAdjustSpeedX = 0f;
    float m_fAdjustSpeedY = 0f;

    public void SetAdjustInfo(float fPosX, float fPosY, int nDirect = 0)
    {
        vecTempPos = GetPos();
        vecTempPos.x = fPosX;
        vecTempPos.y = fPosY;
        SetPos(vecTempPos);
    }


    float m_fExtraSpeedX = 0f;
    float m_fExtraSpeedY = 0f;

    void SetExtraSpeedX(float fDeltaX)
    {
        m_fAdjustSpeedX = fDeltaX;
    }

    void ExtraMoveX()
    {
        if (m_fExtraSpeedX == 0f)
        {
            return;
        }


    }

    void SetExtraSpeedY(float fDeltaY)
    {
        m_fAdjustSpeedY = fDeltaY;
    }


    void UpdateSpitDir()
    {
        if (!Main.s_Instance.IsForceSpitting())
        {
            return;
        }

        if (Main.s_Instance.m_nPlatform != 1) // 这种“多点触控分球”只针对手机版，不针对PC版
        {
            return;
        }

        //MapEditor.s_Instance._txtDebugInfo.text = Main.s_Instance.GetSpitTouchTargetPos ().ToString ();
        //_vecSpitDir = Main.s_Instance.GetSpitTouchTargetPos () - this.GetPos ();
        //_vecSpitDir.Normalize ();
    }

    public void CanceSplit()
    {
    }

    public void CancelSpit()
    {
        /*
		if (_relateTarget) {
			DestroyTarget ( _relateTarget );
		}

		if (_relateSplitTarget) {
			DestroyTarget ( _relateSplitTarget );
		}

		if (m_SpitBallTarget) {
			DestroyTarget ( m_SpitBallTarget );
		}

		if (m_SplitBallTarget) {
			DestroyTarget ( m_SplitBallTarget );
		}
		*/
        if (m_SpitBallTarget)
        {
            m_SpitBallTarget.SetVisible(false);
        }
    }

    public bool CheckIfCanSpitHalf()
    {
        if (IsEjecting())
        {
            return false;
        }

        if (IsDead())
        {
            return false;
        }

        return true;
    }

    public void SpitHalf(float fDelayTime, float fChildSize, float fDistance, float fRunTime, float fShellTime)
    {
        if (!CheckIfCanSpitHalf())
        {
            return;
        }

        float fRadiusMother = GetRadius();
        Ball new_ball = _Player.ReuseOneBall();
        if (new_ball == null)
        {
            return;
        }

        float fRealRunTime = fRunTime - fDelayTime;
        if (fRealRunTime < Main.s_Instance.m_fMinRunTime)
        {
            fRealRunTime = Main.s_Instance.m_fMinRunTime;
        }

        float fRunDistance = fDistance * fRadiusMother;

        vecTempPos = GetPos();

        if (_Player.IsMainPlayer())
        {
            //	Main.s_Instance.g_SystemMsg.SetContent ("二分分球，配置壳时间：" + Main.s_Instance.m_fShellShrinkTotalTime + ", Buff加成后时间:" + fShellTime);
        }
        new_ball.Local_SetShellInfo(fShellTime, 0f, eShellType.w_spilt_ball);
        new_ball.Local_SetPos(vecTempPos);
        new_ball.Local_SetSize(fChildSize);
        new_ball.SetDir(GetDir());
        float fSpeed = fRunDistance / fRealRunTime;
        new_ball.Local_BeginEject_New(fSpeed, fRunDistance);

        Local_SetShellInfo(fShellTime, 0f, eShellType.w_spilt_ball);
        Local_BeginShell();
        Local_SetSize(fChildSize);
    }

    // 参数nPlatform是指吐球方的操作平台，显示方是什么平台无所谓。
    // m_vSpecialDirecton是双摇杆模式中用来控制分球方向的；
    // m_vecdirection是用来控制行走方向的，无论是不是双摇杆模式
    public void Spit(float fDelayTime, int nPlatform, float fChildSize, float fMotherLeftSize, float fWorldCursorPosX, float fWorldCursorPosY, float fSpitBallRunDistance, float fSpitBallRunTime, float fShellTime)
    {
        if (IsEjecting())
        {
            return;
        }

        if (IsDead())
        {
            return;
        }

        float fRadiusMother = GetBoundsSize() / 2.0f; // 尼玛你倒是在母球分出体积之前计取母球的半径啊

        float fMotherSize = GetSize();

        bool bNew = false;
        Ball new_ball = _Player.ReuseOneBall();
        if (new_ball == null)
        {
            return;
        }

        //Ball new_ball = goNewBall.GetComponent<Ball> ();

        //new_ball.transform.parent = _Player.transform;
        //CalculateNewBallBornPosAndRunDire ( new_ball, _dirx, _diry );
        float fRealRunTime = fSpitBallRunTime - fDelayTime;
        if (fRealRunTime < Main.s_Instance.m_fMinRunTime)
        {
            fRealRunTime = Main.s_Instance.m_fMinRunTime;
        }
        float fSpitBallInitSpeed = 0.0f;
        float fSpitBallAccelerate = 0.0f;
        float fRunDistance = fSpitBallRunDistance * fRadiusMother;


        vecTempPos = GetPos();
        vec2Temp.x = fWorldCursorPosX;
        vec2Temp.y = fWorldCursorPosY;
        vec2Temp.x -= vecTempPos.x;
        vec2Temp.y -= vecTempPos.y;
        vec2Temp.Normalize();

        if ( Main.s_Instance.GetPlatformType() == Main.ePlatformType.mobile )
        {
            vec2Temp1 = Main.s_Instance._playeraction.GetSpecialDirection();
            if (vec2Temp1.x == 0 && vec2Temp1.y == 0)
            {

            }
            else
            {
                vec2Temp = vec2Temp1;
            }
        }
        /*
        if (!IsMine()) { // Others  
            if (nPlatform == 0) { // PC
                dirx = m_vecdirection.x;
                diry = m_vecdirection.y;
            }
            else if (nPlatform == 1) // Mobile Phone
            {
                dirx = _Player.m_vSpecialDirecton.x;
                diry = _Player.m_vSpecialDirecton.y;

                if (dirx == 0 && diry == 0f) { // （这种情况仅出现在双摇杆模式）如果摇杆的方向为0，则把分球方向定为行走方向
                    dirx = m_vecLastDirection.x;
                    diry = m_vecLastDirection.y;
                }
            }
        }
        else // MainPlayer
        {
            if (Main.s_Instance.m_nPlatform == 0) { // PC
                dirx = _dirx;
                diry = _diry;
            }
            else if (Main.s_Instance.m_nPlatform == 1) {// Mobile Phone 双摇杆
                dirx = Main.s_Instance.GetSpitDirection().x;
                diry = Main.s_Instance.GetSpitDirection().y;

                if (dirx == 0 && diry == 0f) { // （这种情况仅出现在双摇杆模式）如果摇杆的方向为0，则把分球方向定为行走方向
                    dirx = _dirx;
                    diry = _diry;
                }
            }
        }
        */

        
        if (_Player.IsMainPlayer())
        {
            //		Main.s_Instance.g_SystemMsg.SetContent ("W分球，配置壳时间：" + Main.s_Instance.m_fShellShrinkTotalTime + ", Buff加成后时间:" + fShellTime);
        }
        new_ball.Local_SetShellInfo(fShellTime/*Main.s_Instance.m_fShellShrinkTotalTime*/, 0f, eShellType.w_spilt_ball);
        new_ball.Local_SetPos(vecTempPos);
        new_ball.Local_SetSize(fChildSize);

        new_ball.SetDir(vec2Temp);
        //new_ball.Local_SetThornSize ( GetThornSize() * fChildSize / fMotherSize );

        //new_ball.Local_BeginEject (fSpitBallInitSpeed, fSpitBallAccelerate, fRealRunTime);
        //new_ball.Local_BeginEject_New(fSpeed, fRunDistance, false, false, Main.s_Instance.m_fSpitBallStayTime);
        new_ball.Local_BeginEject_New_2(fRunDistance, fRealRunTime, 0f);
        Local_SetShellInfo(fShellTime/*Main.s_Instance.m_fShellShrinkTotalTime*/, 0f, eShellType.w_spilt_ball);
        Local_BeginShell();
        Local_SetSize(fMotherLeftSize);
        /*
		if (bNew) {
			//new_ball.SetThornSize ( GetThornSize() * fChildSize / fMotherSize );
			new_ball.SetShellInfo( Main.s_Instance.m_fShellShrinkTotalTime, 0f );
			new_ball.BeginEject (fSpitBallInitSpeed, fSpitBallAccelerate, Main.s_Instance.m_fSpitBallRunTime);
			new_ball.SetPos ( vecTempPos );
			new_ball.SetSize ( fChildSize );
			new_ball.SetDir( dirx, diry);
		} else {
			new_ball.Local_SetShellInfo( Main.s_Instance.m_fShellShrinkTotalTime, 0f );
			new_ball.Local_SetPos ( vecTempPos );
			new_ball.Local_SetSize ( fChildSize );
			new_ball.Local_SetDir ( dirx, diry );
			//new_ball.Local_SetThornSize ( GetThornSize() * fChildSize / fMotherSize );
			new_ball.Local_BeginEject (fSpitBallInitSpeed, fSpitBallAccelerate, Main.s_Instance.m_fSpitBallRunTime);
		}
		*/
    }

    public float GetMoveSpeedX()
    {
        return m_fMoveSpeedX;
    }

    public float GetMoveSpeedY()
    {
        return m_fMoveSpeedY;
    }


    float m_fMoveSpeedX = 0.0f;
    float m_fMoveSpeedY = 0.0f;
    Vector2 m_vecdirection = new Vector2();
    public Vector2 m_vecLastDirection = new Vector2();
    public void BeginMove(Vector3 vecWorldCursorPos, float fRealTimeSpeedWithoutSize)
    {
        m_vecWorldCursorPos = vecWorldCursorPos;


        m_vecdirection = vecWorldCursorPos - this.transform.position;
        m_vecdirection.Normalize();

        float fSpeed = _ba.GetRealSpeedBySize(fRealTimeSpeedWithoutSize);
        /*
		float fShitDis = Vector2.Distance (vecWorldCursorPos, GetPos ());
		if ( fShitDis < Main.s_Instance.m_fDistanceToSlowDown) {

			fSpeed = fSpeed * fShitDis / Main.s_Instance.m_fDistanceToSlowDown;
		}
        */

        fSpeed = BallAction.ProcessSomethingAffectVelocity(this, fSpeed);

        //// 限速：
        if (fSpeed > Main.s_Instance.m_fMaxPlayerSpeed)
        {
            fSpeed = Main.s_Instance.m_fMaxPlayerSpeed;
        }

        //// end 限速

        m_fSpeed = fSpeed;
        m_fMoveSpeedX = fSpeed * m_vecdirection.x + m_fAdjustSpeedX;
        m_fMoveSpeedY = fSpeed * m_vecdirection.y + m_fAdjustSpeedY;
    }


    Vector3 m_vecWorldCursorPos = new Vector3();


    float m_fShitSpeed = 0f;
    static Vector2 s_pos1 = new Vector2();
    static Vector2 s_pos2 = new Vector2();
    void Move() // none-mainplayer
    {
        if (_Player == null)
        {
            return;
        }

        if (_Player.photonView.isMine)
        {
            return;
        }

        if (!_Player.IsMoving())
        {
            return;
        }

        if (_Player.DoNotMove())
        {
            return;
        }

        if (IsDead())
        {
            return;
        }

        if (DoNotMove())
        {
            return;
        }

 

        vecTempPos = GetPos();

        // poppin test
        float fShitDis = Vector2.Distance(m_vecWorldCursorPos, GetPos());
        if (fShitDis < Main.s_Instance.m_fDistanceToSlowDown)
        {
            float the_shit = fShitDis / Main.s_Instance.m_fDistanceToSlowDown;
            m_fMoveSpeedX = m_fMoveSpeedX * the_shit;
            m_fMoveSpeedY = m_fMoveSpeedY * the_shit;
        }

        float fDeltaX = m_fMoveSpeedX * Time.fixedDeltaTime;
        float fDeltaY = m_fMoveSpeedY * Time.fixedDeltaTime;

        bool bCanMoveX = true;
        bool bCanMoveY = true;

        MapEditor.s_Instance.CheckIfWillExceedWorldBorder(this, fDeltaX, fDeltaY, ref bCanMoveX, ref bCanMoveY);
        /*
        if (IsStayOnClassCircle() && IsSizeReachClassCircleThreshold())   
        {
            MapEditor.s_Instance.CheckIfCanCrossClassCircle(this, fDeltaX, fDeltaY, ref bCanMoveX, ref bCanMoveY);
        }
        */

        if (bCanMoveX)
        {
            vecTempPos.x += fDeltaX;
        }
        if (bCanMoveY)
        {
            vecTempPos.y += fDeltaY;
        }
        vecTempPos.z = -GetSize();
        //SetPos(vecTempPos); poppin test
        _rigid.MovePosition(vecTempPos);
        _rigid.MoveRotation(0);

        // poppin test
        if (!PhotonNetwork.isMasterClient)
        {
            Main.s_Instance.m_textDebugInfo.text = vecTempPos.x + " , " + Player.s_worinigegui;
        }
    }

    void MoveInGroup() // none-mainplayer
    {
        float fDeltaX = m_fMoveSpeedX * Time.fixedDeltaTime;
        float fDeltaY = m_fMoveSpeedY * Time.fixedDeltaTime;

        //// 检测是否超越了场景边界
        bool bCanMoveX = true;
        bool bCanMoveY = true;

        MapEditor.s_Instance.CheckIfWillExceedWorldBorder(this, fDeltaX, fDeltaY, ref bCanMoveX, ref bCanMoveY);
        //  end 检测是否超越了场景边界

        vecTempPos = GetLocalPosition();


        if (bCanMoveX)
        {
            vecTempPos.x += fDeltaX;
        }
        if (bCanMoveY)
        {
            vecTempPos.y += fDeltaY;
        }
        vecTempPos.z = -GetSize();
        SetLocalPosition(vecTempPos);
    }

    /*
	public void SyncFullInfo()
	{
		vecTempPos = GetPos ();
		//photonView.RPC ( "RPC_SyncFullInfo", PhotonTargets.Others, m_bDead, vecTempPos.x, vecTempPos.y, GetSize(), m_fShellShrinkCurTime, m_fShellTotalTime, GetThornSize() );
	}

	[PunRPC]
	public void RPC_SyncFullInfo( bool bDead, float fPosX, float fPosY, float fSize, float fShellCurTime, float m_fShellTotalTime, float fThornSize )
	{
		float fPosZ = -fSize;
		Local_SetPos ( fPosX,  fPosY, fPosZ);   
		Local_SetSize ( fSize );
		Local_SetThornSize ( fThornSize );
	
		if (fShellCurTime > 0f) {
			Local_SetShellInfo(m_fShellTotalTime, fShellCurTime);
			Local_BeginShell (); 
		}
	
		//Debug.Log ( "全量同步：" + photonView.viewID + " .... " + bDead );
	
		if (bDead) {
			SetDead (true);
			_Player.m_lstRecycledBalls.Remove ( this );
			_Player.m_lstBalls.Remove ( this );
			_Player.m_lstRecycledBalls.Add ( this );
		}
		else
		{
			SetDead (false);
			bool bNew = false;
			_Player.m_lstBalls.Remove ( this );
			_Player.m_lstRecycledBalls.Remove ( this );
			_Player.m_lstBalls.Add ( this );
		}
	}
	*/
    int m_nRealSplitNum = 0;
    float m_fSplitSize = 0f;
    float m_fSplitArea = 0f;
    float m_fLeftArea = 0f;
    float m_fTotalDis = 0f;
    float m_fSegDis = 0f;
    bool m_bMotherDeadAfterSplit = false;

    public void CalculateSplitChildNumAndSize()
    {
        m_nRealSplitNum = 0;
        float fCurSize = GetSize();
        float fCurArea = CyberTreeMath.SizeToArea(fCurSize, Main.s_Instance.m_nShit);
        m_fLeftArea = fCurArea;
        for (int i = Main.s_Instance.m_nSplitNum; i >= 2; i--)
        {
            float fSmallArea = fCurArea / i;
            float fSmallSize = CyberTreeMath.AreaToSize(fSmallArea, Main.s_Instance.m_nShit);
            if (fSmallSize >= Main.BALL_MIN_SIZE)
            {
                m_nRealSplitNum = i;
                m_fSplitArea = fSmallArea;
                m_fSplitSize = fSmallSize;
                break;
            }
        } // end for i
    }

    public void CalculateRealSplitInfo(float fTotalDistance, ref int nRealSplitNum, ref float fRealSplitChildSize, ref float fSegDis)
    {
        float m_fTotalDis = fTotalDistance;
        if (m_fTotalDis > Main.s_Instance.m_fSplitMaxDistance)
        {
            m_fTotalDis = Main.s_Instance.m_fSplitMaxDistance;
        }
        m_fSegDis = m_fTotalDis / Main.s_Instance.m_nSplitNum; // 废弃
        fSegDis = m_fTotalDis / Main.s_Instance.m_nSplitNum;


        m_nRealSplitNum = 0; // 废弃
        nRealSplitNum = 0;
        float fCurSize = GetSize();
        float fCurArea = CyberTreeMath.SizeToArea(fCurSize, Main.s_Instance.m_nShit);
        m_fLeftArea = fCurArea;
        for (int i = Main.s_Instance.m_nSplitNum; i >= 2; i--)
        {
            float fSmallArea = fCurArea / i;
            float fSmallSize = CyberTreeMath.AreaToSize(fSmallArea, Main.s_Instance.m_nShit);
            if (fSmallSize >= Main.BALL_MIN_SIZE)
            {
                m_nRealSplitNum = i; // 废弃
                nRealSplitNum = i;
                fRealSplitChildSize = fSmallSize;
                m_fSplitArea = fSmallArea;
                m_fSplitSize = fSmallSize;
                break;
            }
        } // end for i


    }

    public void CalculateRealSplitInfo(float fWorldCurPosX, float fWorldCurPosY, ref int nRealSplitNum, ref float fRealSplitChildSize, ref float fSegDis)
    {
        vecTempPos.x = fWorldCurPosX;
        vecTempPos.y = fWorldCurPosY;
        float fTotalDistance = Vector2.Distance(GetPos(), vecTempPos);
        CalculateRealSplitInfo(fTotalDistance, ref nRealSplitNum, ref fRealSplitChildSize, ref fSegDis);
    }

    public void SplitOne(int nTimes)
    {
        /*
		if (m_nRealSplitNum <= 0) { // 废弃
			return;
		}
		bool bNew = false;
		Ball new_ball = _Player.ReuseOneBall ( photonView.isMine, ref bNew ); 
		if (new_ball == null) {
			return;
		}
		Debug.Log ( "spit_" + nTimes );
		if (!photonView.isMine) {
			_dirx = m_vecdirection.x;
			_diry = m_vecdirection.y;
		}

		float fDis = m_fSegDis * ( nTimes + 1 );
		float fInitSpeed = CyberTreeMath.GetV0 (fDis, Main.s_Instance.m_fSplitRunTime  );
		float fAccelerate = CyberTreeMath.GetA ( fDis, Main.s_Instance.m_fSplitRunTime);

		if (bNew) {
			new_ball.SetPos (GetPos ());
			new_ball.SetSize (m_fSplitSize);
			new_ball.SetDir (_dirx, _diry);
			new_ball.BeginEject (fInitSpeed, fAccelerate, Main.s_Instance.m_fSplitRunTime, true, true);
		} else {
			new_ball.Local_SetPos (GetPos ());
			new_ball.Local_SetSize (m_fSplitSize);
			new_ball.Local_SetDir (_dirx, _diry);
			new_ball.Local_BeginEject (fInitSpeed, fAccelerate, Main.s_Instance.m_fSplitRunTime, true, true);
		}

		m_fLeftArea -= m_fSplitArea;
		*/
    }

    public void CheckIfDeadAfterSplit()
    {
        //		if (!photonView.isMine) {
        //			return;
        //		}

        if (m_fLeftArea < Main.BALL_MIN_SIZE)
        {
            DestroyBall();
            return;
        }

        float fLeftSize = CyberTreeMath.AreaToSize(m_fLeftArea, Main.s_Instance.m_nShit);
        SetSize(fLeftSize);
    }

  

    /// 回收利用相关
    int m_nIndex = 0;
    public void SetIndex(int nIndex)
    {
        m_nIndex = nIndex;
    }

    public int GetIndex()
    {
        return m_nIndex;
    }

    int m_nMovingToCenterStatus = 0; // 0 - 还没开始移动  1 - 开始移动  2 - 移动完成
    public int GetMovingToCenterStatus()
    {
        return m_nMovingToCenterStatus;
    }

    public bool IsMovingToCenter()
    {
        return m_nMovingToCenterStatus == 1;
    }

    public void SetBallsCenter(Vector3 vecBallsCenter)
    {
        _direction = vecBallsCenter - GetPos();
        _direction.Normalize();
    }

    public void SetMovingToCenterStatus(int nMovingToCenterStatus)
    {
        m_nMovingToCenterStatus = nMovingToCenterStatus;
    }

    public void DoMovingToCenter()
    {
        if (m_nMovingToCenterStatus != 1)
        {
            return;
        }

        vecTempPos = GetPos();
        vecTempPos.x += _speed.x * Time.deltaTime;
        vecTempPos.y += _speed.y * Time.deltaTime;
        this._rigid.MovePosition(vecTempPos);
        _rigid.MoveRotation(0);
        vecTempPos = GetPos();
        vecTempPos.z = -GetSize();
        Local_SetPos(vecTempPos);
        //Local_SetPos ( vecTempPos );
    }

    public void CalculateMoveToCenterRealSpeed()
    {
        /*
        float fSpeed = Main.s_Instance.m_fBallMoveToCenterBaseSpeed;
        if (Main.s_Instance.m_nShit == 2) {
            fSpeed = fSpeed / GetSize2KaiGenHao();
        } else if (Main.s_Instance.m_nShit == 3) {
            fSpeed = fSpeed / GetSize3KaiGenHao();
        }
        _speed.x = fSpeed * _direction.x + m_fAdjustSpeedX;
        _speed.y = fSpeed * _direction.y + m_fAdjustSpeedY;
        */
    }

    public bool IfBallPosInvalid()
    {
        if (GetPos().x < -5000f || GetPos().y < -5000f)
        {
            return true;
        }

        return false;
    }

   

    

    bool m_bIsAdjustMoveProtect = false;
    float m_fAdjustMoveProtectTime = 0f;
    public void BeginAdjustMoveProtectTime()
    {
        m_fAdjustMoveProtectTime = 0f;
        m_bIsAdjustMoveProtect = true;
    }

    public void EndAdjustMoveProtectTime()
    {
        m_fAdjustMoveProtectTime = 0f;
        m_bIsAdjustMoveProtect = false;
    }

    public bool IsAdjustMoveProtect()
    {
        return m_bIsAdjustMoveProtect;
    }

    void AdjustMoveProtectLoop()
    {
        if (!m_bIsAdjustMoveProtect)
        {
            return;
        }

        m_fAdjustMoveProtectTime += Time.deltaTime;
        if (m_fAdjustMoveProtectTime >= Main.s_Instance.m_fAdjustMoveProtectInterval)
        {
            EndAdjustMoveProtectTime();
        }
    }

    Dictionary<int, int> m_dicGroupRoundId = new Dictionary<int, int>();
    public void GroupCount(int nGroupId)
    {
        int nCount = 0;
        if (m_dicGroupRoundId.TryGetValue(nGroupId, out nCount))
        {
            nCount++;
            m_dicGroupRoundId[nGroupId] = nCount;
        }
        else
        {
            m_dicGroupRoundId[nGroupId] = 0;
        }
    }

    public int GetGroupCount(int nGroupId)
    {
        int nCount = 0;
        if (m_dicGroupRoundId.TryGetValue(nGroupId, out nCount))
        {
        }
        return nCount;
    }

    float m_fLocalOffsetX = 0f;
    float m_fLocalOffsetY = 0f;
    public void RecordLocalPosOffset(float fStartLocalPosOfThisRoundX, float fStartLocalPosOfThisRoundY)
    {
        m_fLocalOffsetX = fStartLocalPosOfThisRoundX - GetLocalPosition().x;
        m_fLocalOffsetY = fStartLocalPosOfThisRoundY - GetLocalPosition().y;
    }

    bool m_bLockMove = false;
    public void SetLockMove(bool val)
    {
        m_bLockMove = val;
    }

    public bool GetLockMove()
    {
        return m_bLockMove;
    }

    public void LockMoveLoop()
    {

    }

    public void SetRebornProtectEfffect(bool val)
    {
        if (val)
        {
            cTempColor = _srMouth.color;
            cTempColor.a = 0.3f;
            _srMouth.color = cTempColor;

            cTempColor = _srOutterRing.color;
            cTempColor.a = 0.3f;
            _srOutterRing.color = cTempColor;

        }
        else
        {
            cTempColor = _srMouth.color;
            cTempColor.a = 1f;
            _srMouth.color = cTempColor;

            cTempColor = _srOutterRing.color;
            cTempColor.a = 1f;
            _srOutterRing.color = cTempColor;
        }
    }

    int m_nSizeClassId = -1; // 此球自身的区级
    public void ChangeSizeClassId()
    {
        float fRadisu = GetRadius();
        if (fRadisu >= CClassEditor.s_Instance.GetLowThreshold())
        {
            SetSizeClassId(1);
        }
        else
        {
            SetSizeClassId(0);
        }


    }

    float m_fAttenuateSpeed = 0f;
    float m_fClassInvasionAffectSpeed = 0f;

    public float GetClassInvasionAffectSpeed()
    {
        return m_fClassInvasionAffectSpeed;
    }

    public void ChangeAttenuateSpeed()
    {
        CClassEditor.sClassConfig low_class_config = CClassEditor.s_Instance.GetClassConfigById(0);
        CClassEditor.sClassConfig mid_class_config = CClassEditor.s_Instance.GetClassConfigById(1);
        CClassEditor.sClassConfig high_class_config = CClassEditor.s_Instance.GetClassConfigById(2);
        if (m_nClassId == 1)
        { // 处在中级区
            m_fAttenuateSpeed = mid_class_config.fNormalAttenuate;
            m_fClassInvasionAffectSpeed = 0;
            //	Debug.Log( "执行中级区正常衰减：" + m_fAttenuateSpeed );
        }
        else if (m_nClassId == 0)
        { // 处在低级区
            if (m_nSizeClassId == 1)
            {// 中级球闯低级区则执行低级区的侵略衰减
                m_fAttenuateSpeed = low_class_config.fInvasionAttenuate;
                m_fClassInvasionAffectSpeed = low_class_config.fInvasionSpeedSlowdown;
                //      Debug.Log( "中级球闯低级区则执行低级区的侵略衰减：" + m_fAttenuateSpeed );
            }
            else
            {// 其余情况执行低级区的常规衰减
                m_fAttenuateSpeed = low_class_config.fNormalAttenuate;
                m_fClassInvasionAffectSpeed = 0;
                //	Debug.Log( "执行低级区的常规衰减：" + m_fAttenuateSpeed );
            }
        }

    }

    public void SetSizeClassId(int val)
    {
        bool bChanged = (m_nSizeClassId != val);
        m_nSizeClassId = val;

        if (m_nSizeClassId == 0)
        {

        }
        else if (m_nSizeClassId == 1)
        {

        }
        else if (m_nSizeClassId == 2)
        {

        }

        if (bChanged)
        {
            ChangeAttenuateSpeed();
        }
    }

    int m_nClassId = 0; // 此球当前所呆在的区
    public void SetClassId(int val, bool bYaXian = false)
    {
        bool bChanged = (m_nClassId != val);
        m_nClassId = val;
        string msg = "";
        switch (m_nClassId)
        {
            case 0:
                {
                    msg = "低级区";
                }
                break;
            case 1:
                {
                    msg = "高级区";
                }
                break;
        }

        if (bYaXian)
        {
            if (m_nClassId == 0)
            {
                if (_Player.IsMainPlayer() && bChanged)
                {
                    //	Main.s_Instance.g_SystemMsg.SetContent ("门槛线(压线算低级区)。");
                }
            }
            else if (m_nClassId == 1)
            {
                if (_Player.IsMainPlayer() && bChanged)
                {
                    //Main.s_Instance.g_SystemMsg.SetContent ("高级区门槛线(压线算中级区)。");
                }
            }
        }
        else
        {
            if (_Player.IsMainPlayer() && bChanged)
            {
                //Main.s_Instance.g_SystemMsg.SetContent ("你娃进入了【" + msg + "】");
            }
        }

        if (bChanged)
        {
            ChangeAttenuateSpeed();


        }
    }

    public int GetClassId()
    {
        return m_nClassId;
    }
    /*
    public void ProcessEnterOrLeaveClassCircle()
    {
       if ( Vector2.Distance( GetPos(),  Main.s_Instance.m_vecClassCircleCenterPos)  < Main.s_Instance.m_fClassCircleLength ) // 想出
        {
            ExitClassCircle();
        }
        else // 想入
        {
            EnterClassCircle();
        }
    }
    */

    bool m_bStayOnClassCircle = false;
    int m_nStayClassCircleId = 0;
    public void StayOnClassCircle(string szName)
    {
        if (szName == "ClassCircle0")
        {
            m_nStayClassCircleId = 0;
        }
        if (szName == "ClassCircle1")
        {
            m_nStayClassCircleId = 1;
        }
        m_bStayOnClassCircle = true;
    }

    public bool IsStayOnClassCircle()
    {
        return m_bStayOnClassCircle;
    }

    // 没有压线
    public void NotStayOnClassCircleLoop()
    {

    }

    // 正在压线
    public void StayOnClassCircleLoop()
    {
        if (!m_bStayOnClassCircle)
        {
            return;
        }
        CheckClassStatus();
    }

    public void CheckClassStatus()
    {
        float fBallRadius = GetRadius();
        float fDis = Vector2.Distance(GetPos(), Main.s_Instance.m_vecClassCircleCenterPos);

        if ((CClassEditor.s_Instance.GetClassCircleRadius() + fBallRadius) < fDis)// 在圈外
        {
            SetClassId(0);
            m_bStayOnClassCircle = false; // 没压线了
            return;
        }
        if (CClassEditor.s_Instance.GetClassCircleRadius() > (fBallRadius + fDis)) // 在圈内
        {
            SetClassId(1);
            m_bStayOnClassCircle = false; // 没压线了
            return;
        }

        //// 在压线
        SetClassId(0, true);
    }

    public void EnterClassCircle()
    {
        m_nClassId = 1;
    }

    public void ExitClassCircle()
    {
        m_nClassId = 0;
    }

    public bool IsInClassCircle()
    {
        return m_nClassId == 1;
    }

    /*
    public bool IsSizeReachClassCircleThreshold()
    {
        return GetSize() >= Main.s_Instance.m_fClassCircleThreshold;
    }
    */

   
    //// 湮灭状态
    public void SetAnnihilate(bool val)
    {
        _effectAnnihilate.gameObject.SetActive(val);
        if (val)
        {
            _effectAnnihilate.Play();
        }
    }

    //// 魔盾状态
    public void SetMagicShielding(bool val)
    {
        _effectMagicShield.gameObject.SetActive(val);
        if (val)
        {
            _effectMagicShield.Play();
        }
    }




    public void BeginEffect(CEffect.eEffectType type )
    {
        switch(type)
        {
            case CEffect.eEffectType.gold_shell:
                {
                    _effectGoldShell.gameObject.SetActive( true );
                }
                break;
        }
    }

    public void EndEffect(CEffect.eEffectType type)
    {
        switch (type)
        {
            case CEffect.eEffectType.gold_shell:
                {
                    _effectGoldShell.gameObject.SetActive(false);
                }
                break;
        }
    }

    public void SetDustCollierEnalbed( bool bEnabled )
    {
        _ColliderDust.enabled = true;
        m_fDustTimeCount = 0.5f;
    }

    float m_fDustTimeCount = 0f;
    void DustCollierLoop()
    {
        if (_ColliderDust == null)
        {
            return;
        }

        if ( !_ColliderDust.enabled)
        {
            return;
        }
        m_fDustTimeCount -= Time.deltaTime;
        if (m_fDustTimeCount <= 0f)
        {
            _ColliderDust.enabled = false;
        }
    }

    public void SetActive( bool bActive )
    {
        if (IsEjecting())
        {
            End_Eject_New_2();
            EndEject();
        }

        this.gameObject.SetActive(bActive);


    }

    //// move to some destination
    Vector3 m_vecDest = new Vector3();
    bool m_bIsMovingToDest = false;
    public void BeginMoveToDestination(Vector3 dest)
    {
        m_vecDest = dest;
        m_bIsMovingToDest = true;

    }

    void MovingToDest()
    {
        if (!m_bIsMovingToDest)
        {
            return;
        }

        float fPlayerbaseSpeed = Main.s_Instance.m_MainPlayer.GetBaseSpeed();
        _ba.UpdatePosition(m_vecDest, fPlayerbaseSpeed);
    }

    public void EndMoveToDestination()
    {
        m_bIsMovingToDest = false;
    }



    //// end move to some destination
}