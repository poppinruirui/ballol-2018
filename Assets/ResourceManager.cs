﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ResourceManager : MonoBehaviour {

	public static ResourceManager s_Instance = null;

	public GameObject m_preBuff;
	public GameObject m_preGrass;
	public GameObject m_preSpray;
    public GameObject m_preSceneBall;
    public GameObject m_preGunsight;

	void Awake()
	{
		s_Instance = this;

		m_lstRecycledThorn.Clear ();
		m_lstRecycledBuff.Clear ();
		m_lstRecycledGrass.Clear ();
		m_lstRecycledTarget.Clear ();
	}

	// Use this for initialization
	void Start () {
		
	}
	
	// Update is called once per frame
	void Update () {
		
	}

	public GameObject PrefabInstantiate( string szPrefabName )
	{
		return GameObject.Instantiate((GameObject)Resources.Load(szPrefabName));
	}

	

	public static void DestroyPolygon( Polygon polygon )
	{
		if (polygon != null) {
			GameObject.Destroy (polygon.gameObject);
		}
	}

	static List<SpitBallTarget> m_lstRecycledTarget = new List<SpitBallTarget>();
	public static SpitBallTarget ReuseTarget()
	{
		SpitBallTarget target = null;
		if (m_lstRecycledTarget.Count == 0) {
			target = (GameObject.Instantiate (Main.s_Instance.m_preSpitBallTarget)).GetComponent<SpitBallTarget>();
			return target;
		}
		target = m_lstRecycledTarget [0];
		target.gameObject.SetActive (true );
		m_lstRecycledTarget.RemoveAt (0);
		return target;
	}

	public static void RecycleTarget( SpitBallTarget target )
	{
		target.gameObject.SetActive (false );
		m_lstRecycledTarget.Add (target);
	}

	static List<CMonster> m_lstRecycledBean = new List<CMonster>();
	public static CMonster ReuseBean()
	{
		CMonster bean = null;
		if (m_lstRecycledBean.Count == 0) {
			bean = (GameObject.Instantiate (Main.s_Instance.m_preBean)).GetComponent<CMonster>();
			return bean;
		}
		bean = m_lstRecycledBean [0];
		m_lstRecycledBean.RemoveAt (0);
		bean.gameObject.SetActive (true );
		bean.SetDead ( false );
		return bean;
	}

	public static void RecycleMonster( CMonster monster )
	{
        monster.SetDead(true);
        monster.gameObject.SetActive (false );
		m_lstRecycledBean.Add (monster);
	}


	static List<CMonster> m_lstRecycledThorn = new List<CMonster>();
	static long m_nGuidCount = 0;
	public CMonster ReuseMonster()
	{
		CMonster thorn = null;
       
		if (m_lstRecycledThorn.Count == 0) {
			thorn = (GameObject.Instantiate (Main.s_Instance.m_preThorn)).GetComponent<CMonster>();
			return thorn;
		}
		thorn = m_lstRecycledThorn [0];
		m_lstRecycledThorn.RemoveAt (0);
		thorn.gameObject.SetActive (true );
		thorn.SetDead ( false );
        thorn.Reset();
        return thorn;
        
    }


    Dictionary<int, List<float>> m_dicSceneBeanRebornQueue = new Dictionary<int, List<float>>();

	static List<CBuff> m_lstRecycledBuff = new List<CBuff>();
	public static CBuff ReuseBuff(  )
	{
		CBuff buff = null;
		if (m_lstRecycledBuff.Count > 0) {
			buff = m_lstRecycledBuff [0];
			m_lstRecycledBuff.RemoveAt (0);
		}
		if ( buff == null )
		{
			buff = GameObject.Instantiate ( s_Instance.m_preBuff ).GetComponent<CBuff>();
		}
		return buff;
	}


	public static void RecycleBuff( CBuff buff )
	{
		m_lstRecycledBuff.Add ( buff );
	}

	// =====================================================================================================================

	//// ! ---- Grass
	public CGrass ReuseGrass()
	{
		CGrass grass = null;
		if (m_lstRecycledGrass.Count > 0) {
			grass = m_lstRecycledGrass [0];
			grass.SetActive ( true );
			m_lstRecycledGrass.RemoveAt (0);
		}
		if (grass == null) {
			grass = GameObject.Instantiate (m_preGrass).GetComponent<CGrass> ();
		}
		return grass;
	}

	List<CGrass> m_lstRecycledGrass = new List<CGrass> ();
	public void RecycleGrass( CGrass grass )
	{
		grass.SetActive ( false );
		m_lstRecycledGrass.Add (grass);
	}


	/// ! ---- end Grass


	/// !--- Spray
	public CSpray ReuseSpray()
	{
		return GameObject.Instantiate ( m_preSpray ).GetComponent<CSpray>();
	}
    /// !---- end Spray
    /// 

    /// ! ---- Scene Ball
    /// 
    public CMonster ReuseSceneBall( )
    {
        CMonster scene_ball = null;
        if (m_lstRecycledSceneBall.Count > 0)
        {
            scene_ball = m_lstRecycledSceneBall[0];
            scene_ball.SetActive( true );
            m_lstRecycledSceneBall.RemoveAt(0);
        }
        if (scene_ball == null)
        {
            scene_ball = GameObject.Instantiate(m_preSceneBall).GetComponent<CMonster>();
        }
        return scene_ball;
    }

    List<CMonster> m_lstRecycledSceneBall = new List<CMonster>();
    public void RecycleSceneBall(CMonster scene_ball)
    {
        m_lstRecycledSceneBall.Add(scene_ball);
    }

    /// ! ----end  Scene Ball
    /// 

    List<CGunsight> m_lstRecycledGunsight = new List<CGunsight>();
    public CGunsight NewGunsight()
    {
        CGunsight gunsight = null;
        if (m_lstRecycledGunsight.Count > 0)
        {
            gunsight = m_lstRecycledGunsight[0];
            gunsight.gameObject.SetActive(true);
            m_lstRecycledGunsight.RemoveAt(0);
        }
        else
        {
            gunsight = GameObject.Instantiate(m_preGunsight).GetComponent<CGunsight>();
        }

        return gunsight;
    }

    public void RemoveGunsight(CGunsight gunsight )
    {
        gunsight.gameObject.SetActive( false );
        m_lstRecycledGunsight.Add(gunsight);
    }
}
