﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class CreateNewShit : MonoBehaviour {

	public Text _txtTitle;
	public InputField _inputNewShitName;
	public Button _btnOK;
	public Button _btnCancel;

	public delegate void DelegateMethod_OK( string szName );
	public DelegateMethod_OK delegateMethodOK;   //声明了一个Delegate对象

	// Use this for initialization
	void Start () {
		
	}
	
	// Update is called once per frame
	void Update () {
		
	}

	public void SetTitle( string szTitle )
	{
		_txtTitle.text = szTitle;
	}

	public void OK()
	{
		this.gameObject.SetActive ( false );
		if (delegateMethodOK != null) {
			delegateMethodOK ( _inputNewShitName.text );
		}
	}

	public void Cancel()
	{
		this.gameObject.SetActive ( false );
	}
}
