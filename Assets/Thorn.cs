﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Thorn : MapObj //  “刺”继承于“地图物件”
{
    public enum eThornType
    {
        scene, // 普通的场景刺
        baba,  // 拉的粑粑
    };
    eThornType m_eThornType = eThornType.scene;

    static List<float> m_lstNewThornApplication = new List<float> ();

    public GameObject _main;
    public CircleCollider2D _Trigger;
    public SpriteRenderer _srMain;

	int m_nIndex = 0;

    int m_nOwnerId = 0;

    public static string[] c_aryThornColor =
    {
        "#FFFFFFFF",
        "#FF0000FF",
        "#F6850DFF",
        "#F6F900FF",
        "#70F900FF",
        "#00F9B1FF",
        "#0035F9FF",
        "#7400F6FF",
    };

    public struct ColorThornParam
    {
        public float thorn_size;
        public float ball_size;
		public float reborn_time;
    };

    int m_nColorIdx = 0;

    // Use this for initialization
    void Start () {
		
	}

	void Awake()
	{
        m_eObjType = eMapObjType.thorn;
    }

	// Update is called once per frame
	public override void Update () {
        base.Update();

	}


	void FixedUpdate()
    {

	}

	public static void ApplyGenerateOneNewThorn ()
	{
	//	Debug.Log ( "addthorn：" + (float)PhotonNetwork.time );
		m_lstNewThornApplication.Add (  (float)PhotonNetwork.time );

	}

	static int m_nSceneThornRebornIndex = 0;
	public static void GenerateNewThornLoop()
	{
//		Debug.Log ( Main.s_Instance.m_goThorns.transform.childCount + "," + m_lstNewThornApplication.Count );
		if (m_lstNewThornApplication.Count == 0) {
			return;
		}

		if (m_nSceneThornRebornIndex >= m_lstNewThornApplication.Count) {
			m_nSceneThornRebornIndex = 0;
		}
		if (m_nSceneThornRebornIndex >= m_lstNewThornApplication.Count) {
			return;
		}
		if (PhotonNetwork.time - m_lstNewThornApplication [m_nSceneThornRebornIndex] >= Main.s_Instance.m_fGenerateNewThornTimeInterval) {
			m_lstNewThornApplication.RemoveAt ( m_nSceneThornRebornIndex );
			Main.s_Instance.GenerateThorn ();
		} else {
			m_nSceneThornRebornIndex++;
		}
	}


    public void SetColorIdx(int nColorIdx )
    {
        if (nColorIdx < 0 || nColorIdx >= c_aryThornColor.Length)
        {
            return;
        }
        m_nColorIdx = nColorIdx;
        _srMain.color = GetColorByIdx(m_nColorIdx);
    }

    public int GetColorIdx()
    {
        return m_nColorIdx;
    }

    public static string GetColorStringByIdx(int nColorIdx)
    {
        if (nColorIdx < 0 || nColorIdx >= c_aryThornColor.Length)
        {
            return c_aryThornColor[0];
        }
        return c_aryThornColor[nColorIdx];
    }

    public static Color GetColorByIdx(int nColorIdx)
    {
        Color color = Color.white;
        if (ColorUtility.TryParseHtmlString(GetColorStringByIdx(nColorIdx), out color))
        {
        }
        return color;
    }

	bool m_bIsColorThorn = false;
	public void SetIsColorThorn( bool val )
	{
		m_bIsColorThorn = val;
	}

	public bool GetIsColorThorn()
	{
		return m_bIsColorThorn;
	}

	static List<Thorn> m_lstColorThornRebornList = new List<Thorn> ();
	public static void AddToRebornList( Thorn thorn )
	{
		ColorThornParam param = MapEditor.GetColorThornParamByIdx( thorn.GetColorIdx() );
		thorn.StartRebornCount (param.reborn_time);
		m_lstColorThornRebornList.Add ( thorn );
	}

	public static void ColorThornReborn()
	{
		for (int i = m_lstColorThornRebornList.Count - 1; i >= 0; i--) {
			Thorn thorn = m_lstColorThornRebornList [i];
			if (thorn == null) {
				//Debug.LogError ( "color thorn reborn null" );
				m_lstColorThornRebornList.RemoveAt (i);
				continue;
			}
			if (thorn.ThornRebornCount ()) {
				m_lstColorThornRebornList.RemoveAt (i);
				thorn.Reborn ();
			}
		}
	}

	float m_fRebornTime = 0f;
	public void StartRebornCount( float fRebornTime )
	{
		m_fRebornTime = fRebornTime;
	}

	public bool ThornRebornCount()
	{
		m_fRebornTime -= Time.fixedDeltaTime;
		return ( m_fRebornTime <= 0 );
	}

	public void Reborn()
	{
		SetDead ( false );
	}

	bool m_bDead = false;
	public bool IsDead()
	{
		return m_bDead;
	}

	public void SetDead( bool bDead )
	{
		m_bDead = bDead;
		_Trigger.enabled = !m_bDead;
		this.gameObject.SetActive ( !m_bDead );
	}

	public void SetIndex( int nIndex )
	{
		m_nIndex = nIndex;
	}

	public int GetIndex()
	{
		return m_nIndex;
	}

    public bool IsRebornable()
    {

        return true;
    }

    public void SetThornType( eThornType type )
    {
        m_eThornType = type;
    }

    public eThornType GetThornType()
    {
        return m_eThornType;
    }

    public void SetOwnerId( int nOwnerId )
    {
        m_nOwnerId = nOwnerId;
    }

    public int GetOwnerId()
    {
        return m_nOwnerId;
    }

}
